#!/bin/sh
# Check if the JAR file exists in the specified directory
if [ -f "/app/target/$JAR_FILE" ]; then
    echo "Running the application..."
    java -jar -Dspring.profiles.active=container /app/target/$JAR_FILE
else
    echo "Error: JAR file not found in /app/target"
    exit 1
fi