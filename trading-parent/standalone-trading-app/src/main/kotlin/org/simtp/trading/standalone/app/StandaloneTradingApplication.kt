package org.simtp.trading.standalone.app

import org.simtp.origination.data.source.jpa.EnableJpaOriginationDataSource
import org.simtp.origination.module.EnableOrigination
import org.slf4j.MDC
import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * Created by Grant Little grant@grantlittle.me
 */
@SpringBootApplication
//@EnablePersonR2dbcDataSource
//@EnableContacts
//@EnableContactsStream
//@EnableOrigination
//@EnableOriginationStream
//@EnableAccountBackend
//@EnableAccountRestServer
//@EnableAddressBackend
//@EnableSerialization
//@EnableRedisStream
//@EnableSecuritiesBackend
//@EnableMockSecuritiesProvider
//@EnableOrderVerification
//
//@EnableJpaOrderDataSource
//@EnableRedisMarketDataDataSource
//@EnableOrderVerificationRedisDataSource
//@EnableIgnite
//@EnableCommon
//@EnableHazelcastStorageNode
//@EnableOrders
//@EnableKafkaFeatures
//@EnablePricesBackend
//@EnablePricesApi
//@EnableBasicVetting
//@EnableVettingService
//@EnableTradesProcessor
//@EnableSimpleMatchingEngine
//@EnableSlackIntegration
//@EnableSecuritiesOrdersProcessing
@EnableOrigination
@EnableJpaOriginationDataSource
open class StandaloneTradingApplication {}

fun main(args : Array<String>) {
    runApplication<StandaloneTradingApplication>(*args) {
        MDC.put("instance", "standalone-trading-app")
        setBannerMode(Banner.Mode.OFF)
    }
}
