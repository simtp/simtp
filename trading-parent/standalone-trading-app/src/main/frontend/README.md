# SimTP Desktop

## Overview

This application provides a desktop or large mobile device view for the SimTP applications. 

Primarily as a pro trading application it is not designed for small mobile devices. We are currently working on an
alternative for this.

## Technologies

### Vue JS
Our preferred frontend library is VueJS. We find it strikes a good balance between simplicity, a low initial learning 
curve and available functionality.

### PrimeVue

PrimeVue provides a layout free component and widget library. This allows us to provide layouts that could be quite 
densely packed to provide a lot of valuable information with the available desktop real estate.


### PrimeFlex

For typical desktop usage we use the PrimeFlex library for layout
