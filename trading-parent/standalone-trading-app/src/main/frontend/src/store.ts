// store.ts
import {defineStore} from "pinia";

interface User {
    id: string;
    username: string;
    firstName: string;
    lastName: string;
    // Add more properties as needed...
}

class DefaultUser implements User {
    id: string;
    username: string;
    firstName: string;
    lastName: string;
    constructor(
        id: string,
        username: string,
        firstName: string,
        lastName: string
    ) {
        this.id = id;
        this.username = username
        this.firstName = firstName
        this.lastName = lastName
    }
}

interface State {
    user: User | null;
    symbol: string | null;
}

export const useUserStore = defineStore('user', {
    state: (): State => ({
        user: null,
        symbol: null,
    }),

    actions: {

        login(username: string, password: string): Promise<string> {
            const defaultUser = new DefaultUser('1', 'jbloggs', 'Joe', 'Bloggs');
            const validPassword = 'password123';

            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    if (username === defaultUser.username && password === validPassword) {
                        this.user = defaultUser;
                        localStorage.setItem('user', JSON.stringify(defaultUser));
                        resolve('Login successful');
                    } else {
                        reject('Invalid username or password');
                    }
                }, 500);
            });
        },

        logout() {
            this.user = null;
            localStorage.removeItem('user');
        },
    },

    getters: {
        isLoggedIn(state) {
            return state.user !== null;
        },
    },
});

export default useUserStore; // Add this for a default export


