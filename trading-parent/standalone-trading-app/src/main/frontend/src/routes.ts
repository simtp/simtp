import {createRouter, createWebHistory, RouteRecordRaw} from "vue-router";
import Home from "./components/Home.vue";
import MarketData from "./components/widgets/MarketData.vue";
import OrdersPanel from "./components/widgets/OrderPanel.vue";
import PortfolioPanel from "./components/widgets/PortfolioPanel.vue";
import SignInUp from "./components/SignInUp.vue";


const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        // meta: {
        //     requiresAuth: true
        // }
    },
    {
        path: '/market-data',
        name: 'Market Data',
        component: MarketData,
        // meta: {
        //     requiresAuth: true
        // }
    },
    {
        path: '/orders',
        name: 'Orders',
        component: OrdersPanel,
        // meta: {
        //     requiresAuth: true
        // }
    },
    {
        path: '/portfolio',
        name: 'Portfolio',
        component: PortfolioPanel,
        // meta: {
        //     requiresAuth: true
        // }
    },
    {
        path: '/sign-in-up',
        name: 'SignInUp',
        component: SignInUp,
        // meta: {
        //     requiresAuth: true
        // }
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});


// router.beforeEach((to, _, next) => {
//     const loggedIn = localStorage.getItem('user');
//     if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
//         next('/sign-in');
//     } else {
//         next();
//     }
// });

export default router;