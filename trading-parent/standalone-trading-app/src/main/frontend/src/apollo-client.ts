import { ApolloClient, InMemoryCache, HttpLink, ApolloLink } from '@apollo/client/core';
import {DateTimeFormatter, LocalDate, ZonedDateTime} from "@js-joda/core";


const httpLink = new HttpLink({ uri: 'http://example.com/graphql' });

// create a Middleware link
const middlewareLink = new ApolloLink((operation, forward) => {

    if (operation.variables) {
        // iterate over the variables and convert ZonedDateTime and LocalDate to appropriate strings
        for (let key in operation.variables) {
            if (operation.variables[key] instanceof ZonedDateTime) {
                operation.variables[key] = operation.variables[key].format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
            }
            else if (operation.variables[key] instanceof LocalDate) {
                operation.variables[key] = operation.variables[key].format(DateTimeFormatter.ISO_LOCAL_DATE);
            }
        }
    }

    return forward(operation);
});

const apolloClient = new ApolloClient({
    cache: new InMemoryCache(),
    link: ApolloLink.from([middlewareLink, httpLink]),
});


export default apolloClient;