import { Ref, ref } from 'vue';

// Custom EventEmitter for Mock SSE
class EventEmitter<T> {
    private listeners: ((data: T) => void)[] = [];

    subscribe(callback: (data: T) => void) {
        this.listeners.push(callback);
    }

    unsubscribe(callback: (data: T) => void) {
        this.listeners = this.listeners.filter(listener => listener !== callback);
    }

    emit(data: T) {
        this.listeners.forEach(listener => listener(data));
    }
}

// Mock Data
export class Instrument {
    symbol: string;
    description: string;

    constructor(symbol: string, description: string) {
        this.symbol = symbol;
        this.description = description;
    }
}

export class Price {
    symbol: string; // Ticker symbol
    last: number; // Last traded price
    close: number; // Closing price of the previous session
    open: number; // Opening price of the current session
    high: number; // Highest price during the current session
    low: number; // Lowest price during the current session
    volume: number; // Traded volume during the current session
    change: number; // Change in price compared to the last session (can be calculated as `last - close`)
    changePercent: number; // Percentage change (can be calculated as `(change / close) * 100`)

    constructor(
        symbol: string,
        last: number,
        close: number,
        open: number,
        high: number,
        low: number,
        volume: number
    ) {
        this.symbol = symbol;
        this.last = last;
        this.close = close;
        this.open = open;
        this.high = high;
        this.low = low;
        this.volume = volume;
        this.change = parseFloat((last - close).toFixed(2));
        this.changePercent = parseFloat(((this.change / close) * 100).toFixed(2));
    }
}


export const instruments: Ref<Instrument[]> = ref([
    new Instrument('A2M', 'The a2 Milk Company Ltd'),
    new Instrument('AAA', 'Betashares Australian High Interest Cash ETF'),
    new Instrument('ABC', 'Adbri Ltd'),
    new Instrument('ABP', 'Abacus Property Group'),
    new Instrument('AFI', 'Australian Foundation Investment Company Ltd'),
    new Instrument('AGL', 'AGL Energy Ltd'),
    new Instrument('ALD', 'Ampol Ltd'),
    new Instrument('ALL', 'Aristocrat Leisure Ltd'),
    new Instrument('ALU', 'Altium Ltd'),
    new Instrument('AMC', 'Amcor Plc'),
    new Instrument('AMP', 'AMP Ltd'),
    new Instrument('ANZ', 'Australia and New Zealand Banking Group Ltd'),
    new Instrument('APA', 'APA Group'),
    new Instrument('APT', 'Afterpay Ltd'),
    new Instrument('AZJ', 'Aurizon Holdings Ltd'),
    new Instrument('BEN', 'Bendigo and Adelaide Bank Ltd'),
    new Instrument('BHP', 'BHP Group Ltd'),
    new Instrument('BKW', 'Brickworks Ltd'),
    new Instrument('BLD', 'Boral Ltd'),
    new Instrument('BPT', 'Beach Energy Ltd'),
    new Instrument('BRG', 'Breville Group Ltd'),
    new Instrument('BSL', 'Bluescope Steel Ltd'),
    new Instrument('BWP', 'BWP Trust'),
    new Instrument('BXB', 'Brambles Ltd'),
    new Instrument('CAR', 'Carsales.com Ltd'),
    new Instrument('CBA', 'Commonwealth Bank of Australia'),
    new Instrument('CCL', 'Coca-Cola Amatil Ltd'),
    new Instrument('CCP', 'Credit Corp Group Ltd'),
    new Instrument('CDA', 'Codan Ltd'),
    new Instrument('CGF', 'Challenger Ltd'),
    new Instrument('CHC', 'Charter Hall Group'),
    new Instrument('CHL', 'Chalice Mining Ltd'),
    new Instrument('CIA', 'Champion Iron Ltd'),
    new Instrument('CIM', 'Cimic Group Ltd'),
    new Instrument('CLW', 'Charter Hall Long WALE REIT'),
    new Instrument('CMW', 'Cromwell Property Group'),
    new Instrument('CNU', 'Chorus Ltd'),
    new Instrument('COH', 'Cochlear Ltd'),
    new Instrument('COL', 'Coles Group Ltd'),
    new Instrument('CPU', 'Computershare Ltd'),
    new Instrument('CQR', 'Charter Hall Retail REIT'),
    new Instrument('CSL', 'CSL Ltd'),
    new Instrument('CSR', 'CSR Ltd'),
    new Instrument('CTD', 'Corporate Travel Management Ltd'),
    new Instrument('CWN', 'Crown Resorts Ltd'),
    new Instrument('CWY', 'Cleanaway Waste Management Ltd'),
    new Instrument('DEG', 'De Grey Mining Ltd'),
    new Instrument('DHG', 'Domain Holdings Australia Ltd'),
    new Instrument('DMP', "Domino's Pizza Enterprises Ltd"),
    new Instrument('DOW', 'Downer EDI Ltd'),
    new Instrument('DRR', 'Deterra Royalties Ltd'),
    new Instrument('DXS', 'Dexus'),
    new Instrument('EBO', 'Ebos Group Ltd'),
    new Instrument('ELD', 'Elders Ltd'),
    new Instrument('EML', 'EML Payments Ltd'),
    new Instrument('EVN', 'Evolution Mining Ltd'),
    new Instrument('EVT', 'Event Hospitality and Entertainment Ltd'),
    new Instrument('FBU', 'Fletcher Building Ltd'),
    new Instrument('FLT', 'Flight Centre Travel Group Ltd'),
    new Instrument('GMG', 'Goodman Group'),
    new Instrument('GUD', 'GUD Holdings Ltd'),
    new Instrument('IAG', 'Insurance Australia Group Ltd'),
    new Instrument('JBH', 'James Hardie Industries Ltd'),
    new Instrument('KAR', 'KAR Auction Services Ltd'),
    new Instrument('LLC', 'Lendlease Group'),
    new Instrument('MGR', 'Mirvac Group'),
    new Instrument('MQG', 'Macquarie Group Ltd'),
    new Instrument('NAB', 'National Australia Bank Ltd'),
    new Instrument('OSH', 'Orica Ltd'),
    new Instrument('ORG', 'Origin Energy Ltd'),
    new Instrument('QAN', 'Qantas Airways Ltd'),
    new Instrument('QBE', 'QBE Insurance Group Ltd'),
    new Instrument('QUB', 'Qube Holdings Ltd'),
    new Instrument('REA', 'REA Group Ltd'),
    new Instrument('RHC', 'RHC Capital Ltd'),
    new Instrument('RIO', 'Rio Tinto Ltd'),
    new Instrument('RMD', 'ResMed Inc'),
    new Instrument('SCG', 'Scentre Group'),
    new Instrument('SGP', 'Stockland'),
    new Instrument('SFR', 'Sigma Healthcare Ltd'),
    new Instrument('SHL', 'Sonic Healthcare Ltd'),
    new Instrument('SOL', 'Soliton Group Ltd'),
    new Instrument('SUN', 'Suncorp Group Ltd'),
    new Instrument('SYD', 'Sydney Airport'),
    new Instrument('TCL', 'Transurban Group'),
    new Instrument('TLS', 'Telstra Corporation Ltd'),
    new Instrument('TWE', 'Treasury Wine Estates Ltd'),
    new Instrument('UGL', 'UGL Limited'),
    new Instrument('VOC', 'Vocus Group Ltd'),
    new Instrument('WBC', 'Westpac Banking Corporation'),
    new Instrument('WES', 'Wesfarmers Ltd'),
    new Instrument('WOW', 'Woolworths Group Ltd'),
    new Instrument('IGO', 'IGO Ltd'),
    new Instrument('NCM', 'Newcrest Mining Ltd'),
    new Instrument('TAL', 'TAL International Group Ltd'),
    new Instrument('FMG', 'Fortescue Metals Group Ltd'),
    new Instrument('NSR', 'Northern Star Resources Ltd'),
    new Instrument('ILU', 'Iluka Resources Ltd'),
    new Instrument('RRL', 'Regis Resources Ltd'),
    new Instrument('SAR', 'Saracen Mineral Holdings Ltd'),
    new Instrument('WHC', 'Whitehaven Coal Ltd'),
    new Instrument('JHX', 'JB Hi-Fi Ltd'),
    new Instrument('WOR', 'Worley Ltd'),
    new Instrument('GPT', 'GPT Group'),
    new Instrument('STO', 'Santos Ltd'),
    new Instrument('WPL', 'Woodside Petroleum Ltd'),
    new Instrument('XRO', 'Xero Ltd'),
    new Instrument('WTC', 'WiseTech Global Ltd'),
    new Instrument('APX', 'Appen Ltd'),
    new Instrument('TNE', 'Technology One Ltd'),
    new Instrument('HVN', 'Harvey Norman Holdings Ltd'),
    new Instrument('MYR', 'Myer Holdings Ltd'),
    new Instrument('MFG', 'Magellan Financial Group Ltd'),
    new Instrument('VEA', 'Viva Energy Group Ltd'),
    new Instrument('AWC', 'Alumina Ltd'),
    new Instrument('OZL', 'OZ Minerals Ltd'),
    new Instrument('ASX', 'ASX Ltd'),
    new Instrument('AST', 'Ausnet Services Ltd'),
    new Instrument('MTS', 'Metcash Ltd'),
    new Instrument('NVT', 'Novotech Ltd'),
    new Instrument('PMC', 'Perpetual Ltd'),
    new Instrument('SGO', 'Seabridge Gold Ltd'),
    new Instrument('TPG', 'TPG Telecom Limited'),
    new Instrument('ADC', 'Adacel Ltd'),
    new Instrument('AUB', 'AUB Group Ltd'),
    new Instrument('AVA', 'Avita Medical Ltd'),
    new Instrument('BAP', 'Bapcor Ltd'),
    new Instrument('EHC', 'eHealthcare Investment Group'),
    new Instrument('FRE', 'Freedom Foods Group'),
    new Instrument('HUB', 'Hub24 Ltd'),
    new Instrument('IDP', 'IDP Education Ltd'),
    new Instrument('LNK', 'Link Administration Holdings Ltd'),
    new Instrument('PDL', 'Pendal Group Ltd'),
    new Instrument('PMR', 'Pilbara Minerals Ltd'),
    new Instrument('SHP', 'Smartgroup Corporation Ltd'),
    new Instrument('S32', 'South32 Ltd'),
    new Instrument('SPC', 'Spark Infrastructure Group'),
    new Instrument('SWM', 'South West Mining Ltd'),
    new Instrument('TGR', 'TGR Corporation Ltd'),
    new Instrument('TOM', 'Tomahawk Energy Ltd'),
    new Instrument('VCX', 'Vicinity Centres'),
    new Instrument('ZIN', 'Zinpro Corporation'),
    new Instrument('BIN', 'Bingo Industries Ltd'),
    new Instrument('BVS', 'Bravura Solutions Ltd'),
    new Instrument('COR', 'Coregas Ltd'),
    new Instrument('CRD', 'Cardno Ltd'),
    new Instrument('CTX', 'Citadel Resources Ltd'),
    new Instrument('ENG', 'Engineered Wood Products Ltd'),
    new Instrument('GHL', 'GHL Group Ltd'),
    new Instrument('GNC', 'GrainCorp Ltd'),
    new Instrument('HBF', 'HBF Health Limited'),
    new Instrument('HUL', 'Huon Aquaculture Ltd'),
    new Instrument('ING', 'Inghams Group Ltd'),
    new Instrument('ION', 'ION Energy Ltd'),
    new Instrument('JME', 'Jemena Ltd'),
    new Instrument('KGN', 'Kogan.com Ltd'),
    new Instrument('LMX', 'LMX Group Ltd'),
    new Instrument('NUF', 'National Foods Ltd'),
    new Instrument('ONE', 'OnePath Holdings Ltd'),
    new Instrument('PLS', 'Plus Property Group Ltd'),
    new Instrument('PRY', 'Prudential Portfolio Ltd'),
    new Instrument('PRW', 'ProWealth Ltd'),
    new Instrument('MAY', 'Mayne Pharma Ltd'),
    new Instrument('MCG', 'McGrath Ltd'),
    new Instrument('MLC', 'MLC Limited'),
    new Instrument('MPL', 'MPL Resources Ltd'),
    new Instrument('NEC', 'NEC Australia Ltd'),
    new Instrument('NIM', 'NIM Inc Ltd'),
    new Instrument('NXT', 'NextGen Healthcare Ltd'),
    new Instrument('OCS', 'OCS Group Ltd'),
    new Instrument('OSM', 'OSM Mining Ltd'),
    new Instrument('PFD', 'PFD Food Group Ltd'),
    new Instrument('PHI', 'Phinma Group Ltd'),
    new Instrument('PIC', 'Picnic Foods Ltd'),
    new Instrument('PWR', 'Power Resources Ltd'),
    new Instrument('QIK', 'Quick International Ltd'),
    new Instrument('RCL', 'Rebel Cheese Ltd'),
    new Instrument('RCM', 'RCM Group Ltd'),
    new Instrument('RDC', 'Redrock Developments Ltd'),
    new Instrument('RDF', 'Redfern Mining Ltd'),
    new Instrument('RGT', 'Regent Group Ltd'),
    new Instrument('RHV', 'Rhevade Ltd'),
    new Instrument('SBT', 'Star Bulk Carriers Ltd'),
    new Instrument('SCF', 'Scaffold Industries Ltd'),
    new Instrument('SCP', 'Scorpio Capital Ltd'),
    new Instrument('SEA', 'Sea Swift Ltd'),
    new Instrument('SHY', 'Shays Limited'),
    new Instrument('SMA', 'SMC Ltd'),
    new Instrument('SPT', 'Spitfire Resources Ltd'),
    new Instrument('SSO', 'SSO Group Ltd'),
    new Instrument('STB', 'Steadfast Group Ltd'),
    new Instrument('STL', 'Stellar Lithium Ltd'),
    new Instrument('STR', 'Stratec Pharma Ltd'),
    new Instrument('SWP', 'Swift Properties Ltd'),
    new Instrument('TAP', 'Tap Inc Ltd'),
    new Instrument('TBR', 'TBR Group Ltd'),
    new Instrument('TCB', 'TCB Ltd'),
    new Instrument('TCC', 'TCC Ltd'),
    new Instrument('TEP', 'TEP Ltd'),
    new Instrument('TGS', 'TGS Ltd')
]);


export const prices: Ref<Map<string, Ref<Price>>> = ref(
    new Map([
        ['A2M', ref(new Price('A2M', 10.0, 10.2, 10.0, 10.5, 9.8, 1000))],
        ['AAA', ref(new Price('AAA', 20.0, 20.2, 20.0, 20.5, 19.8, 2000))],
        ['ABC', ref(new Price('ABC', 30.0, 30.2, 30.0, 30.5, 29.8, 3000))],
        ['ABP', ref(new Price('ABP', 40.0, 40.2, 40.0, 40.5, 39.8, 4000))],
        ['AFI', ref(new Price('AFI', 50.0, 50.2, 50.0, 50.5, 49.8, 5000))],
        ['AGL', ref(new Price('AGL', 60.0, 60.2, 60.0, 60.5, 59.8, 6000))],
        ['ALD', ref(new Price('ALD', 70.0, 70.2, 70.0, 70.5, 69.8, 7000))],
        ['ALL', ref(new Price('ALL', 80.0, 80.2, 80.0, 80.5, 79.8, 8000))],
        ['ALU', ref(new Price('ALU', 90.0, 90.2, 90.0, 90.5, 89.8, 9000))],
        ['AMC', ref(new Price('AMC', 100.0, 100.2, 100.0, 100.5, 99.8, 10000))],
        ['AMP', ref(new Price('AMP', 110.0, 110.2, 110.0, 110.5, 109.8, 11000))],
        ['ANZ', ref(new Price('ANZ', 120.0, 120.2, 120.0, 120.5, 119.8, 12000))],
        ['APA', ref(new Price('APA', 130.0, 130.2, 130.0, 130.5, 129.8, 13000))],
        ['APT', ref(new Price('APT', 140.0, 140.2, 140.0, 140.5, 139.8, 14000))],
        ['AZJ', ref(new Price('AZJ', 150.0, 150.2, 150.0, 150.5, 149.8, 15000))],
        ['BEN', ref(new Price('BEN', 160.0, 160.2, 160.0, 160.5, 159.8, 16000))],
        ['BHP', ref(new Price('BHP', 170.0, 170.2, 170.0, 170.5, 169.8, 17000))],
        ['BKW', ref(new Price('BKW', 180.0, 180.2, 180.0, 180.5, 179.8, 18000))],
        ['BLD', ref(new Price('BLD', 190.0, 190.2, 190.0, 190.5, 189.8, 19000))],
        ['BPT', ref(new Price('BPT', 200.0, 200.2, 200.0, 200.5, 199.8, 20000))],
        ['BRG', ref(new Price('BRG', 210.0, 210.2, 210.0, 210.5, 209.8, 21000))],
        ['BSL', ref(new Price('BSL', 220.0, 220.2, 220.0, 220.5, 219.8, 22000))],
        ['BWP', ref(new Price('BWP', 230.0, 230.2, 230.0, 230.5, 229.8, 23000))],
        ['BXB', ref(new Price('BXB', 240.0, 240.2, 240.0, 240.5, 239.8, 24000))],
        ['CAR', ref(new Price('CAR', 250.0, 250.2, 250.0, 250.5, 249.8, 25000))],
        ['CBA', ref(new Price('CBA', 260.0, 260.2, 260.0, 260.5, 259.8, 26000))],
        ['CCL', ref(new Price('CCL', 270.0, 270.2, 270.0, 270.5, 269.8, 27000))],
        ['CCP', ref(new Price('CCP', 280.0, 280.2, 280.0, 280.5, 279.8, 28000))],
        ['CDA', ref(new Price('CDA', 290.0, 290.2, 290.0, 290.5, 289.8, 29000))],
        ['CGF', ref(new Price('CGF', 300.0, 300.2, 300.0, 300.5, 299.8, 30000))],
        ['CHC', ref(new Price('CHC', 310.0, 310.2, 310.0, 310.5, 309.8, 31000))],
        ['CHL', ref(new Price('CHL', 320.0, 320.2, 320.0, 320.5, 319.8, 32000))],
        ['CIA', ref(new Price('CIA', 330.0, 330.2, 330.0, 330.5, 329.8, 33000))],
        ['CIM', ref(new Price('CIM', 340.0, 340.2, 340.0, 340.5, 339.8, 34000))],
        ['CLW', ref(new Price('CLW', 350.0, 350.2, 350.0, 350.5, 349.8, 35000))],
        ['CMW', ref(new Price('CMW', 360.0, 360.2, 360.0, 360.5, 359.8, 36000))],
        ['CNU', ref(new Price('CNU', 370.0, 370.2, 370.0, 370.5, 369.8, 37000))],
        ['COH', ref(new Price('COH', 380.0, 380.2, 380.0, 380.5, 379.8, 38000))],
        ['COL', ref(new Price('COL', 390.0, 390.2, 390.0, 390.5, 389.8, 39000))],
        ['CPU', ref(new Price('CPU', 400.0, 400.2, 400.0, 400.5, 399.8, 40000))],
        ['CQR', ref(new Price('CQR', 410.0, 410.2, 410.0, 410.5, 409.8, 41000))],
        ['CSL', ref(new Price('CSL', 420.0, 420.2, 420.0, 420.5, 419.8, 42000))],
        ['CSR', ref(new Price('CSR', 430.0, 430.2, 430.0, 430.5, 429.8, 43000))],
        ['CTD', ref(new Price('CTD', 440.0, 440.2, 440.0, 440.5, 439.8, 44000))],
        ['CWN', ref(new Price('CWN', 450.0, 450.2, 450.0, 450.5, 449.8, 45000))],
        ['CWY', ref(new Price('CWY', 460.0, 460.2, 460.0, 460.5, 459.8, 46000))],
        ['DEG', ref(new Price('DEG', 470.0, 470.2, 470.0, 470.5, 469.8, 47000))],
        ['DHG', ref(new Price('DHG', 480.0, 480.2, 480.0, 480.5, 479.8, 48000))],
        ['DMP', ref(new Price('DMP', 490.0, 490.2, 490.0, 490.5, 489.8, 49000))],
        ['DOW', ref(new Price('DOW', 500.0, 500.2, 500.0, 500.5, 499.8, 50000))],
        ['DRR', ref(new Price('DRR', 510.0, 510.2, 510.0, 510.5, 509.8, 51000))],
        ['DXS', ref(new Price('DXS', 520.0, 520.2, 520.0, 520.5, 519.8, 52000))],
        ['EBO', ref(new Price('EBO', 530.0, 530.2, 530.0, 530.5, 529.8, 53000))],
        ['ELD', ref(new Price('ELD', 540.0, 540.2, 540.0, 540.5, 539.8, 54000))],
        ['EML', ref(new Price('EML', 550.0, 550.2, 550.0, 550.5, 549.8, 55000))],
        ['EVN', ref(new Price('EVN', 560.0, 560.2, 560.0, 560.5, 559.8, 56000))],
        ['EVT', ref(new Price('EVT', 570.0, 570.2, 570.0, 570.5, 569.8, 57000))],
        ['FBU', ref(new Price('FBU', 580.0, 580.2, 580.0, 580.5, 579.8, 58000))],
        ['FLT', ref(new Price('FLT', 590.0, 590.2, 590.0, 590.5, 589.8, 59000))],
        ['GMG', ref(new Price('GMG', 600.0, 600.2, 600.0, 600.5, 599.8, 60000))],
        ['GUD', ref(new Price('GUD', 610.0, 610.2, 610.0, 610.5, 609.8, 61000))],
        ['IAG', ref(new Price('IAG', 620.0, 620.2, 620.0, 620.5, 619.8, 62000))],
        ['JBH', ref(new Price('JBH', 630.0, 630.2, 630.0, 630.5, 629.8, 63000))],
        ['KAR', ref(new Price('KAR', 640.0, 640.2, 640.0, 640.5, 639.8, 64000))],
        ['LLC', ref(new Price('LLC', 650.0, 650.2, 650.0, 650.5, 649.8, 65000))],
        ['MGR', ref(new Price('MGR', 660.0, 660.2, 660.0, 660.5, 659.8, 66000))],
        ['MQG', ref(new Price('MQG', 670.0, 670.2, 670.0, 670.5, 669.8, 67000))],
        ['NAB', ref(new Price('NAB', 680.0, 680.2, 680.0, 680.5, 679.8, 68000))],
        ['OSH', ref(new Price('OSH', 690.0, 690.2, 690.0, 690.5, 689.8, 69000))],
        ['ORG', ref(new Price('ORG', 700.0, 700.2, 700.0, 700.5, 699.8, 70000))],
        ['QAN', ref(new Price('QAN', 710.0, 710.2, 710.0, 710.5, 709.8, 71000))],
        ['QBE', ref(new Price('QBE', 720.0, 720.2, 720.0, 720.5, 719.8, 72000))],
        ['QUB', ref(new Price('QUB', 730.0, 730.2, 730.0, 730.5, 729.8, 73000))],
        ['REA', ref(new Price('REA', 740.0, 740.2, 740.0, 740.5, 739.8, 74000))],
        ['RHC', ref(new Price('RHC', 750.0, 750.2, 750.0, 750.5, 749.8, 75000))],
        ['RIO', ref(new Price('RIO', 760.0, 760.2, 760.0, 760.5, 759.8, 76000))],
        ['RMD', ref(new Price('RMD', 770.0, 770.2, 770.0, 770.5, 769.8, 77000))],
        ['SCG', ref(new Price('SCG', 780.0, 780.2, 780.0, 780.5, 779.8, 78000))],
        ['SGP', ref(new Price('SGP', 790.0, 790.2, 790.0, 790.5, 789.8, 79000))],
        ['SFR', ref(new Price('SFR', 800.0, 800.2, 800.0, 800.5, 799.8, 80000))],
        ['SHL', ref(new Price('SHL', 810.0, 810.2, 810.0, 810.5, 809.8, 81000))],
        ['SOL', ref(new Price('SOL', 820.0, 820.2, 820.0, 820.5, 819.8, 82000))],
        ['SUN', ref(new Price('SUN', 830.0, 830.2, 830.0, 830.5, 829.8, 83000))],
        ['SYD', ref(new Price('SYD', 840.0, 840.2, 840.0, 840.5, 839.8, 84000))],
        ['TCL', ref(new Price('TCL', 850.0, 850.2, 850.0, 850.5, 849.8, 85000))],
        ['TLS', ref(new Price('TLS', 860.0, 860.2, 860.0, 860.5, 859.8, 86000))],
        ['TWE', ref(new Price('TWE', 870.0, 870.2, 870.0, 870.5, 869.8, 87000))],
        ['UGL', ref(new Price('UGL', 880.0, 880.2, 880.0, 880.5, 879.8, 88000))],
        ['VOC', ref(new Price('VOC', 890.0, 890.2, 890.0, 890.5, 889.8, 89000))],
        ['WBC', ref(new Price('WBC', 900.0, 900.2, 900.0, 900.5, 899.8, 90000))],
        ['WES', ref(new Price('WES', 910.0, 910.2, 910.0, 910.5, 909.8, 91000))],
        ['WOW', ref(new Price('WOW', 920.0, 920.2, 920.0, 920.5, 919.8, 92000))],
        ['IGO', ref(new Price('IGO', 930.0, 930.2, 930.0, 930.5, 929.8, 93000))],
        ['NCM', ref(new Price('NCM', 940.0, 940.2, 940.0, 940.5, 939.8, 94000))],
        ['TAL', ref(new Price('TAL', 950.0, 950.2, 950.0, 950.5, 949.8, 95000))],
        ['FMG', ref(new Price('FMG', 960.0, 960.2, 960.0, 960.5, 959.8, 96000))],
        ['NSR', ref(new Price('NSR', 970.0, 970.2, 970.0, 970.5, 969.8, 97000))],
        ['ILU', ref(new Price('ILU', 980.0, 980.2, 980.0, 980.5, 979.8, 98000))],
        ['RRL', ref(new Price('RRL', 990.0, 990.2, 990.0, 990.5, 989.8, 99000))],
        ['SAR', ref(new Price('SAR', 1000.0, 1000.2, 1000.0, 1000.5, 999.8, 100000))],
        ['WHC', ref(new Price('WHC', 1010.0, 1010.2, 1010.0, 1010.5, 1009.8, 101000))],
        ['JHX', ref(new Price('JHX', 1020.0, 1020.2, 1020.0, 1020.5, 1019.8, 102000))],
        ['WOR', ref(new Price('WOR', 1030.0, 1030.2, 1030.0, 1030.5, 1029.8, 103000))],
        ['GPT', ref(new Price('GPT', 1040.0, 1040.2, 1040.0, 1040.5, 1039.8, 104000))],
        ['STO', ref(new Price('STO', 1050.0, 1050.2, 1050.0, 1050.5, 1049.8, 105000))],
        ['WPL', ref(new Price('WPL', 1060.0, 1060.2, 1060.0, 1060.5, 1059.8, 106000))],
        ['XRO', ref(new Price('XRO', 1070.0, 1070.2, 1070.0, 1070.5, 1069.8, 107000))],
        ['WTC', ref(new Price('WTC', 1080.0, 1080.2, 1080.0, 1080.5, 1079.8, 108000))],
        ['APX', ref(new Price('APX', 1090.0, 1090.2, 1090.0, 1090.5, 1089.8, 109000))],
        ['TNE', ref(new Price('TNE', 1100.0, 1100.2, 1100.0, 1100.5, 1099.8, 110000))],
        ['HVN', ref(new Price('HVN', 1110.0, 1110.2, 1110.0, 1110.5, 1109.8, 111000))],
        ['MYR', ref(new Price('MYR', 1120.0, 1120.2, 1120.0, 1120.5, 1119.8, 112000))],
        ['MFG', ref(new Price('MFG', 1130.0, 1130.2, 1130.0, 1130.5, 1129.8, 113000))],
        ['VEA', ref(new Price('VEA', 1140.0, 1140.2, 1140.0, 1140.5, 1139.8, 114000))],
        ['AWC', ref(new Price('AWC', 1150.0, 1150.2, 1150.0, 1150.5, 1149.8, 115000))],
        ['OZL', ref(new Price('OZL', 1160.0, 1160.2, 1160.0, 1160.5, 1159.8, 116000))],
        ['ASX', ref(new Price('ASX', 1170.0, 1170.2, 1170.0, 1170.5, 1169.8, 117000))],
        ['AST', ref(new Price('AST', 1180.0, 1180.2, 1180.0, 1180.5, 1179.8, 118000))],
        ['MTS', ref(new Price('MTS', 1190.0, 1190.2, 1190.0, 1190.5, 1189.8, 119000))],
        ['NVT', ref(new Price('NVT', 1200.0, 1200.2, 1200.0, 1200.5, 1199.8, 120000))],
        ['PMC', ref(new Price('PMC', 1210.0, 1210.2, 1210.0, 1210.5, 1209.8, 121000))],
        ['SGO', ref(new Price('SGO', 1220.0, 1220.2, 1220.0, 1220.5, 1219.8, 122000))],
        ['TPG', ref(new Price('TPG', 1230.0, 1230.2, 1230.0, 1230.5, 1229.8, 123000))],
        ['ADC', ref(new Price('ADC', 1240.0, 1240.2, 1240.0, 1240.5, 1239.8, 124000))],
        ['AUB', ref(new Price('AUB', 1250.0, 1250.2, 1250.0, 1250.5, 1249.8, 125000))],
        ['AVA', ref(new Price('AVA', 1260.0, 1260.2, 1260.0, 1260.5, 1259.8, 126000))],
        ['BAP', ref(new Price('BAP', 1270.0, 1270.2, 1270.0, 1270.5, 1269.8, 127000))],
        ['EHC', ref(new Price('EHC', 1280.0, 1280.2, 1280.0, 1280.5, 1279.8, 128000))],
        ['FRE', ref(new Price('FRE', 1290.0, 1290.2, 1290.0, 1290.5, 1289.8, 129000))],
        ['HUB', ref(new Price('HUB', 1300.0, 1300.2, 1300.0, 1300.5, 1299.8, 130000))],
        ['IDP', ref(new Price('IDP', 1310.0, 1310.2, 1310.0, 1310.5, 1309.8, 131000))],
        ['LNK', ref(new Price('LNK', 1320.0, 1320.2, 1320.0, 1320.5, 1319.8, 132000))],
        ['PDL', ref(new Price('PDL', 1330.0, 1330.2, 1330.0, 1330.5, 1329.8, 133000))],
        ['PMR', ref(new Price('PMR', 1340.0, 1340.2, 1340.0, 1340.5, 1339.8, 134000))],
        ['SHP', ref(new Price('SHP', 1350.0, 1350.2, 1350.0, 1350.5, 1349.8, 135000))],
        ['S32', ref(new Price('S32', 1360.0, 1360.2, 1360.0, 1360.5, 1359.8, 136000))],
        ['SPC', ref(new Price('SPC', 1370.0, 1370.2, 1370.0, 1370.5, 1369.8, 137000))],
        ['SWM', ref(new Price('SWM', 1380.0, 1380.2, 1380.0, 1380.5, 1379.8, 138000))],
        ['TGR', ref(new Price('TGR', 1390.0, 1390.2, 1390.0, 1390.5, 1389.8, 139000))],
        ['TOM', ref(new Price('TOM', 1400.0, 1400.2, 1400.0, 1400.5, 1399.8, 140000))],
        ['VCX', ref(new Price('VCX', 1410.0, 1410.2, 1410.0, 1410.5, 1409.8, 141000))],
        ['ZIN', ref(new Price('ZIN', 1420.0, 1420.2, 1420.0, 1420.5, 1419.8, 142000))],
        ['BIN', ref(new Price('BIN', 1430.0, 1430.2, 1430.0, 1430.5, 1429.8, 143000))],
        ['BVS', ref(new Price('BVS', 1440.0, 1440.2, 1440.0, 1440.5, 1439.8, 144000))],
        ['COR', ref(new Price('COR', 1450.0, 1450.2, 1450.0, 1450.5, 1449.8, 145000))],
        ['CRD', ref(new Price('CRD', 1460.0, 1460.2, 1460.0, 1460.5, 1459.8, 146000))],
        ['CTX', ref(new Price('CTX', 1470.0, 1470.2, 1470.0, 1470.5, 1469.8, 147000))],
        ['ENG', ref(new Price('ENG', 1480.0, 1480.2, 1480.0, 1480.5, 1479.8, 148000))],
        ['GHL', ref(new Price('GHL', 1490.0, 1490.2, 1490.0, 1490.5, 1489.8, 149000))],
        ['GNC', ref(new Price('GNC', 1500.0, 1500.2, 1500.0, 1500.5, 1499.8, 150000))],
        ['HBF', ref(new Price('HBF', 1510.0, 1510.2, 1510.0, 1510.5, 1509.8, 151000))],
        ['HUL', ref(new Price('HUL', 1520.0, 1520.2, 1520.0, 1520.5, 1519.8, 152000))],
        ['ING', ref(new Price('ING', 1530.0, 1530.2, 1530.0, 1530.5, 1529.8, 153000))],
        ['ION', ref(new Price('ION', 1540.0, 1540.2, 1540.0, 1540.5, 1539.8, 154000))],
        ['JME', ref(new Price('JME', 1550.0, 1550.2, 1550.0, 1550.5, 1549.8, 155000))],
        ['KGN', ref(new Price('KGN', 1560.0, 1560.2, 1560.0, 1560.5, 1559.8, 156000))],
        ['LMX', ref(new Price('LMX', 1570.0, 1570.2, 1570.0, 1570.5, 1569.8, 157000))],
        ['NUF', ref(new Price('NUF', 1580.0, 1580.2, 1580.0, 1580.5, 1579.8, 158000))],
        ['ONE', ref(new Price('ONE', 1590.0, 1590.2, 1590.0, 1590.5, 1589.8, 159000))],
        ['PLS', ref(new Price('PLS', 1600.0, 1600.2, 1600.0, 1600.5, 1599.8, 160000))],
        ['PRY', ref(new Price('PRY', 1610.0, 1610.2, 1610.0, 1610.5, 1609.8, 161000))],
        ['PRW', ref(new Price('PRW', 1620.0, 1620.2, 1620.0, 1620.5, 1619.8, 162000))],
        ['MAY', ref(new Price('MAY', 1630.0, 1630.2, 1630.0, 1630.5, 1629.8, 163000))],
        ['MCG', ref(new Price('MCG', 1640.0, 1640.2, 1640.0, 1640.5, 1639.8, 164000))],
        ['MLC', ref(new Price('MLC', 1650.0, 1650.2, 1650.0, 1650.5, 1649.8, 165000))],
        ['MPL', ref(new Price('MPL', 1660.0, 1660.2, 1660.0, 1660.5, 1659.8, 166000))],
        ['NEC', ref(new Price('NEC', 1670.0, 1670.2, 1670.0, 1670.5, 1669.8, 167000))],
        ['NIM', ref(new Price('NIM', 1680.0, 1680.2, 1680.0, 1680.5, 1679.8, 168000))],
        ['NXT', ref(new Price('NXT', 1690.0, 1690.2, 1690.0, 1690.5, 1689.8, 169000))],
        ['OCS', ref(new Price('OCS', 1700.0, 1700.2, 1700.0, 1700.5, 1699.8, 170000))],
        ['OSM', ref(new Price('OSM', 1710.0, 1710.2, 1710.0, 1710.5, 1709.8, 171000))],
        ['PFD', ref(new Price('PFD', 1720.0, 1720.2, 1720.0, 1720.5, 1719.8, 172000))],
        ['PHI', ref(new Price('PHI', 1730.0, 1730.2, 1730.0, 1730.5, 1729.8, 173000))],
        ['PIC', ref(new Price('PIC', 1740.0, 1740.2, 1740.0, 1740.5, 1739.8, 174000))],
        ['PWR', ref(new Price('PWR', 1750.0, 1750.2, 1750.0, 1750.5, 1749.8, 175000))],
        ['QIK', ref(new Price('QIK', 1760.0, 1760.2, 1760.0, 1760.5, 1759.8, 176000))],
        ['RCL', ref(new Price('RCL', 1770.0, 1770.2, 1770.0, 1770.5, 1769.8, 177000))],
        ['RCM', ref(new Price('RCM', 1780.0, 1780.2, 1780.0, 1780.5, 1779.8, 178000))],
        ['RDC', ref(new Price('RDC', 1790.0, 1790.2, 1790.0, 1790.5, 1789.8, 179000))],
        ['RDF', ref(new Price('RDF', 1800.0, 1800.2, 1800.0, 1800.5, 1799.8, 180000))],
        ['RGT', ref(new Price('RGT', 1810.0, 1810.2, 1810.0, 1810.5, 1809.8, 181000))],
        ['RHV', ref(new Price('RHV', 1820.0, 1820.2, 1820.0, 1820.5, 1819.8, 182000))],
        ['SBT', ref(new Price('SBT', 1830.0, 1830.2, 1830.0, 1830.5, 1829.8, 183000))],
        ['SCF', ref(new Price('SCF', 1840.0, 1840.2, 1840.0, 1840.5, 1839.8, 184000))],
        ['SCP', ref(new Price('SCP', 1850.0, 1850.2, 1850.0, 1850.5, 1849.8, 185000))],
        ['SEA', ref(new Price('SEA', 1860.0, 1860.2, 1860.0, 1860.5, 1859.8, 186000))],
        ['SHY', ref(new Price('SHY', 1870.0, 1870.2, 1870.0, 1870.5, 1869.8, 187000))],
        ['SMA', ref(new Price('SMA', 1880.0, 1880.2, 1880.0, 1880.5, 1879.8, 188000))],
        ['SPT', ref(new Price('SPT', 1890.0, 1890.2, 1890.0, 1890.5, 1889.8, 189000))],
        ['SSO', ref(new Price('SSO', 1900.0, 1900.2, 1900.0, 1900.5, 1899.8, 190000))],
        ['STB', ref(new Price('STB', 1910.0, 1910.2, 1910.0, 1910.5, 1909.8, 191000))],
        ['STL', ref(new Price('STL', 1920.0, 1920.2, 1920.0, 1920.5, 1919.8, 192000))],
        ['STR', ref(new Price('STR', 1930.0, 1930.2, 1930.0, 1930.5, 1929.8, 193000))],
        ['SWP', ref(new Price('SWP', 1940.0, 1940.2, 1940.0, 1940.5, 1939.8, 194000))],
        ['TAP', ref(new Price('TAP', 1950.0, 1950.2, 1950.0, 1950.5, 1949.8, 195000))],
        ['TBR', ref(new Price('TBR', 1960.0, 1960.2, 1960.0, 1960.5, 1959.8, 196000))],
        ['TCB', ref(new Price('TCB', 1970.0, 1970.2, 1970.0, 1970.5, 1969.8, 197000))],
        ['TCC', ref(new Price('TCC', 1980.0, 1980.2, 1980.0, 1980.5, 1979.8, 198000))],
        ['TEP', ref(new Price('TEP', 1990.0, 1990.2, 1990.0, 1990.5, 1989.8, 199000))],
        ['TGS', ref(new Price('TGS', 2000.0, 2000.2, 2000.0, 2000.5, 1999.8, 200000))]
    ])
);



// const eventSource = new EventSource('/api/prices');
// eventSource.onmessage = (event) => {
//     const updatedPrice = JSON.parse(event.data) as Price;
//     const index = prices.value.findIndex(price => price.symbol === updatedPrice.symbol);
//     if (index !== -1) {
//         prices.value[index] = updatedPrice;
//     } else {
//         prices.value.push(updatedPrice);
//     }
// };


// Mock Event Emitters
const pricesEmitter = new EventEmitter<Price>();
const instrumentsEmitter = new EventEmitter<Instrument[]>();

// SSE/Mimicked Updates
const updatePrices = () => {
    prices.value.forEach((priceRef, _) => {
        // Extract the actual Price object from the Ref
        const updatedPrice = { ...priceRef.value };

        // Simulate small random variations in price
        updatedPrice.last = parseFloat((updatedPrice.last + (Math.random() - 0.5) * 0.1).toFixed(2));

        // Update high and low values dynamically
        updatedPrice.high = Math.max(updatedPrice.high, updatedPrice.last);
        updatedPrice.low = Math.min(updatedPrice.low, updatedPrice.last);

        // Increment volume randomly
        updatedPrice.volume += Math.floor(Math.random() * 100);

        // Recalculate change and change percentage
        updatedPrice.change = parseFloat((updatedPrice.last - updatedPrice.close).toFixed(2));
        updatedPrice.changePercent = parseFloat(((updatedPrice.change / updatedPrice.close) * 100).toFixed(2));

        // Update the Price stored in the Ref
        priceRef.value = updatedPrice;

        // Emit the updated Price
        pricesEmitter.emit(updatedPrice);
    });
};



const updateInstruments = () => {
    // For now, we don't change instruments often, but can use it later
    instrumentsEmitter.emit([...instruments.value]); // Broadcast full list
};

setInterval(updatePrices, 1000); // Update prices every second
setInterval(updateInstruments, 5000); // Emit instruments every 5 seconds

// Market Data Service
export const marketDataService = {
    prices, // Public reactive prices list
    instruments, // Public reactive instruments list
    pricesEmitter,
    getPrice(symbol: string): Ref<Price> | undefined {
        return prices.value.get(symbol)
    },

    getInstruments(query: string): Instrument[] {
        if (!query || query.trim() === '') {
            return instruments.value;
        }
        const upperQuery = query.trim().toUpperCase();
        return instruments.value.filter(instrument =>
            instrument.symbol.toUpperCase().startsWith(upperQuery),
        );
    },
};