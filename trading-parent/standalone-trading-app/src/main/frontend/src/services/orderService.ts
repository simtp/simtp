import {ref} from "vue";

export enum TimeInForce {
    GTC = "GTC", // Good-Till-Canceled
    FOK = "FOK", // Fill-Or-Kill
    IOC = "IOC", // Immediate-Or-Cancel
    DAY = "DAY", // Day Order (valid until the end of the trading session)
    GTT = "GTT", // Good-Till-Time (valid until a specified time)
}

class Order {
    id: string;
    symbol: string;
    orderType: string | null; // BUY, SELL
    orderClassification: string | null; // LIMIT, MARKET, STOP, TRAILING
    price: number | null; // Interpret based on orderClassification (e.g., limit, trailing amount, stop price)
    amount: number | null; // Number of units, or trailing distance depending on type
    timeInForce: string | null; // GTC, IOC, FOK
    status: string | null; // OPEN, FILLED, CANCELED

    constructor(
        id: string,
        symbol: string,
        orderType: string | null,
        orderClassification: string | null,
        price: number | null,
        amount: number | null,
        timeInForce: TimeInForce | null,
        status: string | null,
    ) {
        this.id = id;
        this.symbol = symbol;
        this.orderType = orderType;
        this.orderClassification = orderClassification;
        this.price = price;
        this.amount = amount;
        this.timeInForce = timeInForce;
        this.status = status;
    }
}

// Example Default Orders
const defaultOrders: Order[] = [
    new Order("1", "BHP", "BUY", "MARKET", null, 10, TimeInForce.GTC, "OPEN"), // Market order
    new Order("2", "AAPL", "SELL", "LIMIT", 150.00, 50,  TimeInForce.GTC, "OPEN"), // Limit order
    new Order("3", "GOOGL", "BUY", "TRAILING", 2.5, 10,  TimeInForce.GTC, "OPEN"), // Trailing stop
];

export const orderService = {
    orders: ref(defaultOrders),
};