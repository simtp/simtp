// sharedService.ts

export class WebMessage {
    type: string
    data: string

    constructor(
        ticker: string,
        description: string
    ) {
        this.type = ticker
        this.data = description
    }
}
// const onProgress = (evt: AxiosProgressEvent) => {
//     console.log(evt.)
// }

// const socket = new WebSocket('ws://localhost:8080/api/1.0/web/messaging/flux');

// socket.onmessage = (event: MessageEvent<string>) => {
//     console.log(event)
// }

// socket.onclose = (evt: CloseEvent) => {
//     console.log(evt)
// }


// export const messagingService = {
//
//
//     start() {
//         console.log("Registering")
//
//         let clientWebSocket = new WebSocket("ws://localhost:8080/events");
//         clientWebSocket.onopen = function() {
//             console.log("clientWebSocket.onopen", clientWebSocket);
//             console.log("clientWebSocket.readyState", "websocketstatus");
//             clientWebSocket.send("event-me-from-browser");
//         }
//         clientWebSocket.onclose = function(error) {
//             console.log("clientWebSocket.onclose", clientWebSocket, error);
//             events("Closing connection");
//         }
//         clientWebSocket.onerror = function(error) {
//             console.log("clientWebSocket.onerror", clientWebSocket, error);
//             events("An error occured");
//         }
//         clientWebSocket.onmessage = function(error) {
//             console.log("clientWebSocket.onmessage", clientWebSocket, error);
//             events(error.data);
//         }
//         function events(responseEvent: any) {
//             console.log("Events " + responseEvent)
//         }
//
//
//
//     }
//
//
// };




