package org.simtp.trading.standalone.app

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.simtp.spec.Application
import org.simtp.spec.ApplicationApproved
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.context.annotation.Bean
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.web.reactive.function.client.WebClient
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import reactor.kafka.receiver.ReceiverOptions
import reactor.test.StepVerifier
import kotlin.test.assertNotNull

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
)
@Testcontainers
@EmbeddedKafka(partitions = 1, brokerProperties = ["listeners=PLAINTEXT://localhost:9092", "port=9092"])
class StandaloneTradingApplicationTest {

    @LocalServerPort
    private var serverPort: Int = 8888

    companion object {
        @Container
        @ServiceConnection
        private val postgresqlContainer = PostgreSQLContainer("postgres").withNetwork(Network.SHARED)

        @BeforeAll
        @JvmStatic
        fun setUp() {

            postgresqlContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            postgresqlContainer.stop()
        }
    }




    @Autowired
    private lateinit var reactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, Any?>

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @Test
    fun `Create an origination message`() {
        val call = WebClient
            .create()
            .post()
            .uri("http://127.0.0.1:$serverPort/api/origination/1.0/test@simtp.org/Test/User")
            .retrieve()
            .bodyToMono(String::class.java)

        StepVerifier
            .create(call)
            .expectNextMatches { it != null }
            .verifyComplete()

        val receiver = reactiveKafkaConsumerTemplate.receiveAutoAck().take(2)

        // Consume the message using the ReactiveKafkaConsumerTemplate
        StepVerifier
            .create(receiver) // Automatically acknowledges messages
            .expectNextMatches { record ->
                println("Received record: ${record.value()}")
                val application = record.value() as Application
                assertNotNull(application)
                assertNotNull(application.id)
                true
            }
            .expectNextMatches { record ->
                println("Received record: ${record.value()}")
                val approved = record.value() as ApplicationApproved
                assertNotNull(approved)
                true
            }
            .verifyComplete()


    }

    @TestConfiguration
    open class AppTestConfiguration {
        @Bean
        open fun reactiveKafkaConsumerTemplate(
            @Value("\${simtp.origination.stream.name:stream.org.simtp.origination}")
            topicName: String,
            receiverOptions: ReceiverOptions<String, Any?>
        ): ReactiveKafkaConsumerTemplate<String, Any?> {
            return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(topicName)))
        }
    }
}


