@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")

package org.simtp.feature.tests

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.opentest4j.AssertionFailedError
import org.simtp.market.data.securities.api.MarketStreamNames
import org.simtp.market.data.securities.api.Price
import org.simtp.market.data.securities.api.Instrument
import org.simtp.market.data.securities.backend.prices.PricesService
import org.simtp.stream.api.StreamMessageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.data.redis.connection.stream.RecordId
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.retry.backoff.BackOffPolicyBuilder
import org.springframework.retry.policy.SimpleRetryPolicy
import org.springframework.retry.support.RetryTemplate
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import reactor.core.publisher.Mono
import java.time.Duration


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@Tag("integration")
class MarketDataFeatureTests {

    /*
Feature: Send basic security information

  Background: Basic security details include
    symbol/ticker/code
    A description


  Scenario: Send basic security details to SimTP
    Given a source of market information
    When that source sends security information to SimTP via Redis
    Then a Redis cache is updated with the security details

  Scenario: Send basic security prices to SimTP
    Given a source of market information
    When that source sends security information to SimTP via Redis
    Then a Redis cache is updated with the security prices
    And the price is available to consume
     */

    @Test
    @DisplayName("Send basic security details to SimTP")
    fun `send basic security details to SimTP`() {
        given { `a source of market information`() }
        val recordId = `when` { that_source_sends_security_information_to_simtp_via_redis() }
        then { a_redis_cache_is_updated_with_the_security_details(recordId) }
    }

    @Test
    @DisplayName("Send basic security prices to SimTP")
    fun `send basic security prices to SimTP`() {
        given { `a source of market information`() }
        val recordId = `when` { that_source_sends_security_information_to_simtp_via_redis() }
        then { recordId.map { a_redis_cache_is_updated_with_the_security_prices() } }
        and { recordId.map { the_price_is_available_to_consume() } }
    }

    @Autowired
    private lateinit var reactiveRedisOperations: ReactiveRedisOperations<String, String>

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    private fun `a source of market information`() {
        //Don't need to do anything here as the mock securities provider does this
    }

    private fun that_source_sends_security_information_to_simtp_via_redis(): Mono<RecordId> {
        val instrument = Instrument(
            symbol = "BHP",
            description = "BHP Billiton"
        )
        val price = Price(
            symbol = "BHP",
            open = "4.95".toBigDecimal(),
            close = "4.90".toBigDecimal(),
            high = "4.96".toBigDecimal(),
            low = "4.98".toBigDecimal(),
            bid = "4.90".toBigDecimal(),
            ask = "5.10".toBigDecimal(),
            last = "5.00".toBigDecimal()
        )

        val symbolRecord = StreamRecords
            .mapBacked<String, String, String>(mapOf(
                "event-type" to StreamMessageType.CREATE.name,
                "json" to objectMapper.writeValueAsString(instrument)
            ))
            .withStreamKey(MarketStreamNames.MARKET_SECURITIES)

        val priceRecord = StreamRecords
            .mapBacked<String, String, String>(mapOf(
                "event-type" to StreamMessageType.CREATE.name,
                "json" to objectMapper.writeValueAsString(price)
            ))
            .withStreamKey(MarketStreamNames.MARKET_PRICES)


        return reactiveRedisOperations
            .opsForStream<String, String>()
            .add(symbolRecord)
            .flatMap {
                reactiveRedisOperations
                    .opsForStream<String, String>()
                    .add(priceRecord)
            }
            .cache()
    }

    private fun a_redis_cache_is_updated_with_the_security_details(recordId: Mono<RecordId>) {
        val retryTemplate = RetryTemplate().apply {
            this.setBackOffPolicy(BackOffPolicyBuilder.newDefaultPolicy())
            this.setRetryPolicy(SimpleRetryPolicy(5))
        }
        retryTemplate.execute<Unit, Exception> {
            val security =
                recordId
                .flatMap {
                    reactiveRedisOperations
                        .opsForHash<String, String>()
                        .multiGet("securities:BHP", listOf("securityCode", "description"))
                }
                .block(Duration.ofSeconds(10))

            assertEquals("BHP", security?.get(0))
            assertEquals("BHP Billiton", security?.get(1))
        }

    }

    private fun a_redis_cache_is_updated_with_the_security_prices() {
        val retryTemplate = RetryTemplate()
        retryTemplate.execute<Unit, AssertionFailedError> {
            val price = reactiveRedisOperations
                .opsForHash<String, String>()
                .multiGet("securities:BHP", listOf("securityCode", "open", "close", "high", "low", "bid", "ask", "last"))
                .block(Duration.ofSeconds(10))
            assertAll(
                { assertNotNull(price) },
                { assertEquals("BHP", price?.get(0)) },
                { assertEquals("4.95", price?.get(1)) },
                { assertEquals("4.90", price?.get(2)) },
                { assertEquals("4.96", price?.get(3)) },
                { assertEquals("4.98", price?.get(4)) },
                { assertEquals("4.90", price?.get(5)) },
                { assertEquals("5.10", price?.get(6)) },
                { assertEquals("5.00", price?.get(7)) },
            )
        }
    }

    @Autowired
    private lateinit var pricesService: PricesService

    private fun the_price_is_available_to_consume() {
        val retryTemplate = RetryTemplate()
        retryTemplate.execute<Unit, AssertionFailedError> {
           val price = pricesService
                .getPrice("BHP")
                .block(Duration.ofSeconds(10))
            assertNotNull(price)
            assertEquals("4.95", price?.open?.toString())
            assertEquals("4.90", price?.close.toString())
            assertEquals("4.96", price?.high.toString())
            assertEquals("4.98", price?.low.toString())
            assertEquals("4.90", price?.bid.toString())
            assertEquals("5.10", price?.ask.toString())
            assertEquals("5.00", price?.last.toString())
        }


    }

    companion object {


        @Container
        @ServiceConnection
        @JvmStatic
        var redisContainer: GenericContainer<*> = GenericContainer(DockerImageName.parse("redis:latest"))
            .withExposedPorts(6379)

        @Container
        @ServiceConnection
        @JvmStatic
        var postgresContainer = PostgreSQLContainer("postgres")

        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
            postgresContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
            postgresContainer.stop()
        }
    }


}