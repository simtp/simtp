package org.simtp.feature.tests

class TestUtils {
}

inline fun <T> given(function: () -> T): T {
    return function()
}

inline fun <T> `when`(function: () -> T): T {
    return function()
}

inline fun <T> then(function: () -> T): T {
    return function()
}

inline fun <T> and(function: () -> T): T {
    return function()
}