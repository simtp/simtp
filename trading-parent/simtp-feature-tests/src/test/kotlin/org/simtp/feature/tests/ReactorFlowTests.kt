package org.simtp.feature.tests

import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import reactor.test.StepVerifier

class ReactorFlowTests {

    companion object {
        private val log = LoggerFactory.getLogger(ReactorFlowTests::class.java)
    }

    @Test
    fun monoWithDependency() {
        val mono1 = Mono
            .fromSupplier { Thread.sleep(10000); "mono1" }
            .subscribeOn(Schedulers.boundedElastic())

        val mono2 = Mono
            .fromSupplier { Thread.sleep(100); "mono2" }
            .subscribeOn(Schedulers.boundedElastic())

        val join = mono1.flatMap { first ->
            mono2
                .map { "$first;$it" }
        }

        StepVerifier
            .create(join)
            .expectNext("mono1;mono2")
            .expectComplete()
            .verify()



    }


    @Test
    fun monoOrdersWithZip() {
        val mono = Mono
            .fromCallable { "mono1" }
            .zipWith(Mono.fromCallable { "mono2" }) { t1, t2 ->
                "$t1;$t2"
            }

        StepVerifier
            .create(mono)
            .expectNext("mono1;mono2")
            .expectComplete()
            .verify()



    }

    @Test
    fun contextualTest() {
        Mono.deferContextual { ctx ->
            ctx.get<String>("")
            Mono.just("")
        }
    }
}