package org.simtp.feature.tests

import org.simtp.account.rest.server.EnableAccountRestServer
import org.simtp.account.starter.EnableAccountBackend
import org.simtp.account.datasource.EnableJpaAccountDataSource
import org.simtp.address.backend.EnableAddressBackend
import org.simtp.backoffice.backend.EnableBackOfficeBackend
import org.simtp.contacts.starter.EnableContacts
import org.simtp.contacts.stream.EnableContactsStream
import org.simtp.framework.serialization.EnableSerialization
import org.simtp.market.data.mock.provider.securities.EnableMockSecuritiesProvider
import org.simtp.market.data.prices.rest.client.EnablePricesRestClient
import org.simtp.market.data.securities.backend.EnableSecuritiesBackend
import org.simtp.market.data.securities.datasource.redis.EnableRedisMarketDataDataSource
import org.simtp.order.lifecycle.backend.EnableOrderLifecycleBackend
import org.simtp.order.lifecycle.datasource.jpa.EnableJpaOrderDataSource
import org.simtp.origination.data.source.jpa.EnableJpaOriginationDataSource
import org.simtp.origination.module.EnableOrigination
import org.simtp.origination.stream.EnableOriginationStream
import org.simtp.stream.redis.EnableRedisStream
import org.simtp.vetting.datasource.redis.EnableOrderVerificationRedisDataSource
import org.simtp.vetting.modules.basic.EnableOrderVerification
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean

@SpringBootApplication
@EnableContacts
@EnableContactsStream
@EnableOrigination
@EnableOriginationStream
@EnableAccountBackend
@EnableAccountRestServer
@EnableAddressBackend
@EnableSerialization
@EnableRedisStream
@EnableBackOfficeBackend
@EnableSecuritiesBackend
@EnableMockSecuritiesProvider
@EnablePricesRestClient
@EnableOrderLifecycleBackend
@EnableOrderVerification
@EnableJpaOrderDataSource
@EnableJpaAccountDataSource
@EnableJpaOriginationDataSource
@EnableRedisMarketDataDataSource
@EnableOrderVerificationRedisDataSource
open class MyApplication {

    @Bean
    open fun eventRegistry(): EventRegistry {
        return EventRegistry()
    }

}

class EventRegistry: ApplicationListener<ApplicationEvent> {

    private val events = mutableListOf<ApplicationEvent>()
    override fun onApplicationEvent(event: ApplicationEvent) {
        events.add(event)
    }

}

