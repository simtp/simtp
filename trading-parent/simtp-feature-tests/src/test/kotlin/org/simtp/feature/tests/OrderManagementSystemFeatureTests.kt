@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")

package org.simtp.feature.tests

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.common.dto.MarketSide
import org.simtp.common.dto.OrderPriceType
import org.simtp.kotlin.util.uuid
import org.simtp.order.lifecycle.api.*
import org.simtp.order.lifecycle.backend.OrderLifecycleService
import org.simtp.stream.api.StreamMessageType
import org.simtp.stream.redis.RedisStream
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.retry.backoff.BackOffPolicyBuilder
import org.springframework.retry.policy.SimpleRetryPolicy
import org.springframework.retry.support.RetryTemplate
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import java.math.BigDecimal
import java.time.Duration
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@Tag("integration")
class OrderManagementSystemFeatureTests {

    /*
    Feature: Order Management System

      Background: Market exists
        Given the security exists with a price on market

      Scenario: Place a limit price equities buy order

        Given a limit price
        And a number of units
        And a security code
        When the order is submitted
        Then the security code should pass validation
        And an event distributed to other system components
    */



    @BeforeEach
    fun beforeEach() {
        given { a_security_exists_with_a_price_on_market() }
    }

    @Test
    @DisplayName("Place a limit price equities buy order")
    fun `place a limit price equities buy order`() {
        val limitPrice = given { a_limit_price() }
        val units = given { a_number_of_units() }
        val securityCode = given { a_security_code() }
        val orderResponse = `when` { the_order_is_submitted(
            securityCode = securityCode,
            limitPrice = limitPrice,
            quantity = units
        ) }
        then { the_security_code_should_pass_validation(orderResponse) }
        and { an_event_distributed_to_other_system_components() }
    }


    @Autowired
    private lateinit var orderManagementService: OrderLifecycleService

//    @Autowired
//    private lateinit var orderDataSource: IOrderDataSource

    @Autowired
    private lateinit var reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory

    @Autowired
    private lateinit var reactiveRedisOperations: ReactiveRedisOperations<String, String>

    @Autowired
    private lateinit var objectMapper: ObjectMapper


    private val latch = CountDownLatch(1)
    private val counter = AtomicInteger(0)

    private fun a_security_exists_with_a_price_on_market() {

        val orderEvents  = mutableListOf<Order>()
        RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisOperations = reactiveRedisOperations,
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            streamName = ORDER_LIFECYCLE_STREAM_NAME,
            groupName = "OrderManagementSystemFeatureTests",
            consumerName = "OrderManagementSystemFeatureTests"
        )

        .filter { it.value["event-type"] == StreamMessageType.CREATE.name }
        .map { objectMapper.readValue(it.value["json"], Order::class.java) }
        .subscribe {
            orderEvents.add(it)
            latch.countDown()
            counter.getAndIncrement()
        }


        reactiveRedisOperations
            .opsForHash<String, String>()
            .put("securities:BHP", "securityCode", "BHP")
            .doOnNext { log.info("Put security into redis") }
            .block(Duration.ofSeconds(5))
    }


    private fun a_limit_price(): BigDecimal {
        return BigDecimal.TEN
    }

    private fun a_number_of_units(): Int {
        return 100
    }

    private fun a_security_code(): String {
        return "BHP"
    }

    private fun the_order_is_submitted(
        securityCode: String,
        quantity: Int,
        limitPrice: BigDecimal
    ): OrderResponse {
        val order = NewOrder(
            marketReference = uuid(),
            symbol = securityCode,
            quantity = quantity,
            marketSide = MarketSide.BUY,
            orderPriceType = OrderPriceType.LIMIT,
            limitPrice = limitPrice
        )
        return orderManagementService
            .create(order)
            .mapNotNull { it }
            .doOnNext { log.info("Received order Response from API {}", it) }
            .block(Duration.ofSeconds(10))!!
    }

//    fun the_order_should_be_recorded_in_the_order_management_system(orderResponse: OrderResponse) {
//        orderDataSource
//            .findById(orderResponse.id!!)
//            .doOnNext { log.info("Found order={} for id={}", it, orderResponse.id!!) }
//            .block(Duration.ofSeconds(10))
//
//    }

    private fun the_security_code_should_pass_validation(orderResponse: OrderResponse) {
        assertEquals(0, orderResponse.errors.size)
    }

    private fun an_event_distributed_to_other_system_components() {

        val retryTemplate = RetryTemplate().apply {
            this.setBackOffPolicy(BackOffPolicyBuilder.newDefaultPolicy())
            this.setRetryPolicy(SimpleRetryPolicy(5))
        }
        retryTemplate.execute<Unit, Exception> {
            assertTrue(latch.await(10, TimeUnit.SECONDS))
            assertEquals(1, counter.get())
        }

    }

    companion object {
        private val log = LoggerFactory.getLogger(OrderManagementSystemFeatureTests::class.java)

        @Container
        @ServiceConnection
        @JvmStatic
        var redisContainer: GenericContainer<*> = GenericContainer(DockerImageName.parse("redis:latest"))
            .withExposedPorts(6379)

        @Container
        @ServiceConnection
        @JvmStatic
        var postgresContainer = PostgreSQLContainer("postgres")

        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
            postgresContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
            postgresContainer.stop()
        }

    }
}