//@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "unused")
//
//package org.simtp.feature.tests
//
//import com.fasterxml.jackson.databind.ObjectMapper
//import org.junit.jupiter.api.*
//import org.junit.jupiter.api.Assertions.*
//import org.simtp.address.backend.AddressJpaRepository
//import org.simtp.backoffice.api.CASH_TRANSACTIONS_STREAM_NAME
//import org.simtp.backoffice.api.CashTransaction
//import org.simtp.backoffice.backend.BackOfficeSettlementAccountEntity
//import org.simtp.backoffice.backend.BackOfficeSettlementAccountRepository
//import org.simtp.origination.api.IApplicationDataSource
//import org.simtp.origination.api.Application
//import org.simtp.stream.api.StreamMessageType
//import org.slf4j.LoggerFactory
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.boot.test.web.server.LocalServerPort
//import org.springframework.boot.testcontainers.service.connection.ServiceConnection
//import org.springframework.data.redis.connection.stream.StreamRecords
//import org.springframework.data.redis.core.ReactiveStreamOperations
//import org.springframework.retry.backoff.BackOffPolicyBuilder
//import org.springframework.retry.policy.SimpleRetryPolicy
//import org.springframework.retry.support.RetryTemplate
//import org.testcontainers.containers.GenericContainer
//import org.testcontainers.containers.PostgreSQLContainer
//import org.testcontainers.junit.jupiter.Container
//import org.testcontainers.junit.jupiter.Testcontainers
//import org.testcontainers.utility.DockerImageName
//import java.math.BigDecimal
//import java.time.Duration
//
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@Testcontainers
//@Tag("integration")
//class BackOfficeFeatureTests {
//
//
//    /*
//Feature: Mock back office implementation
//
//
//#    Although SimTP does not provide back office functionality. We have a basic concept to allow for basic testing
//#    and overall workflow testing. It does not provide and real functionality or connect to any 3rd party environment.
//#    Nor does it provide any corporate actions functionality
//
//  Scenario: Create a cash settlement account
//    Given an account is originated
//    When the back office receives the notification
//    Then a new back office cash settlement account is created
//
//  Scenario: Financial transactions need to be disseminated
//    Given a customer has a cash settlement account in the back office
//    When the customer lodges cash into that account
//    Then an event should be disseminated to SimTP
//    And the balance of the account updated in SimTP
//     */
//
//    @Test
//    @DisplayName("Create a cash settlement account")
//    fun `create a cash settlement account`() {
//        val id = given { an_account_is_originated() }
//        `when` { the_back_office_receives_the_notification() }
//        then { a_new_back_office_cash_settlement_account_is_created(id) }
//    }
//
//    @Test
//    @DisplayName("Financial transactions need to be disseminated")
//    fun `financial transactions need to be disseminated`() {
//        val id = given { a_customer_has_a_cash_settlement_account_in_the_back_office() }
//        `when` { the_back_office_receives_the_notification() }
//        then { a_new_back_office_cash_settlement_account_is_created(id) }
//    }
//
//    @LocalServerPort
//    private var localServerPort: Int? = null
//
//    @Autowired
//    lateinit var addressJpaRepository: AddressJpaRepository
//
//    @Autowired
//    lateinit var objectMapper: ObjectMapper
//
//    @Autowired
//    lateinit var backOfficeSettlementAccountRepository: BackOfficeSettlementAccountRepository
//
//    @Autowired
//    lateinit var reactiveStreamOperations: ReactiveStreamOperations<String, String, String>
//
//    private var originationRequestData: Application? = null
//
////    @Autowired
////    lateinit var settlementAccountDataSource: ISettlementAccountDataSource
//
//    @Autowired
//    lateinit var originationDataSource: IApplicationDataSource
//
//    @Autowired
//    private lateinit var eventRegistry: EventRegistry
//
//    private fun an_account_is_originated(): String {
//        val data = defaultData()
//        return originateAnAccount(localServerPort!!, data)
//            .doOnNext { log.info("Account originated {}", it) }
//            .block(Duration.ofSeconds(5))!!
//    }
//
//    private fun the_back_office_receives_the_notification() {
//
//    }
//
//    private fun a_new_back_office_cash_settlement_account_is_created(id: String) {
//        retryTemplate.execute<Unit, Exception> {
//            val originatedData = originationDataSource.findById(id).block(Duration.ofSeconds(5))!!
//            val account = backOfficeSettlementAccountRepository
//                .findById(originatedData.account!!.settlementAccounts["primary"]!!.id)
//            assertNotNull(account.get())
//        }
//    }
//
//    private fun a_customer_has_a_cash_settlement_account_in_the_back_office(): String {
//        val id = an_account_is_originated()
//
//        val retryTemplate = RetryTemplate().apply {
//            this.setBackOffPolicy(BackOffPolicyBuilder.newDefaultPolicy())
//            this.setRetryPolicy(SimpleRetryPolicy(5))
//        }
//        val originatedData = retryTemplate.execute<Application, Exception> { originationDataSource.findById(id).block(Duration.ofSeconds(5))!! }
//
//        val result1 = retryTemplate.execute<BackOfficeSettlementAccountEntity, Exception> { backOfficeSettlementAccountRepository
//            .findById(originatedData.account!!.settlementAccounts["primary"]!!.id)
//            .get()
//        }
//        assertNotNull(result1)
//        return id
//    }
//
//    fun the_customer_lodges_cash_into_that_account(originatedData: Application) {
//        val transaction = CashTransaction(
//            accountId = originatedData.account!!.settlementAccounts["primary"]!!.id,
//            sum = BigDecimal.TEN,
//            balance = BigDecimal.TEN
//        )
//        val record = StreamRecords
//            .newRecord()
//            .ofMap(mapOf(
//                "event-type" to StreamMessageType.CREATE.name,
//                "json" to objectMapper.writeValueAsString(transaction)
//            ))
//            .withStreamKey(CASH_TRANSACTIONS_STREAM_NAME)
//        reactiveStreamOperations.add(record).block(Duration.ofSeconds(5))
//    }
//
//    fun an_event_should_be_disseminated_to_SIMTP() {
//
//    }
//
//    fun the_balance_of_the_account_updated_in_SIMTP(originatedData: Application) {
//
//        val id = originatedData
//            .account!!
//            .settlementAccounts["primary"]!!
//            .id
//
//        retryTemplate.execute<Unit, Exception> {
//            val settlementAccount = backOfficeSettlementAccountRepository
//                .findById(id)
//                .get()
//            assertNotNull(settlementAccount)
//            assertTrue(BigDecimal.TEN.compareTo(settlementAccount.balance) == 0)
//        }
//    }
//
//    companion object {
//
//        private val log = LoggerFactory.getLogger(BackOfficeFeatureTests::class.java)
//
//        @Container
//        @ServiceConnection
//        @JvmStatic
//        var redisContainer: GenericContainer<*> = GenericContainer(DockerImageName.parse("redis:latest"))
//            .withExposedPorts(6379)
//
//        @Container
//        @ServiceConnection
//        @JvmStatic
//        var postgresContainer = PostgreSQLContainer("postgres")
//
//        @BeforeAll
//        @JvmStatic
//        fun setUp() {
//            redisContainer
//                .withReuse(true)
//                .start()
//            postgresContainer
//                .withReuse(true)
//                .start()
//        }
//
//        @AfterAll
//        @JvmStatic
//        fun tearDown() {
//            redisContainer.stop()
//            postgresContainer.stop()
//        }
//    }
//
//}