//package org.simtp.feature.tests
//
//import org.junit.jupiter.api.AfterAll
//import org.junit.jupiter.api.Assertions.*
//import org.junit.jupiter.api.BeforeAll
//import org.junit.jupiter.api.DisplayName
//import org.junit.jupiter.api.Tag
//import org.junit.jupiter.api.Test
//import org.simtp.address.backend.AddressJpaRepository
//import org.simtp.origination.api.IApplicationDataSource
//import org.simtp.origination.api.Application
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.boot.test.web.server.LocalServerPort
//import org.springframework.boot.testcontainers.service.connection.ServiceConnection
//import org.springframework.retry.backoff.BackOffPolicyBuilder
//import org.springframework.retry.policy.SimpleRetryPolicy
//import org.springframework.retry.support.RetryTemplate
//import org.testcontainers.containers.GenericContainer
//import org.testcontainers.containers.PostgreSQLContainer
//import org.testcontainers.junit.jupiter.Container
//import org.testcontainers.junit.jupiter.Testcontainers
//import org.testcontainers.utility.DockerImageName
//import java.time.Duration
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@Testcontainers
//@Tag("integration")
//open class OriginationFeatureTests {
//
//    @Test
//    @DisplayName("A new customer can be originated via REST APIs")
//    fun `a new customer can be originated via REST APIs`() {
//        val originalData = given { an_origination_request_is_created() }
//        val result = `when` { the_origination_request_is_submitted(originalData) }
//        then { a_trading_account_should_be_created_but_disabled_for_trading(result)  }
//        then { the_contact_details_recorded(result) }
//        then { the_address_details(result) }
//    }
//
//
//    @LocalServerPort
//    private var localServerPort: Int? = null
//
//    @Autowired
//    lateinit var originationDataSource: IApplicationDataSource
//
//    @Autowired
//    lateinit var addressJpaRepository: AddressJpaRepository
//
//    private fun an_origination_request_is_created(): Application {
//        return defaultData()
//    }
//
//    private fun the_origination_request_is_submitted(originationRequestData: Application): String {
//        return originateAnAccount(localServerPort!!, originationRequestData).block(Duration.ofSeconds(5))!!
//    }
//
//    internal open fun a_trading_account_should_be_created_but_disabled_for_trading(id: String) {
//
//        val retryTemplate = RetryTemplate().apply {
//            this.setRetryPolicy(SimpleRetryPolicy(5))
//            this.setBackOffPolicy(BackOffPolicyBuilder.newDefaultPolicy())
//        }
//        val result = retryTemplate.execute<Application, Exception> { getOriginatedData(localServerPort!!, id).block(Duration.ofSeconds(5)) }
//        val originatedData = originationDataSource.findById(id).block(Duration.ofSeconds(5))
//        assertNotNull(result)
//        assertFalse(originatedData!!.approved)
//
//    }
//
//    private fun the_address_details(id: String) {
//        val originatedData = originationDataSource.findById(id).block(Duration.ofSeconds(5))!!
//        val result = addressJpaRepository
//            .findById(originatedData.contacts!!["primary"]!!.addresses!!["work"]!!.id)
//
//
//        assertNotNull(result)
//    }
//
//    private fun the_contact_details_recorded(id: String) {
//        val originatedData = originationDataSource.findById(id).block()!!
//        assertNotNull(originatedData.contacts!!["primary"]!!.id)
//    }
//
//    companion object {
//
//
//        @Container
//        @ServiceConnection
//        @JvmStatic
//        var redisContainer: GenericContainer<*> = GenericContainer(DockerImageName.parse("redis:latest"))
//            .withExposedPorts(6379)
//
//        @Container
//        @ServiceConnection
//        @JvmStatic
//        var postgresContainer = PostgreSQLContainer("postgres")
//
//        @BeforeAll
//        @JvmStatic
//        fun setUp() {
//            redisContainer
//                .withReuse(true)
//                .start()
//            postgresContainer
//                .withReuse(true)
//                .start()
//        }
//
//        @AfterAll
//        @JvmStatic
//        fun tearDown() {
//            redisContainer.stop()
//            postgresContainer.stop()
//        }
//    }
//
//}