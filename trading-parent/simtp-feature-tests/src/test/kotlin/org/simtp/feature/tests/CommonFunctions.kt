package org.simtp.feature.tests

import org.simtp.origination.api.*
import org.springframework.retry.backoff.ExponentialBackOffPolicy
import org.springframework.retry.policy.SimpleRetryPolicy
import org.springframework.retry.support.RetryTemplate
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import java.util.UUID

internal fun defaultData(id: String = UUID.randomUUID().toString()): Application = Application(
    id = id,
    applicants = mutableMapOf(
        "primary" to Applicant(
            id = id,
            firstName = "John",
            lastName = "Smith",
            email = "johnsmith@error.com",
            mobile = "12345678",
            addresses = mapOf(
                "work" to ApplicantAddress(
                    id = id,
                    houseNumber = "1",
                    streetNumber = "2",
                    streetAddress1 = "streetAddress1",
                    streetAddress2 = "streetAddress2",
                    city = "city",
                    postcode = "postCode",
                )
            )
        )
    ),
    account = ApplicationAccount(
        id = id,
        type = "TEST",
        settlementAccounts = mapOf(
            "primary" to ApplicationSettlementAccount(
                id = id,
                name = "John Smith"
            )
        )
    )
)

internal fun originateAnAccount(port: Int, originationData: Application): Mono<String> {
    val webClient = WebClient
        .create("http://localhost:$port")

    return webClient
        .post()
        .uri("/api/origination/1.0")
        .bodyValue(originationData)
        .retrieve()
        .bodyToMono(String::class.java)


}

internal fun getOriginatedData(port: Int, id: String): Mono<Application> {
    val webClient = WebClient
        .create("http://localhost:$port")

    return webClient
        .get()
        .uri("/api/origination/1.0/{id}", mapOf("id" to id))
        .retrieve()
        .bodyToMono(Application::class.java)


}


internal val retryTemplate = RetryTemplate().apply {
    val backOffPolicy = ExponentialBackOffPolicy().apply {
        this.initialInterval = 200
    }
    setBackOffPolicy(backOffPolicy)
    val retryPolicy = SimpleRetryPolicy().apply {
        maxAttempts = 5
    }
    setRetryPolicy(retryPolicy)
}
