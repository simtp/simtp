package org.simtp.order.lifecycle.backend

import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.simtp.common.serialization.withDefaults
import org.simtp.order.lifecycle.api.NewOrder
import org.simtp.order.lifecycle.api.OrderError
import org.springframework.data.redis.connection.stream.MapRecord
import org.springframework.data.redis.connection.stream.RecordId
import org.springframework.data.redis.core.ReactiveStreamOperations
import reactor.core.publisher.Mono
import reactor.test.StepVerifier

class OrderLifecycleServiceTest {

    @Test
    fun `Create a new order`() {

        val reactiveStreamOperations = mockk<ReactiveStreamOperations<String, String, String>>()
        @Suppress("ReactiveStreamsUnusedPublisher")
        every { reactiveStreamOperations.add(any<MapRecord<String, String, Any>>()) } returns Mono.just(RecordId.of(1000, 1))
        val service = OrderLifecycleService(
            orderDao = mockk(),
            reactiveStreamOperations = reactiveStreamOperations,
            objectMapper = ObjectMapper().withDefaults(),
            newOrderValidator = { Mono.just(it to emptySet()) },
            updateOrderValidator = { Mono.just(it to emptySet()) }
        )

        val newOrder = NewOrder()

        StepVerifier
            .create(service.create(newOrder))
            .expectNextMatches { it.errors == emptySet<OrderError>() && it.success == true }
            .verifyComplete()

    }

}