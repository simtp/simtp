package org.simtp.order.lifecycle.backend

import quickfix.*
import quickfix.field.*
import org.junit.jupiter.api.*
import quickfix.fix44.ExecutionReport
import quickfix.fix44.NewOrderSingle
import quickfix.fix44.component.Parties

// The main class that includes all the tests.
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class QuickFIXJTest {

    private lateinit var acceptor: SocketAcceptor
    private lateinit var initiator: SocketInitiator
    private lateinit var acceptorApp: ServerApp
    private lateinit var initiatorApp: ClientApp

    @BeforeAll
    fun setup() {
        val acceptorSettings = SessionSettings(QuickFIXJTest::class.java.classLoader.getResource("acceptor.cfg")!!.path)
        acceptorApp = ServerApp()
        val acceptorStoreFactory = FileStoreFactory(acceptorSettings)
        val logFactory = ScreenLogFactory(acceptorSettings)
        val messageFactory = DefaultMessageFactory()
        acceptor = SocketAcceptor(acceptorApp, acceptorStoreFactory, acceptorSettings, logFactory, messageFactory)

        val initiatorSettings =
            SessionSettings(QuickFIXJTest::class.java.classLoader.getResource("initiator.cfg")!!.path)
        initiatorApp = ClientApp()
        val initiatorStoreFactory = FileStoreFactory(initiatorSettings)
        initiator = SocketInitiator(initiatorApp, initiatorStoreFactory, initiatorSettings, logFactory, messageFactory)

        acceptor.start()
        initiator.start()

        Thread.sleep(5000)  // Adjust the sleep time as per your connectivity scenario
    }

    @AfterAll
    fun tearDown() {
        acceptor.stop(true)
        initiator.stop(true)
    }

    private fun createAndSendOrder(sessionID: SessionID, initiator: Initiator): NewOrderSingle {
        // Create order
        val newOrder = NewOrderSingle(
            ClOrdID(System.currentTimeMillis().toString()), // Unique identifier for Order as assigned by trader
            Side(Side.BUY), // Side of order
            quickfix.field.TransactTime(), // Time of order
            OrdType(OrdType.LIMIT)  // Order type e.g., LIMIT
        ).apply {
            set(HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PUBLIC_BROKER_INTERVENTION_OK)) // Order handling instruction
            set(Symbol("EUR/USD")) // Ticker symbol
            set(quickfix.field.OrderQty("5.0".toDouble())) // Quantity ordered
            set(quickfix.field.Price("1.23456".toDouble())) // Price

            // Add party
            val party1 = Parties.NoPartyIDs()
            party1.setField(PartyID("operatorID"))
            party1.setField(PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE))
            party1.setField(PartyRole(PartyRole.EXECUTING_TRADER))
            addGroup(party1)

            // Send order
        }
        Session.lookupSession(sessionID)?.send(newOrder) ?: println("Session not found")

        return newOrder
    }

    @Test
    fun testSendAndReceiveOrder() {
        val sessionID = initiator.sessions.first() // Get session ID

        kotlin.runCatching {
            createAndSendOrder(sessionID, initiator)
        }.onFailure { ex ->
            ex.printStackTrace()
        }

        Thread.sleep(5000) // Wait for the order to be sent and processed

        // Add your assertions here
        Assertions.assertTrue(acceptorApp.orderReceived)
        Assertions.assertTrue(initiatorApp.executionReportReceived)
    }
}

class ServerApp : quickfix.MessageCracker(), Application {
    // Implement required methods
    // Accept order message in fromApp() and update the orderReceived flag.

    var orderReceived = false

    override fun fromApp(message: Message, sessionId: SessionID) {
        crack(message, sessionId) // calling message cracker
    }

    @Handler
    fun processOrder(newOrderSingle: NewOrderSingle, sessionId: SessionID) {
        orderReceived = true

        val noPartyIDs = newOrderSingle.noPartyIDs.value

        for (i in 1..noPartyIDs) {
            val partyGroup = NewOrderSingle.NoPartyIDs()
            newOrderSingle.getGroup(i, partyGroup)

            val partyId = PartyID()
            partyGroup.getField(partyId)

            val partyIdSource = PartyIDSource()
            partyGroup.getField(partyIdSource)

            val partyRole = PartyRole()
            partyGroup.getField(partyRole)

            println("Party Id: ${partyId.value}")
            println("Party Id Source: ${partyIdSource.value}")
            println("Party Role: ${partyRole.value}")
        }
        println()

        val execReport = ExecutionReport(
            OrderID("ORDERID"),
            ExecID("EXECID"),
            ExecType(ExecType.FILL),
            OrdStatus(OrdStatus.FILLED),
            Side(newOrderSingle.side.value),
            LeavesQty(0.0),
            CumQty(newOrderSingle.orderQty.value),
            AvgPx(1.0)
        )

        execReport.set(newOrderSingle.clOrdID)
        execReport.set(newOrderSingle.symbol)
        execReport.set(AvgPx(1.23456))
        execReport.set(LastPx(1.23456))
        try {
            Session.sendToTarget(execReport, sessionId)
        } catch (e: Exception) {
            println("Session not found: $e")
        }
    }

    override fun onCreate(p0: SessionID?) {
        println("onCreate")
    }

    override fun onLogon(p0: SessionID?) {
        println("ServerApp: onLogon")
    }

    override fun onLogout(p0: SessionID?) {
        println("ServerApp: onLogout")
    }

    override fun toAdmin(p0: Message?, p1: SessionID?) {
        println("ServerApp: toAdmin")
    }

    override fun fromAdmin(p0: Message?, p1: SessionID?) {
        println("ServerApp: fromAdmin")
    }

    override fun toApp(p0: Message?, p1: SessionID?) {
        println("ServerApp: toApp")
    }
}

class ClientApp : quickfix.MessageCracker(), Application {
    // Implement required methods
    // Confirm order message received by checking execution report and update the executionReportReceived flag.

    var executionReportReceived = false

    override fun fromApp(message: Message, sessionId: SessionID) {
        crack(message, sessionId) // calling message cracker
    }

    @Handler
    fun processExecutionReport(executionReport: ExecutionReport, sessionId: SessionID) {
        executionReportReceived = true
    }

    override fun onCreate(p0: SessionID?) {
        println("ClientApp: onCreate")
    }

    override fun onLogon(p0: SessionID?) {
        println("ClientApp: onLogon")
    }

    override fun onLogout(p0: SessionID?) {
        println("ClientApp: onLogout")
    }

    override fun toAdmin(p0: Message?, p1: SessionID?) {
        println("ClientApp: toAdmin")
    }

    override fun fromAdmin(p0: Message?, p1: SessionID?) {
        println("ClientApp: fromAdmin")
    }

    override fun toApp(p0: Message?, p1: SessionID?) {
        println("ClientApp: toApp")
    }
}