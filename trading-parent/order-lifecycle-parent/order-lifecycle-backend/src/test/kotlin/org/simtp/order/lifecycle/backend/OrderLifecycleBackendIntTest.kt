package org.simtp.order.lifecycle.backend

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.simtp.common.dto.MarketSide
import org.simtp.framework.serialization.EnableSerialization
import org.simtp.kotlin.util.uuid
import org.simtp.order.lifecycle.api.*
import org.simtp.order.lifecycle.api.Order
import org.simtp.stream.redis.EnableRedisStream
import org.simtp.stream.redis.RedisStream
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.testcontainers.containers.GenericContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.utility.DockerImageName
import reactor.core.Disposable
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.math.BigDecimal
import java.time.Duration


@SpringBootTest
@Tag("integration")
@EnableRedisStream
internal class OrderLifecycleBackendIntTest {

    companion object {
        @Container
        @ServiceConnection
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)



        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()

        }

    }

    @Autowired
    lateinit var orderLifecycleApi: IOrderLifecycleApi


    @Autowired
    lateinit var orderDataSource: InMemoryOrderDataSource

    @Autowired
    lateinit var waitingFor: () -> MutableMap<Pair<String, String>, (Order) -> Unit>

    private var disposable: Disposable? = null


    @BeforeEach
    fun beforeEach() {
        waitingFor().clear()


    }

    @AfterEach
    fun afterEach() {
        waitingFor().clear()
        disposable?.dispose()
    }


    @Test
    fun createOrder(){

        val newOrder = NewOrder(
            symbol = "BHP",
            quantity = 100,
            marketSide = MarketSide.BUY
        )
        assertNotNull(orderLifecycleApi
            .create(newOrder)
            .block(Duration.ofMillis(1000)))
    }

    @Test
    fun getOrder() {

        val id = saveOrder()
            .flatMap { retrieveOrder(it) }
            .block(Duration.ofSeconds(5))

        assertNotNull(id)
    }

    private fun retrieveOrder(id: String): Mono<Order> {
        return orderLifecycleApi[id]
    }

    private fun saveOrder(): Mono<String> {
        return orderDataSource.save(
            Order(
                symbol = "BHP",
                quantity = 100,
                marketSide = MarketSide.BUY
            )
        )
            .map { it.id }

    }

    @Test
    fun updateOrder() {
        println("Executing updateOrder")
        val updateOrder = UpdateOrder(
            id = uuid(),
            orderId = uuid(),
            quantity = 200,
            limitPrice = BigDecimal.TEN
        )

        val mono = orderLifecycleApi.amend(updateOrder)
        StepVerifier
            .create(mono)
            .expectNext( OrderResponse(id = updateOrder.id, success = true, emptySet()) )
            .expectComplete()
            .verify(Duration.ofSeconds(5))


    }
}

@SpringBootApplication
@EnableOrderLifecycleBackend
@EnableSerialization
@EnableInMemoryOrderDao
open class OrderLifecycleBackendTestApp: ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    lateinit var connectionFactory: ReactiveRedisConnectionFactory


    @Autowired
    lateinit var reactiveRedisOperations: ReactiveRedisOperations<String, String>

    @Autowired
    private lateinit var objectMapper: ObjectMapper


    private var waitingFor = mutableMapOf<Pair<String, String>, (Order) -> Unit>()

    @Bean
    open fun waitingFor(): () -> MutableMap<Pair<String, String>, (Order) -> Unit> {
        return {
            waitingFor
        }
    }


    override fun onApplicationEvent(event: ApplicationReadyEvent) {

        RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = connectionFactory,
            streamName = "org.simtp.order.lifecycle.persistence",
            groupName = "OrderLifecycleBackendIntTest",
            consumerName = "OrderLifecycleBackendIntTest",
            reactiveRedisOperations = reactiveRedisOperations
        )
        .map { record ->
            val type = record.value["type"]
            val id = record.value["id"]
            waitingFor
                .filter { it.key.first == type && it.key.second == id }
                .map { it.value }
                .forEach { it(objectMapper.readValue(record.value["json"], Order::class.java)) }

        }
        .subscribe()

        reactiveRedisOperations
            .opsForHash<String, String>()
            .putIfAbsent("securities:BHP", "securityCode", "BHP")
            .block(Duration.ofSeconds(5))
    }
}


