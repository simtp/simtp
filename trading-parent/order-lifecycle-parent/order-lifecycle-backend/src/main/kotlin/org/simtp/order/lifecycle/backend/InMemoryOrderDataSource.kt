package org.simtp.order.lifecycle.backend

import org.simtp.order.lifecycle.api.IOrderDataSource
import org.simtp.order.lifecycle.api.Order
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(InMemoryOrderDaoConfiguration::class)
annotation class EnableInMemoryOrderDao
@Configuration
open class InMemoryOrderDaoConfiguration {

    @Bean
    open fun inMemoryOrderDao(): InMemoryOrderDataSource {
        return InMemoryOrderDataSource()
    }
}

class InMemoryOrderDataSource: IOrderDataSource {

    val map: MutableMap<String, Order> = mutableMapOf()

    override fun save(order: Order): Mono<Order> {
        map[order.id] = order
        return Mono.just(order)
    }

    override fun findById(id: String): Mono<Order> {
        return map[id].let { Mono.just(it!!) } ?: Mono.empty()
    }

    override fun findAll(): Flux<Order> {
        return Flux.fromIterable(map.values)
    }
}