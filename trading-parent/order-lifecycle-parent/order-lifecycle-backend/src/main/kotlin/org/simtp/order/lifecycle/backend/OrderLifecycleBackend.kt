package org.simtp.order.lifecycle.backend

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.common.dto.OrderPriceType
import org.simtp.common.dto.OrderType
import org.simtp.kotlin.util.uuid
import org.simtp.order.lifecycle.api.*
import org.simtp.stream.api.StreamMessageType
import org.simtp.stream.redis.EnableRedisStream
import org.simtp.stream.redis.RedisStream
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.connection.stream.MapRecord
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.redis.core.ReactiveStreamOperations
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.*

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(OrderLifecycleBackend::class)
annotation class EnableOrderLifecycleBackend


@Configuration
@EnableRedisStream
open class OrderLifecycleBackend {


    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    open fun orderManagementService(
        orderDao: IOrderDataSource,
        reactiveStreamOperations: ReactiveStreamOperations<String, String, String>,
        objectMapper: ObjectMapper,
        newOrderValidator: (NewOrder) -> Mono<Pair<NewOrder, Set<OrderError>>>,
        updateOrderValidator: (UpdateOrder) -> Mono<Pair<UpdateOrder, Set<OrderError>>>
    ): OrderLifecycleService {
        return OrderLifecycleService(
            orderDao = orderDao,
            reactiveStreamOperations = reactiveStreamOperations,
            objectMapper = objectMapper,
            newOrderValidator = newOrderValidator,
            updateOrderValidator = updateOrderValidator
        )
    }

    @Bean
    open fun newOrderValidator(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>
    ): (NewOrder) -> Mono<Pair<NewOrder, Set<OrderError>>> {
        return { newOrder ->
            reactiveRedisOperations
                .opsForHash<String, String>()
                .get("securities:${newOrder.symbol}", "securityCode")
                .handle { it, sink ->
                    if (it != newOrder.symbol) {
                        sink.error(IllegalStateException("Invalid symbol"))
                    }  else {
                        sink.next(newOrder to emptySet())
                    }
                }
        }

    }

    @Bean
    open fun updateOrderValidator(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>
    ): (UpdateOrder) -> Mono<Pair<UpdateOrder, Set<OrderError>>> {
        return { Mono.just(it to emptySet())}

    }

    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    open fun eventHandler(
        objectMapper: ObjectMapper,
        orderDao: IOrderDataSource,
        reactiveStreamConnectionFactory: ReactiveRedisConnectionFactory,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>
    ): EventHandler {
        return EventHandler(
            objectMapper = objectMapper,
            reactiveStreamConnectionFactory = reactiveStreamConnectionFactory,
            orderDao = orderDao,
            reactiveRedisOperations = reactiveRedisOperations
        )
    }
}

internal fun NewOrder.toOrder(): Order {
    return Order(
        id = uuid(),
        marketReference = this.marketReference ?: uuid(),
        internalReference =  this.internalReference ?: uuid(),
        symbol = this.symbol,
        quantity = this.quantity,
        marketSide = this.marketSide,
        orderPriceType = this.orderPriceType ?: if (limitPrice != null) OrderPriceType.LIMIT else OrderPriceType.MARKET,
        limitPrice = this.limitPrice,
        orderType = OrderType.NEW,
        placementTime = this.placementTime ?: ZonedDateTime.now()
    )
}

internal fun Order.update(updated: UpdateOrder): Order {
    return Order(
        id = id,
        marketReference = this.marketReference,
        symbol = this.symbol,
        quantity = if (updated.quantity != 0) updated.quantity else this.quantity,
        originalQuantity = this.originalQuantity,
        marketSide = this.marketSide,
        orderPriceType = this.orderPriceType,
        limitPrice = if (updated.limitPrice != null) updated.limitPrice else this.limitPrice,
        orderType = this.orderType,
        labels = this.labels,
        orderStatus = this.orderStatus,
        placementTime = this.placementTime
    )
}


open class OrderLifecycleService(
    private val orderDao: IOrderDataSource,
    private val reactiveStreamOperations: ReactiveStreamOperations<String, String, String>,
    private val objectMapper: ObjectMapper,
    private val newOrderValidator: (NewOrder)->Mono<Pair<NewOrder, Set<OrderError>>>,
    private val updateOrderValidator: (UpdateOrder)->Mono<Pair<UpdateOrder, Set<OrderError>>>,
): IOrderLifecycleApi {

    companion object {
        private val log = LoggerFactory.getLogger(OrderLifecycleService::class.java)
    }

    override fun get(id: String): Mono<Order> {
        return orderDao
            .findById(id)
    }

    override fun getAllForCurrentUser(): Flux<Order> {
        return orderDao
            .findAll()
    }



    override fun create(newOrder: NewOrder): Mono<OrderResponse> {
        log.atInfo().log("Entered create")

        val order = newOrder.toOrder()

        return newOrderValidator(newOrder)
            .map { order to it.second }
            .map { OrderResponse(id = it.first.id, success = it.second.isEmpty(), it.second) }
            .doOnNext { log.atInfo().log("Persisting orderResponse={}", it) }
            .flatMap { orderResponse ->
                reactiveStreamOperations
                    .add(
                        MapRecord.create(
                            ORDER_LIFECYCLE_STREAM_NAME,
                            mapOf(
                                "event-type" to StreamMessageType.CREATE,
                                "json" to objectMapper.writeValueAsString(order)
                            )
                        ))
                    .map { orderResponse to it }
            }
            .doOnNext { log.atInfo().log("Recorded order={} on Redis stream={}, recordId={}", it.second, ORDER_LIFECYCLE_STREAM_NAME, it.second) }
            .map { it.first }


    }

    @Transactional(propagation = Propagation.REQUIRED)
    override fun amend(updateOrder: UpdateOrder): Mono<OrderResponse> {
        log.atInfo().log("Entered amend")

        return updateOrderValidator(updateOrder)
            .map { OrderResponse(id = it.first.id, success = it.second.isEmpty(), it.second) }
            .flatMap { orderResponse ->
                reactiveStreamOperations
                    .add(
                        MapRecord.create(
                            ORDER_LIFECYCLE_STREAM_NAME,
                            mapOf(
                                "event-type" to StreamMessageType.CREATE,
                                "json" to objectMapper.writeValueAsString(updateOrder)
                            )
                        ))
                    .map { orderResponse }
            }
    }
}

class EventHandler(
    private val objectMapper: ObjectMapper,
    private val orderDao: IOrderDataSource,
    private val reactiveStreamConnectionFactory: ReactiveRedisConnectionFactory,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val log = LoggerFactory.getLogger(EventHandler::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {

        RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveStreamConnectionFactory,
            streamName = ORDER_LIFECYCLE_STREAM_NAME,
            groupName = "OrderLifecycleBackend",
            consumerName = "OrderLifecycleBackend",
            reactiveRedisOperations = reactiveRedisOperations
        )
        .flatMap { record ->
            when (record.value["event-type"]) {
                StreamMessageType.CREATE.name -> {
                    handleNew(record)
                }
                "AMEND" -> {
                    handleAmend(record)
                }
                StreamMessageType.UPDATE.name -> {
                    handleUpdate(record)
                }
                "VETTED" -> {
                    handleVetted(record)
                }
                else -> {
                    log.atError().log("Received unexpected event-type={}") { record.value["event-type"] }
                    Mono.empty()
                }
            }

        }
        .doOnNext { log.atInfo().log("Handled record for order={}", it) }
        .subscribe()
    }

    private fun handleNew(record: MapRecord<String, String, String>): Mono<Order> {
        return objectMapper
            .readValue(record.value["json"], Order::class.java)!!
            .let { orderDao.save(it) }
            .doOnNext { log.atInfo().log("Handle new. Order saved for order={}", it) }

    }

    private fun handleVetted(record: MapRecord<String, String, String>): Mono<Order> {
        val updateOrder = objectMapper.readValue(record.value["json"], UpdateOrder::class.java)!!
        return updateOrder
            .orderId
            .let { orderDao.findById(updateOrder.orderId!!) }
            .map { savedOrder -> savedOrder.update(updateOrder) }
            .flatMap { orderDao.save(it) }
            .doOnNext { log.atInfo().log("Handle vetted. Order saved for order={}", it) }

    }

    private fun handleUpdate(record: MapRecord<String, String, String>): Mono<Order> {
        val updateOrder = objectMapper.readValue(record.value["json"], UpdateOrder::class.java)!!
        return updateOrder
            .orderId
            .let { orderDao.findById(updateOrder.orderId!!) }
            .map { savedOrder -> savedOrder.update(updateOrder) }
            .flatMap { orderDao.save(it) }
            .doOnNext { log.atInfo().log("Handle update. Order saved for order={}", it) }

    }

    private fun handleAmend(record: MapRecord<String, String, String>): Mono<Order> {
        val updateOrder = objectMapper.readValue(record.value["json"], UpdateOrder::class.java)!!
        return updateOrder
            .orderId
            .let { orderDao.findById(updateOrder.orderId!!) }
            .map { savedOrder -> savedOrder.update(updateOrder) }
            .flatMap { orderDao.save(it) }
            .flatMap {order ->
                reactiveRedisOperations.opsForStream<String, String>()
                    .add(
                        MapRecord.create(
                            ORDER_LIFECYCLE_STREAM_NAME,
                            mapOf(
                                "event-type" to StreamMessageType.UPDATE.name,
                                "json" to objectMapper.writeValueAsString(order)
                            )
                        )
                    )
                    .map {order }
            }
            .doOnNext { log.atInfo().log("Handle amend. Order saved and sent to topic=$ORDER_LIFECYCLE_STREAM_NAME for order={}", it) }

    }
}