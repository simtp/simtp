@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")

package org.simtp.order.lifecycle.datasource.jpa

import jakarta.persistence.*
import org.simtp.common.IHasLabels
import org.simtp.common.dto.MarketSide
import org.simtp.common.dto.OrderPriceType
import org.simtp.common.dto.OrderType
import org.simtp.kotlin.util.uuid
import org.simtp.order.lifecycle.api.*
import org.simtp.utilities.mapOptional
import org.simtp.utilities.scheduleForFlux
import org.simtp.utilities.scheduleForMono
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.time.*


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(JpaOrderDataSourceConfiguration::class)
annotation class EnableJpaOrderDataSource

@Repository
interface OrderRepository: CrudRepository<OrderEntity, String>

@Configuration
@EnableJpaRepositories
@EntityScan
open class JpaOrderDataSourceConfiguration {

    @Bean
    open fun jpaOrderDataSource(orderRepository: OrderRepository): JpaOrderDataSource {
        return JpaOrderDataSource(orderRepository)
    }
}

class JpaOrderDataSource(
    private val orderRepository: OrderRepository
): IOrderDataSource {

    override fun save(order: Order): Mono<Order> {
        return scheduleForMono { orderRepository.save(order.toOrderEntity()) }
            .map { it.toOrder() }
    }

    override fun findById(id: String): Mono<Order> {
        return scheduleForMono { orderRepository.findById(id) }
            .mapOptional { it.toOrder() }
    }

    override fun findAll(): Flux<Order> {
        return scheduleForFlux { orderRepository.findAll() }
            .map { it.toOrder() }
    }
}




@Entity
@Table(name = "orders")
open class OrderEntity(
    @Id
    open var id : String? = uuid(),
    open var marketReference: String = uuid(),
    open var internalReference: String = uuid(),
    open var symbol: String? = null,
    open var quantity: Int? = null,
    open var originalQuantity: Int? = null,
    open var marketSide: MarketSide? = null,
    open var orderPriceType: OrderPriceType = OrderPriceType.MARKET,
    open var limitPrice: BigDecimal? = null,
    open var orderType: OrderType = OrderType.NEW,
    open var orderStatus: OrderStatus = OrderStatus.PENDING,
    @Temporal(TemporalType.TIMESTAMP)
    open var placementTime: Instant = Instant.now(),
    @ElementCollection
    override var labels : Set<String>? = null
): IHasLabels {



    override fun toString(): String {
        return "OrderEntity(id=$id, marketReference='$marketReference', internalReference='$internalReference', symbol=$symbol, quantity=$quantity, originalQuantity=$originalQuantity, marketSide=$marketSide, orderPriceType=$orderPriceType, limitPrice=$limitPrice, orderType=$orderType, orderStatus=$orderStatus, placementTime=$placementTime)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OrderEntity

        if (id != other.id) return false
        if (marketReference != other.marketReference) return false
        if (internalReference != other.internalReference) return false
        if (symbol != other.symbol) return false
        if (quantity != other.quantity) return false
        if (originalQuantity != other.originalQuantity) return false
        if (marketSide != other.marketSide) return false
        if (orderPriceType != other.orderPriceType) return false
        if (limitPrice != other.limitPrice) return false
        if (orderType != other.orderType) return false
        return (orderStatus != other.orderStatus)
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + marketReference.hashCode()
        result = 31 * result + internalReference.hashCode()
        result = 31 * result + (symbol?.hashCode() ?: 0)
        result = 31 * result + (quantity ?: 0)
        result = 31 * result + (originalQuantity ?: 0)
        result = 31 * result + (marketSide?.hashCode() ?: 0)
        result = 31 * result + orderPriceType.hashCode()
        result = 31 * result + (limitPrice?.hashCode() ?: 0)
        result = 31 * result + orderType.hashCode()
        result = 31 * result + orderStatus.hashCode()
        return result
    }
}

internal fun OrderEntity.toOrder(): Order {
    return Order(
        id = this.id ?: error("Unexpected null key stored"),
        marketReference = this.marketReference,
        internalReference = this.internalReference,
        symbol = this.symbol,
        quantity = this.quantity,
        originalQuantity = this.originalQuantity,
        marketSide = this.marketSide,
        orderPriceType = this.orderPriceType,
        limitPrice = this.limitPrice,
        orderType = this.orderType,
        placementTime = ZonedDateTime.ofInstant(this.placementTime, ZoneId.systemDefault())
//        labels = this.labels
    )
}


internal fun Order.toOrderEntity(): OrderEntity {
    return OrderEntity(
        id = this.id,
        marketReference = this.marketReference,
        internalReference = this.internalReference,
        symbol = this.symbol,
        quantity = this.quantity,
        marketSide = this.marketSide,
        orderPriceType = this.orderPriceType,
        limitPrice = this.limitPrice,
        orderType = this.orderType,
        orderStatus = this.orderStatus,
        placementTime = this.placementTime.toInstant()
    )
}

internal fun OrderEntity.update(updated: UpdateOrder): OrderEntity {
    return OrderEntity(
        id = id,
        marketReference = this.marketReference,
        symbol = this.symbol,
        quantity = if (updated.quantity != 0) updated.quantity else this.quantity,
        originalQuantity = this.originalQuantity,
        marketSide = this.marketSide,
        orderPriceType = this.orderPriceType,
        limitPrice = if (updated.limitPrice != null) updated.limitPrice else this.limitPrice,
        orderType = this.orderType,
//        labels = this.labels,
        orderStatus = this.orderStatus,
        placementTime = this.placementTime
    )
}
