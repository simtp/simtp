package org.simtp.order.lifecycle.datasource.jpa

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.simtp.order.lifecycle.api.Order
import reactor.test.StepVerifier
import java.util.Optional
import java.time.Duration

class JpaOrderDataSourceTest {

    @Test
    fun save() {
        val orderRepository = mockk<OrderRepository>(relaxed = true)
        val order = Order()
        val entity = order.toOrderEntity()
        every { orderRepository.save(any()) } returns entity
        val orderDataSource = JpaOrderDataSource(orderRepository)
        StepVerifier
            .create(orderDataSource.save(order))
            .expectNext(order)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val orderRepository = mockk<OrderRepository>(relaxed = true)
        val order = Order()
        val entity = order.toOrderEntity()
        every { orderRepository.findById(order.id) } returns Optional.of(entity)
        val orderDataSource = JpaOrderDataSource(orderRepository)
        StepVerifier
            .create(orderDataSource.findById(order.id))
            .expectNext(order)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val orderRepository = mockk<OrderRepository>(relaxed = true)
        val order = Order()
        val entity = order.toOrderEntity()
        every { orderRepository.findAll() } returns listOf(entity)
        val orderDataSource = JpaOrderDataSource(orderRepository)
        StepVerifier
            .create(orderDataSource.findAll())
            .expectNext(order)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }
}