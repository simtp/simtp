package org.simtp.order.lifecycle.datasource.jpa

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.order.lifecycle.api.Order
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import reactor.test.StepVerifier

@SpringBootTest
@EnableJpaOrderDataSource
@Tag("integration")
class JpaOrderDataSourceConfigurationTest {

    @Autowired
    private lateinit var orderDataSource: JpaOrderDataSource

    @Test
    fun contextLoads() {

        val order = Order(
            id = "order-one",
            symbol = "BHP"
        );

        StepVerifier
            .create(orderDataSource.save(order))
            .expectNext(order)
            .verifyComplete()

        StepVerifier
            .create(orderDataSource.findById(order.id))
            .expectNext(order)
            .verifyComplete()

    }

}

@SpringBootApplication
open class JpaOrderDataSourceTestApp