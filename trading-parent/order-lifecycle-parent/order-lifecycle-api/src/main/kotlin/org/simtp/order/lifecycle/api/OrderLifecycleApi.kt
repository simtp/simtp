package org.simtp.order.lifecycle.api

import org.simtp.common.IHasLabels
import org.simtp.common.dto.MarketSide
import org.simtp.common.dto.OrderPriceType
import org.simtp.common.dto.OrderType
import org.simtp.kotlin.util.uuid
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.UUID

interface IOrderLifecycleApi {

    operator fun get(id: String): Mono<Order>

    fun create(newOrder: NewOrder): Mono<OrderResponse>

    fun amend(updateOrder: UpdateOrder): Mono<OrderResponse>

    fun getAllForCurrentUser(): Flux<Order>
}

data class OrderError(
    var code: String? = null,
    var description: String? = null
)

data class OrderResponse(
    var id: String? = null,
    var success: Boolean = true,
    var errors: Set<OrderError> = emptySet()
)

data class NewOrder(
    var marketReference: String? = uuid(),
    var internalReference: String? = uuid(),
    var symbol: String? = null,
    var quantity: Int? = null,
    var marketSide: MarketSide? = null,
    var orderPriceType: OrderPriceType? = OrderPriceType.MARKET,
    var limitPrice: BigDecimal? = null,
    var placementTime: ZonedDateTime ? = ZonedDateTime.now(),
)

data class UpdateOrder(
    var id: String? = UUID.randomUUID().toString(),
    var verificationReference: String? = null,
    var orderId: String? = null,
    var quantity: Int = 0,
    var limitPrice: BigDecimal? = null,
    var orderStatus: OrderStatus = OrderStatus.PENDING
)

data class Order(
    var id : String = uuid(),
    var marketReference: String = uuid(),
    var internalReference: String = uuid(),
    var symbol: String? = null,
    var quantity: Int? = null,
    var originalQuantity: Int? = null,
    var marketSide: MarketSide? = null,
    var orderPriceType: OrderPriceType = OrderPriceType.MARKET,
    var limitPrice: BigDecimal? = null,
    var orderType: OrderType = OrderType.NEW,
    var orderStatus: OrderStatus = OrderStatus.PENDING,
    var placementTime: ZonedDateTime = ZonedDateTime.now(),
    override var labels : Set<String>? = null
) : IHasLabels {

    override fun toString(): String {
        return "Order(placementTime=$placementTime)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Order

        if (id != other.id) return false
        if (marketReference != other.marketReference) return false
        if (internalReference != other.internalReference) return false
        if (symbol != other.symbol) return false
        if (quantity != other.quantity) return false
        if (originalQuantity != other.originalQuantity) return false
        if (marketSide != other.marketSide) return false
        if (orderPriceType != other.orderPriceType) return false
        if (limitPrice != other.limitPrice) return false
        if (orderType != other.orderType) return false
        if (orderStatus != other.orderStatus) return false
        return labels == other.labels
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + marketReference.hashCode()
        result = 31 * result + internalReference.hashCode()
        result = 31 * result + (symbol?.hashCode() ?: 0)
        result = 31 * result + (quantity ?: 0)
        result = 31 * result + (originalQuantity ?: 0)
        result = 31 * result + (marketSide?.hashCode() ?: 0)
        result = 31 * result + orderPriceType.hashCode()
        result = 31 * result + (limitPrice?.hashCode() ?: 0)
        result = 31 * result + orderType.hashCode()
        result = 31 * result + orderStatus.hashCode()
        result = 31 * result + (labels?.hashCode() ?: 0)
        return result
    }


}

@Suppress("unused")
enum class OrderStatus {
    ON_MARKET,
    PENDING,
    ERROR,
    FAILED_VETTING,
    VETTED
}


const val ORDER_LIFECYCLE_STREAM_NAME = "org.simtp.order.lifecycle"

interface IOrderDataSource {
    fun save(order: Order): Mono<Order>

    fun findById(id: String): Mono<Order>

    fun findAll(): Flux<Order>
}