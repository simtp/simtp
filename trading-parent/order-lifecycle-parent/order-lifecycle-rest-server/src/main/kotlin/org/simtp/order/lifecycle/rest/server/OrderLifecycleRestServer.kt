package org.simtp.order.lifecycle.rest.server

import org.simtp.kotlin.spring.web.responseEntity
import org.simtp.order.lifecycle.api.*
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(OrderLifecycleRestServerConfiguration::class)
annotation class EnableOrderLifecycleRestServer

@Configuration
@ComponentScan
open class OrderLifecycleRestServerConfiguration

@RestController
@CrossOrigin
@RequestMapping("/api/orders/1.0")
class OrderLifecycleRestController(
    private val orderLifecycleApi: IOrderLifecycleApi
) {


    @GetMapping("/{id}")
    fun get(@PathVariable("id") id: String): Mono<ResponseEntity<Order>> {
        return orderLifecycleApi[id]
            .map { it.responseEntity() }
    }

    @PostMapping
    fun post(
        @RequestBody body: UpdateOrder
    ): Mono<ResponseEntity<OrderResponse>> {
        return orderLifecycleApi
            .amend(updateOrder = body)
            .map { it.responseEntity() }
    }


    @PutMapping
    fun put(
        @RequestBody body: NewOrder
    ): Mono<ResponseEntity<OrderResponse>> {
        return orderLifecycleApi
            .create(newOrder = body)
            .map { it.responseEntity() }
    }

    @GetMapping
    fun getAllForCurrentUser(): Flux<Order> {
        return orderLifecycleApi
            .getAllForCurrentUser()
            .map { it }
    }

}

