package org.simtp.order.lifecycle.rest.server

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.simtp.order.lifecycle.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.time.Duration
import java.time.ZonedDateTime


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integration")
internal class OrderLifecycleRestControllerIntTest {

    @LocalServerPort
    private var localPort: Int? = null

    @Autowired
    val inMemoryOrderManagementApi : InMemoryOrderLifecycleApi? = null

    @Test
    fun getAll() {

        val orders = listOf(
            Order(
                id = "1",
                marketReference = "11",
                placementTime = ZonedDateTime.now()

            ),
            Order(
                id = "2",
                marketReference = "22",
                placementTime = ZonedDateTime.now()
            ),
            Order(
                id = "3",
                marketReference = "33",
                placementTime = ZonedDateTime.now()
            )
        )

        inMemoryOrderManagementApi!!.getAllHandler = {
            Flux.fromIterable(orders)
        }

        val result = defaultWebClientBuilder.build()
            .get()
            .uri( "http://localhost:$localPort/api/orders/1.0")
            .retrieve()
            .toEntity(Array<Order>::class.java)
            .block(Duration.ofMillis(5000))
            ?.body

        assertEquals(orders, result?.toList())
    }

    @Test
    fun get() {

        val order = Order(
            id = "1",
            marketReference = "TTTT",
            placementTime = ZonedDateTime.now()
        )

        inMemoryOrderManagementApi!!.getHandler = {
            when (it == "1") {
                true -> Mono.just(order)
                false -> Mono.empty()
            }
        }

        val result = defaultWebClientBuilder
            .build()
            .get()
            .uri( "http://localhost:$localPort/api/orders/1.0/{id}", "1")
            .retrieve()
            .toEntity(Order::class.java)
            .block(Duration.ofMillis(5000))
            ?.body

        assertEquals(order, result)
    }

    @Test
    fun create() {

        inMemoryOrderManagementApi!!.createHandler = {
            when (it.marketReference == "TTTT") {
                true -> Mono.just(
                    OrderResponse(id = "1")
                )
                false -> Mono.empty()
            }
        }

        val newOrder = NewOrder(marketReference = "TTTT")

        val result = WebClient
            .create()
            .put()
            .uri( "http://localhost:$localPort/api/orders/1.0")
            .body(BodyInserters.fromValue(newOrder))
            .retrieve()
            .toEntity(OrderResponse::class.java)
            .block(Duration.ofMillis(5000))
            ?.body

        assertEquals(
            "1",
            result?.id
        )
    }

    @Test
    fun update() {

        inMemoryOrderManagementApi!!.updateHandler = {
            when (it.orderId == "1") {
                true -> Mono.just(
                    OrderResponse(id = "1")
                )
                false -> Mono.empty()
            }
        }

        val updateOrder = UpdateOrder(orderId = "1", quantity = 100, limitPrice = BigDecimal.TEN)

        val result = WebClient
            .create()
            .post()
            .uri( "http://localhost:$localPort/api/orders/1.0")
            .body(BodyInserters.fromValue(updateOrder))
            .retrieve()
            .toEntity(OrderResponse::class.java)
            .block(Duration.ofMillis(5000))
            ?.body

        assertEquals(
            "1",
            result?.id
        )
    }
}

@SpringBootApplication
@EnableOrderLifecycleRestServer
internal open class OrderLifecycleRestControllerApp {

    @Bean
    open fun orderLifecycleApi(): InMemoryOrderLifecycleApi {
        return InMemoryOrderLifecycleApi()
    }

}

internal class InMemoryOrderLifecycleApi(
    var getHandler: (String) -> Mono<Order> = { error("Not yet implemented") },
    var createHandler: (NewOrder) -> Mono<OrderResponse> = { error("Not yet implemented") },
    var updateHandler: (UpdateOrder) -> Mono<OrderResponse> = { error("Not yet implemented") },
    var getAllHandler: () -> Flux<Order> = { error("Not yet implemented") }
): IOrderLifecycleApi {

    override fun get(id: String): Mono<Order> {
        return getHandler(id)
    }

    override fun create(newOrder: NewOrder): Mono<OrderResponse> {
        return createHandler(newOrder)
    }

    override fun amend(updateOrder: UpdateOrder): Mono<OrderResponse> {
        return updateHandler(updateOrder)
    }

    override fun getAllForCurrentUser(): Flux<Order> {
        return getAllHandler()
    }
}