package org.simtp.vetting.modules.basic

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.simtp.market.data.securities.api.Price
import org.simtp.vetting.api.VerificationRequest
import reactor.core.publisher.Mono
import java.time.Duration

class MarketRulesTest {

    @ParameterizedTest
    @CsvSource(
        "10,10,0",
        "50,100,100",
        "50,25,50",
    )
    fun percentageDiff(price: String, limitPrice: String, diff: Int) {
        assertEquals(diff, price.toBigDecimal().percentageDiff(limitPrice.toBigDecimal()))
    }

    @ParameterizedTest
    @CsvSource(
        "10,10,true",
        "50,100,false",
        "50,25,false",
    )
    fun marketDeviation(price: String, limitPrice: String, expected: Boolean) {
        val result = checkMarketPriceDeviation(
            Mono.just(VerificationRequest(limitPrice = limitPrice.toBigDecimal())),
            Mono.just(Price(last = price.toBigDecimal()))
        ).block(Duration.ofSeconds(1))

        assertEquals(expected, result?.isEmpty())

    }
}