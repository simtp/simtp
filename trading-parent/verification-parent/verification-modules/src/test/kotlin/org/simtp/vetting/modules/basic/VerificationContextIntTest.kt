package org.simtp.vetting.modules.basic

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.market.data.securities.api.IPricesApi
import org.simtp.market.data.securities.api.Price
import org.simtp.stream.redis.EnableRedisStream
import org.simtp.vetting.api.VerificationContext
import org.simtp.vetting.api.VerificationRequest
import org.simtp.vetting.datasource.memory.EnableOrderVerificationMemoryDataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.time.Duration

@SpringBootTest
@Tag("integration")
open class VerificationContextIntTest {

    @Autowired
    private lateinit var contextFactory: (VerificationRequest) -> VerificationContext

    @Test
    open fun contextLoads() {
        val order = createVerificationRequest()
        val context = contextFactory(order)
        assertEquals(order, context.orderProvider.block(Duration.ofSeconds(5)))
    }
}

@SpringBootApplication
@EnableOrderVerification
@EnableRedisStream
@EnableOrderVerificationMemoryDataSource
open class TestApplication {

    @Bean
    open fun pricesApi(): IPricesApi {
        return object: IPricesApi {
            override fun getPrice(symbol: String): Mono<Price> {
                return Mono.just(Price(last = BigDecimal.ONE))
            }
        }
    }
}