package org.simtp.vetting.modules.basic

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.simtp.common.dto.MarketSide
import org.simtp.market.data.securities.api.Price
import org.simtp.vetting.api.VerificationContext
import org.simtp.vetting.api.VerificationRequest
import reactor.core.publisher.Mono
import java.time.Duration
import java.util.concurrent.atomic.AtomicInteger

@Suppress("ReactiveStreamsUnusedPublisher")
class AdvancedContextRules {

    private val orderProviderCount = AtomicInteger(0)
    private val priceProviderCount = AtomicInteger(0)
    private var context: VerificationContext? = null

    @BeforeEach
    fun setUp() {
        context = VerificationContext()
        orderProviderCount.set(0)
        priceProviderCount.set(0)
    }

    @Test
    fun gatherersDontGetExecutedTwice() {
        givenAnIOGatherer()
        andTheGatherIsExecutedByACheck()
        whenTheGathererIsExecutedTwice()
        thenTheTheGathererIOOperationShouldOnlyBeExecutedOnce()
    }



    @Test
    fun dependentGatherersDontGetExecutedTwice() {
        givenMultipleIOGatherers()
        whenACheckWithMultipleDependenciesIsExecuted()
        thenTheTheGathererIOOperationsShouldOnlyBeExecutedOnce()
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    private fun givenAnIOGatherer() {
        this.context?.apply {
            this.orderProvider = Mono.fromCallable {
                orderProviderCount.incrementAndGet()
                createVerificationRequest()
            }.cache()

        }
    }

    private fun givenMultipleIOGatherers() {
        context?.apply {
            this.orderProvider = getVerificationRequest {
                orderProviderCount.incrementAndGet()
                createVerificationRequest()
            }

        }

        context?.apply {
            this.priceProvider = getPrice(this.orderProvider) {
                priceProviderCount.incrementAndGet()
                Mono.just(Price())
            }
        }

    }

    private fun andTheGatherIsExecutedByACheck() {
        context?.orderProvider?.block(Duration.ofSeconds(5))
    }

    private fun whenACheckWithMultipleDependenciesIsExecuted() {
        context?.priceProvider?.block(Duration.ofSeconds(5))
    }

    private fun whenTheGathererIsExecutedTwice() {

        context?.orderProvider?.block(Duration.ofSeconds(5))
    }

    private fun thenTheTheGathererIOOperationShouldOnlyBeExecutedOnce() {
        assertEquals(1, orderProviderCount.get())
        assertEquals(0, priceProviderCount.get())
    }

    private fun thenTheTheGathererIOOperationsShouldOnlyBeExecutedOnce() {
        assertEquals(1, orderProviderCount.get())
        assertEquals(1, priceProviderCount.get())
    }

}

val createVerificationRequest: () -> VerificationRequest ={
    VerificationRequest(
        symbol = "BHP",
        quantity = 100,
        marketSide = MarketSide.BUY
    )
}