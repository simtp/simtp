package org.simtp.vetting.modules.basic

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.IPricesApi
import org.simtp.stream.api.StreamMessageType
import org.simtp.stream.redis.RedisStream
import org.simtp.vetting.api.*
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Scope
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.connection.stream.MapRecord
import org.springframework.data.redis.connection.stream.RecordId
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Mono


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(VettingConfiguration::class)
annotation class EnableOrderVerification

@Configuration
open class VettingConfiguration {

    @Bean
    @Scope("prototype")
    open fun contextFactory(
        pricesService: IPricesApi
    ): (VerificationRequest) -> VerificationContext = { order ->
        VerificationContext().apply {
            orderProvider = getVerificationRequest { order }
            priceProvider = getPrice(this.orderProvider) { pricesService.getPrice(symbol = it) }
        }
    }

    @Bean
    open fun orderVettingHandler(
        reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
        verificationContextFactory: (VerificationRequest) -> VerificationContext,
        objectMapper: ObjectMapper,
        verificationDataSource: IVerificationDataSource,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>
    ): OrderVettingHandler {
        return OrderVettingHandler(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            verificationContextFactory = verificationContextFactory,
            objectMapper = objectMapper,
            verificationDataSource = verificationDataSource,
            reactiveRedisOperations = reactiveRedisOperations
        )
    }
}





open class OrderVettingHandler(
    private val reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
    private val verificationContextFactory: (VerificationRequest) -> VerificationContext,
    private val verificationDataSource: IVerificationDataSource,
    private val objectMapper: ObjectMapper,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val log = LoggerFactory.getLogger(OrderVettingHandler::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            streamName = ORDER_VERIFICATION_STREAM_NAME,
            groupName = "OrderVettingHandler",
            consumerName = "OrderVettingHandler",
            reactiveRedisOperations = reactiveRedisOperations
        )
        .flatMap { record ->
            val message = record.value["json"]
            when (record.value["type"]) {
                StreamMessageType.CREATE.name -> {
                    handleNew(message)
                }

                "VETTED" -> {
                    //Do nothing
                    Mono.empty()
                }

                else -> {
                    log.warn("Unexpected event type {}", record.value["type"])
                    Mono.empty()
                }
            }
        }
        .subscribe()
    }

    private fun handleNew(message: String?): Mono<RecordId> {
        val order = objectMapper
            .readValue(message, VerificationRequest::class.java)!!


        return applyRules(order)

            .flatMap {
                verificationDataSource.save(it)
            }
            .flatMap {
                reactiveRedisOperations.opsForStream<String, String>()
                    .add(
                        MapRecord.create(
                            ORDER_VERIFICATION_STREAM_NAME,
                            mapOf(
                                "event-type" to "VETTED",
                                "json" to objectMapper.writeValueAsString(it)
                            )
                        )
                    )
            }

    }

    private fun applyRules(request: VerificationRequest): Mono<OrderVerificationResult> {
        val context = verificationContextFactory(request)
        return checkMarketPriceDeviation(context.orderProvider, context.priceProvider)
            .map {
                OrderVerificationResult(
                    id = request.id,
                    errors = it
                )
            }
    }
}

