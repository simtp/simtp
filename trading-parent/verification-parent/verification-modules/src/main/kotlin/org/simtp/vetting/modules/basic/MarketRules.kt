package org.simtp.vetting.modules.basic

import org.simtp.market.data.securities.api.Price
import org.simtp.vetting.api.VerificationError
import org.simtp.vetting.api.VerificationRequest
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.absoluteValue

internal val getVerificationRequest: (() -> VerificationRequest) -> Mono<VerificationRequest> = {
    Mono.just(it())
}

internal val getPrice: (Mono<VerificationRequest>, (String) -> Mono<Price>) -> Mono<Price> = { request, priceProvider ->
    request
        .flatMap { priceProvider(it.symbol!!) }
        .cache()
}

internal val checkMarketPriceDeviation: (Mono<VerificationRequest>, Mono<Price>) -> Mono<Set<VerificationError>> = { orderMono, priceMono ->
    Mono.zip(orderMono, priceMono) { order, price ->
        when (order.limitPrice == null) {
            true -> emptySet()
            false -> {
                if ((price.last?.percentageDiff(order.limitPrice!!) ?: 0 ) > 25) {
                    setOf(
                        VerificationError(
                            description = "Price deviates too much from current market price"
                        )
                    )
                } else {
                    emptySet()
                }
            }
        }
    }
}

internal fun BigDecimal.percentageDiff(other: BigDecimal): Int {
    return other
        .subtract(this)
        .divide(this.abs(), 2, RoundingMode.HALF_UP)
        .multiply(BigDecimal("100"))
        .toInt()
        .absoluteValue
}



