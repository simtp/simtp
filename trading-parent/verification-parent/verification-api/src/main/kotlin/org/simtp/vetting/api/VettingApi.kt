package org.simtp.vetting.api

import org.simtp.common.IHasLabels
import org.simtp.common.dto.MarketSide
import org.simtp.common.dto.OrderPriceType
import org.simtp.common.dto.OrderType
import org.simtp.kotlin.util.uuid
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.time.Instant
import java.time.ZonedDateTime
import java.util.*

interface IVerificationApi {
    /**
     * VerificationException should be thrown should invocation fail
     */
    operator fun invoke(request: VerificationRequest): Mono<VerificationContext>
}

interface IVerificationDataSource {
    /**
     * returns a VerificationPersistException should the save fail
     */
    fun save(orderVerificationResult: OrderVerificationResult): Mono<OrderVerificationResult>
}

open class VerificationException(message: String) : RuntimeException(message)

open class VerificationPersistException(message: String) : VerificationException(message)

const val ORDER_VERIFICATION_STREAM_NAME = "org.simtp.order.verification"

data class VerificationError(
    var code: String? = null,
    var description: String? = null
)

data class OrderVerificationResult(
    var id : String? = UUID.randomUUID().toString(),
    var correlationId: String = UUID.randomUUID().toString(),
    var errors: Set<VerificationError>? = emptySet(),
    var date: ZonedDateTime = ZonedDateTime.now()
)

data class VerificationRequest(
    var id : String = uuid(),
    var reference: String? = null,
    var symbol: String? = null,
    var quantity: Int? = null,
    var originalQuantity: Int? = null,
    var marketSide: MarketSide? = null,
    var orderPriceType: OrderPriceType = OrderPriceType.MARKET,
    var limitPrice: BigDecimal? = null,
    var orderType: OrderType = OrderType.NEW,
    var placementTime: Instant? = Instant.now(),
    override var labels : Set<String>? = null
) : IHasLabels