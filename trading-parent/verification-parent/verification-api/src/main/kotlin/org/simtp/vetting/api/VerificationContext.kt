package org.simtp.vetting.api

import org.simtp.market.data.securities.api.Price
import reactor.core.publisher.Mono


open class VerificationContext(
    store: MutableMap<String, Any?> = mutableMapOf()
) {
    var orderProvider: Mono<VerificationRequest> by store
    var priceProvider: Mono<Price> by store
}