package org.simtp.vetting.datasource.memory

import org.simtp.vetting.api.IVerificationDataSource
import org.simtp.vetting.api.OrderVerificationResult
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import reactor.core.publisher.Mono


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(OrderVerificationMemoryDataSourceConfiguration::class)
annotation class EnableOrderVerificationMemoryDataSource

@Configuration
open class OrderVerificationMemoryDataSourceConfiguration {

    @Bean
    open fun memoryVerificationDataSource(): MemoryVerificationDataSource {
        return MemoryVerificationDataSource()
    }

}

class MemoryVerificationDataSource: IVerificationDataSource {

    private val map: MutableMap<String, OrderVerificationResult> = mutableMapOf()

    override fun save(orderVerificationResult: OrderVerificationResult): Mono<OrderVerificationResult> {
        return orderVerificationResult
            .id
            ?.also { map[it] = orderVerificationResult }
            ?.let { Mono.just(orderVerificationResult) }
            ?: Mono.empty()
    }
}

