package org.simtp.vetting.datasource.redis

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.vetting.api.IVerificationDataSource
import org.simtp.vetting.api.OrderVerificationResult
import org.simtp.vetting.api.VerificationPersistException
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.DataAccessException
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Mono


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(OrderVerificationRedisDataSourceConfiguration::class)
annotation class EnableOrderVerificationRedisDataSource

@Configuration
open class OrderVerificationRedisDataSourceConfiguration {

    @Bean
    open fun redisVerificationDataSource(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        objectMapper: ObjectMapper
    ): RedisVerificationDataSource {
        return RedisVerificationDataSource(
            reactiveRedisOperations = reactiveRedisOperations,
            objectMapper = objectMapper
        )
    }

}

fun OrderVerificationResult.toMap(objectMapper: ObjectMapper): Map<String, String> {
    val result = this
    return mapOf(
        "id" to result.id!!,
        "date" to result.date.toInstant().toString(),
        "correlationId" to result.correlationId,
        "errors" to objectMapper.writeValueAsString(result.errors)
    )
}



class RedisVerificationDataSource(
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val objectMapper: ObjectMapper
): IVerificationDataSource {

    internal fun save(map: Map<String, String>): Mono<Boolean> {
        return reactiveRedisOperations
            .opsForHash<String, String>()
            .putAll("order:verifications:${map["id"]}", map)
    }

    override fun save(orderVerificationResult: OrderVerificationResult): Mono<OrderVerificationResult> {
        return save(orderVerificationResult.toMap(objectMapper))
            .flatMap {
                when (it) {
                    true -> Mono.just(orderVerificationResult)
                    false -> Mono.error(VerificationPersistException("Failed to persist order for $orderVerificationResult"))
                }
            }
    }
}