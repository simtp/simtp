package org.simtp.vetting.datasource.redis

import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.simtp.common.serialization.withDefaults
import org.simtp.vetting.api.OrderVerificationResult
import org.simtp.vetting.api.VerificationError
import org.simtp.vetting.api.VerificationPersistException
import org.springframework.data.redis.core.ReactiveHashOperations
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.ZonedDateTime.now

class RedisVerificationDataSourceTest {

    private val error = VerificationError(code = "code", description = "description")
    private val date = now()
    private val orderVerificationResult = OrderVerificationResult(
        id = "123",
        correlationId = "321",
        errors = setOf(error),
        date = date
    )


    @Test
    fun `Successful save`() {

        val objectMapper = ObjectMapper().withDefaults()
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>()
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash

        every { opsForHash.putAll(any(), any()) } returns Mono.just(true)

        val ds = RedisVerificationDataSource(
            reactiveRedisOperations = reactiveRedisOperations,
            objectMapper = objectMapper
        )

        StepVerifier
            .create(ds.save(orderVerificationResult))
            .expectNext(orderVerificationResult)
            .verifyComplete()

    }

    @Test
    fun `Unsuccessful save`() {

        val objectMapper = ObjectMapper().withDefaults()
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>()
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash

        every { opsForHash.putAll("order:verifications:${orderVerificationResult.id}", any()) } returns Mono.just(false)

        val ds = RedisVerificationDataSource(
            reactiveRedisOperations = reactiveRedisOperations,
            objectMapper = objectMapper
        )

        StepVerifier
            .create(ds.save(orderVerificationResult))
            .verifyError(VerificationPersistException::class.java)

    }

}