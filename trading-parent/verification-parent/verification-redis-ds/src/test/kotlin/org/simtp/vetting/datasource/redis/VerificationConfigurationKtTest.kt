package org.simtp.vetting.datasource.redis

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.simtp.common.serialization.withDefaults
import org.simtp.vetting.api.OrderVerificationResult
import org.simtp.vetting.api.VerificationError
import java.time.ZonedDateTime.now

class VerificationConfigurationKtTest {

    @Test
    fun convert() {

        val objectMapper = ObjectMapper().withDefaults()
        val error = VerificationError(code = "code", description = "description")
        val date = now()
        val result = OrderVerificationResult(
            id = "123",
            correlationId = "321",
            errors = setOf(error),
            date = date
        ).toMap(objectMapper)

        assertAll(
            { assertEquals(result["id"], "123") },
            { assertEquals(result["correlationId"], "321") },
            { assertEquals(result["correlationId"], "321") },
            { assertEquals(result["date"], date.toInstant().toString()) },
            { assertEquals(result["errors"], objectMapper.writeValueAsString(setOf(error))) }
        )
    }
}