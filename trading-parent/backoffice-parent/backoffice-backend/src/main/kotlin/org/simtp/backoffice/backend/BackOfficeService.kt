@file:Suppress("unused", "SpringJavaInjectionPointsAutowiringInspection")

package org.simtp.backoffice.backend

import com.fasterxml.jackson.databind.ObjectMapper
import jakarta.persistence.ElementCollection
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.simtp.backoffice.api.CASH_TRANSACTIONS_STREAM_NAME
import org.simtp.backoffice.api.CashTransaction
import org.simtp.common.IHasLabels

import org.simtp.origination.api.Application
import org.simtp.origination.stream.OriginationStreamProperties
import org.simtp.stream.redis.RedisStream
import org.simtp.utilities.mapOptional
import org.simtp.utilities.scheduleForMono
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.util.*

@Configuration
@ComponentScan
@EnableJpaRepositories
@EntityScan
open class BackOfficeBackendConfiguration {

    @Bean
    open fun backOfficeService(
        backOfficeSettlementAccountRepository: BackOfficeSettlementAccountRepository,
        reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        objectMapper: ObjectMapper,
        originationStreamProperties: OriginationStreamProperties
    ): BackOfficeService {
        return BackOfficeService(
            backOfficeSettlementAccountRepository = backOfficeSettlementAccountRepository,
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            objectMapper = objectMapper,
            originationStreamProperties = originationStreamProperties
        )
    }


}
@Repository
interface BackOfficeSettlementAccountRepository: CrudRepository<BackOfficeSettlementAccountEntity, String>

class BackOfficeService(
    private val backOfficeSettlementAccountRepository: BackOfficeSettlementAccountRepository,
    private val reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val objectMapper: ObjectMapper,
    private val originationStreamProperties: OriginationStreamProperties
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val log = LoggerFactory.getLogger(BackOfficeService::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {

        backOfficeSettlementListenerFactory().subscribe()


        backOfficeCashTransactionListenerFactory().subscribe()

    }

    private fun backOfficeCashTransactionListenerFactory(): Flux<BackOfficeSettlementAccountEntity> {
        return RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = CASH_TRANSACTIONS_STREAM_NAME,
            groupName = "BackOfficeService",
            consumerName = "BackOfficeService"
        )
        .flatMap {
            handleCashTransactionMessage(
                settlementAccountDataSource = backOfficeSettlementAccountRepository,
                objectMapper = objectMapper,
                msg = it.value
            )
        }
        .doOnNext { log.info("Handling cash transaction {}", it) }
    }

    private fun backOfficeSettlementListenerFactory(): Flux<BackOfficeSettlementAccountEntity> =
        RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = originationStreamProperties.name,
            groupName = "BackOfficeService",
            consumerName = "BackOfficeService"
        )
        .flatMap { msg ->
            (objectMapper.readValue(msg.value["json"]!!, Application::class.java)
                ?.account
                ?.settlementAccounts
                ?.map { it.value }
                ?.map {
                    BackOfficeSettlementAccountEntity(
                        id = it.id,
                        name = it.name,
                        balance = it.balance,
                        labels = it.labels
                    )
                }
                ?: emptyList())
                .let { Flux.fromIterable(it) }
        }
        .flatMap {
            scheduleForMono { backOfficeSettlementAccountRepository.save(it) }
        }
        .doOnNext { log.info("Created backoffice settlement account into the database account={}", it) }

}


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(BackOfficeBackendConfiguration::class)
annotation class EnableBackOfficeBackend


@Entity
@Table(name = "backoffice_settlement_account")
open class BackOfficeSettlementAccountEntity(
    @Id
    open var id: String? = null,
    open var name: String? = null,
    open var balance: BigDecimal? = null,
    @ElementCollection
    override var labels : Set<String>? = null
): IHasLabels {

    override fun toString(): String {
        return "BackOfficeSettlementAccount(id=$id, name=$name, balance=$balance, labels=$labels)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BackOfficeSettlementAccountEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (balance != other.balance) return false
        if (labels != other.labels) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (balance?.hashCode() ?: 0)
        result = 31 * result + (labels?.hashCode() ?: 0)
        return result
    }
}

private val log = LoggerFactory.getLogger("org.simtp.backoffice.backend.BackOfficeServiceKt")


data class AddressMessage(
    var id: String? = UUID.randomUUID().toString(),
    var houseNumber: String? = null,
    var streetNumber: String? = null,
    var streetAddress1: String? = null,
    var streetAddress2: String? = null,
    var city: String? = null,
    var postcode: String? = null,
)

internal fun handleCashTransactionMessage(
    settlementAccountDataSource: BackOfficeSettlementAccountRepository,
    objectMapper: ObjectMapper,
    msg: Map<String, String>
): Mono<BackOfficeSettlementAccountEntity> =

    Mono
    .fromCallable {
        objectMapper
            .readValue(msg["json"]!!, CashTransaction::class.java)
    }
    .doOnNext { log.info("Received cash transaction={}", it) }
    .flatMap { transaction ->
        when (transaction.sum != BigDecimal.ZERO) {
            true -> scheduleForMono { settlementAccountDataSource
                .findById(transaction.accountId!!) }
                .mapOptional { transaction to it }
            false -> Mono.empty()
        }

    }.flatMap {
        val transaction = it.first
        val account = it.second
        account.balance = (account.balance ?: BigDecimal.ZERO).add(transaction.sum ?: BigDecimal.ZERO)
        scheduleForMono { settlementAccountDataSource.save(it.second) }
    }.doOnNext { log.info("Created cash transaction in data store for account={}", it) }