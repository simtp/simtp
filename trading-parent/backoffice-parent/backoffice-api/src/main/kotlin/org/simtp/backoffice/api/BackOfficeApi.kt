@file:Suppress("unused")

package org.simtp.backoffice.api

import org.simtp.common.IHasLabels
import java.math.BigDecimal

const val CASH_TRANSACTIONS_STREAM_NAME = "cash.transactions"

open class CashTransaction(
    var id: String? = null,
    var accountId: String? = null,
    var sum: BigDecimal? = null,
    var balance: BigDecimal? = null,
    override var labels : Set<String>? = null

): IHasLabels








