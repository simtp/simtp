package org.simtp.market.data.securities.backend.prices

import org.simtp.market.data.securities.api.Price
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RequestMapping("/api/prices/1.0")
@RestController
class PricesRestController(
    private val pricesService: PricesService
) {

    @GetMapping("/{securityCode}")
    fun get(@PathVariable securityCode: String): Mono<Price> {
        return pricesService.getPrice(securityCode)
    }

}