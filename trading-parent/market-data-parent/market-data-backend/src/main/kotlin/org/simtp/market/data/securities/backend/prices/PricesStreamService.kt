package org.simtp.market.data.securities.backend.prices

import org.simtp.market.data.securities.api.Price
import java.util.*

class PricesStreamService {
    private val queues: MutableMap<String, Queue<Price>> = mutableMapOf()

    fun addPrice(price: Price) {
        queues[price.symbol!!]?.add(price)
    }
}
