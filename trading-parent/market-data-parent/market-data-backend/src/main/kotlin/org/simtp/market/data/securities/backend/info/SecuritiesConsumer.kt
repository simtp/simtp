package org.simtp.market.data.securities.backend.info

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.MarketStreamNames
import org.simtp.market.data.securities.api.Instrument
import org.simtp.stream.redis.RedisStream
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations

open class SecuritiesConsumer(
    private val reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val objectMapper: ObjectMapper
): ApplicationListener<ApplicationReadyEvent> {

    private val redisOps = reactiveRedisOperations.opsForHash<String, String>()

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            streamName = MarketStreamNames.MARKET_SECURITIES,
            groupName = "SecuritiesConsumer",
            consumerName = "SecuritiesConsumer",
            reactiveRedisOperations = reactiveRedisOperations
        )
        .flatMap {
            val instrument = objectMapper.readValue(it.value["json"], Instrument::class.java)
            redisOps
                .putAll(
                    "securities:${instrument.symbol}",
                    mapOf(
                        "securityCode" to instrument.symbol!!,
                        "description" to instrument.description!!
                    )
                )
        }
        .subscribe()
    }
}