@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "SpringFacetCodeInspection")

package org.simtp.market.data.securities.backend

import org.simtp.market.data.securities.backend.info.EnableSecurityInfo
import org.simtp.market.data.securities.backend.prices.EnableSecurityPrices



@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@EnableSecurityInfo
@EnableSecurityPrices
annotation class EnableSecuritiesBackend
