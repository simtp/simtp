package org.simtp.market.data.securities.backend.prices

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.IPricesDataSource
import org.simtp.market.data.securities.api.MarketStreamNames
import org.simtp.market.data.securities.api.Price
import org.simtp.stream.redis.RedisStream
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Mono

open class PricesConsumer(
    private val reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val pricesDataSource: IPricesDataSource,
    private val objectMapper: ObjectMapper,
    private val pricesStreamService: PricesStreamService,
): ApplicationListener<ApplicationReadyEvent> {

    override fun onApplicationEvent(event: ApplicationReadyEvent) {

        RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = MarketStreamNames.MARKET_PRICES,
            groupName = "PricesConsumer",
            consumerName = "PricesConsumer"
        )
        .flatMap { mapRecord ->
            Mono.just(objectMapper
                .readValue(mapRecord.value["json"], Price::class.java))
        }
        .flatMap {
            pricesDataSource.save(it)
        }
        .doOnNext {
            pricesStreamService.addPrice(it)
        }
        .subscribe()
    }
}