package org.simtp.market.data.securities.backend.prices

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.IPricesDataSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(PricesConfiguration::class)
annotation class EnableSecurityPrices

@Configuration
@ComponentScan
open class PricesConfiguration {

    @Bean
    open fun pricesConsumer(
        reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        pricesDataSource: IPricesDataSource,
        objectMapper: ObjectMapper,
        pricesStreamService: PricesStreamService
    ): PricesConsumer {
        return PricesConsumer(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            pricesDataSource = pricesDataSource,
            objectMapper = objectMapper,
            reactiveRedisOperations = reactiveRedisOperations,
            pricesStreamService = pricesStreamService
        )
    }

    @Bean
    open fun pricesService(
        pricesDataSource: IPricesDataSource
    ): PricesService {
        return PricesService(
            pricesDataSource = pricesDataSource
        )
    }

    @Bean
    open fun pricesStreamService(): PricesStreamService {
        return PricesStreamService()
    }
}