@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "SpringFacetCodeInspection")

package org.simtp.market.data.securities.backend.info

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(SecurityInfoConfiguration::class)
annotation class EnableSecurityInfo

@Configuration
open class SecurityInfoConfiguration {
    @Bean
    open fun securitiesConsumer(
        reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        objectMapper: ObjectMapper
    ): SecuritiesConsumer {
        return SecuritiesConsumer(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            objectMapper = objectMapper
        )
    }
}