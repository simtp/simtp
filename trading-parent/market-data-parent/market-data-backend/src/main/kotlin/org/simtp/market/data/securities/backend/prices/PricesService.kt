package org.simtp.market.data.securities.backend.prices

import org.simtp.market.data.securities.api.IPricesApi
import org.simtp.market.data.securities.api.IPricesDataSource
import org.simtp.market.data.securities.api.Price
import reactor.core.publisher.Mono

class PricesService(
    private val pricesDataSource: IPricesDataSource
): IPricesApi {

    override fun getPrice(symbol: String): Mono<Price> {
        return pricesDataSource.findBySymbol(symbol = symbol)
    }
}