package org.simtp.market.data.securities.backend.prices

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.market.data.securities.datasource.memory.EnableMemoryMarketDataDataSource
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integration")
class PricesConfigurationTest {

    @Test
    fun contextLoads() {

    }

}

@SpringBootApplication
@EnableSecurityPrices
@EnableMemoryMarketDataDataSource
open class PricesConfigurationTestApp
