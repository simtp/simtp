package org.simtp.market.data.mock.provider.securities

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.simtp.market.data.securities.api.Price
import reactor.test.StepVerifier
import java.math.BigDecimal
import java.time.Duration

class MockSecuritiesConfigurationKtTest {

    private val pricesMap = prices.associateBy { it.symbol }


    @ParameterizedTest
    @ValueSource(strings = ["0.1", "-0.1"])
    fun testRandomPriceProducerFunction(incrementValue: String) {
        val increment = BigDecimal(incrementValue)
        val starter = pricesMap["BHP"]!!

        StepVerifier
            .create(randomPriceProducerFunction(10, { pricesMap["BHP"]!! },  { increment }))
            .expectNextMatches {
                val last = starter.last!! + increment
                it == Price(
                    symbol = "BHP",
                    open = starter.open,
                    close = starter.close,
                    high = starter.high!!.max(last),
                    low = starter.low!!.min(last),
                    bid = starter.bid!! + increment,
                    ask = starter.ask!! + increment,
                    last = last
                )
            }
            .expectNextMatches {
                val last = starter.last!! + increment
                it == Price(
                    symbol = "BHP",
                    open = starter.open,
                    close = starter.close,
                    high = starter.high!!.max(last),
                    low = starter.low!!.min(last),
                    bid = starter.bid!! + increment,
                    ask = starter.ask!! + increment,
                    last = last
                )
            }
            .thenCancel()
            .verify(Duration.ofSeconds(30))
    }
}