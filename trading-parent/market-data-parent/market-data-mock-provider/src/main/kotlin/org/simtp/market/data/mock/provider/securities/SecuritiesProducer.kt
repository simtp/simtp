package org.simtp.market.data.mock.provider.securities

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.MarketStreamNames
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveStreamOperations
import reactor.core.publisher.Flux




class SecuritiesProducer(
    private val reactiveStreamOperations: ReactiveStreamOperations<String, String, String>,
    private val objectMapper: ObjectMapper
): ApplicationListener<ApplicationReadyEvent> {


    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        Flux
            .fromIterable(instruments)
            .map {
                StreamRecords
                    .newRecord()
                    .ofMap(mapOf(
                        "event-type" to "CREATE",
                        "json" to objectMapper.writeValueAsString(it)
                    ))
                    .withStreamKey(MarketStreamNames.MARKET_SECURITIES)

            }.flatMap {
                reactiveStreamOperations.add(it)
            }
            .subscribe()

    }
}

