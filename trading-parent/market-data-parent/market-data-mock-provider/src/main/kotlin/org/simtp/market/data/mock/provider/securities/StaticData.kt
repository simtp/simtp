package org.simtp.market.data.mock.provider.securities

import org.simtp.market.data.securities.api.Price
import org.simtp.market.data.securities.api.Instrument
import java.math.BigDecimal

internal val instruments = listOf(
    Instrument("ANZ", "Australia and New Zealand Bank"),
    Instrument("BHP", "BHP Billiton"),
    Instrument("BCA", "BCA Limited"),
    Instrument("CBA", "Commonwealth Bank of Australia"),
    Instrument("TLS", "Telstra Limited"),
    Instrument("NAB", "National Australia Bank"),
    Instrument("WEA", "WEA Limited"),
    Instrument("WES", "Westfarmers"),
    Instrument("WOW", "Woolworths")
)

internal val prices = listOf(
    createPrice("ANZ"),
    createPrice("BHP"),
    createPrice("BCA"),
    createPrice("CBA"),
    createPrice("TLS"),
    createPrice("NAB"),
    createPrice("WEA"),
    createPrice("WES"),
    createPrice("WOW")
)

fun createPrice(securityCode: String): Price {
    return Price(
        symbol = securityCode,
        open = BigDecimal("4.95"),
        close = BigDecimal("4.90"),
        high = BigDecimal("4.96"),
        low = BigDecimal("4.98"),
        bid = BigDecimal("4.90"),
        ask = BigDecimal("5.10"),
        last = BigDecimal("5.00")
    )
}