@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "SpringFacetCodeInspection")

package org.simtp.market.data.mock.provider.securities

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.Price
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.core.ReactiveStreamOperations
import reactor.core.publisher.Flux
import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext
import java.time.Duration
import java.util.*
import kotlin.math.log10
import kotlin.math.max


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(MockSecuritiesConfiguration::class)
annotation class EnableMockSecuritiesProvider

@Configuration
open class MockSecuritiesConfiguration {

    @Bean
    open fun securitiesProducer(
        reactiveStreamOperations: ReactiveStreamOperations<String, String, String>,
        objectMapper: ObjectMapper
    ): SecuritiesProducer {
        return SecuritiesProducer(
            reactiveStreamOperations = reactiveStreamOperations,
            objectMapper = objectMapper
        )
    }

    private val mutablePrices = prices.associateBy { it.symbol }.toMutableMap()

    @Bean
    open fun pricesProvider(): () -> Flux<Price> {
        return { randomPriceProducerFunction(
            5000,
            { mutablePrices.map { it.key to it.value }.random().second },
            {
                between(BigDecimal("-0.1"), BigDecimal("0.1"))
            })
            .doOnNext { mutablePrices[it.symbol] = it }
        }
    }

    @Bean
    open fun pricesProducer(
        reactiveStreamOperations: ReactiveStreamOperations<String, String, String>,
        objectMapper: ObjectMapper,
        pricesProducerFunction: () -> Flux<Price>
    ): PricesProducer {
        return PricesProducer(
            reactiveStreamOperations = reactiveStreamOperations,
            objectMapper = objectMapper,
            producerFunction = pricesProducerFunction
        )
    }
}




val randomPriceProducerFunction: (
    intervalInMillis: Long,
    existingPriceProvider: () -> Price,
    deviationProvider: (Price) -> BigDecimal
) -> Flux<Price> = { intervalInMillis, existingPriceProvider, deviationProvider ->
    Flux
        .interval(Duration.ofMillis(intervalInMillis))
        .map {
            val existingPrice = existingPriceProvider()
            val deviation = deviationProvider(existingPrice)
            val last = existingPrice.last!! + deviation
            Price(
                symbol = existingPrice.symbol,
                open = existingPrice.open,
                close = existingPrice.close,
                high = existingPrice.high!!.max(last),
                low = existingPrice.low!!.min(last),
                bid = existingPrice.bid!! + deviation,
                ask = existingPrice.ask!! + deviation,
                last = last
            )
        }
}

// The short version
fun between(min: BigDecimal, max: BigDecimal): BigDecimal {
    val digitCount = max(min.precision().toDouble(), max.precision().toDouble()).toInt()
    val bitCount = (digitCount / log10(2.0)).toInt()

    // convert Random BigInteger to a BigDecimal between 0 and 1
    val alpha: BigDecimal = BigDecimal(
        BigInteger(bitCount, Random())
    ).movePointLeft(digitCount)

    return min.add(max.subtract(min).multiply(alpha, MathContext(digitCount)))
}