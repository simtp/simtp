package org.simtp.market.data.mock.provider.securities

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.MarketStreamNames
import org.simtp.market.data.securities.api.Price
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveStreamOperations
import reactor.core.publisher.Flux


class PricesProducer(
    private val reactiveStreamOperations: ReactiveStreamOperations<String, String, String>,
    private val objectMapper: ObjectMapper,
    private val producerFunction: () -> Flux<Price>
): ApplicationListener<ApplicationReadyEvent> {


    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        producerFunction()
            .map {
                StreamRecords
                    .newRecord()
                    .ofMap(mapOf(
                        "event-type" to "CREATE",
                        "json" to objectMapper.writeValueAsString(it)
                    ))
                    .withStreamKey(MarketStreamNames.MARKET_PRICES)

            }.flatMap {
                reactiveStreamOperations.add(it)
            }
            .subscribe()

    }
}