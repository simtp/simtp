package org.simtp.market.data.securities.api

import org.simtp.common.dto.MarketSide
import org.simtp.common.dto.OrderStatus

import java.io.Serializable
import java.math.BigDecimal
import java.util.Date

/**
 */
data class MarketOrder(val marketReference: String?,
                       val simtpReference: String?,
                       val securityCode: String?,
                       val quantity: Long?,
                       val marketSide: MarketSide?,
                       val orderStatus: OrderStatus? = null,
                       val price: BigDecimal? = null,
                       val expiryDate: Date? = null
) : Serializable, Comparable<MarketOrder> {

    override fun compareTo(other: MarketOrder): Int {
        return other.price!!.compareTo(price!!)
    }

}
