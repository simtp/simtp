package org.simtp.market.data.securities.api

import java.io.Serializable
import java.math.BigDecimal
import java.time.ZonedDateTime

/**
 */
data class Trade(val id: String?,
                 val symbol: Instrument?,
                 val price: BigDecimal?,
                 val buyerReference: String?,
                 val sellerReference: String?,
                 val tradeDate: ZonedDateTime?) : Serializable
