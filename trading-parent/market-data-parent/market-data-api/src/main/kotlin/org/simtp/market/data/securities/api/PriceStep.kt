package org.simtp.market.data.securities.api

import java.math.BigDecimal

data class PriceStep(
    var id: Long? = null,
    var securityCode: String? = null,
    var exchange: String? = null,
    var low: BigDecimal? = null,
    var high: BigDecimal? = null,
    var step: BigDecimal? = null
)