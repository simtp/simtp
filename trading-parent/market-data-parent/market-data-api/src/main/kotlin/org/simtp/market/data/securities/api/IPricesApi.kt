package org.simtp.market.data.securities.api

import reactor.core.publisher.Mono

interface IPricesApi {

    fun getPrice(symbol: String): Mono<Price>

}

interface IPricesDataSource {

    fun save(price: Price): Mono<Price>

    fun findBySymbol(symbol: String): Mono<Price>
}


