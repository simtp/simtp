package org.simtp.market.data.securities.api

/**
 * User: Grant Little <grant.little></grant.little>@inventivesoftware.com.au>
 */
interface MarketCacheNames {
    companion object {

        val SECURITY_CODES = "org.simtp.market.security-codes"
        val DEPTH = "org.simtp.market.depth"
        val PRICES = "org.simtp.market.prices"
        val ORDERS = "org.simtp.market.orders"
        val TRADES = "org.simtp.market.trades"
    }

}
