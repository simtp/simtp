package org.simtp.market.data.securities.api

import java.io.Serializable

/**
 */
data class Instrument(val symbol: String?,
                      val description: String?) : Serializable