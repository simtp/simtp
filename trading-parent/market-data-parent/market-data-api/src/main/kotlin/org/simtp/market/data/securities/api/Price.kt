package org.simtp.market.data.securities.api

import java.math.BigDecimal

/**
 */
data class Price(
    var symbol: String? = null,
    var open: BigDecimal? = null,
    var close: BigDecimal? = null,
    var high: BigDecimal? = null,
    var low: BigDecimal? = null,
    var bid: BigDecimal? = null,
    var ask: BigDecimal? = null,
    var last: BigDecimal? = null
)
