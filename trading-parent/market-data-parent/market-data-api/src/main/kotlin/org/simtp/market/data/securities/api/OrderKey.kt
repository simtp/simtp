package org.simtp.market.data.securities.api

import java.io.Serializable

/**
 */
data class OrderKey(val instrument: Instrument?,
                    val id: String?) : Serializable