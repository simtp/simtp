package org.simtp.market.data.securities.api

/**
 * User: grant
 */
interface MarketStreamNames {
    companion object {
        const val MARKET_SECURITIES = "stream.org.simtp.market.data.securities"
        const val MARKET_PRICES = "stream.org.simtp.market.data.prices"
    }

}
