package org.simtp.market.data.securities.api

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

/**
 * Test class for PriceStep data class.
 * It tests the correctness of the initialization of its instances.
 */
class PriceStepTest {

    /**
     * Test case that verifies the correctness of initializing the PriceStep
     * instance with specific values for each field.
     */
    @Test
    fun `Should correctly initialize PriceStep instance with specific values`() {
        val priceStep = PriceStep(1, "APPL", "NYSE", BigDecimal.valueOf(100), BigDecimal.valueOf(200), BigDecimal.ONE)

        assertEquals(1, priceStep.id, "The id must be correctly initialized")
        assertEquals("APPL", priceStep.securityCode, "The security code must be correctly initialized")
        assertEquals("NYSE", priceStep.exchange, "The exchange must be correctly initialized")
        assertEquals(BigDecimal.valueOf(100), priceStep.low, "The low price must be correctly initialized")
        assertEquals(BigDecimal.valueOf(200), priceStep.high, "The high price must be correctly initialized")
        assertEquals(BigDecimal.ONE, priceStep.step, "The step price must be correctly initialized")
    }

    /**
     * Test case that verifies the correctness of initializing the PriceStep
     * instance with null values for each field.
     */
    @Test
    fun `Should correctly initialize PriceStep instance with null values`() {
        val priceStep = PriceStep()

        assertEquals(null, priceStep.id, "The id must be null")
        assertEquals(null, priceStep.securityCode, "The security code must be null")
        assertEquals(null, priceStep.exchange, "The exchange must be null")
        assertEquals(null, priceStep.low, "The low price must be null")
        assertEquals(null, priceStep.high, "The high price must be null")
        assertEquals(null, priceStep.step, "The step price must be null")
    }
}