package org.simtp.market.data.securities.api

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class PriceTest {

    @Test
    fun ensureSymbolIsSetCorrectly() {
        val price = Price(symbol = "Test")
        assertEquals("Test", price.symbol)
    }

    @Test
    fun ensureOpenPriceIsSetCorrectly() {
        val price = Price(open = BigDecimal.TEN)
        assertEquals(BigDecimal.TEN, price.open)
    }

    @Test
    fun ensureClosePriceIsSetCorrectly() {
        val price = Price(close = BigDecimal.ONE)
        assertEquals(BigDecimal.ONE, price.close)
    }

    @Test
    fun ensureHighPriceIsSetCorrectly() {
        val price = Price(high = BigDecimal.valueOf(100))
        assertEquals(BigDecimal.valueOf(100), price.high)
    }

    @Test
    fun ensureLowPriceIsSetCorrectly() {
        val price = Price(low = BigDecimal.valueOf(50))
        assertEquals(BigDecimal.valueOf(50), price.low)
    }

    @Test
    fun ensureBidPriceIsSetCorrectly() {
        val price = Price(bid = BigDecimal.valueOf(150))
        assertEquals(BigDecimal.valueOf(150), price.bid)
    }

    @Test
    fun ensureAskPriceIsSetCorrectly() {
        val price = Price(ask = BigDecimal.valueOf(200))
        assertEquals(BigDecimal.valueOf(200), price.ask)
    }

    @Test
    fun ensureLastPriceIsSetCorrectly() {
        val price = Price(last = BigDecimal.valueOf(300))
        assertEquals(BigDecimal.valueOf(300), price.last)
    }
}