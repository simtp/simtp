package org.simtp.market.data.securities.api

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class InstrumentTest {

    /**
     * Test class for [Instrument]
     * Tests for the correct initialization of an instance of [Instrument]
     */
    @Test
    fun testInitializeInstrument() {

        val symbol = "AAPL"
        val description = "Apple Inc."

        val instrument = Instrument(symbol, description)

        assertEquals(
            symbol,
            instrument.symbol,
            "Expected and actual values of symbol should be the same"
        )

        assertEquals(
            description,
            instrument.description,
            "Expected and actual values of description should be the same"
        )
    }

    /**
     * Test for null values in [Instrument]
     * This test asserts that when null values are used during instantiation,
     * the resulting instance of [Instrument] should have null values.
     */
    @Test
    fun testNullValuesInInstrument() {

        val instrument = Instrument(null, null)

        assertNull(
            instrument.symbol,
            "Expected value of symbol should be null"
        )

        assertNull(
            instrument.description,
            "Expected value of description should be null"
        )
    }
}