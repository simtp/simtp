package org.simtp.market.data.securities.datasource.redis.prices

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.core.ReactiveRedisOperations

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(PricesRedisDataSourceConfiguration::class)
annotation class EnableRedisPricesDataSource

@Configuration
@ComponentScan
open class PricesRedisDataSourceConfiguration {

    @Bean
    open fun pricesRedisDataSource(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    ): PricesRedisDataSource {
        return PricesRedisDataSource(
            reactiveRedisOperations = reactiveRedisOperations
        )
    }
}