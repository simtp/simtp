package org.simtp.market.data.securities.datasource.redis.prices

import org.simtp.market.data.securities.api.IPricesDataSource
import org.simtp.market.data.securities.api.Price
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Mono
import java.math.BigDecimal

class PricesRedisDataSource(
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>
): IPricesDataSource {

    override fun save(price: Price): Mono<Price> {
        return reactiveRedisOperations
            .opsForHash<String, String>()
            .putAll(
                "securities:${price.symbol}",
                mapOf(
                    "open" to price.open?.toString(),
                    "close" to price.close?.toString(),
                    "high" to price.high?.toString(),
                    "low" to price.low?.toString(),
                    "bid" to price.bid?.toString(),
                    "ask" to price.ask?.toString(),
                    "last" to price.last?.toString(),
                )
            )
            .flatMap {
                when (it) {
                    true -> Mono.just(price)
                    false -> Mono.empty()
                }
            }
    }

    override fun findBySymbol(symbol: String): Mono<Price> {
        return reactiveRedisOperations
            .opsForHash<String, String>()
            .multiGet("securities:$symbol", listOf("open", "close", "high", "low", "bid", "ask", "last"))
            .map {fields ->
                Price(
                    symbol = symbol,
                    open = fields[0]?.toString()?.let { BigDecimal(it) },
                    close = fields[1]?.toString()?.let { BigDecimal(it) },
                    high = fields[2]?.toString()?.let { BigDecimal(it) },
                    low = fields[3]?.toString()?.let { BigDecimal(it) },
                    bid = fields[4]?.toString()?.let { BigDecimal(it) },
                    ask = fields[5]?.toString()?.let { BigDecimal(it) },
                    last = fields[6]?.toString()?.let { BigDecimal(it) }
                )
            }
    }

}