@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "SpringFacetCodeInspection")

package org.simtp.market.data.securities.datasource.redis

import org.simtp.market.data.securities.datasource.redis.prices.EnableRedisPricesDataSource


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@EnableRedisPricesDataSource
annotation class EnableRedisMarketDataDataSource
