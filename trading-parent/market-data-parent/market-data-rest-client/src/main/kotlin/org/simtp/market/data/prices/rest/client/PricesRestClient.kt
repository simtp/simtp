package org.simtp.market.data.prices.rest.client

import org.simtp.market.data.securities.api.IPricesApi
import org.simtp.market.data.securities.api.Price
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

const val PRICES_CONFIG_PREFIX = "simtp.prices.rest.client"

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(PricesRestClientConfiguration::class)
annotation class EnablePricesRestClient

@Configuration
open class PricesRestClientConfiguration {

    @Bean
    @ConfigurationProperties(prefix = PRICES_CONFIG_PREFIX)
    open fun pricesRestClientSettings(): PricesRestClientSettings {
        return PricesRestClientSettings()
    }

    @Bean
    open fun pricesRestClient(
        pricesRestClientSettings: PricesRestClientSettings
    ): PricesRestClient {
        return PricesRestClient(
            settings = pricesRestClientSettings
        )
    }
}

const val CONTEXT_PATH = "/api/prices/1.0"
data class PricesRestClientSettings(
    var baseUrl: String = "http://localhost:8080",
)
class PricesRestClient(
    settings: PricesRestClientSettings
): IPricesApi {

    private val webClient = WebClient.create(settings.baseUrl)

    override fun getPrice(symbol: String): Mono<Price> {
        return webClient
            .get()
            .uri("$CONTEXT_PATH/{securityCode}", mapOf("securityCode" to symbol))
            .retrieve()
            .bodyToMono(Price::class.java)
    }

}