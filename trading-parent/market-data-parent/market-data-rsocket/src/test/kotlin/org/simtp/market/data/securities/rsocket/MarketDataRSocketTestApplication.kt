package org.simtp.market.data.securities.rsocket

import com.fasterxml.jackson.databind.ObjectMapper
import io.rsocket.metadata.WellKnownMimeType
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.common.serialization.withDefaults
import org.simtp.market.data.securities.api.MarketStreamNames
import org.simtp.market.data.securities.api.Price
import org.simtp.market.data.securities.rsocket.prices.PricesQueueManager
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.data.redis.connection.stream.RecordId
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveStreamOperations
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.RSocketStrategies
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.rsocket.RSocketSecurity
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService
import org.springframework.security.core.userdetails.User
import org.springframework.security.messaging.handler.invocation.reactive.AuthenticationPrincipalArgumentResolver
import org.springframework.security.rsocket.core.PayloadSocketAcceptorInterceptor
import org.springframework.security.rsocket.metadata.SimpleAuthenticationEncoder
import org.springframework.security.rsocket.metadata.UsernamePasswordMetadata
import org.springframework.util.MimeTypeUtils
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.math.BigDecimal
import java.net.URI
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference
import kotlin.test.assertEquals

@SpringBootTest
@Testcontainers
@EnableRedisStream
@Tag("integration")
open class MarketDataRSocketTest {

    companion object {
        @ServiceConnection
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)
                .withNetwork(Network.SHARED)

        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
        }
        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
        }
    }
    @Autowired
    private lateinit var rSocketRequester: RSocketRequester

    @Test
    fun contextLoads() {
        assertNotNull(rSocketRequester)
    }

    @Autowired
    private lateinit var reactiveStreamOperations: ReactiveStreamOperations<String, String, String>

    @Autowired
    private lateinit var pricesQueueManager: PricesQueueManager



    @Test
    fun receivesPrices() {

        pricesQueueManager.queueFor("TEST")
        val objectMapper = ObjectMapper().withDefaults()
        val price = Price(
            symbol = "TEST",
            open = BigDecimal.ONE
        )

        val otherPrice = Price(
            symbol = "OTHER",
            open = BigDecimal.ONE
        )

        val latch = CountDownLatch(2)
        val received = mutableListOf<Price>()



        rSocketRequester
            .route("prices.TEST")
            .retrieveFlux(Price::class.java)
            .doOnNext {
                received.add(it)
                latch.countDown()
            }
            .subscribe { println("Subd")}


        val function: (Price) -> Mono<RecordId> = {
            reactiveStreamOperations
                .add(
                    StreamRecords
                        .newRecord()
                        .ofMap(
                            mapOf(
                                "event-type" to "CREATE",
                                "json" to objectMapper.writeValueAsString(it)
                            )
                        )
                        .withStreamKey(MarketStreamNames.MARKET_PRICES)
                )
        }

        StepVerifier
            .create(function(price))
            .expectNextMatches { it is RecordId }
            .verifyComplete()

        StepVerifier
            .create(function(price))
            .expectNextMatches { it is RecordId }
            .verifyComplete()

        //Using otherPrice, different symbol, therefore should not be received.
        StepVerifier
            .create(function(otherPrice))
            .expectNextMatches { it is RecordId }
            .verifyComplete()

        assertTrue(latch.await(5, TimeUnit.SECONDS))
        assertEquals(2, received.size)


    }

}

@SpringBootApplication
open class MarketDataRSocketTestApplication {
//    companion object {
//        private val log = LoggerFactory.getLogger(WebMessageApplication::class.java)
//    }

    @Bean
    open fun rSocketRequester(builder: RSocketRequester.Builder): RSocketRequester {
        return builder
            .rsocketStrategies{ b -> b.encoder(SimpleAuthenticationEncoder()) }
            .setupMetadata(UsernamePasswordMetadata("jay", "pw"), MimeTypeUtils.parseMimeType(WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION.string))
            .websocket(URI.create("ws://localhost:7000"))
    }

    @Bean
    open fun rSocketMessageHandler(strategies: RSocketStrategies): RSocketMessageHandler {
        val handler = RSocketMessageHandler()
        handler.argumentResolverConfigurer
            .addCustomResolver(AuthenticationPrincipalArgumentResolver())
        handler.rSocketStrategies = strategies
        return handler
    }

    @Bean
    open fun atomicReference(): AtomicReference<CountDownLatch> {
        return AtomicReference(CountDownLatch(1))
    }

    @Bean
    open fun eventsAtomicReference(): AtomicReference<CountDownLatch> {
        return eventsAtomicReference
    }

    private val eventsAtomicReference = AtomicReference(CountDownLatch(1))

    @Bean
    @Primary
    open fun priceSocketMessageHandler(atomicReference: AtomicReference<CountDownLatch>): (Price) -> Unit {
        return { atomicReference().get().countDown() }
    }

    @Bean
    open fun authentication(): MapReactiveUserDetailsService {
        return MapReactiveUserDetailsService(
            User.builder()
                .username("jay")
                .password("{noop}pw")
                .roles("ADMIN", "USER")
                .build()
        )
    }

    @Bean
    open fun authorization(security: RSocketSecurity): PayloadSocketAcceptorInterceptor {
        return security
            .authorizePayload { authorize ->
                authorize.route("auth").authenticated()
                    .anyRequest().permitAll()
                    .anyExchange().permitAll()
            }
            .simpleAuthentication(Customizer.withDefaults())
            .build()
    }



}