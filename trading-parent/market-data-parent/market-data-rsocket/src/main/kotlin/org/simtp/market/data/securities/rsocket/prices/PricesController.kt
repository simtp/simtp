package org.simtp.market.data.securities.rsocket.prices

import org.simtp.market.data.securities.api.Price
import org.springframework.messaging.handler.annotation.DestinationVariable
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.stereotype.Controller
import reactor.core.publisher.Flux

@Controller
class PricesController(
    private val pricesQueueManager: PricesQueueManager
) {

//    companion object {
//        private val log = LoggerFactory.getLogger(PricesController::class.java)
//    }

    @MessageMapping("prices.{symbol}")
    fun pricesForSymbolHandler(
        @DestinationVariable symbol: String
    ): Flux<Price> {
        return Flux.fromIterable(
            pricesQueueManager.queueFor(symbol)
        )
    }

}