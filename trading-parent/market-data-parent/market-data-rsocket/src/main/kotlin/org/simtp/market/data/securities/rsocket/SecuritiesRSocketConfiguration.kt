@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "SpringFacetCodeInspection")

package org.simtp.market.data.securities.rsocket

import org.simtp.market.data.securities.rsocket.prices.EnableRSocketSecurityPrices


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
//@EnableSecurityInfo
@EnableRSocketSecurityPrices
annotation class EnableRSocketSecuritiesBackend
