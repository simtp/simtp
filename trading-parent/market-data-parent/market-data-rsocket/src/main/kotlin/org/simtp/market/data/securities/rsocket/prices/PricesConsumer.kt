package org.simtp.market.data.securities.rsocket.prices

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.MarketStreamNames
import org.simtp.market.data.securities.api.Price
import org.simtp.stream.redis.RedisStream.Companion.createGroupAndReceiverAutoAck
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations

open class PricesRSocketConsumer(
    private val reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val pricesQueueManager: PricesQueueManager,
    private val objectMapper: ObjectMapper
): ApplicationListener<ApplicationReadyEvent> {

    override fun onApplicationEvent(event: ApplicationReadyEvent) {

        createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            streamName = MarketStreamNames.MARKET_PRICES,
            groupName = "PricesRSocketConsumer",
            consumerName = "PricesRSocketConsumer",
            reactiveRedisOperations = reactiveRedisOperations
        )
        .mapNotNull { mapRecord ->
            objectMapper
                .readValue(mapRecord.value["json"], Price::class.java)
        }
        .doOnNext {
            pricesQueueManager.addPriceForSymbol(it.symbol!!, it)
        }
        .subscribe()
    }
}