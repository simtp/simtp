package org.simtp.market.data.securities.rsocket.prices

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.market.data.securities.api.Price
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import java.util.*

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(PricesRSocketConfiguration::class)
annotation class EnableRSocketSecurityPrices

@Configuration
@ComponentScan
open class PricesRSocketConfiguration {

    @Bean
    open fun pricesConsumer(
        reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        pricesQueueManager: PricesQueueManager,
        objectMapper: ObjectMapper
    ): PricesRSocketConsumer {
        return PricesRSocketConsumer(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            pricesQueueManager = pricesQueueManager,
            objectMapper = objectMapper,
            reactiveRedisOperations = reactiveRedisOperations
        )
    }

    @Bean
    open fun pricesQueueManager(): PricesQueueManager {
        return PricesQueueManager()
    }

}

open class PricesQueueManager(
    private val queues: MutableMap<String, LinkedList<Price>> = mutableMapOf()
) {

    fun queueFor(symbol: String): Queue<Price> {
        return queues.computeIfAbsent(symbol) { LinkedList<Price>() }
    }

    fun addPriceForSymbol(symbol: String, price: Price) {
        queues[symbol]
            ?.add(price)
    }
}

