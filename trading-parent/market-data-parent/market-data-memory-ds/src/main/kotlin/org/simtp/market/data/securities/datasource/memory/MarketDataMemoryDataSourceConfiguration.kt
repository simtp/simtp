@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "SpringFacetCodeInspection")

package org.simtp.market.data.securities.datasource.memory

import org.simtp.market.data.securities.datasource.memory.prices.EnableMemoryPricesDataSource


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@EnableMemoryPricesDataSource
annotation class EnableMemoryMarketDataDataSource
