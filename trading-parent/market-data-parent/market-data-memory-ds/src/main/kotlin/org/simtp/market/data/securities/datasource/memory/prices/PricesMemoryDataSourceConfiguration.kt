package org.simtp.market.data.securities.datasource.memory.prices

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(PricesMemoryDataSourceConfiguration::class)
annotation class EnableMemoryPricesDataSource

@Configuration
@ComponentScan
open class PricesMemoryDataSourceConfiguration {

    @Bean
    open fun pricesRedisDataSource(): PricesMemoryDataSource {
        return PricesMemoryDataSource()
    }
}