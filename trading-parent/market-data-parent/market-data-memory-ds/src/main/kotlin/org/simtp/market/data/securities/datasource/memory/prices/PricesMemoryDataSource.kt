package org.simtp.market.data.securities.datasource.memory.prices

import org.simtp.market.data.securities.api.IPricesDataSource
import org.simtp.market.data.securities.api.Price
import reactor.core.publisher.Mono

class PricesMemoryDataSource: IPricesDataSource {

    private val map : MutableMap<String, Price> = mutableMapOf()

    override fun save(price: Price): Mono<Price> {
        return price
            .symbol
            ?.also { map[it] = price }
            ?.let { Mono.just(price) }
            ?: Mono.empty()
    }

    override fun findBySymbol(symbol: String): Mono<Price> {
        return map[symbol]?.let { Mono.just(it) } ?: Mono.empty()
    }

}