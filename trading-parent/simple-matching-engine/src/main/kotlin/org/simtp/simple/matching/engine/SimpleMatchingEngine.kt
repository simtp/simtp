package org.simtp.simple.matching.engine

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.common.dto.MarketSide
import org.simtp.order.lifecycle.api.ORDER_LIFECYCLE_STREAM_NAME
import org.simtp.order.lifecycle.api.Order
import org.simtp.stream.api.StreamMessageType
import org.simtp.stream.redis.RedisStream.Companion.createGroupAndReceiverAutoAck
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations

/**
 */
@Target(AnnotationTarget.CLASS)
@Import(SimpleMatchingEngineConfiguration::class)
annotation class EnableSimpleMatchingEngine

@Configuration
open class SimpleMatchingEngineConfiguration {

    @Bean
    open fun simpleMatchingEngineEventHandler(
        objectMapper: ObjectMapper,
        reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>
    ): SimpleMatchingEngineEventHandler {
        return SimpleMatchingEngineEventHandler(
            objectMapper = objectMapper,
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations
        )
    }

}

data class Depth(
    val buyOrders: List<Order> = emptyList(),
    val sellOrders: List<Order> = emptyList()
)


class SimpleMatchingEngineEventHandler(
    private val objectMapper: ObjectMapper,
    private val reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>
): ApplicationListener<ApplicationReadyEvent> {

    private var marketPicture = mapOf<String, Depth>()

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            streamName = ORDER_LIFECYCLE_STREAM_NAME,
            groupName = "MatchingEngine",
            consumerName = "MatchingEngine",
            reactiveRedisOperations = reactiveRedisOperations
        )
        .map { record ->
            val message = record.value["json"]
            when (record.value["type"]) {
                StreamMessageType.CREATE.name -> {
                    objectMapper
                        .readValue(message, Order::class.java)!!
                        .also {
                            marketPicture = addOrderToDepth(it)
                        }
                }
                StreamMessageType.UPDATE.name -> {
                    TODO("Implement order matching for update orders")
                }
                else -> {
                    IllegalStateException("Unknown Exception type")
                }
            }
        }
        .subscribe()
    }

    private fun addOrderToDepth(order: Order): Map<String, Depth> {
        val symbol = order.symbol!!
        val side = order.marketSide!!
        val depth = marketPicture[symbol]
        val newDepth = when (side) {
            MarketSide.BUY -> {
                when (depth == null) {
                    true -> {
                        Depth(
                            buyOrders = listOf(order)
                        )
                    }
                    false -> Depth(
                        buyOrders = (depth.buyOrders + listOf(order)).sortedByDescending { it.limitPrice },
                        sellOrders = depth.sellOrders
                    )
                }
            }
            MarketSide.SELL -> {
                when (depth == null) {
                    true -> {
                        Depth(
                            buyOrders = listOf(order)
                        )
                    }
                    false -> Depth(
                        sellOrders = (depth.sellOrders + listOf(order)).sortedBy { it.limitPrice },
                        buyOrders = depth.buyOrders
                    )
                }
            }
        }
        return marketPicture
            .filterKeys { it != symbol }
            .map { it.key to it.value }
            .let {
                it + listOf(symbol to newDepth)
            }
            .toMap()
    }

}


//fun Config.configureMatchingEngine( getEventBus : () -> EventBus ) {
//    val listener = HazelcastLambdaEntryListener<String, Order>(
//            entryAddedHandler = { hzInstance, event ->
//                findOpposingOrders (
//                    event = { event.value },
//                    localOrders = {
//                        val map = hzInstance.getMap<String, Order>("orders")
//                        map.getAll(map.localKeySet()).values.toList()
//                    }
//                )
//                .createTrades(event.value)
//                .distribute(getEventBus())
//
//            }
//    )
//    val entryListenerConfig = EntryListenerConfig()
//    entryListenerConfig.setImplementation(listener)
//    this.getMapConfig("orders")
//            .addEntryListenerConfig(entryListenerConfig)
//}
//
//
//fun List<Trade>.distribute(eventBus : EventBus) {
//    this.forEach {
//        eventBus.send("trades", it)
//    }
//}
//
//fun List<Order>.createTrades(order : Order) : List<Trade> {
//
//    val trades = mutableListOf<Trade>()
//    var remaining = order.quantity
//
//    for (opposing in this) {
//        if (remaining > 0) {
//            if (remaining <= opposing.quantity) {
//                trades.addTrade(order, opposing)
//                remaining = 0
//            } else {
//                trades.addTrade(order, opposing)
//                remaining -= opposing.quantity
//            }
//        } else {
//            break
//        }
//    }
//    return trades
//}
//
//fun MutableList<Trade>.addTrade(order: Order, opposing: Order) {
//    this.add(Trade(
//            id = UUID.randomUUID().toString(),
//            bidReference = reference(order, opposing, MarketSide.BID),
//            askReference = reference(order, opposing, MarketSide.ASK),
//            symbol = order.symbol,
//            quantity = order.quantity
//    ))
//
//}
//
//fun reference(order : Order,
//             opposing : Order,
//             marketSide: MarketSide) : String {
//    return when (marketSide) {
//        order.marketSide -> order.reference
//        opposing.marketSide -> opposing.reference
//        else -> throw IllegalStateException("Neither side matches expected market side")
//    }
//}
//
//fun findOpposingOrders(event : () -> Order,
//                       localOrders : () -> List<Order>) : List<Order> {
//    val order = event()
//    val opposingSide = order.opposingSide()
//
//    return localOrders()
//            .filter { it.symbol == order.symbol  }
//            .filter { it.marketSide == opposingSide }
//            .filter { it.limitPrice == order.limitPrice }
//
//}
//
//fun Order.opposingSide() : MarketSide {
//    return when(this.marketSide) {
//        MarketSide.BID -> MarketSide.ASK
//        MarketSide.ASK -> MarketSide.BID
//    }
//}
//
