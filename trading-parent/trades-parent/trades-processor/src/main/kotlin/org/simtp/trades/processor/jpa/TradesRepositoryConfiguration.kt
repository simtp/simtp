package org.simtp.trades.processor.jpa

import org.simtp.common.IHasTags
import org.simtp.trades.processor.transfer.Trade
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.ZonedDateTime
import java.util.*

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(TradesRepositoryConfiguration::class)
annotation class EnableRedisTrades
/**
 */
@Configuration
@EnableRedisRepositories
@ComponentScan
open class TradesRepositoryConfiguration

@Repository
interface TradesRepository : CrudRepository<TradeEntity, String> {
//    @Query("select t.id from TradeEntity t")
    fun findKeys() : Iterable<String>

//    @Query("select t from TradeEntity t where t.id in :keys")
    fun findByKeys(@Param("keys") keys : Collection<String>) : Iterable<TradeEntity>

}

@RedisHash("trades")
open class TradeEntity(
    @Id
    open var id: String? = UUID.randomUUID().toString(),

    open var bidReference : String? = null,
    open var askReference : String? = null,
    open var symbol : String? = null,
    open var quantity : Int? = null,
    open var timestamp: ZonedDateTime? = ZonedDateTime.now(),

    override var tags: Map<String, String>? = null

): IHasTags

fun TradeEntity.toDto() : Trade {
    return Trade(
        id = this.id,
        bidReference = this.bidReference,
        askReference = this.askReference,
        symbol = this.symbol,
        quantity = this.quantity,
        timestamp = this.timestamp,
        tags = this.tags
    )
}

fun Trade.toEntity() : TradeEntity {
    return TradeEntity(
        id = this.id,
        bidReference = this.bidReference,
        askReference = this.askReference,
        symbol = this.symbol,
        quantity = this.quantity,
        timestamp = this.timestamp,
        tags = this.tags
    )
}
