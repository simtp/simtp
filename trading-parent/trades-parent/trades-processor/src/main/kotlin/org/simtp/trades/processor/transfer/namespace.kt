package org.simtp.trades.processor.transfer

import org.simtp.common.IHasTags
import java.io.Serializable
import java.time.ZonedDateTime
import java.util.*

data class Trade(val id: String? = UUID.randomUUID().toString(),
                 val bidReference : String? = null,
                 val askReference : String? = null,
                 val symbol : String? = null,
                 val quantity : Int? = null,
                 val timestamp: ZonedDateTime? = ZonedDateTime.now(),
                 override var tags : Map<String, String>? = null) : Serializable, IHasTags {

}