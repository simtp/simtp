@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")

package org.simtp.trades.processor

import org.simtp.trades.processor.jpa.EnableRedisTrades
import org.simtp.trades.processor.jpa.TradesRepository
import org.springframework.context.annotation.Configuration

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class EnableTradesProcessor


const val TRADES_MAP_NAME = "trades"
@Configuration
@EnableRedisTrades
open class TradesProcessorPreIgnitionConfiguration(private val tradesRepository: TradesRepository) {

}
//
//@Configuration
//open class TradesIgnitePostIgnitionConfiguration(private val ignite: Ignite,
//                                                 private val eventBus: IEventBus) {
//
//    @Bean
//    open fun tradesCache() : Cache<String, Trade> {
//        return ignite.cache<String, Trade>(TRADES_MAP_NAME)
//    }
//
//    @Bean
//    open fun tradesProcessor(): TradesProcessor {
//        return TradesProcessor(eventBus, tradesCache())
//    }
//
//}
//
//
//open class TradesProcessor(private val eventBus: IEventBus,
//                           private val tradesCache: Cache<String, Trade>) {
//
//    @PostConstruct
//    open fun start() {
//        eventBus.listen(topic = "trades",
//                clientId = "marketTradesProcessor") { message, _ ->  handleTrade(message as Trade, tradesCache)}
//    }
//}
//
//fun handleTrade(trade : Trade,
//                tradesCache: Cache<String, Trade>) {
//    //Please the trade in the trades map
//    tradesCache.put(trade.id, trade)
//}
//
