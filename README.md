[![pipeline status](https://gitlab.com/simtp/simtp/badges/develop/pipeline.svg)](https://gitlab.com/simtp/simtp/commits/develop)

[//]: # ([![coverage report]&#40;https://gitlab.com/simtp/simtp/badges/develop/coverage.svg&#41;]&#40;https://gitlab.com/simtp/simtp/commits/develop&#41;)

# SimTP - Simplified Transactional Platforms
 

## Overview

Building a transactional platform is no simple task. More often than not, each implementation of a platform requires 
customizations and bespoke functionality. After years of experience in this field, we decided that a one solution fits
all was near impossible to create let alone maintain. Instead, we have decided to create the building blocks for custom 
transactional platforms that are more likely to meet your needs.

## Concept

SimTP is actually made up of a number of subprojects and modules, you can think of these as the building blocks of the 
platform you want to create.

Currently, the main project [SIMTP](https://gitlab.com/simtp/simtp) is made up of many modules which provides different
functionality such as

* An Event Bus
* OAuth2 integration
* Flexible security model based on labels/tags
* Base Rule Engine
* Simple Accounts


The idea is you can use these building blocks to create a custom transactional platform.

As time goes on we will be providing some common re-usable libraries for defining common vetting rules, 
for example market integrity rules, but the main concept is that you can and should be able to easily define your own.

## Technology

Currently, SimTP is built using [Kotlin](https://kotlinlang.org) and is fully compatible with 
[Java](https://www.java.com/en/), so it is designed to run on the JVM (Java Virtual Machine).

To aid integration we also use the Spring Framework and recommend the use of 
[Spring Boot](https://projects.spring.io/spring-boot/). However, if there is enough demand we will consider other 
frameworks but at this stage there has been no demand for this.  

All the projects will be licensed under the Apache 2 license which is a very liberal license, allowing you to use the
binaries and code as you wish even for commercial purposes.

If you want access to the projects you can view (or contribute to the project via GitLab)
[SIMTP](https://gitlab.com/simtp)

## SimTP Framework

The [SimTP Framework](simtp-framework/readme.md) provides the bedrock on which to quickly create new components. The opinionated view creates a 
standardized template from which new components can be created that easily fit into the defined architecture.

The SimTP framework is designed as a stream first design, effectively a platform that primarily 
adopts CQRS - Command Query Responsibility Segregation

![StreamDesign](./simtp-framework/docs/StreamFirstDesign.svg)

# Running

## Distributed Example

See [Distributed Example](simtp-examples/distributed-example/README.md)

## Local

To run on a local machine, it is recommended to use Docker and run the required dependencies (RabbitMQ, Consul etc.) 
in containers to allow for easier development.



### Docker Compose
```shell
docker network create simtp
cd simtp-examples
docker-compose up -d
cd distributed-example
docker-compose up -d
```

### HELM

Package up the helm directory

```shell
helm package helm
```

Deploy (with debug)

```shell
# helm install simtp kubernetes-app-helm --debug --dry-run
helm install simtp helm --debug
```

Uninstall

```shell
helm uninstall simtp
```
# Modular
The architecture is designed to be modular where any module can be replaced by any suitable 
backend suitable for the business case. Integration between the modules is implemented using event streams.


```mermaid
mindmap
  root(Event Streams)
    id[Context: CRM]
    id[Context: Market Data]
    id[Context: Order Placement]
        id[Order Lifecycle]
        id[Order Verification]
    id[Context: Back Office]
```

This solution only provides an implementation of the Order Management System (OMS). It is expected that
other solutions will be provided that implement the other modules of:-
* Back Office
* CRM
* Market Data

This gives the freedom for the order management to be integrated into any existing implementation of these you
might already have.

# Security
The security within the SimTP order management system module mainly ensures that the token provided has the 
is authenticated and has the required scope for authorization. It does not enforce and organisational structural rules.
This is up to the calling system to enforce.


# Overall Process Flow

```mermaid
timeline
%%    title Order Process Timeline
    Order Placement  : User submits order
                     : Order rules applied
                     : Order Event Notifications
    Market Order Interface    : Order submitted to market
                        : Response received from market
                        : Order Event Notifications
    Market Bookings Interface    : One or more bookings received from market
                : Booking made against order
                : Mark order as complete once fully filled
                : Order Event Notifications
                

    
```

# Project Management

## Update maven dependencies
```bash
mvn -N versions:update-parent
mvn -N versions:update-properties
mvn -N versions:commit
```





