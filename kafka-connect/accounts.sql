create table accounts (
                          id int primary key ,
                          first_name varchar(200) not null ,
                          last_name varchar(200) not null,
                          modified timestamp not null
);

insert into accounts (id, first_name, last_name, modified) values (1, 'Joe', 'Bloggs', CURRENT_TIMESTAMP);

insert into accounts (id, first_name, last_name, modified) values (2, 'Jim', 'Smith', CURRENT_TIMESTAMP);
update accounts set last_name='Jones', modified=CURRENT_TIMESTAMP where id=1;

insert into accounts (id, first_name, last_name, modified) values (3, 'Joan', 'Dalton', CURRENT_TIMESTAMP);