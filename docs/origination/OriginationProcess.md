# Origination 

*Demonstration* 

Client origination is a large area in its own right. SimTP does not provide a 
full origination solution. Instead, it is assumed that any integration with
SimTP will provide their own solution. 

Data can be provided to SimTP via the Kafka streaming topic.

# Origination Process

For demonstration, SimTP provides a simple registration endpoint API and UI. This covers steps 1-5 in the following 
diagram.

![OriginationStream](OriginationStream.svg)

A system integrating with SimTP would fulfill the equivalent of these steps with the relevant messages being 
distributed to Kafka and then consumed by the SimTP platform.

Other modules within SimTP, then consume these messages.

The name of the Kafka stream is configurable via the `simtp.origination.stream.name` configuration property 

Primary examples are:
* [Customer Service](../customer/CustomerService.md)
* Account Service
* Address Service

# Internal Origination Flow (Example)

![OriginationSwimlanes](OriginationSwimlanes.svg)

