# Customer Service

## Creation

Customers are generally created via the application process. 

The basic process is as follows:-

![Customer Creation](CustomerService.svg)
