package org.simtp.account.datasource

import jakarta.persistence.*
import org.simtp.account.api.*
import org.simtp.common.IHasLabels
import org.simtp.kotlin.util.uuid
import org.simtp.utilities.mapOptional
import org.simtp.utilities.scheduleForFlux
import org.simtp.utilities.scheduleForMono
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

/**
 * Created by Grant Little grant@grantlittle.me
 */

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(AccountDataSourceConfiguration::class)
annotation class EnableJpaAccountDataSource


@Configuration
@ComponentScan
@EnableJpaRepositories
@EntityScan
open class AccountDataSourceConfiguration {

    @Bean
    open fun accountDataSource(
        accountJpaRepository: AccountJpaRepository
    ): AccountDataSource {
        return AccountDataSource(accountJpaRepository)
    }
//
//    @Bean
//    open fun settlementAccountDataSource(
//        settlementAccountJpaRepository: SettlementAccountJpaRepository
//    ): SettlementAccountDataSource {
//        return SettlementAccountDataSource(settlementAccountJpaRepository)
//    }

}

@Repository
interface AccountJpaRepository: CrudRepository<AccountEntity, String>

@Repository
interface SettlementAccountJpaRepository: CrudRepository<SettlementAccountEntity, String>

class AccountDataSource(
    private val accountJpaRepository: AccountJpaRepository
): IAccountDataSource {
    override fun save(account: Account): Mono<Account> {
        return scheduleForMono { accountJpaRepository
            .save(account.toAccountEntity()) }
            .map { it.toAccount() }
    }

    override fun findById(id: String): Mono<Account> {
        return scheduleForMono { accountJpaRepository
            .findById(id) }
            .mapOptional { it.toAccount() }
    }

    override fun findAll(): Flux<Account> {
        return scheduleForFlux { accountJpaRepository
            .findAll() }
            .map { it.toAccount() }
    }
}

////class SettlementAccountDataSource(
////    private val settlementAccountJpaRepository: SettlementAccountJpaRepository
////): ISettlementAccountDataSource {
////    override fun save(account: SettlementAccount): Mono<SettlementAccount> {
////        return scheduleForMono { settlementAccountJpaRepository
////            .save(account.toSettlementAccountDocument()) }
////            .map { it.toSettlementAccount() }
////    }
////
////    override fun saveAll(accounts: Collection<SettlementAccount>): Flux<SettlementAccount> {
////
////        return accounts
////            .map { it.toSettlementAccountDocument() }
////            .let {
////                scheduleForFlux { settlementAccountJpaRepository.saveAll(it) }
////            }
////            .map { it.toSettlementAccount() }
////    }
////
////
////    override fun findById(id: String): Mono<SettlementAccount> {
////        return scheduleForMono { settlementAccountJpaRepository.findById(id) }
////            .mapOptional { it.toSettlementAccount() }
////    }
////
////    override fun findAll(): Flux<SettlementAccount> {
////        return scheduleForFlux { settlementAccountJpaRepository
////            .findAll() }
////            .map { it.toSettlementAccount() }
////    }
////}
//
//fun SettlementAccount.toSettlementAccountDocument(): SettlementAccountEntity {
//    return SettlementAccountEntity(
//        id = this.id,
//        name = this.name,
//        balance = this.balance,
//        labels = this.labels
//    )
//}
//
//fun SettlementAccountEntity.toSettlementAccount(): SettlementAccount {
//    return SettlementAccount(
//        id = this.id,
//        name = this.name,
//        balance = this.balance,
//        labels = this.labels
//    )
//}

@Table(name = "settlement_account")
@Entity
open class SettlementAccountEntity(
    @Id
    open var id: String = uuid(),
    open var name: String? = null,
    open var balance: BigDecimal? = null,
    @Column(nullable = false)
    open var keyReference: String? = null,
//    var balance: Money? = null,
    @ElementCollection
    @JoinTable(name = "settlement_account_labels")
    override var labels : Set<String>? = null,
    @Version
    open var version: Long? = null,

): IHasLabels {
    override fun toString(): String {
        return "SettlementAccount(id=$id, name=$name, balance=$balance, labels=$labels)"
    }
}

internal fun AccountEntity.toAccount(): Account {
    return Account(
        id = this.id,
        type = this.type,
        labels = this.labels
    )
}

fun Account.toAccountEntity(): AccountEntity {
    return AccountEntity(
        id = this.id,
        type = this.type,
        settlementAccounts = this.settlementAccounts.map { it.value.toSettlementAccountEntity(it.key) }.toMutableList(),
        labels = this.labels?.toMutableSet(),
        version = this.version
    )
}

fun SettlementAccount.toSettlementAccountEntity(key: String): SettlementAccountEntity {
    return SettlementAccountEntity(
        id = this.id,
        keyReference = key,
        name = this.name,
        balance = this.balance,
        labels = this.labels?.toMutableSet(),
        version = this.version
    )
}


fun SettlementAccountEntity.toSettlementAccount(): Pair<String, SettlementAccountEntity> {
    return this.keyReference!! to SettlementAccountEntity(
        id = this.id,
        name = this.name,
        balance = this.balance,
        labels = this.labels?.toMutableSet(),
        version = this.version
    )
}

@Entity
@Table(name = "account")
open class AccountEntity(
    @Id
    open var id: String = uuid(),

    open  var type: String? = null,

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    open var settlementAccounts: MutableList<SettlementAccountEntity> = mutableListOf(),

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    open var contacts: MutableList<AccountContactEntity>? = null,

    @ElementCollection
    @JoinTable(name = "account_labels")
    override var labels : Set<String>? = null,

    @Version
    open var version : Long? = null,

) : IHasLabels {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AccountEntity

        if (id != other.id) return false
        if (type != other.type) return false
        if (labels != other.labels) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + (labels?.hashCode() ?: 0)
        return result
    }
}

@Entity
@Table(name = "account_contact")
open class AccountContactEntity(
    @Id
    open var id: String = uuid(),
    open var friendlyName: String? = null,
    open var firstName: String? = null,
    open var lastName: String? = null,
    open var email: String? = null,
    open var mobile: String? = null,

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    open var addresses: MutableList<AccountAddressEntity>? = null,
    @Version
    open var version: Long? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AccountContactEntity

        if (id != other.id) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode() ?: 0
        result = 31 * result + (firstName?.hashCode() ?: 0)
        result = 31 * result + (lastName?.hashCode() ?: 0)
        return result
    }
}

@Entity
@Table(name = "account_address")
open class AccountAddressEntity(
    @Id
    open var id: String = uuid(),
    open var friendlyName: String? = null,
    open var houseNumber: String? = null,
    open var streetNumber: String? = null,
    open var streetAddress1: String? = null,
    open var streetAddress2: String? = null,
    open var city: String? = null,
    open var postcode: String? = null,
    @Version
    open var version: Long? = null

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AccountAddressEntity

        if (id != other.id) return false
        if (friendlyName != other.friendlyName) return false
        if (houseNumber != other.houseNumber) return false
        if (streetNumber != other.streetNumber) return false
        if (streetAddress1 != other.streetAddress1) return false
        if (streetAddress2 != other.streetAddress2) return false
        if (city != other.city) return false
        if (postcode != other.postcode) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (friendlyName?.hashCode() ?: 0)
        result = 31 * result + (houseNumber?.hashCode() ?: 0)
        result = 31 * result + (streetNumber?.hashCode() ?: 0)
        result = 31 * result + (streetAddress1?.hashCode() ?: 0)
        result = 31 * result + (streetAddress2?.hashCode() ?: 0)
        result = 31 * result + (city?.hashCode() ?: 0)
        result = 31 * result + (postcode?.hashCode() ?: 0)
        result = 31 * result + (version?.hashCode() ?: 0)
        return result
    }
}




fun AccountContact.toAccountContactEntity(ref: String): AccountContactEntity {
    return AccountContactEntity(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        email = this.email,
        mobile = this.mobile,
        addresses = this.addresses?.map { it.value.toAccountAddressEntity(it.key) }?.toMutableList(),
        version = this.version
    )
}

fun AccountAddress.toAccountAddressEntity(ref: String): AccountAddressEntity {
    return AccountAddressEntity(
        id = this.id,
        friendlyName = ref,
        houseNumber = this.houseNumber,
        streetNumber = this.streetNumber,
        streetAddress1 = this.streetAddress1,
        streetAddress2 = this.streetAddress2,
        city = this.city,
        postcode = this.postcode,
        version = this.version
    )
}




