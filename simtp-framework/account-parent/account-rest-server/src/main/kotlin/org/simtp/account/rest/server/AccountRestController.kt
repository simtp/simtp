@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")

package org.simtp.account.rest.server

import org.simtp.account.api.Account
import org.simtp.account.api.IAccountApi
import org.simtp.framework.serialization.EnableSerialization
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Import(AccountRestServerConfiguration::class)
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
annotation class EnableAccountRestServer

@Configuration
@ComponentScan
@EnableSerialization
open class AccountRestServerConfiguration

/**
 * Created by Grant Little grant@grantlittle.me
 */
@RestController
@RequestMapping("/api/account/1.0")
open class AccountRestController(
    private val accountApi: IAccountApi,
) {


    @GetMapping
    open fun all() : Flux<Account> = accountApi.getAll()

    @PostMapping(consumes = ["application/json"])
    open fun add(@RequestBody account: Account) : Mono<String> {
        return accountApi.add(account)
    }

    @GetMapping(path = ["/{id}"])
    open fun get(@PathVariable id: String) : Mono<Account> {
        return accountApi[id]
    }

    @DeleteMapping(path = ["/{id}"])
    open fun delete(@PathVariable id: String) : Mono<String> {
        return accountApi.delete(id)
    }
}