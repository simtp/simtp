@file:Suppress("ReactiveStreamsUnusedPublisher")

package org.simtp.account.rest.server

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.simtp.account.api.Account
import org.simtp.account.api.IAccountApi
import org.springframework.data.redis.core.ReactiveStreamOperations
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

class AccountRestControllerTest {

    private var accountApi: IAccountApi = mockk(relaxed = true)
    private var ops: ReactiveStreamOperations<String, String, String> = mockk()
    private var controller = AccountRestController(
        accountApi = accountApi,
    )
    private var webClient = WebTestClient
        .bindToController(controller)
        .build()

    @BeforeEach
    fun setUp() {
        accountApi = mockk(relaxed = true)
        ops = mockk(relaxed = true)
        controller = AccountRestController(
            accountApi = accountApi,
        )
        webClient = WebTestClient
            .bindToController(controller)
            .build()

    }
    @Test
    fun get() {
        val expected = Account(
            id = "1"
        )
        every { accountApi["1"] } returns Mono.just(expected)
        val result = webClient
            .get()
            .uri("/api/account/1.0/{id}", mapOf(
                "id" to expected.id
            ))
            .exchange()
            .expectStatus().isOk
            .expectBody(Account::class.java)
            .returnResult()

        assertEquals(expected, result.responseBody)
    }
}