//@file:Suppress("ReactiveStreamsUnusedPublisher")
//
//package org.simtp.account.starter
//
//import com.fasterxml.jackson.databind.ObjectMapper
//import io.mockk.every
//import io.mockk.mockk
//import io.mockk.verify
//import org.junit.jupiter.api.Test
//import org.simtp.account.api.SettlementAccount
//import org.simtp.backoffice.api.CashTransaction
//import org.simtp.framework.serialization.withDefaults
//import org.simtp.origination.api.OriginationAccount
//import org.simtp.origination.api.OriginationData
//import org.simtp.origination.api.OriginationSettlementAccount
//import reactor.core.publisher.Mono
//import reactor.test.StepVerifier
//import java.math.BigDecimal
//import java.time.Duration
//
//internal class SettlementAccountServiceKtTest  {
//
//    @Test
//    fun testHandleOriginationMessage() {
//        //Setup
//        val settlementAccountDataSource = mockk<ISettlementAccountDataSource>()
//        val objectMapper = ObjectMapper().withDefaults()
//        val account =  OriginationSettlementAccount()
//        val accounts = listOf(account)
//        val originationData = OriginationData(
//            account = OriginationAccount(
//                settlementAccounts = mapOf("main" to OriginationSettlementAccount())
//            )
//        )
//        every { settlementAccountDataSource.save(any()) } returns accounts.first().let { Mono.just(it.toSettlementAccount()) }
//
//        //Create handler
//        val handler = handleOriginationMessage(
//            settlementAccountDataSource = settlementAccountDataSource,
//            objectMapper = objectMapper,
//            msg = mapOf(
//                "json" to objectMapper.writeValueAsString(originationData)
//            )
//        )
//
//        //Execute & Verify
//        StepVerifier
//            .create(handler)
//            .expectNext(accounts.first().toSettlementAccount())
//            .expectComplete()
//            .verify(Duration.ofSeconds(5))
//
//        verify { settlementAccountDataSource.save(any()) }
//
//    }
//
//    @Test
//    fun testHandleCashTransactionMessage() {
//        val settlementAccountDataSource = mockk<ISettlementAccountDataSource>()
//        val settlementAccount = SettlementAccount()
//        val objectMapper = ObjectMapper().withDefaults()
//        val cashTransaction = CashTransaction(
//            accountId = settlementAccount.id,
//            sum = BigDecimal.ONE
//        )
//
//        every { settlementAccountDataSource.findById(settlementAccount.id) } returns Mono.just(settlementAccount)
//        every { settlementAccountDataSource.save(any()) } answers { Mono.just(args[0] as SettlementAccount) }
//
//
//        val function = handleCashTransactionMessage(
//            settlementAccountDataSource = settlementAccountDataSource,
//            objectMapper = objectMapper,
//            msg = mapOf("json" to objectMapper.writeValueAsString(cashTransaction))
//        )
//
//        StepVerifier
//            .create(function)
//            .expectNext(SettlementAccount(id = settlementAccount.id, balance = BigDecimal.ONE))
//            .expectComplete()
//            .verify(Duration.ofSeconds(5))
//
//        verify(exactly = 1) { settlementAccountDataSource.save(any()) }
//
//    }
//
//    @Test
//    fun testHandleCashTransactionMessage_withZeroValue() {
//        val settlementAccountDataSource = mockk<ISettlementAccountDataSource>()
//        val settlementAccount = SettlementAccount()
//        val objectMapper = ObjectMapper().withDefaults()
//        val cashTransaction = CashTransaction(
//            accountId = settlementAccount.id,
//            sum = BigDecimal.ZERO
//        )
//
//        every { settlementAccountDataSource.findById(settlementAccount.id) } returns Mono.just(settlementAccount)
//        every { settlementAccountDataSource.save(any()) } answers { Mono.just(args[0] as SettlementAccount) }
//
//
//        val function = handleCashTransactionMessage(
//            settlementAccountDataSource = settlementAccountDataSource,
//            objectMapper = objectMapper,
//            msg = mapOf("json" to objectMapper.writeValueAsString(cashTransaction))
//        )
//
//        StepVerifier
//            .create(function)
//            .expectComplete()
//            .verify(Duration.ofSeconds(5))
//
//        verify(exactly = 0) { settlementAccountDataSource.save(any()) }
//
//    }
//}