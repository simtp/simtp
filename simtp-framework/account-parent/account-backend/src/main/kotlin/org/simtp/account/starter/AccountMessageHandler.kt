package org.simtp.account.starter

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.account.api.*
import org.simtp.account.starter.AccountHelper.Companion.handleOriginationMessageForAccount
import org.simtp.origination.api.ApplicationAccount
import org.simtp.origination.api.Application
import org.simtp.origination.api.ApplicationSettlementAccount
import org.simtp.origination.stream.OriginationStreamProperties
import org.simtp.stream.api.StreamMessageType
import org.simtp.stream.redis.RedisStream.Companion.createGroupAndReceiverAutoAck
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Mono

class AccountMessageHandler(
    private val accountDataSource: IAccountDataSource,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
    private val objectMapper: ObjectMapper,
    private val accountApi: IAccountApi,
    private val originationStreamProperties: OriginationStreamProperties
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val LOG = LoggerFactory.getLogger(AccountMessageHandler::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = originationStreamProperties.name,
            groupName =  "AccountService",
            consumerName =  "AccountService"
        )
        .flatMap {
            handleOriginationMessageForAccount(
                accountApi = accountApi,
                objectMapper = objectMapper,
                msg = it.value
            )
        }.subscribe()

        createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = ACCOUNTS_STREAM_NAME,
            groupName =  "AccountService",
            consumerName =  "AccountService"
        )
            .flatMap {
                when (val type = it.value["event-type"]) {
                    StreamMessageType.CREATE.name -> {
                        val account = objectMapper.readValue(it.value["json"], Account::class.java)
                        LOG.info("Saving account={}", account)
                        accountDataSource.save(account)
                    }
                    else -> {
                        LOG.warn("Currently implemented event type {}", type)
                        Mono.empty()
                    }
                }
            }
            .subscribe()
    }
}

open class AccountHelper {

    companion object {

        @JvmStatic
        private val LOG = LoggerFactory.getLogger(AccountHelper::class.java)

        @JvmStatic
        internal fun handleOriginationMessageForAccount(
            accountApi: IAccountApi,
            objectMapper: ObjectMapper,
            msg: Map<String, String>
        ): Mono<String> {
            return originationDataMessageToAccount(objectMapper, msg)
                .doOnNext { LOG.info("Created account in data store for account={}", it) }
                .flatMap { accountApi.add(account = it)}
        }

        @JvmStatic
        internal fun ApplicationAccount.toAccount(): Account {
            return Account(
                id = this.id,
                type = this.type,
                labels = this.labels,
                settlementAccounts = this.settlementAccounts.asSequence().associate { it.key to it.value.toSettlementAccount() },
                version = this.version
            )
        }

        @JvmStatic
        internal fun ApplicationSettlementAccount.toSettlementAccount(): SettlementAccount {
            return SettlementAccount(
                id = this.id,
                name = this.name,
                balance = this.balance,
                labels = this.labels,
                version = this.version
            )
        }

//        @JvmStatic
//        internal fun OriginationAccountType.toAccountType(): AccountType {
//            return when (this) {
//                OriginationAccountType.EQUITIES -> AccountType.EQUITIES
//            }
//        }


//        @JvmStatic
//        internal fun Collection<OriginationSettlementAccount>.toSettlementAccounts(): List<SettlementAccount> {
//            return this.map { it.toSettlementAccount() }
//        }

        @JvmStatic
        private fun originationDataMessageToAccount(
            objectMapper: ObjectMapper,
            msg: Map<String, String>
        ): Mono<Account> {
            return objectMapper
                .readValue(msg["json"]!!, Application::class.java)
                ?.also { LOG.info("Received originationData={}", it) }
                ?.account
                ?.toAccount()
                ?.let { Mono.just(it) }
                ?: Mono.empty()
        }
    }
}