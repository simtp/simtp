package org.simtp.account.starter

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.account.api.*
import org.simtp.stream.api.StreamMessageType
import org.slf4j.LoggerFactory
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


class AccountService(
    private val accountDataSource: IAccountDataSource,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val objectMapper: ObjectMapper,
): IAccountApi {

    companion object {
        @JvmStatic
        private val LOG = LoggerFactory.getLogger(AccountService::class.java)
    }

    override fun get(accountNumber: String): Mono<Account> {
       return accountDataSource
           .findById(accountNumber)
    }

    override fun getAll(): Flux<Account> {
        return accountDataSource
            .findAll()
    }

    override fun add(account: Account) : Mono<String> {
        val record = StreamRecords
            .mapBacked<String, String, String>(mapOf(
                "event-type" to StreamMessageType.CREATE.name,
                "json" to objectMapper.writeValueAsString(account)
            ))
            .withStreamKey(ACCOUNTS_STREAM_NAME)

        return reactiveRedisOperations
            .opsForStream<String, String>()
            .add(record)
            .map { account.id }

    }

    override fun delete(id: String) : Mono<String> {

        val record = StreamRecords
            .mapBacked<String, String, String>(mapOf(
                "event-type" to StreamMessageType.DELETE.name,
                "id" to id
            ))
            .withStreamKey(ACCOUNTS_STREAM_NAME)

        return reactiveRedisOperations
            .opsForStream<String, String>()
            .add(record)
            .map { id }
    }
}



