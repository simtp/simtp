package org.simtp.account.starter

import org.simtp.account.api.Account
import org.simtp.account.api.IAccountDataSource
import org.simtp.account.api.SettlementAccount
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Import(InMemoryAccountConfiguration::class)
annotation class EnableInMemoryAccountDataSource

@Configuration
open class InMemoryAccountConfiguration {

    @Bean
    open fun inMemoryAccountDataSource(): InMemoryAccountDataSource {
        return InMemoryAccountDataSource()
    }
}

class InMemoryAccountDataSource: IAccountDataSource {

    private val accounts: MutableMap<String, Account> = mutableMapOf()
    override fun save(account: Account): Mono<Account> {
        accounts[account.id] = account
        return Mono.just(account)
    }

    override fun findById(id: String): Mono<Account> {
        return accounts[id]
            ?.let { Mono.just(it) }
            ?: Mono.empty()
    }

    override fun findAll(): Flux<Account> {
        return Flux.fromIterable(accounts.values)
    }
}

//class InMemorySettlementAccountDataSource: ISettlementAccountDataSource {
//
//    private val accounts: MutableMap<String, SettlementAccount> = mutableMapOf()
//    override fun save(account: SettlementAccount): Mono<SettlementAccount> {
//        accounts[account.id] = account
//        return Mono.just(account)
//    }
//
//    override fun findById(id: String): Mono<SettlementAccount> {
//        return accounts[id]
//            ?.let { Mono.just(it) }
//            ?: Mono.empty()
//    }
//
//    override fun findAll(): Flux<SettlementAccount> {
//        return Flux.fromIterable(accounts.values)
//    }
//
//    override fun saveAll(accounts: Collection<SettlementAccount>): Flux<SettlementAccount> {
//        accounts.associateBy { it.id }
//            .also { this.accounts.putAll(it) }
//            .map { accounts }
//
//        return Flux.fromIterable(accounts)
//    }
//}