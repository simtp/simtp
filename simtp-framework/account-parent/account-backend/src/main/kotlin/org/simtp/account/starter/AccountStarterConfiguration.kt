package org.simtp.account.starter

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.account.api.IAccountApi
import org.simtp.account.api.IAccountDataSource
import org.simtp.origination.stream.OriginationStreamProperties
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisOperations

/**
 * Created by Grant Little grant@grantlittle.me
 */
@Configuration
@ComponentScan
@EnableRedisStream
open class AccountStarterConfiguration {

    @Bean
    open fun accountService(
        accountDataSource: IAccountDataSource,
        objectMapper: ObjectMapper,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    ): IAccountApi {
        return AccountService(
            accountDataSource = accountDataSource,
            objectMapper = objectMapper,
            reactiveRedisOperations = reactiveRedisOperations
        )
    }

    @Bean
    open fun accountMessageHandler(
        accountDataSource: IAccountDataSource,
        objectMapper: ObjectMapper,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
        accountApi: IAccountApi
    ): AccountMessageHandler {
        return AccountMessageHandler(
            accountDataSource = accountDataSource,
            objectMapper = objectMapper,
            reactiveRedisOperations = reactiveRedisOperations,
            accountApi = accountApi,
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            originationStreamProperties = OriginationStreamProperties()
        )
    }

//    @Bean
//    open fun settlementAccountService(
//        settlementAccountDataSource: ISettlementAccountDataSource,
//        objectMapper: ObjectMapper,
//        applicationEventPublisher: ApplicationEventPublisher,
//        reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
//        reactiveRedisOperations: ReactiveRedisOperations<String, String>
//    ): SettlementAccountService {
//        return SettlementAccountService(
//            settlementAccountDataSource = settlementAccountDataSource,
//            objectMapper = objectMapper,
//            applicationEventPublisher = applicationEventPublisher,
//            reactiveRedisOperations = reactiveRedisOperations,
//            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory
//        )
//    }

}


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(AccountStarterConfiguration::class)
annotation class EnableAccountBackend