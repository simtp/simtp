@file:Suppress("unused")

package org.simtp.account.starter

import org.simtp.account.api.AccountType
import org.simtp.common.IHasLabels
import org.springframework.data.annotation.Id
import java.util.UUID

//@Document("account")
open class AccountDocument(
    @Id
    var id: String? = null,
    var type: AccountType = AccountType.EQUITIES,
    var primaryContact: AccountContactDocument? = null,
    override var labels : Set<String>? = null) : IHasLabels

data class AccountContactDocument(
    var id: String? = UUID.randomUUID().toString(),
    var firstName: String?,
    var lastName: String? = null,
)

//class SettlementAccountService(
//    private val settlementAccountDataSource: ISettlementAccountDataSource,
//    private val reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
//    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
//    private val objectMapper: ObjectMapper,
//    private val applicationEventPublisher: ApplicationEventPublisher
//): ApplicationListener<ApplicationReadyEvent> {
//
//    private var backOfficeDisposable: Disposable? = null
//
//    companion object {
//        private val log = LoggerFactory.getLogger(SettlementAccountService::class.java)
//    }
//
//    override fun onApplicationEvent(event: ApplicationReadyEvent) {
//        RedisStream.createGroupAndReceiverAutoAck(
//            connectionFactory = reactiveRedisConnectionFactory,
//            reactiveRedisOperations = reactiveRedisOperations,
//            streamName = ORIGINATION_STREAM_KEY,
//            groupName = "SettlementAccountService",
//            consumerName = "SettlementAccountService"
//        )
//        .flatMap {
//            handleOriginationMessage(
//                settlementAccountDataSource = settlementAccountDataSource,
//                objectMapper = objectMapper,
//                msg = it.value
//            )
//        }.subscribe()
//
//
//        RedisStream.createGroupAndReceiverAutoAck(
//            connectionFactory = reactiveRedisConnectionFactory,
//            reactiveRedisOperations = reactiveRedisOperations,
//            streamName = CASH_TRANSACTIONS_STREAM_NAME,
//            groupName = "SettlementAccountService",
//            consumerName = "SettlementAccountService"
//        )
//        .flatMap {
//            handleCashTransactionMessage(
//                settlementAccountDataSource = settlementAccountDataSource,
//                objectMapper = objectMapper,
//                msg = it.value
//            )
//        }.subscribe()
//
//
//    }
//
//}
//
//class SettlementAccountHelper {
//    companion object {
//        @JvmStatic
//        private val log = LoggerFactory.getLogger("org.simtp.account.starter.SettlementAccountServiceKt")
//
//        @JvmStatic
//        internal fun handleOriginationMessage(
//            settlementAccountDataSource: ISettlementAccountDataSource,
//            objectMapper: ObjectMapper,
//            msg: Map<String, String>
//        ): Flux<SettlementAccount> =
//            Flux
//                .fromIterable(originationDataMessageToSettlementAccounts(objectMapper, msg))
//                .flatMap { settlementAccountDataSource.save(it) }
//                .doOnNext { log.info("Created settlement account in data store for account={}", it) }
//
//
//        @JvmStatic
//        private fun originationDataMessageToSettlementAccounts(
//            objectMapper: ObjectMapper,
//            msg: Map<String, String>
//        ): List<SettlementAccount> {
//            return objectMapper
//                .readValue(msg["json"]!!, OriginationData::class.java)
//                ?.account
//                ?.settlementAccounts
//                ?.map { it.value }
//                ?.map { it.toSettlementAccount() }
//                ?: emptyList()
//        }
//
//        @JvmStatic
//        internal fun OriginationSettlementAccount.toSettlementAccount(): SettlementAccount {
//            return SettlementAccount(
//                id = this.id,
//                name = this.name,
////        balance = it.balance,
//                labels = this.labels
//            )
//        }
//
//
//        @JvmStatic
//        internal fun handleCashTransactionMessage(
//            settlementAccountDataSource: ISettlementAccountDataSource,
//            objectMapper: ObjectMapper,
//            msg: Map<String, String>
//        ): Mono<SettlementAccount> =
//
//            Mono.fromCallable {
//                objectMapper
//                    .readValue(msg["json"]!!, CashTransaction::class.java)
//            }.flatMap { transaction ->
//                when (transaction.sum != BigDecimal.ZERO) {
//                    true -> settlementAccountDataSource
//                        .findById(transaction.accountId!!)
//                        .map { transaction to it }
//                    false -> Mono.empty()
//                }
//
//            }.flatMap {
//                val transaction = it.first
//                val account = it.second
//                account.balance = (account.balance ?: BigDecimal.ZERO).add(transaction.sum ?: BigDecimal.ZERO)
//                settlementAccountDataSource.save(it.second)
//            }
//                .doOnNext { log.info("Created cash transaction in data store for account={}", it) }
//
//    }
//
//
//}





