package org.simtp.account.client

import org.simtp.common.IHasLabels
import java.util.*

//const val ACCOUNTS_STREAM_NAME = "accounts"

//interface IAccountApi {
//
//    operator fun get(accountNumber: String): Account?
//
//    fun getAll(): List<Account>
//}

/**
 */
data class Account(val id: String? = UUID.randomUUID().toString(),
                   override var type: String? = null,
                   override var labels : Set<String>? = null) : CreateAccount(type = type, labels = labels) {
       constructor(createAccount: CreateAccount) : this(type = createAccount.type, labels = createAccount.labels)
}

open class CreateAccount(
    open var type: String? = null,
    override var labels : Set<String>? = null) : IHasLabels {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CreateAccount

        if (type != other.type) return false
        if (labels != other.labels) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type?.hashCode() ?: 0
        result = 31 * result + (labels?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "CreateAccount(type=$type, labels=$labels)"
    }


}





