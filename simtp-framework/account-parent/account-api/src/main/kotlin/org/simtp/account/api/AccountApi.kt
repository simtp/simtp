package org.simtp.account.api

import org.simtp.common.IHasLabels
import org.simtp.kotlin.util.uuid
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal

const val ACCOUNTS_STREAM_NAME = "stream.org.simtp.accounts"

interface IAccountApi {

    operator fun get(accountNumber: String): Mono<Account>

    fun getAll(): Flux<Account>

    fun add(account: Account): Mono<String>

    fun delete(id: String) : Mono<String>
}

interface IAccountDataSource {
    fun save(account: Account): Mono<Account>

    fun findById(id: String): Mono<Account>

    fun findAll(): Flux<Account>
}

//interface ISettlementAccountDataSource {
//    fun save(account: SettlementAccount): Mono<SettlementAccount>
//    fun saveAll(accounts: Collection<SettlementAccount>): Flux<SettlementAccount>
//
//    fun findById(id: String): Mono<SettlementAccount>
//
//    fun findAll(): Flux<SettlementAccount>
//}


enum class AccountType {
    EQUITIES
}

/**
 */
data class Account(
    val id: String = uuid(),
    var type: String? = null,
    var contacts: MutableList<AccountContact>? = null,
    var settlementAccounts: Map<String, SettlementAccount> = emptyMap(),
    var labels : Set<String>? = null,
    var version: Long? = null,
)

data class AccountContact(
    var id: String = uuid(),
    var firstName: String? = null,
    var lastName: String? = null,
    var email: String? = null,
    var mobile: String? = null,
    var addresses: Map<String, AccountAddress>? = null,
    var version: Long? = null
)

data class AccountAddress(
    var id: String = uuid(),
    var houseNumber: String? = null,
    var streetNumber: String? = null,
    var streetAddress1: String? = null,
    var streetAddress2: String? = null,
    var city: String? = null,
    var postcode: String? = null,
    var version: Long? = null
)


open class SettlementAccount(
    var id: String = uuid(),
    var name: String? = null,
    var balance: BigDecimal? = null,
//    var balance: Money? = null,
    override var labels : Set<String>? = null,
    var version: Long? = null,

): IHasLabels {




    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SettlementAccount

        if (id != other.id) return false
        if (name != other.name) return false
        if (balance != other.balance) return false
        if (labels != other.labels) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (balance?.hashCode() ?: 0)
        result = 31 * result + (labels?.hashCode() ?: 0)
        result = 31 * result + (version?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "SettlementAccount(id='$id', name=$name, balance=$balance, labels=$labels, version=$version)"
    }
}








