package org.simtp.swagger.starter

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.properties.AbstractSwaggerUiConfigProperties.SwaggerUrl
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator
import org.springframework.cloud.gateway.discovery.DiscoveryLocatorProperties
import org.springframework.context.annotation.*
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(SwaggerConfiguration::class)
annotation class EnableSwagger

@Configuration
@ComponentScan
open class SwaggerConfiguration {
    @Bean
    open fun discoveryClientRouteLocator(
        discoveryClient: ReactiveDiscoveryClient,
        properties: DiscoveryLocatorProperties
    ): DiscoveryClientRouteDefinitionLocator {
        return DiscoveryClientRouteDefinitionLocator(discoveryClient, properties)
    }
}

@RestController
class SwaggerUiConfig(
    private val discoveryClient: DiscoveryClient
) {

    @GetMapping("/swagger-config.json")
    @Operation(hidden = true)
    fun swaggerConfig(): Map<String, Any> {
        return mapOf("urls" to
                discoveryClient
                    .services
                    .flatMap {serviceName ->
                        discoveryClient
                            .getInstances(serviceName)
                            .filter { it.metadata["api"] == "true" }
                            .map { _ ->
                                SwaggerUrl(
                                    serviceName,
                                    "/proxy/$serviceName/v3/api-docs",
                                    serviceName
                                )
                            }
                     })
    }

}
