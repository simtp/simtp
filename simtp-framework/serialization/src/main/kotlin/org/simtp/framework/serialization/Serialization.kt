package org.simtp.framework.serialization

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jodamoney.AmountRepresentation
import com.fasterxml.jackson.datatype.jodamoney.JodaMoneyModule
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Primary
import java.util.*


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(SerializationConfiguration::class)
annotation class EnableSerialization

@Configuration
open class SerializationConfiguration {

    @Bean
    @Primary
    open fun objectMapper(): ObjectMapper {
        return ObjectMapper().withDefaults()
    }

//    @Bean
//    open fun jackson2ObjectMapperBuilder(): Jackson2ObjectMapperBuilder? {
//        return Jackson2ObjectMapperBuilder().serializers(LOCAL_DATETIME_SERIALIZER)
//            .serializationInclusion(JsonInclude.Include.NON_NULL)
//    }
//
//    @Bean
//    open fun jackson2ObjectMapperBuilderCustomizer(): Jackson2ObjectMapperBuilderCustomizer {
//        return Jackson2ObjectMapperBuilderCustomizer { builder ->
//            builder
//                .apply {
//                    serializationInclusion(JsonInclude.Include.NON_NULL)
//                    modules(JavaTimeModule())
//                    val kotlinModule = KotlinModule.Builder()
//                        .withReflectionCacheSize(512)
//                        .configure(KotlinFeature.NullToEmptyCollection, false)
//                        .configure(KotlinFeature.NullToEmptyMap, false)
//                        .configure(KotlinFeature.NullIsSameAsDefault, false)
//                        .configure(KotlinFeature.SingletonSupport, false)
//                        .configure(KotlinFeature.StrictNullChecks, false)
//                        .build()
//                    val jodaMoneyModule = JodaMoneyModule().withAmountRepresentation(AmountRepresentation.DECIMAL_STRING)
//                    modules(jodaMoneyModule, kotlinModule)
//                    timeZone(TimeZone.getDefault())
//                    propertyNamingStrategy(PropertyNamingStrategies.LOWER_CAMEL_CASE)
//                    featuresToDisable(
//                        SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
//                        DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
//                    )
//                }
//        }
//
//    }
}

fun ObjectMapper.withDefaults(): ObjectMapper {
    return this.also {
        setSerializationInclusion(JsonInclude.Include.NON_NULL)
        disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        registerModule(JavaTimeModule())
        val jodaMoneyModule = JodaMoneyModule().apply {
            withAmountRepresentation(AmountRepresentation.DECIMAL_STRING)
        }
        registerModule(jodaMoneyModule)
        val kotlinModule = KotlinModule.Builder()
            .withReflectionCacheSize(512)
            .configure(KotlinFeature.NullToEmptyCollection, false)
            .configure(KotlinFeature.NullToEmptyMap, false)
            .configure(KotlinFeature.NullIsSameAsDefault, false)
            .configure(KotlinFeature.SingletonSupport, false)
            .configure(KotlinFeature.StrictNullChecks, false)
            .build()
        registerModule(kotlinModule)
        setTimeZone(TimeZone.getDefault())
        propertyNamingStrategy = PropertyNamingStrategies.LOWER_CAMEL_CASE
    }
}
