package org.simtp.framework.test.serialization

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.framework.serialization.withDefaults
import org.springframework.http.MediaType
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient


var strategies: ExchangeStrategies = ExchangeStrategies
    .builder()
    .codecs { clientDefaultCodecsConfigurer ->
        clientDefaultCodecsConfigurer.defaultCodecs().jackson2JsonEncoder(
            Jackson2JsonEncoder(
                ObjectMapper().withDefaults(),
                MediaType.APPLICATION_JSON
            )
        )
        clientDefaultCodecsConfigurer.defaultCodecs().jackson2JsonDecoder(
            Jackson2JsonDecoder(
                ObjectMapper().withDefaults(),
                MediaType.APPLICATION_JSON
            )
        )
    }.build()

@JvmField
val defaultWebClientBuilder: WebClient.Builder = WebClient.builder().exchangeStrategies(strategies)