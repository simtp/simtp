package org.simtp.utilities

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers
import java.util.Optional

fun <T> (() -> T).toMono(scheduler: Scheduler = Schedulers.boundedElastic()): Mono<T> {
    return Mono
        .fromCallable(this)
        .subscribeOn(scheduler)
}

fun <T> scheduleForMono(
    scheduler: Scheduler = Schedulers.boundedElastic(),
    callable: () -> T
): Mono<T> {
    return Mono
        .fromCallable(callable)
        .subscribeOn(scheduler)
}

inline fun <T, V> Mono<Optional<T>>.mapOptional(crossinline function: (T) -> V): Mono<V> {
    return this.flatMap {
        val value = it.get()
        if (it.isPresent) {
            Mono.just(function(value)!!)
        } else {
            Mono.empty()
        }
    }
}



fun <T> scheduleForFlux(
    scheduler: Scheduler = Schedulers.boundedElastic(),
    callable: () -> Iterable<T>
): Flux<T> {
    return Flux
        .fromIterable(callable())
        .subscribeOn(scheduler)
}