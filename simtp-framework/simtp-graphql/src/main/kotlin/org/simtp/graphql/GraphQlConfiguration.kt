package org.simtp.graphql

import graphql.GraphQLContext
import graphql.execution.CoercedVariables
import graphql.language.StringValue
import graphql.language.Value
import graphql.scalars.ExtendedScalars
import graphql.schema.Coercing
import graphql.schema.GraphQLScalarType
import graphql.schema.idl.RuntimeWiring
import org.simtp.configuration.YamlPropertySourceFactory
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.graphql.execution.RuntimeWiringConfigurer
import org.springframework.stereotype.Controller
import reactor.core.publisher.Mono
import java.time.LocalDate
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@Import(GraphQlConfiguration::class)
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class EnableSimTPGraphQl

@Configuration
@PropertySource("classpath:graphql.yml", factory = YamlPropertySourceFactory::class)
@ComponentScan
open class GraphQlConfiguration {

    @Bean
    open fun localDateScalar(): GraphQLScalarType {
        return GraphQLScalarType.newScalar()
            .name("LocalDate")
            .description("Java 8 LocalDate as a scalar.")
            .coercing(object : Coercing<LocalDate, String> {

                override fun parseValue(input: Any, graphQLContext: GraphQLContext, locale: Locale): LocalDate? {
                    return LocalDate.parse(input as String, DateTimeFormatter.ISO_LOCAL_DATE)
                }

                override fun parseLiteral(
                    input: Value<*>,
                    variables: CoercedVariables,
                    graphQLContext: GraphQLContext,
                    locale: Locale
                ): LocalDate? {
                    val stringValue = (input as StringValue).value
                    return LocalDate.parse(stringValue, DateTimeFormatter.ISO_LOCAL_DATE)
                }

                override fun serialize(
                    dataFetcherResult: Any,
                    graphQLContext: GraphQLContext,
                    locale: Locale
                ): String? {
                    return (dataFetcherResult as LocalDate).format(DateTimeFormatter.ISO_LOCAL_DATE)
                }

            })
            .build()
    }

    @Bean
    open fun zonedDateTimeScalar(): GraphQLScalarType {
        return GraphQLScalarType.newScalar()
            .name("ZonedDateTime")
            .description("Java ZonedDateTime as a scalar.")
            .coercing(object : Coercing<ZonedDateTime, String> {

                override fun serialize(
                    dataFetcherResult: Any,
                    graphQLContext: GraphQLContext,
                    locale: Locale
                ): String? {
                    return (dataFetcherResult as ZonedDateTime).format(DateTimeFormatter.ISO_ZONED_DATE_TIME)
                }

                override fun parseValue(input: Any, graphQLContext: GraphQLContext, locale: Locale): ZonedDateTime? {
                    return ZonedDateTime.parse(input as String, DateTimeFormatter.ISO_LOCAL_DATE)
                }

                override fun parseLiteral(
                    input: Value<*>,
                    variables: CoercedVariables,
                    graphQLContext: GraphQLContext,
                    locale: Locale
                ): ZonedDateTime? {
                    val stringValue = (input as StringValue).value
                    return ZonedDateTime.parse(stringValue, DateTimeFormatter.ISO_ZONED_DATE_TIME)
                }
            })
            .build()
    }

    @Bean
    open fun runtimeWiringConfigurer(
        zonedDateTimeScalar: GraphQLScalarType,
        localDateScalar: GraphQLScalarType
    ): RuntimeWiringConfigurer {
        return RuntimeWiringConfigurer { wiringBuilder: RuntimeWiring.Builder ->
            wiringBuilder
                .scalar(ExtendedScalars.GraphQLBigDecimal)
                .scalar(localDateScalar)
                .scalar(zonedDateTimeScalar)
        }
    }
}

@Controller
class GraphQlCommonCommandController {

    init {
        log.info("Initializing GraphQlCommonCommandController")
    }

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(GraphQlCommonCommandController::class.java)
    }

    @QueryMapping
    fun getFrameworkVersion(): Mono<String> {
        return Mono.just("1.0")
    }

    @MutationMapping
    fun echo(@Argument data: String): Mono<String> {
        return Mono.just(data)
    }

}