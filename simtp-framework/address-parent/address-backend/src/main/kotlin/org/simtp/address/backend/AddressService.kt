@file:Suppress("unused")

package org.simtp.address.backend

import com.fasterxml.jackson.databind.ObjectMapper
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.simtp.address.api.ADDRESSES_STREAM_NAME
import org.simtp.address.api.Address
import org.simtp.address.api.IAddressApi
import org.simtp.origination.api.Application
import org.simtp.origination.stream.OriginationStreamProperties
import org.simtp.stream.redis.RedisStream
import org.simtp.utilities.scheduleForFlux
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Configuration
@ComponentScan
@EnableJpaRepositories
@EntityScan
open class AddressBackendConfiguration {

    @Bean
    open fun addressService(
        addressJpaRepository: AddressJpaRepository,
        lettuceConnectionFactory: ReactiveRedisConnectionFactory,
        objectMapper: ObjectMapper,
        applicationEventPublisher: ApplicationEventPublisher,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        originationStreamProperties: OriginationStreamProperties
    ): IAddressApi {
        return AddressService(
            addressJpaRepository,
            lettuceConnectionFactory,
            objectMapper,
            applicationEventPublisher,
            reactiveRedisOperations,
            originationStreamProperties
        )
    }

}

@Repository
interface AddressJpaRepository: CrudRepository<AddressEntity, String>

class AddressService(
    private val addressJpaRepository: AddressJpaRepository,
    private val lettuceConnectionFactory: ReactiveRedisConnectionFactory,
    private val objectMapper: ObjectMapper,
    private val applicationEventPublisher: ApplicationEventPublisher,
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val originationStreamProperties: OriginationStreamProperties
): IAddressApi, ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val log = LoggerFactory.getLogger(AddressService::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = lettuceConnectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = originationStreamProperties.name,
            groupName = "AddressService",
            consumerName = "AddressService"
        )
        .map {
            objectMapper
                .readValue(it.value["json"]!!, Application::class.java)
                ?.applicants
                ?.flatMap { entry ->
                    entry
                        .value
                        .addresses
                        ?.map { addressEntry ->
                            val address = addressEntry.value

                            AddressEntity(
                                id = address.id,
                                houseNumber = address.houseNumber,
                                streetNumber = address.streetNumber,
                                streetAddress1 = address.streetAddress1,
                                streetAddress2 = address.streetAddress2,
                                city = address.city,
                                postcode = address.postcode
                            )
                        }
                        ?: emptyList()
                }
                ?: emptyList()
        }
        .flatMap {
            scheduleForFlux { addressJpaRepository.saveAll(it) }
        }
        .flatMap {
            reactiveRedisOperations
                .opsForStream<String, String>()
                .add(StreamRecords
                .newRecord()
                .ofMap(mapOf("json" to objectMapper.writeValueAsString(it.toAddressMessage())))
                .withStreamKey(ADDRESSES_STREAM_NAME))
        }
        .subscribe()
    }

}

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(AddressBackendConfiguration::class)
annotation class EnableAddressBackend

fun AddressEntity.toAddressMessage(): AddressMessage {
    return AddressMessage(
        id = this.id,
        houseNumber = this.houseNumber,
        streetNumber = this.streetNumber,
        streetAddress1 = this.streetAddress1,
        streetAddress2 = this.streetAddress2,
        city = this.city,
        postcode = this.postcode
    )
}

fun Address.toAddressEntity(): AddressEntity {
    return AddressEntity(
        id = this.id,
        houseNumber = this.houseNumber,
        streetNumber = this.streetNumber,
        streetAddress1 = this.streetAddress1,
        streetAddress2 = this.streetAddress2,
        city = this.city,
        postcode = this.postcode
    )
}

@Entity
@Table(name = "address")
open class AddressEntity(
    @Id
    open var id: String? = UUID.randomUUID().toString(),
    open var houseNumber: String? = null,
    open var streetNumber: String? = null,
    open var streetAddress1: String? = null,
    open var streetAddress2: String? = null,
    open var city: String? = null,
    open var postcode: String? = null,

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AddressEntity

        if (id != other.id) return false
        if (houseNumber != other.houseNumber) return false
        if (streetNumber != other.streetNumber) return false
        if (streetAddress1 != other.streetAddress1) return false
        if (streetAddress2 != other.streetAddress2) return false
        if (city != other.city) return false
        if (postcode != other.postcode) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (houseNumber?.hashCode() ?: 0)
        result = 31 * result + (streetNumber?.hashCode() ?: 0)
        result = 31 * result + (streetAddress1?.hashCode() ?: 0)
        result = 31 * result + (streetAddress2?.hashCode() ?: 0)
        result = 31 * result + (city?.hashCode() ?: 0)
        result = 31 * result + (postcode?.hashCode() ?: 0)
        return result
    }
}


data class AddressMessage(
    var id: String? = UUID.randomUUID().toString(),
    var houseNumber: String? = null,
    var streetNumber: String? = null,
    var streetAddress1: String? = null,
    var streetAddress2: String? = null,
    var city: String? = null,
    var postcode: String? = null,
)