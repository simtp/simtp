package org.simtp.address.api

import java.util.*

const val ADDRESSES_STREAM_NAME = "addresses"
data class Address(
    var id: String? = UUID.randomUUID().toString(),
    var houseNumber: String? = null,
    var streetNumber: String? = null,
    var streetAddress1: String? = null,
    var streetAddress2: String? = null,
    var city: String? = null,
    var postcode: String? = null,
)

interface IAddressApi