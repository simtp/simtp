package org.simtp.accp.starter

import com.amazon.corretto.crypto.provider.AmazonCorrettoCryptoProvider
import com.amazon.corretto.crypto.provider.SelfTestStatus
import org.apache.commons.lang3.SystemUtils
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.context.annotation.*
import org.springframework.core.annotation.Order
import org.springframework.core.type.AnnotatedTypeMetadata


@Configuration
@Order(Integer.MIN_VALUE)
@Conditional(OnUnixCondition::class)
open class AccpAutoConfiguration {

    init {
        AmazonCorrettoCryptoProvider.install()
    }


    @Bean
    open fun accpHealthIndicator(): HealthIndicator {
        return HealthIndicator {
            when (AmazonCorrettoCryptoProvider.INSTANCE.loadingError == null
                    && AmazonCorrettoCryptoProvider.INSTANCE.runSelfTests() == SelfTestStatus.PASSED) {
                true -> Health.up()
                false -> Health.status("WARNING").withDetail("msg", "Corretto Crypto Provider is not in use")
            }
            .build()
        }
    }

}

internal class OnUnixCondition : Condition {
    override fun matches(
            context: ConditionContext,
            metadata: AnnotatedTypeMetadata): Boolean {
        return SystemUtils.IS_OS_LINUX
    }
}