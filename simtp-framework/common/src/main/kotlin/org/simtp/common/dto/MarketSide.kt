package org.simtp.common.dto

/**
 * User: Grant Little <grant.little></grant.little>@inventivesoftware.com.au>
 */
enum class MarketSide {
    BUY,
    SELL
}
