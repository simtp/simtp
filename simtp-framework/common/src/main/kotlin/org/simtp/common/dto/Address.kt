package org.simtp.common.dto

/**
 * A postal address.
 */
data class Address(
    val street: String, // The street address (Max length: 100)
    val city: String, // The city (Max length: 50)
    val state: String, // The state (Max length: 50)
    val zipCode: String, // The postal/ZIP code (Max length: 15)
    val country: CountryCode? = null // The 2-character ISO 3166-1 alpha-2 country code (Optional)
)