package org.simtp.common.dto

import java.io.Serializable
import java.math.BigDecimal

/**
 */
data class PriceDto(var securityCode : String? = null,
                    var exchangeCode : String? = null,
                    var high : BigDecimal? = null,
                    var low : BigDecimal? = null,
                    var open : BigDecimal? = null,
                    var close : BigDecimal? = null,
                    var bestBid : BigDecimal? = null,
                    var bestAsk: BigDecimal? = null) : Serializable