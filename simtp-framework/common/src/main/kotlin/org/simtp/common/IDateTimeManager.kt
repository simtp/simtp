package org.simtp.common

import java.time.LocalDateTime
import java.time.ZonedDateTime

/**
 * User: grant
 */
interface IDateTimeManager {

    val currentZonedDateTime: ZonedDateTime

    val currentLocalDateTime: LocalDateTime
}

