package org.simtp.common.dto

/**
 * User: grant
 */
enum class OrderType {

    NEW,
    AMEND,
    CANCEL
}
