package org.simtp.common.serialization

import com.google.gson.Gson
import jakarta.annotation.PostConstruct
import org.springframework.stereotype.Component


/**
 * User: grant
 */
@Component
class JsonDeserializerImpl : JsonDeserializer {

    private var gson: Gson? = null

    @PostConstruct
    fun init() {
        gson = Gson()
    }

    override fun <T> toObject(json: String, klass: Class<T>): T {
        return gson!!.fromJson(json, klass)
    }
}
