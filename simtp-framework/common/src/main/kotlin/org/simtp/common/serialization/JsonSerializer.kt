package org.simtp.common.serialization

/**
 * User: grant
 */
interface JsonSerializer {

    fun toJson(`object`: Any): String
}
