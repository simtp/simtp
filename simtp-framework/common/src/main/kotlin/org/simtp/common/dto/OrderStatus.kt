package org.simtp.common.dto

/**
 * User: grant
 */
enum class OrderStatus {

    ON_MARKET,
    CANCELLED,
    TRADED
}
