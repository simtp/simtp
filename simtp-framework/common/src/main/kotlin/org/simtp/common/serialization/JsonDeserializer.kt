package org.simtp.common.serialization

/**
 * User: grant
 */
interface JsonDeserializer {

    fun <T> toObject(json: String, klass: Class<T>): T

}
