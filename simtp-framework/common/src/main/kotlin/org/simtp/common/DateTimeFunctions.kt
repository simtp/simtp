package org.simtp.common

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

fun LocalDateTime.toISOString(): String {
    return ZonedDateTime.of(this, ZoneId.systemDefault()).format( DateTimeFormatter.ISO_INSTANT )
}

fun LocalDate.toISOString(): String {
    return this.atStartOfDay(ZoneId.systemDefault()).format( DateTimeFormatter.ofPattern("yyyy-MM-dd") )
}

fun ZonedDateTime.toISOString(): String {
    return this.format( DateTimeFormatter.ISO_INSTANT )
}

fun String.toZonedDateTime(): ZonedDateTime {
    return ZonedDateTime.parse(this)
}

fun String.toLocalDateTime(): LocalDateTime {
    return LocalDateTime.parse(this)
}

fun String.toLocalDate(): LocalDate {
    return LocalDate.parse(this)
}