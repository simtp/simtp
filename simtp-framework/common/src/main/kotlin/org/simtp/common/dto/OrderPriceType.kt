package org.simtp.common.dto

/**
 * User: grant
 */
enum class OrderPriceType {

    MARKET,
    LIMIT
}
