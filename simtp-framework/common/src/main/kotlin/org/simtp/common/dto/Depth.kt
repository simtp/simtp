package org.simtp.common.dto

import java.io.Serializable
import java.math.BigDecimal

/**
 * Created by Grant Little grant@grantlittle.me
 */
data class DepthLineDto(val securityCode: String? = null,
                        val exchangeCode: String? = null,
                        val buyOrders : Int? = null,
                        val sellOrders: Int? = null,
                        val price: BigDecimal? = null) : Serializable

data class DepthLineKey(val securityCode: String? = null,
                        val exchangeCode: String? = null,
                        val price: BigDecimal? = null)  {
}