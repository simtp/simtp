package org.simtp.common.dto

import java.math.BigDecimal

/**
 * Created by Grant Little grant@grantlittle.me
 */
data class VettingValidationRequest(val accountId: String? = null,
                                    val price: BigDecimal? = null,
                                    val side: MarketSide? = null,
                                    val quantity: Int? = null)
