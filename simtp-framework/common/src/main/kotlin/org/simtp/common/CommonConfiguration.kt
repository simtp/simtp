package org.simtp.common

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(CommonConfiguration::class)
annotation class EnableCommonConfiguration

@Configuration
open class CommonConfiguration {

    @Bean
    open fun dateTimeManager(): DateTimeManager {
        return DateTimeManager()
    }
}