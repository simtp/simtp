package org.simtp.common.reactive

import org.springframework.web.reactive.function.client.WebClientResponseException
import reactor.core.publisher.Mono
import reactor.util.retry.Retry
import java.time.Duration

fun <T> Mono<T>.retryOn4xx(maxAttempts: Long = 5, backoffPeriod: Duration = Duration.ofMillis(200)): Mono<T> {
    return this.retryWhen(
        Retry.backoff(maxAttempts, backoffPeriod)
            .filter {
                    throwable -> throwable is WebClientResponseException && throwable.statusCode.is4xxClientError
            }
            .onRetryExhaustedThrow { _, signal ->
                signal.failure()
            }
    )
}