package org.simtp.common.serialization

import com.google.gson.Gson
import jakarta.annotation.PostConstruct
import org.springframework.stereotype.Component


/**
 * User: grant
 */
@Component
class JsonSerializerImpl : JsonSerializer {

    private var gson: Gson? = null

    @PostConstruct
    fun init() {
        gson = Gson()
    }

    override fun toJson(`object`: Any): String {
        return gson!!.toJson(`object`)
    }
}
