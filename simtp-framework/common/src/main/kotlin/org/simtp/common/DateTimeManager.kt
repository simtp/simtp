package org.simtp.common

import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.ZonedDateTime


/**
 * User: grant
 */
@Component
class DateTimeManager : IDateTimeManager {
    override val currentZonedDateTime: ZonedDateTime
        get() = ZonedDateTime.now()

    override val currentLocalDateTime: LocalDateTime
        get() = LocalDateTime.now()
}
