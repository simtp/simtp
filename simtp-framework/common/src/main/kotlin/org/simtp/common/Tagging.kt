@file:Suppress("unused")

package org.simtp.common

import java.io.Serializable

/**
 */
interface IHasTags : Serializable {
    var tags: Map<String, String>?
}

interface IHasLabels : Serializable {
    var labels: Set<String>?
}






