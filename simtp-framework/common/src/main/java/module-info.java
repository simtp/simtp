module org.simtp.common {
    // Specify Java modules required by the dependencies
    requires java.base; // Implicitly added but good to be explicit
    requires kotlin.stdlib;
    requires spring.beans;
    requires spring.core;
    requires spring.context;
    requires spring.webflux;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires com.fasterxml.jackson.kotlin;
    requires com.google.gson;
    requires jakarta.annotation;
    requires reactor.core;
    requires jdk.naming.rmi;
    
    // Open or export packages as necessary
    // You may need to add specific package exports or opens here per your project setup
//    opens your.package.name to spring.core; // Example of opening for reflection
    exports org.simtp.common; // Exported packages

    // Specify that certain modules are only needed for testing
    // (Note: this usage is more complex and usually not part of `module-info.java`)
    // requires static org.junit.jupiter.api;
    // requires static org.junit.jupiter.engine;
}