package org.simtp.common

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.time.LocalDate

class DateTimeFunctionsKtTest {

    @Test
    fun toLocalDate() {
        val date = LocalDate.now()
        val dateAsString = date.toString()
        val result = dateAsString.toLocalDate()
        assertEquals(date, result)
    }
}