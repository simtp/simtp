package org.simtp.common

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CommonConfigurationTest {

    @Test
    fun configure() {
        val commonConfiguration = CommonConfiguration()

        assertNotNull(commonConfiguration.dateTimeManager().currentZonedDateTime)

    }

}