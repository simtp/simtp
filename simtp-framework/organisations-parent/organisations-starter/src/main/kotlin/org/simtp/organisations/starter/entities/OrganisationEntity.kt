package org.simtp.organisations.starter.entities

import java.util.*
import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

/**
 * Created by Grant Little grant@grantlittle.me
 */
@RedisHash
open class OrganisationEntity(
        @Id
        open var id: String? = UUID.randomUUID().toString(),

        open var name: String? = null
)
