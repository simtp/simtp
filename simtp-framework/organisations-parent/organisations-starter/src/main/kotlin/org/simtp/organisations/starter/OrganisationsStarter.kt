package org.simtp.organisations.starter

import org.simtp.kotlin.spring.web.responseEntity
import org.simtp.organisations.starter.entities.OrganisationEntity
import org.springframework.cache.annotation.Cacheable
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.cache.RedisCacheConfiguration
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer
import org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.time.Duration


@Configuration
@ComponentScan
@EnableRedisRepositories
open class OrganisationsConfiguration {

    @Bean
    open fun cacheConfiguration(): RedisCacheConfiguration? {
        return RedisCacheConfiguration.defaultCacheConfig()
            .entryTtl(Duration.ofSeconds(60))
            .disableCachingNullValues()
            .serializeValuesWith(SerializationPair.fromSerializer(GenericJackson2JsonRedisSerializer()))
    }
}


@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(OrganisationsConfiguration::class)
annotation class EnableOrganisations


@Repository
interface OrganisationsRepository: CrudRepository<OrganisationEntity, String>, PagingAndSortingRepository<OrganisationEntity, String>

@RestController
@RequestMapping("/api/organisations/1.0")
open class OrganisationsRestController(private val organisationsRepository: OrganisationsRepository) {

    @PostMapping
//    @Operation(summary = "Create a new organisation")
    @Transactional(propagation = Propagation.REQUIRED)
    open fun post(@RequestBody entity: OrganisationEntity): ResponseEntity<OrganisationEntity> {
        return organisationsRepository.save(entity).responseEntity()
    }

    @GetMapping
    @Transactional(propagation = Propagation.REQUIRED)
    @Cacheable("organisations")
    open fun get(): ResponseEntity<List<OrganisationEntity>> {
        return organisationsRepository.findAll().toList().responseEntity()
    }

    @GetMapping(path = ["/{id}"])
    @Transactional(propagation = Propagation.REQUIRED)
    open fun get(@PathVariable("id") id: String): ResponseEntity<OrganisationEntity> {
        return organisationsRepository.findById(id).responseEntity()
    }
}
