module configuration {
    // Requires Kotlin standard library and JDK 8 support
    requires kotlin.stdlib;

    // Requires Spring Framework modules
    requires spring.beans;
    requires spring.core;
    requires spring.context;
    requires spring.context.support;

    // Requires Jackson modules
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jsr310;

    // Requires Jakarta Annotations
    requires jakarta.annotation;

    // Requires Gson
    requires com.google.gson;

    // Optional test dependencies, if needed in module descriptor
    // These are typically not required in module-info, as tests are generally not modularized.
    // requires org.junit.jupiter.api;
    // requires org.junit.jupiter.engine;
}