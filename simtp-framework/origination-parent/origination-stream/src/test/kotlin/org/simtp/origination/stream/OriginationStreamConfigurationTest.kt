package org.simtp.origination.stream

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.origination.api.Applicant
import org.simtp.origination.api.Application
import org.simtp.stream.redis.RedisStream.Companion.createConsumerGroup
import org.simtp.stream.redis.RedisStream.Companion.createStreamReceiver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.connection.stream.Consumer
import org.springframework.data.redis.connection.stream.MapRecord
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


@SpringBootTest
@Testcontainers
@Disabled
@Tag("integration")
class OriginationConfigurationTest {
    companion object {

        @Container
        @ServiceConnection
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)
                .withNetwork(Network.SHARED)

        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
        }
        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
        }


    }

    @Autowired
    private lateinit var reactiveRedisOperations: ReactiveRedisOperations<String, String>

    @Autowired
    private lateinit var lettuceConnectionFactory: LettuceConnectionFactory

    @Autowired
    private lateinit var objectMapper: ObjectMapper


    @Test
    fun lifecycle() {

        val latch = CountDownLatch(1)
        val contactMessage = Applicant(
            id = UUID.randomUUID().toString(),
            firstName = "John",
            lastName = "Smith",
            email = "john@smith.me",
            mobile = "041000000"
        )
        val originationData = Application(
            applicants = mutableMapOf(
                "primary" to contactMessage
            )
        )
        val received = CountDownLatch(1)
        var receivedCount = 0

        val disposable = createConsumerGroup(
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = ORIGINATION_STREAM_KEY,
            groupName = "OriginationTest"
        )
        .flux()
        .flatMap {
            createStreamReceiver(
                connectionFactory = lettuceConnectionFactory,
                streamName = ORIGINATION_STREAM_KEY,
                consumer = Consumer.from("OriginationTest", "OriginationTest")
            )
        }
        .subscribe {
            received.countDown()
            receivedCount++
        }



        val record: MapRecord<String, String, String> =
            StreamRecords
            .mapBacked<String, String, String>(mapOf("json" to objectMapper.writeValueAsString(originationData)))
            .withStreamKey(ORIGINATION_STREAM_KEY)

        reactiveRedisOperations
            .opsForStream<String, String>()
            .add(record)
            .subscribe { latch.countDown() }

        latch.await(10, TimeUnit.SECONDS)

        received.await(10, TimeUnit.SECONDS)

        assertEquals(1, receivedCount)

        disposable.dispose()

    }
}

@SpringBootApplication
@EnableOriginationStream
open class ContactsStreamConfigurationTestApp {


    @Bean
    open fun objectMapper(): ObjectMapper {
        return ObjectMapper()
    }


}
