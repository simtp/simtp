package org.simtp.origination.stream

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

internal const val ORIGINATION_STREAM_KEY = "stream.org.simtp.origination"

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(OriginationStreamConfiguration::class)
annotation class EnableOriginationStream

@Configuration
open class OriginationStreamConfiguration {

    @Bean
    @ConfigurationProperties("simtp.origination.stream")
    open fun originationStreamProperties(): OriginationStreamProperties {
        return OriginationStreamProperties()
    }
}

data class OriginationStreamProperties(
    var name: String = ORIGINATION_STREAM_KEY
)

