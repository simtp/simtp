package org.simtp.origination.api

//import jakarta.validation.constraints.Email
//import jakarta.validation.constraints.NotBlank
//import jakarta.validation.constraints.NotNull
//import jakarta.validation.constraints.Size
//import org.simtp.common.dto.Address
//import org.springframework.validation.annotation.Validated
//import java.time.LocalDate
//import java.time.OffsetDateTime
//
//@Validated
//data class Applicant(
//    @field:NotBlank @field:Size(max = 36)
//    val userId: String,
//
//    @field:NotBlank @field:Size(max = 50)
//    val firstName: String,
//
//    @field:NotBlank @field:Size(max = 50)
//    val lastName: String,
//
//    @field:NotBlank @field:Size(max = 100) @field:Email
//    val email: String,
//
//    @field:NotBlank @field:Size(min = 8, max = 36)
//    val password: String,
//
//    @field:Size(max = 15)
//    val phoneNumber: String? = null,
//
//    val dateOfBirth: LocalDate? = null,
//
//    val address: Address? = null,
//
//    @field:NotNull
//    val createdAt: OffsetDateTime
//)