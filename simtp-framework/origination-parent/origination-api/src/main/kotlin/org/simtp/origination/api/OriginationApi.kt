package org.simtp.origination.api

import org.simtp.kotlin.util.uuid

import reactor.core.publisher.Mono
import java.math.BigDecimal
import java.time.ZonedDateTime

interface IOriginationCommandApi {
    fun persist(email: String, firstName: String, lastName: String): Mono<String>

    fun persist(originationData: Application): Mono<String>

}

interface IApplicationDataSource {

    fun save(originationData: Application): Mono<Application>

    fun findById(id: String): Mono<Application>
}

data class Application(
    var id: String = uuid(),
    var applicants: MutableMap<String, Applicant>? = null,
    var acceptedTerms: Boolean? = false,
    var account: ApplicationAccount? = null,
    var approved: Boolean = false,
    var version: Long? = null

)

data class Applicant(
    var id: String = uuid(),
    var firstName: String? = null,
    var lastName: String? = null,
    var email: String? = null,
    var mobile: String? = null,
    var password: String? = null,
    var addresses: Map<String, ApplicantAddress>? = null,
    var version: Long? = null,
    var dateOfBirth: ZonedDateTime? = null
)

data class ApplicantAddress(
    var id: String = uuid(),
    var houseNumber: String? = null,
    var streetNumber: String? = null,
    var streetAddress1: String? = null,
    var streetAddress2: String? = null,
    var city: String? = null,
    var postcode: String? = null,
    var version: Long? = null
)

data class ApplicationAccount(
    val id: String = uuid(),
    var type: String? = null,
    var settlementAccounts: Map<String, ApplicationSettlementAccount> = emptyMap(),
    var labels : Set<String>? = null,
    var version: Long? = null

)


data class ApplicationSettlementAccount(
    var id: String = uuid(),
    var name: String? = null,
    var balance: BigDecimal? = null,
    var labels : Set<String>? = null,
    var version: Long? = null

)