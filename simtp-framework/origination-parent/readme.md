# Origination

The origination module provides the basic building blocks to originate new clients etc 

It is quite possible that you might want to extend this functionality of this module based on your own needs, 
but you should find the basic building blocks here

# Origination States

![Origination States](./docs/OriginationStates.svg)

