package org.simtp.origination.rest.client


import org.simtp.origination.api.IOriginationCommandApi
import org.simtp.origination.api.Application
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

@Suppress("unused")
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(OriginationRestClientConfiguration::class)
annotation class EnableOriginationRestClient

data class OriginationRestClientSettings(
    var baseUrl: String = "lb://origination/origination-1.0"
)

@Configuration
open class OriginationRestClientConfiguration {

    @Bean
    @ConfigurationProperties("simtp.origination.rest.client")
    open fun originationSettings(): OriginationRestClientSettings {
        return OriginationRestClientSettings()
    }

    @Bean
    open fun originationRestClient(originationRestClientSettings: OriginationRestClientSettings): OriginationCommandRestClient {
        return OriginationCommandRestClient(
            WebClient.create(originationRestClientSettings.baseUrl)
        )
    }
}

class OriginationCommandRestClient(
    private val webClient: WebClient,
): IOriginationCommandApi {

    override fun persist(email: String, firstName: String, lastName: String): Mono<String> {
        return webClient
            .put()
            .uri("/{email}/{firstName}/{lastName}", mapOf(
                "email" to email,
                "firstName" to firstName,
                "lastName" to lastName
            ))
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(String::class.java)

    }

    override fun persist(originationData: Application): Mono<String> {
        return webClient
            .put()
            .bodyValue(BodyInserters.fromValue(originationData))
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(String::class.java)

    }

}


