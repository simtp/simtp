package org.simtp.origination.rest.client

import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.simtp.origination.api.Application
import org.springframework.web.reactive.function.client.WebClient
import java.io.IOException
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.time.Duration


class OriginationRestClientTest {

    private var mockWebServer: MockWebServer? = null

    @BeforeEach
    @Throws(IOException::class)
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer?.start()
    }

    @AfterEach
    fun tearDown() {
        mockWebServer?.shutdown()
    }

    @Test
    @DisplayName("Test basic registration")
    fun register() {
        val response = MockResponse()
            .setBody(expectedResponse)
            .addHeader("Content-Type", "application/json")

        mockWebServer!!.enqueue(response)

        val webClient = WebClient
            .create("http://localhost:${mockWebServer!!.port}/api/origination/1.0")

        val client = OriginationCommandRestClient(webClient)
        val actual = client
            .persist(
                firstName = "Joe",
                lastName = "Bloggs",
                email = "joe.bloggs@simtp.org"
            )
            .block(Duration.ofSeconds(2))

        val expected = ObjectMapper().readValue(originationDataStr, Application::class.java)
        assertEquals(expected.id, actual)
        val request = mockWebServer!!.takeRequest()
        assertEquals("PUT", request.method)
        val encodedEmail = URLEncoder.encode("joe.bloggs@simtp.org", StandardCharsets.UTF_8)
        assertEquals("http://${mockWebServer!!.hostName}:${mockWebServer!!.port}/api/origination/1.0/$encodedEmail/Joe/Bloggs", request.requestUrl.toString())
    }

    @Test
    @DisplayName("Test advanced registration")
    fun registerComplete() {
        val response = MockResponse()
            .setBody(expectedResponse)
            .addHeader("Content-Type", "application/json")

        mockWebServer!!.enqueue(response)

        val webClient = WebClient
            .create("http://localhost:${mockWebServer!!.port}/api/origination/1.0")

        val expected = ObjectMapper().readValue(originationDataStr, Application::class.java)
        val client = OriginationCommandRestClient(webClient)
        val actual = client
            .persist(expected)
            .block(Duration.ofSeconds(2))

        assertEquals(expected.id, actual)
        val request = mockWebServer!!.takeRequest()
        assertEquals("PUT", request.method)
        assertEquals("http://${mockWebServer!!.hostName}:${mockWebServer!!.port}/api/origination/1.0", request.requestUrl.toString())
    }

//    @Test
//    @DisplayName("Get an existing origination")
//    fun getOrigination() {
//        val response = MockResponse()
//            .setBody(originationDataStr)
//            .addHeader("Content-Type", "application/json")
//
//        mockWebServer!!.enqueue(response)
//
//        val webClient = WebClient
//            .create("http://localhost:${mockWebServer!!.port}/api/origination/1.0")
//
//
//        val expected = ObjectMapper().readValue(originationDataStr, Application::class.java)
//        val client = OriginationCommandRestClient(webClient)
//        val actual = client
//            .get("123")
//            .block(Duration.ofSeconds(2))
//
//        assertEquals(expected, actual)
//
//        val request = mockWebServer!!.takeRequest()
//        assertEquals("GET", request.method)
//        assertEquals("http://${mockWebServer!!.hostName}:${mockWebServer!!.port}/api/origination/1.0/${expected.id}", request.requestUrl.toString())
//    }

//    @Test
//    @DisplayName("Update an existing origination")
//    fun update() {
//        val response = MockResponse()
//            .setBody(expectedResponse)
//            .addHeader("Content-Type", "application/json")
//
//        mockWebServer!!.enqueue(response)
//
//        val webClient = WebClient
//            .create("http://localhost:${mockWebServer!!.port}/api/origination/1.0")
//
//        val originationData = jacksonObjectMapper().readValue(originationDataStr, Application::class.java)
//
//        val client = OriginationCommandRestClient(webClient)
//        val actual = client
//            .update(originationData.id, originationData)
//            .block(Duration.ofSeconds(2))
//
//        assertEquals(originationData.id, actual)
//
//        val request = mockWebServer!!.takeRequest()
//        assertEquals("PATCH", request.method)
//        assertEquals("http://${mockWebServer!!.hostName}:${mockWebServer!!.port}/api/origination/1.0/${originationData.id}", request.requestUrl.toString())
//    }
}

private const val expectedResponse = "123"

private val originationDataStr = """
{
    "id": "123",
    "applicants": {
        "primary": {
            "id": "111",
            "firstName": "Joe",
            "lastName": "Bloggs",
            "email": "joe.bloggs@simtp.org"
        }
    }
}
""".trimIndent()

