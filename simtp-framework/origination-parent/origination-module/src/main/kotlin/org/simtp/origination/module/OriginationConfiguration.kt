package org.simtp.origination.module

import org.simtp.origination.api.IOriginationCommandApi
import org.simtp.origination.api.IApplicationDataSource
import org.simtp.origination.api.Application
import org.simtp.stream.kafka.EnableSimtpKafka
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(OriginationConfiguration::class)
annotation class EnableOrigination

@Configuration
@ComponentScan
@EnableSimtpKafka
open class OriginationConfiguration {

    @Bean
    open fun originationCommandService(
        originationDataSource: IApplicationDataSource,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        @Value("\${simtp.origination.stream.name:stream.org.simtp.origination}")topicName: String
    ): OriginationCommandService {
        return OriginationCommandService(
            originationDataSource = originationDataSource,
            streamWriter = { id, data ->
                reactiveKafkaProducerTemplate
                    .send(topicName, "", data)
                    .map { id }
            }
        )
    }
}

@RestController
@RequestMapping("/api/origination/1.0")
open class OriginationCommandRestController(
    private val originationCommandService: OriginationCommandService,
): IOriginationCommandApi {

    companion object {
        @Suppress("unused")
        private val log = LoggerFactory.getLogger(OriginationCommandRestController::class.java)
    }

    @PostMapping("/{email}/{firstName}/{lastName}")
    @Transactional(propagation = Propagation.REQUIRED)
    override fun persist(@PathVariable email: String,
                         @PathVariable firstName: String,
                         @PathVariable lastName: String): Mono<String> {
        return originationCommandService.persist(email, firstName, lastName)
    }

    @PostMapping
    @Transactional(propagation = Propagation.REQUIRED)
    override fun persist(@RequestBody application: Application): Mono<String> {
        return originationCommandService.persist(application)
    }

}