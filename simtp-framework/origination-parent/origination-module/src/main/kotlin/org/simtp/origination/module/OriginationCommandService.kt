package org.simtp.origination.module

import org.simtp.common.dto.Address
import org.simtp.origination.api.Applicant
import org.simtp.origination.api.IOriginationCommandApi
import org.simtp.origination.api.IApplicationDataSource
import org.simtp.origination.api.Application
import org.simtp.spec.ApplicationApproved
import reactor.core.publisher.Mono
import java.time.ZonedDateTime.now
import org.simtp.spec.Application as AvroApplication
import org.simtp.spec.Applicant as AvroApplicant
import org.simtp.spec.Address as AvroAddress
import org.simtp.spec.CountryCode as AvroCountryCode

class OriginationCommandService(
    private val originationDataSource: IApplicationDataSource,
    private val streamWriter: (String, Any) -> Mono<String>,

): IOriginationCommandApi {

    override fun persist(
        email: String,
        firstName: String,
        lastName: String
    ): Mono<String> {
        return Application(
            applicants = mutableMapOf(
                "primary" to Applicant(
                    email = email,
                    lastName = lastName,
                    firstName = firstName
                )
            )
        ).let {
            persist(it)
        }
    }

    override fun persist(application: Application): Mono<String> {
        return originationDataSource
            .save(application)
            .flatMap { it -> streamWriter(application.id, it.toAvroApplication()) }
            .flatMap { it -> streamWriter(application.id, ApplicationApproved(it))}
            .map { application.id }
    }

}

fun Application.toAvroApplication(): AvroApplication {
    val value = this
    return AvroApplication().apply {
        this.id = value.id
        this.acceptedTerms = value.acceptedTerms
        this.applicants = value.applicants?.mapValues { entry ->
            // Assuming it's a map with `Contact.toAvroContact` defined elsewhere
            entry.value.toAvroApplicant()
        }
        this.approved = value.approved
        this.version = value.version
    }
}

fun Applicant.toAvroApplicant(): AvroApplicant {
    return AvroApplicant().apply {
        this.userId = this@toAvroApplicant.id
        this.firstName = this@toAvroApplicant.firstName
        this.lastName = this@toAvroApplicant.lastName
        this.email = this@toAvroApplicant.email
        this.phoneNumber = this@toAvroApplicant.mobile
        this.dateOfBirth = this@toAvroApplicant.dateOfBirth?.toString() // Convert LocalDate to String
        this.createdAt = now().toString() // Convert OffsetDateTime to String
//        this.address = this@toAvroApplicant.address?.toAvroAddress() // Handle Address mapping
    }
}

fun Address.toAvroAddress(): AvroAddress {
    return AvroAddress().apply {
        this.street = this@toAvroAddress.street
        this.city = this@toAvroAddress.city
        this.state = this@toAvroAddress.state
        this.zipCode = this@toAvroAddress.zipCode
        this.country = this@toAvroAddress.country?.let { AvroCountryCode.valueOf(it.name) } // Map country code
    }
}


