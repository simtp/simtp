package org.simtp.origination.module

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.origination.api.Applicant
import org.simtp.origination.api.ApplicantAddress
import org.simtp.origination.api.Application
import org.simtp.origination.stream.EnableOriginationStream
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.web.reactive.function.client.WebClient
import org.testcontainers.junit.jupiter.Testcontainers
import reactor.test.StepVerifier


@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = [
        "spring.kafka.properties.schema.registry.url=mock://localhost",
    ]
)
@Testcontainers
@Tag("integration")
@EnableOrigination
@EnableOriginationStream
@EnableRedisStream
@EmbeddedKafka(partitions = 1,
    topics = ["stream.org.simtp.origination" ],
    brokerProperties = [
        "listeners=PLAINTEXT://localhost:9092",
        "port=9092",
        "schema.registry.url=mock://localhost"
    ]
)
internal class OriginationConfigurationTest {

    @LocalServerPort
    private var localServerPort: Int = 8888

    @Test
    fun simpleRegistration() {
        val webClient = WebClient
            .create("http://localhost:$localServerPort/api/origination/1.0")

        val originationData = Application(
            applicants = mutableMapOf(
                "primary" to Applicant(
                    firstName = "John",
                    lastName = "Smith",
                    email = "johnsmith@error.com",
                    mobile = "12345678",
                    addresses = mapOf(
                        "work" to ApplicantAddress(
                            houseNumber = "1",
                            streetNumber = "2",
                            streetAddress1 = "streetAddress1",
                            streetAddress2 = "streetAddress2",
                            city = "city",
                            postcode = "postCode",
                        )
                    )
                )
            )
        )

        StepVerifier
            .create(webClient
                .post()
//            .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(originationData)
                .retrieve()
                .bodyToMono(String::class.java))
            .expectNextMatches { it != null }
            .verifyComplete()
    }
}

