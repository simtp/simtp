package org.simtp.origination.module

import org.simtp.origination.data.source.inmemory.EnableInMemoryOriginationDataSource
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
@EnableInMemoryOriginationDataSource
open class OriginationTestApp