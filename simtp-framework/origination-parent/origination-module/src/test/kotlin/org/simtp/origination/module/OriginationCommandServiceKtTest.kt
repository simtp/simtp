package org.simtp.origination.module

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.simtp.common.dto.Address
import org.simtp.common.dto.CountryCode
import org.simtp.origination.api.Applicant
import org.simtp.origination.api.Application
import java.time.ZonedDateTime.now
import kotlin.test.assertNotNull
import org.simtp.spec.CountryCode as AvroCountryCode

class OriginationCommandServiceKtTest {

    @Test
    fun `toAvroAddress should map all fields correctly`() {
        val address = Address(
            street = "123 Main St",
            city = "Springfield",
            state = "IL",
            zipCode = "62704",
            country = CountryCode.US
        )

        val avroAddress = address.toAvroAddress()

        assertEquals("123 Main St", avroAddress.street.toString())
        assertEquals("Springfield", avroAddress.city.toString())
        assertEquals("IL", avroAddress.state.toString())
        assertEquals("62704", avroAddress.zipCode.toString())
        assertEquals(AvroCountryCode.US, avroAddress.country)
    }

    @Test
    fun `toAvroAddress should handle null country`() {
        val address = Address(
            street = "456 Elm St",
            city = "Shelbyville",
            state = "IN",
            zipCode = "46176",
            country = null
        )

        val avroAddress = address.toAvroAddress()

        assertEquals("456 Elm St", avroAddress.street.toString())
        assertEquals("Shelbyville", avroAddress.city.toString())
        assertEquals("IN", avroAddress.state.toString())
        assertEquals("46176", avroAddress.zipCode.toString())
        assertEquals(null, avroAddress.country)
    }

    @Test
    fun `toAvroAddress should handle empty fields`() {
        val address = Address(
            street = "",
            city = "",
            state = "",
            zipCode = "",
            country = null
        )

        val avroAddress = address.toAvroAddress()

        assertEquals("", avroAddress.street.toString())
        assertEquals("", avroAddress.city.toString())
        assertEquals("", avroAddress.state.toString())
        assertEquals("", avroAddress.zipCode.toString())
        assertEquals(null, avroAddress.country)
    }
    @Test
    fun `toAvroApplicant should map all fields correctly`() {
        val applicant = Applicant(
            id = "1",
            firstName = "John",
            lastName = "Doe",
            email = "john.doe@example.com",
            mobile = "+123456789",
            dateOfBirth = now(),
            addresses = null // addresses not passed in this test
        )

        val avroApplicant = applicant.toAvroApplicant()

        assertEquals("1", avroApplicant.userId.toString())
        assertEquals("John", avroApplicant.firstName.toString())
        assertEquals("Doe", avroApplicant.lastName.toString())
        assertEquals("john.doe@example.com", avroApplicant.email.toString())
        assertEquals("+123456789", avroApplicant.phoneNumber.toString())
        assertEquals(applicant.dateOfBirth?.toString(), avroApplicant.dateOfBirth.toString())
        assertNotNull(avroApplicant.createdAt)
    }

    @Test
    fun `toAvroApplicant should handle null fields`() {
        val applicant = Applicant(
            id = "2",
            firstName = null,
            lastName = null,
            email = null,
            mobile = null,
            dateOfBirth = null,
            addresses = null
        )

        val avroApplicant = applicant.toAvroApplicant()

        assertEquals("2", avroApplicant.userId.toString())
        assertEquals(null, avroApplicant.firstName)
        assertEquals(null, avroApplicant.lastName)
        assertEquals(null, avroApplicant.email)
        assertEquals(null, avroApplicant.phoneNumber)
        assertEquals(null, avroApplicant.dateOfBirth)
        assertNotNull(avroApplicant.createdAt)
    }

    @Test
    fun `toAvroApplicant should handle empty fields`() {
        val applicant = Applicant(
            id = "3",
            firstName = "",
            lastName = "",
            email = "",
            mobile = "",
            dateOfBirth = null,
            addresses = null
        )

        val avroApplicant = applicant.toAvroApplicant()

        assertEquals("3", avroApplicant.userId.toString())
        assertEquals("", avroApplicant.firstName.toString())
        assertEquals("", avroApplicant.lastName.toString())
        assertEquals("", avroApplicant.email.toString())
        assertEquals("", avroApplicant.phoneNumber.toString())
        assertEquals(null, avroApplicant.dateOfBirth)
        assertNotNull(avroApplicant.createdAt)
    }
    @Test
    fun `toAvroApplication should map all fields correctly`() {
        val application = Application(
            id = "app123",
            applicants = mutableMapOf(
                "primary" to Applicant(
                    id = "1",
                    firstName = "Alice",
                    lastName = "Smith",
                    email = "alice@example.com",
                    mobile = "+123456789",
                    dateOfBirth = now(),
                    addresses = null
                )
            ),
            acceptedTerms = true,
            approved = false,
            version = 1
        )

        val avroApplication = application.toAvroApplication()

        assertEquals("app123", avroApplication.id.toString())
        assertEquals(true, avroApplication.acceptedTerms)
        assertEquals(false, avroApplication.approved)
        assertEquals(1, avroApplication.version)

        val avroContact = avroApplication.applicants?.get("primary")
        assertNotNull(avroContact)
        assertEquals("1", avroContact.userId?.toString())
        assertEquals("Alice", avroContact.firstName?.toString())
        assertEquals("Smith", avroContact.lastName?.toString())
        assertEquals("alice@example.com", avroContact.email?.toString())
    }

    @Test
    fun `toAvroApplication should handle null fields`() {
        val application = Application(
            id = "app456",
            applicants = null,
            acceptedTerms = null,
            approved = false,
            version = null
        )

        val avroApplication = application.toAvroApplication()

        assertEquals("app456", avroApplication.id.toString())
        assertNull(avroApplication.acceptedTerms)
        assertEquals(false, avroApplication.approved)
        assertNull(avroApplication.version)
        assertNull(avroApplication.applicants)
    }

    @Test
    fun `toAvroApplication should handle empty fields`() {
        val application = Application(
            id = "",
            applicants = mutableMapOf(
                "primary" to Applicant(
                    id = "",
                    firstName = "",
                    lastName = "",
                    email = "",
                    mobile = "",
                    dateOfBirth = null,
                    addresses = null
                )
            ),
            acceptedTerms = false,
            approved = true,
            version = 0
        )

        val avroApplication = application.toAvroApplication()

        assertEquals("", avroApplication.id.toString())
        assertEquals(false, avroApplication.acceptedTerms)
        assertEquals(true, avroApplication.approved)
        assertEquals(0, avroApplication.version)

        val avroContact = avroApplication.applicants?.get("primary")
        assertNotNull(avroContact)
        assertEquals("", avroContact.userId?.toString())
        assertEquals("", avroContact.firstName?.toString())
        assertEquals("", avroContact.lastName?.toString())
        assertEquals("", avroContact.email?.toString())
    }
}