@file:Suppress("ReactiveStreamsUnusedPublisher")

package org.simtp.origination.module

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.simtp.origination.api.Applicant
import org.simtp.origination.api.Application
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

class OriginationRestControllerTest {


    @Test
    fun `Register using minimal user details`() {
        val originationCommandService = mockk<OriginationCommandService>()

        every { originationCommandService.persist("joe.bloggs@simtp.org", "Joe", "Bloggs") } returns Mono.just("1")

        val controller = OriginationCommandRestController(
            originationCommandService = originationCommandService,
        )
        val client = WebTestClient
            .bindToController(controller)
            .build()

        val result = client
            .post()
            .uri("/api/origination/1.0/{email}/{firstName}/{lastName}", mapOf(
                "email" to "joe.bloggs@simtp.org",
                "firstName" to "Joe",
                "lastName" to "Bloggs"
            ))
            .exchange()
            .expectStatus().isOk
            .expectBody(String::class.java)
            .returnResult()

        assertNotNull(result.responseBody!!)

        verify(exactly = 1) { originationCommandService.persist("joe.bloggs@simtp.org", "Joe", "Bloggs") }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun `Register using full user details`() {
        val expected = Application(
            id = "1",
            applicants = mutableMapOf(
                "primary" to Applicant(
                    id="c1",
                    firstName = "Joe",
                    lastName = "Bloggs"
                )
            )
        )
        val originationCommandService = mockk<OriginationCommandService>()

        every { originationCommandService.persist(any()) } returns Mono.just("1")

        val controller = OriginationCommandRestController(
            originationCommandService = originationCommandService
        )
        val client = WebTestClient
            .bindToController(controller)
            .build()

        val result = client
            .post()
            .uri("/api/origination/1.0")
            .bodyValue(expected)
            .exchange()
            .expectStatus().isOk
            .expectBody(String::class.java)
            .returnResult()

        assertEquals(expected.id, result.responseBody)

        verify(exactly = 1) { originationCommandService.persist(any()) }
    }

}
