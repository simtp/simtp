package org.simtp.origination.module

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.origination.api.IApplicationDataSource
import org.simtp.origination.api.Application
import org.simtp.origination.data.source.inmemory.InMemoryApplicationDataSource
import reactor.test.StepVerifier
import kotlin.test.assertEquals

@Tag("integration")
class OriginationRepositoryTest {

    private val originationDataSource: IApplicationDataSource = InMemoryApplicationDataSource()

    @Test
    fun basic() {
        val expected = Application()
        val result = originationDataSource
            .save(expected)
            .flatMap {
                originationDataSource
                    .findById(it.id)
            }
        StepVerifier
            .create(result)
            .expectNextMatches {
                assertEquals(expected, it)
                true
            }
            .expectComplete()
            .verify()

    }
}
