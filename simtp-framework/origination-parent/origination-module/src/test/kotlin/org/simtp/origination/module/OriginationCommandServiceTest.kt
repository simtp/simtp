@file:Suppress("ReactiveStreamsUnusedPublisher")

package org.simtp.origination.module

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.simtp.origination.api.Application
import org.simtp.origination.api.IApplicationDataSource
import org.simtp.origination.api.Applicant
import reactor.core.publisher.Mono
import reactor.test.StepVerifier

class OriginationCommandServiceTest {

    @Test
    fun `test persist with email, firstName, and lastName`() {
        val originationDataSource = mockk<IApplicationDataSource>()
        val streamWriter: (String, Any) -> Mono<String> = mockk<(String, Any) -> Mono<String>>()

        val service = OriginationCommandService(originationDataSource, streamWriter)

        val email = "test@example.com"
        val firstName = "John"
        val lastName = "Doe"

        val mockApplication = Application()

        every { originationDataSource.save(any()) } returns Mono.just(mockApplication)

        every { streamWriter.invoke(any(), any()) } returns Mono.just("stream-id")

        var applicationId = mockApplication.id

        val result = service.persist(email, firstName, lastName)

        StepVerifier.create(result)
            .expectNextMatches { applicationId = it; it != null }
            .verifyComplete()

        verify(exactly = 1) { originationDataSource.save(any()) }
        verify(exactly = 2) { streamWriter.invoke(any(), any()) }
    }

    @Test
    fun `test persist with Application object`() {
        val originationDataSource = mockk<IApplicationDataSource>()
        val streamWriter: (String, Any) -> Mono<String> = mockk<(String, Any) -> Mono<String>>()

        val service = OriginationCommandService(originationDataSource, streamWriter)

        val application = Application(
            id = "app-id",
            applicants = mutableMapOf(
                "primary" to Applicant(
                    id = "1",
                    firstName = "John",
                    lastName = "Doe",
                    email = "test@example.com",
                    mobile = "123456789",
                    dateOfBirth = null
                )
            ),
            acceptedTerms = true,
            approved = true,
            version = 1L
        )

        val mockApplication = application

        every { originationDataSource.save(any()) } returns Mono.just(mockApplication)

        every { streamWriter.invoke(application.id, any()) } returns Mono.just("stream-id")

        val result = service.persist(application)

        StepVerifier.create(result)
            .expectNext(application.id)
            .verifyComplete()

        verify(exactly = 1) { originationDataSource.save(any()) }
        verify(exactly = 2) { streamWriter.invoke(application.id, any()) }
    }
}