package org.simtp.origination.demo.data

import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.origination.api.*
import org.simtp.origination.stream.EnableOriginationStream
import org.simtp.origination.stream.OriginationStreamProperties
import org.simtp.stream.api.StreamMessageType
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.connection.stream.RecordId
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.redis.core.ReactiveStreamOperations
import reactor.core.publisher.Mono
import java.util.*

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(OriginationDemoDataConfiguration::class)
annotation class EnableOriginationDemoData

@Configuration
@EnableOriginationStream
open class OriginationDemoDataConfiguration {


    @Bean
    open fun demoData(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        objectMapper: ObjectMapper,
        originationStreamProperties: OriginationStreamProperties
    ): ApplicationListener<ApplicationReadyEvent> {
        return ApplicationListener<ApplicationReadyEvent> {
            val redisStreamOperations = reactiveRedisOperations.opsForStream<String, String>()
            addStreamRecord(
                reactiveStreamOperations = redisStreamOperations,
                eventType = StreamMessageType.CREATE,
                originationData = defaultData(),
                objectMapper = objectMapper,
                streamName = originationStreamProperties.name
            ).subscribe()
        }
    }
}

private fun addStreamRecord(
    reactiveStreamOperations: ReactiveStreamOperations<String, String, String>,
    eventType: StreamMessageType,
    originationData: Application,
    objectMapper: ObjectMapper,
    streamName: String
): Mono<RecordId> {
    return reactiveStreamOperations.add(
        StreamRecords
            .newRecord()
            .ofMap(mapOf(
                "event-type" to eventType.name,
                "json" to objectMapper.writeValueAsString(originationData)
            ))
            .withStreamKey(streamName))

}

internal fun defaultData(id: String = UUID.randomUUID().toString()): Application = Application(
    id = id,
    applicants = mutableMapOf(
        "primary" to Applicant(
            id = id,
            firstName = "John",
            lastName = "Smith",
            email = "johnsmith@error.com",
            mobile = "12345678",
            addresses = mapOf(
                "work" to ApplicantAddress(
                    id = id,
                    houseNumber = "1",
                    streetNumber = "2",
                    streetAddress1 = "streetAddress1",
                    streetAddress2 = "streetAddress2",
                    city = "city",
                    postcode = "postCode",
                )
            )
        )
    ),
    account = ApplicationAccount(
        id = id,
        type = "TEST",
        settlementAccounts = mapOf(
            "primary" to ApplicationSettlementAccount(
                id = id,
                name = "John Smith"
            )
        )
    )
)

