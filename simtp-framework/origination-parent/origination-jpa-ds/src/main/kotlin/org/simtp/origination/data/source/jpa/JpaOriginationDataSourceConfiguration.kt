package org.simtp.origination.data.source.jpa


import jakarta.persistence.*
import org.simtp.kotlin.util.uuid
import org.simtp.origination.api.*
import org.simtp.utilities.mapOptional
import org.simtp.utilities.scheduleForMono
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.repository.CrudRepository
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import java.math.BigDecimal

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(JpaDataSourceOriginationConfiguration::class)
annotation class EnableJpaOriginationDataSource

@Configuration
@EnableCaching
@EnableScheduling
@ComponentScan
@EnableJpaRepositories
@EntityScan
open class JpaDataSourceOriginationConfiguration {

    @Bean
    open fun jpaOriginationDataSource(
        originationJpaRepository: OriginationJpaRepository
    ): JpaApplicationDataSource {
        return JpaApplicationDataSource(originationJpaRepository)
    }
}


@Repository
interface OriginationJpaRepository: CrudRepository<OriginationDataEntity, String>

open class JpaApplicationDataSource(
    private val originationJpaRepository: OriginationJpaRepository
): IApplicationDataSource {

    @Transactional(propagation = Propagation.REQUIRED)
    override fun save(originationData: Application): Mono<Application> {
        return scheduleForMono { originationJpaRepository
            .save(originationData.toOriginationDataEntity())
            .toOriginationData()
        }
    }

    @Transactional(readOnly = true)
    override fun findById(id: String): Mono<Application> {
        return scheduleForMono { originationJpaRepository
            .findById(id)
            .map { it.toOriginationData()  }
        }.mapOptional { it }
    }
}


fun Application.toOriginationDataEntity(): OriginationDataEntity {
    return OriginationDataEntity(
        id = this.id,
        contacts = this.applicants?.map { it.value.toOriginationContactEntity(it.key) }?.toMutableList(),
        acceptedTerms = this.acceptedTerms,
        account = this.account?.toOriginationAccountEntity(),
        approved = this.approved,
        version = this.version
    )
}

fun OriginationDataEntity.toOriginationData(): Application {
    return Application(
        id = this.id,
        applicants = this.contacts?.associate { it.toOriginationContact() }?.toMutableMap(),
        acceptedTerms = this.acceptedTerms,
        account = this.account?.toOriginationAccount(),
        approved = this.approved,
        version = this.version
    )
}

fun Applicant.toOriginationContactEntity(ref: String): OriginationContactEntity {
    return OriginationContactEntity(
        id = this.id,
        friendlyName = ref,
        firstName = this.firstName,
        lastName = this.lastName,
        email = this.email,
        mobile = this.mobile,
        addresses = this.addresses?.map { it.value.toOriginationAddressEntity(it.key) }?.toMutableList(),
        version = this.version
    )
}

fun OriginationContactEntity.toOriginationContact(): Pair<String, Applicant> {
    return this.friendlyName!! to Applicant(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        email = this.email,
        mobile = this.mobile,
        addresses = this.addresses?.associate { it.toOriginationAddress() }?.toMap()?.toMutableMap(),
        version = this.version
    )
}

fun ApplicantAddress.toOriginationAddressEntity(ref: String): OriginationAddressEntity {
    return OriginationAddressEntity(
        id = this.id,
        friendlyName = ref,
        houseNumber = this.houseNumber,
        streetNumber = this.streetNumber,
        streetAddress1 = this.streetAddress1,
        streetAddress2 = this.streetAddress2,
        city = this.city,
        postcode = this.postcode,
        version = this.version
    )
}

fun OriginationAddressEntity.toOriginationAddress(): Pair<String, ApplicantAddress> {
    return this.friendlyName!! to ApplicantAddress(
        id = this.id,
        houseNumber = this.houseNumber,
        streetNumber = this.streetNumber,
        streetAddress1 = this.streetAddress1,
        streetAddress2 = this.streetAddress2,
        city = this.city,
        postcode = this.postcode,
        version = this.version
    )
}

fun ApplicationAccount.toOriginationAccountEntity(): OriginationAccountEntity {
    return OriginationAccountEntity(
        id = this.id,
        type = this.type,
        settlementAccounts = this.settlementAccounts.map { it.value.toOriginationSettlementAccountEntity(it.key) }.toMutableList(),
        labels = this.labels?.toMutableSet(),
        version = this.version
    )
}

fun OriginationAccountEntity.toOriginationAccount(): ApplicationAccount {
    return ApplicationAccount(
        id = this.id,
        type = this.type,
        settlementAccounts = this.settlementAccounts.associate { it.toOriginationSettlementAccount() },
        labels = this.labels?.toMutableSet(),
        version = this.version
    )
}

fun ApplicationSettlementAccount.toOriginationSettlementAccountEntity(key: String): OriginationSettlementAccountEntity {
    return OriginationSettlementAccountEntity(
        id = this.id,
        keyReference = key,
        name = this.name,
        balance = this.balance,
        labels = this.labels?.toMutableSet(),
        version = this.version
    )
}

fun OriginationSettlementAccountEntity.toOriginationSettlementAccount(): Pair<String, ApplicationSettlementAccount> {
    return this.keyReference!! to ApplicationSettlementAccount(
        id = this.id,
        name = this.name,
        balance = this.balance,
        labels = this.labels?.toMutableSet(),
        version = this.version
    )
}


@Entity
@Table(name = "origination_date")
open class OriginationDataEntity(

    @Id
    open var id: String = uuid(),
    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    open var contacts: MutableList<OriginationContactEntity>? = null,
    open var acceptedTerms: Boolean? = false,

    @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    open var account: OriginationAccountEntity? = null,
    open var approved: Boolean = false,
    @Version
    open var version: Long? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OriginationDataEntity

        if (id != other.id) return false
        if (contacts != other.contacts) return false
        if (acceptedTerms != other.acceptedTerms) return false
        if (account != other.account) return false
        if (approved != other.approved) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (contacts?.hashCode() ?: 0)
        result = 31 * result + (acceptedTerms?.hashCode() ?: 0)
        result = 31 * result + (account?.hashCode() ?: 0)
        result = 31 * result + approved.hashCode()
        result = 31 * result + (version?.hashCode() ?: 0)
        return result
    }
}

@Entity
@Table(name = "origination_account")
open class OriginationAccountEntity(
    @Id
    open val id: String = uuid(),

    open var type: String? = null,

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    open var settlementAccounts: MutableList<OriginationSettlementAccountEntity> = mutableListOf(),

    @ElementCollection(fetch = FetchType.EAGER)
    open var labels : MutableSet<String>? = null,

    @Version
    open var version: Long? = null

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OriginationAccountEntity

        if (id != other.id) return false
        if (type != other.type) return false
        if (settlementAccounts != other.settlementAccounts) return false
        if (labels != other.labels) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + settlementAccounts.hashCode()
        result = 31 * result + (labels?.hashCode() ?: 0)
        result = 31 * result + (version?.hashCode() ?: 0)
        return result
    }
}

@Entity
@Table(name = "origination_settlement_account")
open class OriginationSettlementAccountEntity(
    @Id
    @Column(name = "id")
    open var id: String = uuid(),

    @Column(nullable = false)
    open var keyReference: String? = null,
    @Column(nullable = false)
    open var name: String? = null,
    open var balance: BigDecimal? = null,
    @ElementCollection(fetch = FetchType.EAGER)
    open var labels : MutableSet<String>? = null,
    @Version
    open var version: Long? = null


) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OriginationSettlementAccountEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (balance != other.balance) return false
        if (labels != other.labels) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (balance?.hashCode() ?: 0)
        result = 31 * result + (labels?.hashCode() ?: 0)
        return result
    }
}

@Entity
@Table(name = "origination_contact")
open class OriginationContactEntity(
    @Id
    open var id: String = uuid(),
    open var friendlyName: String? = null,
    open var firstName: String? = null,
    open var lastName: String? = null,
    open var email: String? = null,
    open var mobile: String? = null,

    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    open var addresses: MutableList<OriginationAddressEntity>? = null,
    @Version
    open var version: Long? = null

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OriginationContactEntity

        if (id != other.id) return false
        if (friendlyName != other.friendlyName) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (email != other.email) return false
        if (mobile != other.mobile) return false
        if (addresses != other.addresses) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (friendlyName?.hashCode() ?: 0)
        result = 31 * result + (firstName?.hashCode() ?: 0)
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (mobile?.hashCode() ?: 0)
        result = 31 * result + (addresses?.hashCode() ?: 0)
        result = 31 * result + (version?.hashCode() ?: 0)
        return result
    }
}

@Entity
@Table(name = "origination_address")
open class OriginationAddressEntity(
    @Id
    open var id: String = uuid(),
    open var friendlyName: String? = null,
    open var houseNumber: String? = null,
    open var streetNumber: String? = null,
    open var streetAddress1: String? = null,
    open var streetAddress2: String? = null,
    open var city: String? = null,
    open var postcode: String? = null,
    @Version
    open var version: Long? = null

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OriginationAddressEntity

        if (id != other.id) return false
        if (friendlyName != other.friendlyName) return false
        if (houseNumber != other.houseNumber) return false
        if (streetNumber != other.streetNumber) return false
        if (streetAddress1 != other.streetAddress1) return false
        if (streetAddress2 != other.streetAddress2) return false
        if (city != other.city) return false
        if (postcode != other.postcode) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (friendlyName?.hashCode() ?: 0)
        result = 31 * result + (houseNumber?.hashCode() ?: 0)
        result = 31 * result + (streetNumber?.hashCode() ?: 0)
        result = 31 * result + (streetAddress1?.hashCode() ?: 0)
        result = 31 * result + (streetAddress2?.hashCode() ?: 0)
        result = 31 * result + (city?.hashCode() ?: 0)
        result = 31 * result + (postcode?.hashCode() ?: 0)
        result = 31 * result + (version?.hashCode() ?: 0)
        return result
    }
}


