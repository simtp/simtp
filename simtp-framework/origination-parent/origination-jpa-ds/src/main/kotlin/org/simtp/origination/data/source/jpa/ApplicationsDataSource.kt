package org.simtp.origination.data.source.jpa

import jakarta.persistence.OneToOne
import jakarta.persistence.Table
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size
import org.springframework.data.annotation.Id
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.validation.annotation.Validated
import java.time.LocalDate
import java.time.OffsetDateTime

@Table(name = "applicant_address")
data class ApplicantAddressEntity(
    @Id
    var id: Long? = null,
    val street: String, // The street address (Max length: 100)
    val city: String, // The city (Max length: 50)
    val state: String, // The state (Max length: 50)
    val zipCode: String, // The postal/ZIP code (Max length: 15)
    @field:Size(max = 100)
    val country: String? = null // The 2-character ISO 3166-1 alpha-2 country code (Optional)
)

@Validated
@Table(name = "applicant")
data class ApplicantEntity(

    @Id
    var id: Long? = null,

    @field:NotBlank @field:Size(max = 36)
    val userId: String,

    @field:NotBlank @field:Size(max = 50)
    val firstName: String,

    @field:NotBlank @field:Size(max = 50)
    val lastName: String,

    @field:NotBlank
    @field:Size(max = 100)
    @field:Email
    val email: String,

    @field:NotBlank
    @field:Size(min = 8, max = 36)
    val password: String,

    @field:Size(max = 15)
    val phoneNumber: String? = null,

    val dateOfBirth: LocalDate? = null,

    @OneToOne(optional = true)
    val address: ApplicantAddressEntity? = null,

    @field:NotNull
    val createdAt: OffsetDateTime
)



@Repository
interface ApplicantJpaRepository: CrudRepository<ApplicantEntity, String>