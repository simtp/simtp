package org.simtp.origination.data.source.inmemory


import org.simtp.origination.api.*
import org.simtp.origination.api.Application
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import reactor.core.publisher.Mono

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(InMemoryOriginationDataSourceConfiguration::class)
annotation class EnableInMemoryOriginationDataSource

@Configuration
open class InMemoryOriginationDataSourceConfiguration {

    @Bean
    open fun inMemoryOriginationDataSource(): InMemoryApplicationDataSource {
        return InMemoryApplicationDataSource()
    }
}

class InMemoryApplicationDataSource: IApplicationDataSource {

    private val map: MutableMap<String, Application> = mutableMapOf()

    override fun save(originationData: Application): Mono<Application> {
        map[originationData.id] = originationData
        return Mono.just(originationData)
    }

    override fun findById(id: String): Mono<Application> {
        return map[id]?.let { Mono.just(it) } ?: Mono.empty()
    }
}