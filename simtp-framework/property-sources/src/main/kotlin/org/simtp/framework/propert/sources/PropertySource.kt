package org.simtp.framework.propert.sources

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean
import org.springframework.boot.env.PropertySourceLoader
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.core.Ordered
import org.springframework.core.env.CompositePropertySource
import org.springframework.core.env.ConfigurableEnvironment
import org.springframework.core.env.Environment
import org.springframework.core.env.PropertiesPropertySource
import org.springframework.core.env.PropertySource
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.core.io.support.SpringFactoriesLoader

open class SimtpPropertySourcesApplicationContextListener: ApplicationContextInitializer<ConfigurableApplicationContext>, Ordered {

     companion object {
         private val LOG = LoggerFactory.getLogger(SimtpPropertySourcesApplicationContextListener::class.java)
         const val PROPERTY_NAME: String = "simtpProperties"
     }

    override fun initialize(applicationContext: ConfigurableApplicationContext) {
        val environment = applicationContext.environment

        val factories = SpringFactoriesLoader
            .loadFactories(SimtpPropertyLocator::class.java, null)

        var ps = environment.propertySources.get(PROPERTY_NAME) as CompositePropertySource?

        if (null == ps) {
            ps = CompositePropertySource(PROPERTY_NAME)
            environment.propertySources.addLast(ps)
        }

        factories
            .forEach { factory -> ps.addPropertySource( factory.locate(environment) ) }
    }

    override fun getOrder(): Int {
        return Ordered.HIGHEST_PRECEDENCE
    }
}


object ConfigurationLoader {

    private val log = LoggerFactory.getLogger(ConfigurationLoader::class.java)

    @JvmStatic
    fun getPropertySource(env: Environment, sourceName: String, filePrefix: String): PropertySource<*> {
        val environment = env as ConfigurableEnvironment
        try {
            val compositePropertySource = CompositePropertySource(sourceName)

            environment
                .activeProfiles
                .map { ClassPathResource("$filePrefix-$it.yml") }
                .filter { it.exists() }
                .forEach { resource ->
                    YamlPropertiesFactoryBean()
                        .also { it.setResources(resource) }
                        .`object`
                        ?.also { props -> compositePropertySource.addFirstPropertySource(PropertiesPropertySource("$filePrefix.yml", props)) }
                }
            ClassPathResource("$filePrefix.yml")
                .apply {
                    if (this.exists()) {
                        YamlPropertiesFactoryBean()
                            .also { it.setResources(this) }
                            .`object`
                            ?.also { props -> compositePropertySource.addFirstPropertySource(PropertiesPropertySource("$filePrefix.yml", props)) }

                    }
                }

            return compositePropertySource
        } catch (ex: Exception) {
            log.error("Exception thrown while trying to read properties from sourceName=$sourceName filePrefix=$filePrefix")
            throw RuntimeException(ex)
        }
    }
}

interface SimtpPropertyLocator {
    fun locate(environment: Environment): PropertySource<*>
}

open class SimtpPropertySourceLoader : PropertySourceLoader {

    override fun getFileExtensions(): Array<String> {
        TODO("Not yet implemented")
    }

    override fun load(name: String?, resource: Resource?): MutableList<PropertySource<*>> {
        TODO("Not yet implemented")
    }
}