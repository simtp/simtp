package org.simtp.kotlin.spring.web

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.http.ResponseEntity

class NamespaceKtTests {

    @Test
    fun `responseEntity should return notFound when input is null`() {
        val input: String? = null
        val expected = ResponseEntity.notFound().build<String>()
        val actual = input.responseEntity()
        assertEquals(expected, actual)
    }

    @Test
    fun `responseEntity should return ok with input when input is not null`() {
        val input = "Hello, world!"
        val expected = ResponseEntity.ok(input)
        val actual = input.responseEntity()
        assertEquals(expected, actual)
    }
}
