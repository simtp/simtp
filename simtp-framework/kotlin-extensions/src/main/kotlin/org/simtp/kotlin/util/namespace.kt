package org.simtp.kotlin.util

import org.slf4j.Logger
import java.util.*
import java.util.concurrent.ExecutorService

/**
 * Created by Grant Little grant@grantlittle.me
 */

fun ExecutorService?.safeSubmit(exceptionHandler : (Throwable) -> Unit = { onThrowable(it) }, task : () -> Unit
                               ) {

    if (this == null) return
    try {
        task()
    } catch (ex : Throwable) {
        exceptionHandler(ex)
    }
}

fun ExecutorService?.safeShutdown() {
    if (this == null) return
    if (!this.isTerminated && !this.isShutdown) {
        this.shutdown()
    }
}

fun onThrowable(ex : Throwable,
                log : Logger? = log()) {
    log?.info( "Exception thrown", ex)
}

fun log() : Logger? {
    return null
}

var uuid: () -> String = {
    UUID.randomUUID().toString()
}