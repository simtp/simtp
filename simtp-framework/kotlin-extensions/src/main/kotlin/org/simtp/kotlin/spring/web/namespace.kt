@file:Suppress("unused")

package org.simtp.kotlin.spring.web

import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestOperations
import java.util.*

/**
 * Created by Grant Little grant@grantlittle.me
 */
fun <T> T?.responseEntity() : ResponseEntity<T> {
    return if (this == null) {
        ResponseEntity.notFound().build()
    } else {
        ResponseEntity.ok(this)
    }
}

fun <T> Optional<T>.responseEntity() : ResponseEntity<T> {
    return if (!this.isPresent) {
        ResponseEntity.notFound().build()
    } else {
        ResponseEntity.ok(this.get())
    }
}

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
inline fun <reified R : Any> Any.post(restOperations : RestOperations,
                                      url : String,
                                      vararg args : Any) : R {
    return restOperations.postForObject(url, this, R::class.java, args)
}
