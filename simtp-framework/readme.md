# SimTP Framework

## Overview
The SimTP's main aim is to provide the framework required to make it easy to quickly create transactional applications. 

## Stream First Design
The SimTP framework is designed as a stream first approach. The primary data source for all data is a stream.

Secondary data sources such as SQL & NoSQL databases can be used, but they are considered secondary and get their data
from one or more streams. As such they act as a view of the data.

Here is a typical flow of data within SimTP components

![StreamDesign](./docs/StreamFirstDesign.svg)

# Archetypes

The SimTP framework includes a number of archetypes that make it quick and easy to create basic modules to follow the
suggested pattern for your resources. You must simply choose a name for your resource/entity and pass this to the
archetype which will then create all the basic scaffolding code for you.

To use the archetype you will need to run the following command

```shell
mvn archetype:generate -DarchetypeArtifactId=simtp-parent-archetype -DarchetypeGroupId=org.simtp -DgroupId=org.simtp -Dpackage=org.simtp.<entityPackage> -entityName=<entityName>
```
Required parameters:-
* -DentityName - Name of the entity the component represents. First letter should be capitalised
* -DartifactId - Name of artifact. Standard format is <entityName>-parent where <entityName> is all lower case
* -Dpackage - Base package to be used. Standard format is base.package.<entityName> where <entityName> is all lower case

As an example:-
```shell
mvn archetype:generate -DarchetypeArtifactId=simtp-parent-archetype -DarchetypeGroupId="org.simtp" -DarchetypeVersion=0-SNAPSHOT -DartifactId=plane-parent -DgroupId="org.simtp" -Dversion=0-SNAPSHOT -Dpackage="org.simtp.plane" -DentityName=Plane
```

# Modules

* [Origination](./origination-parent/readme.md)