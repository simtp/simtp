#set($dollar = '$')
/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "R2dbcDataSourceORMInspection")
package ${package}.datasource.r2dbc

import io.r2dbc.spi.ConnectionFactory
import org.springframework.r2dbc.connection.init.CompositeDatabasePopulator
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator
import org.springframework.core.io.ClassPathResource
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.common.IDateTimeManager
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import org.simtp.kotlin.util.uuid
import ${package}.api.*
import ${package}.api.${entityName}FailureEvent
import ${package}.api.stream.*
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.kafka.receiver.ReceiverOptions
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZonedDateTime

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(${entityName}R2dbcDataSourceConfiguration::class)
annotation class Enable${entityName}R2dbcDataSource

@Repository
interface ${entityName}Repository: ReactiveCrudRepository<${entityName}Entity, String> {
    fun deleteByIdAndVersion(id: String, version: Long): Mono<Void>
}

@Repository
interface ${entityName}MessageIdRepository: ReactiveCrudRepository<${entityName}MessageIdEntity, String>

@Repository
interface ${entityName}FailedMessageRepository: ReactiveCrudRepository<${entityName}FailedMessageEntity, Long>

@Configuration
@EnableR2dbcRepositories
@EntityScan
open class ${entityName}R2dbcDataSourceConfiguration {

    @Bean
    open fun ${entityName.toLowerCase()}R2dbcDataSource(
        ${entityName.toLowerCase()}Repository: ${entityName}Repository,
        ${entityName.toLowerCase()}MessageIdRepository: ${entityName}MessageIdRepository,
        ${entityName.toLowerCase()}FailedMessageRepository: ${entityName}FailedMessageRepository
    ): ${entityName}R2dbcDataSource {
        return ${entityName}R2dbcDataSource(
            ${entityName.toLowerCase()}Repository = ${entityName.toLowerCase()}Repository,
            ${entityName.toLowerCase()}MessageIdRepository = ${entityName.toLowerCase()}MessageIdRepository,
            ${entityName.toLowerCase()}FailedMessageRepository = ${entityName.toLowerCase()}FailedMessageRepository
        )
    }

    @Bean
    open fun ${entityName.toLowerCase()}R2dbcReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\\${dollar}{spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "${entityName.toLowerCase()}R2dbcReactiveKafkaConsumer:#[[$applicationName]]#"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "${entityName.toLowerCase()}R2dbcReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(${entityName.toUpperCase()}_STREAM_NAME)))
    }

    @Bean
    open fun ${entityName.toLowerCase()}R2dbcStreamConsumer(
        ${entityName.toLowerCase()}R2dbcReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        ${entityName.toLowerCase()}R2dbcDataSource: ${entityName}R2dbcDataSource,
        dateTimeManager: IDateTimeManager
    ): ${entityName}R2dbcStreamConsumer {
        return ${entityName}R2dbcStreamConsumer(
            ${entityName.toLowerCase()}R2dbcReactiveKafkaConsumerTemplate = ${entityName.toLowerCase()}R2dbcReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            ${entityName.toLowerCase()}R2dbcDataSource = ${entityName.toLowerCase()}R2dbcDataSource,
            dateTimeManager = dateTimeManager
        )
    }


    @Bean
    open fun ${entityName.toLowerCase()}Initializer(connectionFactory: ConnectionFactory): ConnectionFactoryInitializer {
        return ConnectionFactoryInitializer().apply {
            setConnectionFactory(connectionFactory)
            setDatabasePopulator(
                CompositeDatabasePopulator().apply {
                    addPopulators(
                        ResourceDatabasePopulator(
                            ClassPathResource("${entityName.toLowerCase()}.sql"),
                        )
                    )
                }
            )
        }
    }
}

class ${entityName}R2dbcStreamConsumer(
    private val ${entityName.toLowerCase()}R2dbcReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val ${entityName.toLowerCase()}R2dbcDataSource: ${entityName}R2dbcDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(${entityName}R2dbcStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<${entityName}> {
        return ${entityName.toLowerCase()}R2dbcReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                ${entityName.toLowerCase()}StreamHandler(
                    it.value(),
                    ${entityName.toLowerCase()}R2dbcDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, ${entityName.toUpperCase()}_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.to${entityName}FailureStreamEvent(this::class.simpleName!!) }
                }
            }


    }

}



open class ${entityName}R2dbcDataSource(
    private val ${entityName.toLowerCase()}Repository: ${entityName}Repository,
    private val ${entityName.toLowerCase()}MessageIdRepository: ${entityName}MessageIdRepository,
    private val ${entityName.toLowerCase()}FailedMessageRepository: ${entityName}FailedMessageRepository
): I${entityName}DataSource {

    companion object {
        private val log = LoggerFactory.getLogger(${entityName}R2dbcDataSource::class.java)
    }

    private fun Mono<${entityName}>.saveMessageId(messageId: String): Mono<${entityName}> {
        return this
            .flatMap {
                ${entityName.toLowerCase()}MessageIdRepository
                    .save(${entityName}MessageIdEntity(id = messageId))
                    .doOnError { ex -> log.error("Failed to save {}", messageId, ex) }
                    .doOnNext { messageId -> log.info("Saving messageId {}", messageId.id) }
                    .thenReturn(it)
            }
    }

    override fun create(${entityName.toLowerCase()}CreateEvent: ${entityName}CreateEvent): Mono<${entityName}> {
        return ${entityName.toLowerCase()}MessageIdRepository
            .existsById(${entityName.toLowerCase()}CreateEvent.messageId.toString())
            .filter { it == false }
            .flatMap { save(${entityName.toLowerCase()}CreateEvent.to${entityName}()) }
            .saveMessageId(${entityName.toLowerCase()}CreateEvent.messageId.toString())
    }

    override fun update(${entityName.toLowerCase()}UpdateEvent: ${entityName}UpdateEvent): Mono<${entityName}> {
        return ${entityName.toLowerCase()}MessageIdRepository
            .existsById(${entityName.toLowerCase()}UpdateEvent.messageId.toString())
            .filter { it == false }
            .flatMap { save(${entityName.toLowerCase()}UpdateEvent.to${entityName}()) }
            .saveMessageId(${entityName.toLowerCase()}UpdateEvent.messageId.toString())
    }

    override fun delete(${entityName.toLowerCase()}DeleteEvent: ${entityName}DeleteEvent): Mono<${entityName}> {
        return ${entityName.toLowerCase()}MessageIdRepository
            .existsById(${entityName.toLowerCase()}DeleteEvent.messageId.toString())
            .filter { it == false }
            .flatMap {
                delete(${entityName.toLowerCase()}DeleteEvent.id.toString(), ${entityName.toLowerCase()}DeleteEvent.version)
            }
            .saveMessageId(${entityName.toLowerCase()}DeleteEvent.messageId.toString())

    }

    private fun delete(id: String, version: Long): Mono<${entityName}> {
        val cachedValue = ${entityName.toLowerCase()}Repository
            .findById(id)
            .log("INFO")
            .doOnNext { if (it.version != version) throw OptimisticLockingFailureException("Provided providedValue=${version} currentValue=${it.version}")  }
            .cache()

        return cachedValue
            .flatMap {
                ${entityName.toLowerCase()}Repository.deleteByIdAndVersion(id, version)
            }
            .then(cachedValue)
            .map { it.to${entityName}() }
    }

    internal fun save(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        return ${entityName.toLowerCase()}Repository
            .save(${entityName.toLowerCase()}.to${entityName}Entity())
            .map { it.to${entityName}() }
    }

    override fun findById(id: String): Mono<${entityName}> {
        return ${entityName.toLowerCase()}Repository
            .findById(id)
            .map { it.to${entityName}() }
    }

    override fun findAll(): Flux<${entityName}> {
        return ${entityName.toLowerCase()}Repository
            .findAll()
            .map { it.to${entityName}() }
    }

    override fun failure(${entityName.toLowerCase()}FailureEvent: ${entityName}FailureEvent): Mono<Unit> {
        return ${entityName.toLowerCase()}FailedMessageRepository
            .save(${entityName.toLowerCase()}FailureEvent.to${entityName}FailedMessageEntity())
            .doOnNext { log.error("Failed to consumer message") }
            .then(Mono.empty())

    }

}


@Table(name = "${entityName.toLowerCase()}")
open class ${entityName}Entity(
    @Id
    open var id : String = uuid(),

    @Column("name")
    open var name : String = "",

    @Column("last_modified_timestamp")
    open var lastModifiedTimestamp: OffsetDateTime = OffsetDateTime.now(),

    @Version
    open var version: Long = 0
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ${entityName}Entity

        if (id != other.id) return false
        if (name != other.name) return false
        if (lastModifiedTimestamp != other.lastModifiedTimestamp) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + lastModifiedTimestamp.hashCode()
        result = 31 * result + version.hashCode()
        return result
    }

    override fun toString(): String {
        return "${entityName}Entity(id='$id', name='$name', lastModifiedTimestamp=$lastModifiedTimestamp, version=$version)"
    }


}

@Table(name = "${entityName.toLowerCase()}_message_id")
open class ${entityName}MessageIdEntity(
    @Suppress("unused")
    @Id
    @Column("id")
    var id: String? = null,

    @Version
    open var version: Long? = null
)

internal fun ${entityName}.to${entityName}Entity(): ${entityName}Entity {
    return ${entityName}Entity(
        id = this.id,
        name = this.name,
        lastModifiedTimestamp = this.lastModifiedTimestamp.toOffsetDateTime(),
        version = this.version
    )
}

internal fun ${entityName}Entity.to${entityName}(): ${entityName} {
    return ${entityName}(
        id = this.id,
        name = this.name,
        lastModifiedTimestamp = this.lastModifiedTimestamp.toZonedDateTime(),
        version = this.version
    )
}

fun ${entityName}FailureEvent.to${entityName}FailedMessageEntity(): ${entityName}FailedMessageEntity {
    return ${entityName}FailedMessageEntity(
        id = null,
        ${entityName.toLowerCase()}Id = this.id,
        messageId = this.messageId,
        name = this.name,
        updated = this.updated,
        ${entityName.toLowerCase()}Version = this.version,
        modifiedBy = this.modifiedBy,
        source = this.source,
        reference = this.reference,
        failureDateTime = this.failureDateTime
    )
}

@Table(name = "${entityName.toLowerCase()}_failed_message")
open class ${entityName}FailedMessageEntity(
    @Id
    val id: Long? = null,
    val ${entityName.toLowerCase()}Id: String? = null,
    val messageId: String = uuid(),
    val name: String = "",
    val updated: ZonedDateTime = ZonedDateTime.now(),
    val ${entityName.toLowerCase()}Version: Long? = null,
    val modifiedBy: String = "",
    val source: String = "",
    val reference: String = uuid(),
    val failureDateTime: ZonedDateTime = ZonedDateTime.now(),

    @Version
    val version: Long = 0

)
