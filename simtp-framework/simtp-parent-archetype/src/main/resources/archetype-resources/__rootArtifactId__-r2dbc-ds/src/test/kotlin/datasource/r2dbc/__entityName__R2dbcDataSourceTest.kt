/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("ReactiveStreamsUnusedPublisher")

package ${package}.datasource.r2dbc

import io.mockk.*
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import ${package}.api.${entityName}
import ${package}.api.stream.*
import org.junit.jupiter.api.Assertions.*
import org.simtp.kotlin.util.uuid
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class ${entityName}R2dbcDataSourceTest {

    @Test
    fun save() {
        val ${entityName.toLowerCase()}Repository = mockk<${entityName}Repository>(relaxed = true)
        val ${entityName.toLowerCase()}MessageIdRepository = mockk<${entityName}MessageIdRepository>(relaxed = true)
        val ${entityName.toLowerCase()}FailedMessageRepository = mockk<${entityName}FailedMessageRepository>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        val entity = ${entityName.toLowerCase()}.to${entityName}Entity()
        every { ${entityName.toLowerCase()}Repository.save(any()) } returns Mono.just(entity)
        every { ${entityName.toLowerCase()}MessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { ${entityName.toLowerCase()}MessageIdRepository.save(any<${entityName}MessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as ${entityName}MessageIdEntity) }
        val ${entityName.toLowerCase()}DataSource = ${entityName}R2dbcDataSource(${entityName.toLowerCase()}Repository, ${entityName.toLowerCase()}MessageIdRepository, ${entityName.toLowerCase()}FailedMessageRepository)
        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.save(${entityName.toLowerCase()}))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val ${entityName.toLowerCase()}Repository = mockk<${entityName}Repository>(relaxed = true)
        val ${entityName.toLowerCase()}MessageIdRepository = mockk<${entityName}MessageIdRepository>(relaxed = true)
        val ${entityName.toLowerCase()}FailedMessageRepository = mockk<${entityName}FailedMessageRepository>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        val entity = ${entityName.toLowerCase()}.to${entityName}Entity()
        every { ${entityName.toLowerCase()}Repository.findById(${entityName.toLowerCase()}.id) } returns Mono.just(entity)
        every { ${entityName.toLowerCase()}MessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { ${entityName.toLowerCase()}MessageIdRepository.save(any<${entityName}MessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as ${entityName}MessageIdEntity) }
        val ${entityName.toLowerCase()}DataSource = ${entityName}R2dbcDataSource(${entityName.toLowerCase()}Repository, ${entityName.toLowerCase()}MessageIdRepository, ${entityName.toLowerCase()}FailedMessageRepository)
        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.findById(${entityName.toLowerCase()}.id))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val ${entityName.toLowerCase()}Repository = mockk<${entityName}Repository>(relaxed = true)
        val ${entityName.toLowerCase()}MessageIdRepository = mockk<${entityName}MessageIdRepository>(relaxed = true)
        val ${entityName.toLowerCase()}FailedMessageRepository = mockk<${entityName}FailedMessageRepository>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        val entity = ${entityName.toLowerCase()}.to${entityName}Entity()
        every { ${entityName.toLowerCase()}Repository.findAll() } returns Flux.fromIterable(listOf(entity))
        val ${entityName.toLowerCase()}DataSource = ${entityName}R2dbcDataSource(${entityName.toLowerCase()}Repository, ${entityName.toLowerCase()}MessageIdRepository, ${entityName.toLowerCase()}FailedMessageRepository)
        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.findAll())
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun delete() {
        val ${entityName.toLowerCase()}Repository = mockk<${entityName}Repository>(relaxed = true)
        val ${entityName.toLowerCase()}MessageIdRepository = mockk<${entityName}MessageIdRepository>(relaxed = true)
        val ${entityName.toLowerCase()}FailedMessageRepository = mockk<${entityName}FailedMessageRepository>(relaxed = true)

        val ${entityName.toLowerCase()}DeleteEvent = ${entityName}DeleteEvent().apply {
            id = uuid()
            messageId = uuid()
            version = 0
        }

        val ${entityName.toLowerCase()} = ${entityName}(
            id = ${entityName.toLowerCase()}DeleteEvent.id.toString(),
            version = ${entityName.toLowerCase()}DeleteEvent.version
        )

        val entity = ${entityName.toLowerCase()}.to${entityName}Entity()
        every { ${entityName.toLowerCase()}Repository.findById(${entityName.toLowerCase()}.id) } returns Mono.just(entity)
        every { ${entityName.toLowerCase()}Repository.deleteByIdAndVersion(${entityName.toLowerCase()}.id, ${entityName.toLowerCase()}.version) } returns Mono.empty()

        every { ${entityName.toLowerCase()}MessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { ${entityName.toLowerCase()}MessageIdRepository.save(any<${entityName}MessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as ${entityName}MessageIdEntity) }

        val ${entityName.toLowerCase()}DataSource = ${entityName}R2dbcDataSource(${entityName.toLowerCase()}Repository, ${entityName.toLowerCase()}MessageIdRepository, ${entityName.toLowerCase()}FailedMessageRepository)

        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.delete(${entityName.toLowerCase()}DeleteEvent))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify { ${entityName.toLowerCase()}Repository.deleteByIdAndVersion(${entityName.toLowerCase()}.id, ${entityName.toLowerCase()}.version) }
    }

    @Test
    fun testMapping() {
        val ${entityName.toLowerCase()} = ${entityName}()
        assertEquals(${entityName.toLowerCase()}, ${entityName.toLowerCase()}.to${entityName}Entity().to${entityName}())
    }

}