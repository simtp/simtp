CREATE TABLE IF NOT EXISTS ${entityName.toLowerCase()} (
    id varchar(36),
    version BIGINT not null,
    last_modified_timestamp TIMESTAMP WITH TIME ZONE not null,
    last_modified_by varchar(200),
    name varchar(100) not null,
    primary key(ID)
);

CREATE TABLE IF NOT EXISTS ${entityName.toLowerCase()}_message_id
(
    id varchar(36) not null,
    version BIGINT not null,
    primary key (id)
);

CREATE TABLE IF NOT EXISTS ${entityName.toLowerCase()}_failed_message
(
    id BIGINT not null,
    ${entityName.toLowerCase()}Id                varchar(36) not null,
    messageId                varchar(36) not null,
    version                 BIGINT                   not null,
    last_modified_timestamp TIMESTAMP WITH TIME ZONE not null,
    last_modified_by        varchar(200),
    first_name              varchar(100)             not null,
    last_name               varchar(100)             not null,
    dob                     DATE,
    source               varchar(100)             not null,
    previous_version                 BIGINT                   not null,
    reference               varchar(100)             not null,
    failure_timestamp TIMESTAMP WITH TIME ZONE not null,
    primary key (id)
);