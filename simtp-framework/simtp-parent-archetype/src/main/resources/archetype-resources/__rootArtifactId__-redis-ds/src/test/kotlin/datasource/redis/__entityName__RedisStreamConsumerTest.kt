package ${package}.datasource.redis

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.junit.jupiter.api.Test
import ${package}.api.*
import ${package}.api.stream.*
import ${package}.api.stream.${entityName}FailureEvent

import org.simtp.common.IDateTimeManager
import org.simtp.common.toISOString
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration
import java.time.ZonedDateTime

class ${entityName}RedisStreamConsumerTest {
    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun consumerHandlesError() {
        val template = mockk<ReactiveKafkaConsumerTemplate<String, *>>()
        val consumerRecord = mockk<ConsumerRecord<String, Any>>()
        val zonedDateTime = ZonedDateTime.now()
        every { template.receiveAutoAck() } returns Flux.fromIterable(listOf(consumerRecord))

        every { consumerRecord.value() } returns ${entityName}CreateEvent().apply {
            this.id = "123"
            this.name = "Joe"
            this.createdBy = "test"
            this.createdTimestamp = zonedDateTime.toISOString()
            this.reference = "ref"
        }

        val dataSource = mockk<${entityName}RedisDataSource>()

        every { dataSource.create(any()) } returns Mono.error(IllegalStateException("ERROR"))
        every { dataSource.failure(any()) } returns Mono.empty()

        val dateTimeManager = mockk<IDateTimeManager>()
        val reactiveKafkaProducerTemplate = mockk<ReactiveKafkaProducerTemplate<String, Any>>()

        every { reactiveKafkaProducerTemplate.send(any<String>(), any<${entityName}FailureEvent>()) } returns Mono.just(mockk(relaxed = true))

        every { dateTimeManager.currentZonedDateTime } returns zonedDateTime
        val consumer = ${entityName}RedisStreamConsumer(
            ${entityName.toLowerCase()}RedisReactiveKafkaConsumerTemplate = template,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            ${entityName.toLowerCase()}RedisDataSource = dataSource,
            dateTimeManager = dateTimeManager
        )

        StepVerifier
            .create(consumer.createConsumer())
            .expectNextCount(0)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) {reactiveKafkaProducerTemplate.send("$${entityName.toUpperCase()}_STREAM_NAME.DLQ", any<Any>())  }
    }

}