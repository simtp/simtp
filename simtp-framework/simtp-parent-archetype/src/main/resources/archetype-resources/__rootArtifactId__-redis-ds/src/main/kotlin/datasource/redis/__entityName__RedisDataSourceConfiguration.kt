#set($dollar = '$')
/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package ${package}.datasource.redis

import ${package}.api.*
import ${package}.api.${entityName}FailureEvent
import ${package}.api.stream.*
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.common.IDateTimeManager
import org.simtp.common.toISOString
import org.simtp.common.toZonedDateTime
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.core.ReactiveHashOperations
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.receiver.ReceiverOptions
import kotlin.reflect.full.memberProperties
import com.fasterxml.jackson.databind.ObjectMapper

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(${entityName}RedisDataSourceConfiguration::class)
annotation class Enable${entityName}RedisDataSource

@Configuration
open class ${entityName}RedisDataSourceConfiguration {

    @Bean
    open fun ${entityName.toLowerCase()}RedisDataSource(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        objectMapper: ObjectMapper
    ): ${entityName}RedisDataSource {
        return ${entityName}RedisDataSource(reactiveRedisOperations, objectMapper)
    }

    @Bean
    open fun ${entityName.toLowerCase()}RedisReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\\${dollar}{spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "${entityName.toLowerCase()}RedisReactiveKafkaConsumer:#[[$applicationName]]#"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "${entityName.toLowerCase()}RedisReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(${entityName.toUpperCase()}_STREAM_NAME)))
    }

    @Bean
    open fun ${entityName.toLowerCase()}RedisStreamConsumer(
        ${entityName.toLowerCase()}RedisReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        ${entityName.toLowerCase()}RedisDataSource: ${entityName}RedisDataSource,
        dateTimeManager: IDateTimeManager
    ): ${entityName}RedisStreamConsumer {
        return ${entityName}RedisStreamConsumer(
            ${entityName.toLowerCase()}RedisReactiveKafkaConsumerTemplate = ${entityName.toLowerCase()}RedisReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            ${entityName.toLowerCase()}RedisDataSource = ${entityName.toLowerCase()}RedisDataSource,
            dateTimeManager = dateTimeManager
        )
    }
}


class ${entityName}RedisStreamConsumer(
    private val ${entityName.toLowerCase()}RedisReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val ${entityName.toLowerCase()}RedisDataSource: ${entityName}RedisDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(${entityName}RedisStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<${entityName}> {
        return ${entityName.toLowerCase()}RedisReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                ${entityName.toLowerCase()}StreamHandler(
                    it.value(),
                    ${entityName.toLowerCase()}RedisDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, ${entityName.toUpperCase()}_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.to${entityName}FailureStreamEvent(this::class.simpleName!!) }
                }
            }

    }
}

class ${entityName}RedisDataSource(
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val objectMapper: ObjectMapper,
): I${entityName}DataSource {

    companion object {
        private val log = LoggerFactory.getLogger(${entityName}RedisDataSource::class.java)
    }

    private val hashOps = reactiveRedisOperations
        .opsForHash<String, String>()

    private val setOps = reactiveRedisOperations
        .opsForSet()


    override fun create(${entityName.toLowerCase()}CreateEvent: ${entityName}CreateEvent): Mono<${entityName}> {
        return setOps
            .isMember("${entityName.toLowerCase()}:messageIds", ${entityName.toLowerCase()}CreateEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                save(${entityName.toLowerCase()}CreateEvent.to${entityName}())
            }
            .saveMessageId(${entityName.toLowerCase()}CreateEvent.messageId.toString())
    }

    override fun update(${entityName.toLowerCase()}UpdateEvent: ${entityName}UpdateEvent): Mono<${entityName}> {
        return setOps
            .isMember("${entityName.toLowerCase()}:messageIds", ${entityName.toLowerCase()}UpdateEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                save(${entityName.toLowerCase()}UpdateEvent.to${entityName}())
            }
            .saveMessageId(${entityName.toLowerCase()}UpdateEvent.messageId.toString())

    }

    internal fun save(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        val existing = hashOps
            .multiGet("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", listOf("version"))
            .cache()

        val new${entityName} = ${entityName.toLowerCase()}.cloneAndIncrementVersion()

        return existing
            .flatMap {
                val result = it.filterNotNull().isNotEmpty()
                //This isn't very elegant
                if (result && it[0].toLong() != ${entityName.toLowerCase()}.version) {
                    Mono.error(OptimisticLockingFailureException("Current version=${it[0]} does not match provided version=${dollar}{${ entityName.toLowerCase() }.version}"))
                } else {
                    Mono.just(it)
                }
            }
            .hasElement()
            .flatMap {
                hashOps.save${entityName}(new${entityName})
            }
    }

    private fun Mono<${entityName}>.saveMessageId(messageId: String): Mono<${entityName}> {
        return this
            .flatMap { ${entityName.toLowerCase()} ->
                setOps.add("${entityName.toLowerCase()}:messageIds:$messageId", messageId).map { ${entityName.toLowerCase()} }
            }
    }



    override fun findById(id: String): Mono<${entityName}> {
        return hashOps
            .multiGet("${entityName.toLowerCase()}:${id}", all${entityName}Fields)
            .mapNotNull {
                when (it.filterNotNull().isEmpty()) {
                    true -> {
                        null
                    }
                    else -> {
                        it.to${entityName}()
                    }
                }
            }
            .cast(${entityName}::class.java)
            .doOnNext { log.info("findById: {} = {}", id, it) }
    }

    override fun findAll(): Flux<${entityName}> {
        return hashOps
            .keys("${entityName.toLowerCase()}:*")
            .flatMap { key ->
                reactiveRedisOperations
                    .opsForHash<String, String>()
                    .multiGet(key, all${entityName}Fields)
                    .map { it.to${entityName}() }
            }
    }

    override fun delete(${entityName.toLowerCase()}DeleteEvent: ${entityName}DeleteEvent): Mono<${entityName}> {
        return setOps
            .isMember("${entityName.toLowerCase()}:messageIds", ${entityName.toLowerCase()}DeleteEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                delete(${entityName.toLowerCase()}DeleteEvent.id.toString(), ${entityName.toLowerCase()}DeleteEvent.version)
            }
            .doOnNext {
                log.info("Successfully deleted item")
            }
            .doOnError {
                log.error("Exception encountered", it)
            }
            .saveMessageId(${entityName.toLowerCase()}DeleteEvent.messageId.toString())

    }

    internal fun delete(id: String, version: Long): Mono<${entityName}> {
        val value = findById(id).cache()

        val deleteOp = hashOps
            .delete("${entityName.toLowerCase()}:${id}")

        return value
            .flatMap {
                if (it.version != version) {
                    Mono.error(OptimisticLockingFailureException("Provided ${entityName.toLowerCase()}.version=${version} version does not latest ${entityName.toLowerCase()}.version=${it.version} version"))
                } else {
                    Mono.just(it)
                }
            }
            .doOnError {
                log.error("Failed with exception", it)
            }
            .flatMap {
                deleteOp
            }
            .flatMap {
                value
            }


    }

    override fun failure(${entityName.toLowerCase()}FailureEvent: ${entityName}FailureEvent): Mono<Unit> {
        return setOps
            .add("${entityName.toLowerCase()}:failures:${${entityName.toLowerCase()}FailureEvent.id}", objectMapper.writeValueAsString(${entityName.toLowerCase()}FailureEvent))
            .doOnError {
                log.error("Failed to store ${entityName.toLowerCase()} failure event")
            }
            .doOnNext {
                log.info("Stored ${entityName.toLowerCase()} failure event for id={}", ${entityName.toLowerCase()}FailureEvent.id)
            }
            .then(Mono.empty())
    }
}

internal val all${entityName}Fields = ${entityName}Map()::class.memberProperties.map { it.name }

fun ReactiveHashOperations<String, String, String>.save${entityName}(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
    return this
        .putAll("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", ${entityName.toLowerCase()}.toMap())
        .map { ${entityName.toLowerCase()} }
}

open class ${entityName}Map(val map: MutableMap<String, String?> = mutableMapOf<String, String?>().withDefault { null }) {
    var id: String by map
    var name: String by map
    var lastModifiedBy: String by map
    var version: String by map
    var lastModifiedTimestamp: String by map
}

fun List<String?>.to${entityName}(): ${entityName} {
    return this.mapIndexed { index, it -> all${entityName}Fields[index] to it }
        .toMap()
        .toMutableMap()
        .let { ${entityName}Map(it) }
        .to${entityName}()
}

fun ${entityName}.to${entityName}Map(): ${entityName}Map {
    val ${entityName.toLowerCase()} = this
    return ${entityName}Map().apply {
        id = ${entityName.toLowerCase()}.id
        version = ${entityName.toLowerCase()}.version.toString()
        lastModifiedTimestamp = ${entityName.toLowerCase()}.lastModifiedTimestamp.toISOString()
        name = ${entityName.toLowerCase()}.name
        lastModifiedBy = ${entityName.toLowerCase()}.lastModifiedBy ?: "unknown"
    }
}

fun ${entityName}Map.to${entityName}(): ${entityName} {
    return ${entityName}(
        id = this.id,
        version = this.version.toLong(),
        lastModifiedTimestamp = this.lastModifiedTimestamp.toZonedDateTime(),
        name = this.name,
        lastModifiedBy = this.lastModifiedBy
    )
}

fun ${entityName}Map.toValuesList(): List<String?> {
    return all${entityName}Fields.map { this.map[it] }
}


fun ${entityName}.toMap(): Map<String, String> {
    return this
        .to${entityName}Map()
        .map
        .asSequence()
        .filter { it.value != null }
        .associate{ it.key to it.value!! }
}