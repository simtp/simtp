/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.service

import ${package}.api.*
import ${package}.api.stream.${entityName}CreateEvent
import ${package}.api.stream.${entityName}DeleteEvent
import ${package}.api.stream.${entityName}UpdateEvent

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Mono
import reactor.kafka.sender.SenderResult
import reactor.test.StepVerifier
import java.time.Duration

class ${entityName}CommandServiceTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun create() {
        val ${entityName.toLowerCase()}Create = ${entityName}Create()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(${entityName.toUpperCase()}_STREAM_NAME, any<String>(), any<${entityName}CreateEvent>()) } returns Mono.just(senderResult)
        val ${entityName.toLowerCase()}Service = ${entityName}CommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(${entityName.toLowerCase()}Service.create(${entityName.toLowerCase()}Create))
            .expectNext(${entityName.toLowerCase()}Create.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(${entityName.toUpperCase()}_STREAM_NAME, any<String>(), any<${entityName}CreateEvent>()) }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun update() {
        val ${entityName.toLowerCase()}Update = ${entityName}Update()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(${entityName.toUpperCase()}_STREAM_NAME, any<String>(), any<${entityName}UpdateEvent>()) } returns Mono.just(senderResult)
        val ${entityName.toLowerCase()}Service = ${entityName}CommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(${entityName.toLowerCase()}Service.update(${entityName.toLowerCase()}Update))
            .expectNext(${entityName.toLowerCase()}Update.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(${entityName.toUpperCase()}_STREAM_NAME, any<String>(), any<${entityName}UpdateEvent>()) }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun delete() {
        val ${entityName.toLowerCase()}Delete = ${entityName}Delete()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(${entityName.toUpperCase()}_STREAM_NAME, any<String>(), any<${entityName}DeleteEvent>()) } returns Mono.just(senderResult)
        val ${entityName.toLowerCase()}Service = ${entityName}CommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(${entityName.toLowerCase()}Service.delete(${entityName.toLowerCase()}Delete))
            .expectNext(${entityName.toLowerCase()}Delete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(${entityName.toUpperCase()}_STREAM_NAME, any<String>(), any<${entityName}DeleteEvent>()) }
    }
}