/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.service

import ${package}.api.*
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Mono


class ${entityName}CommandService(
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>
): I${entityName}CommandApi {


    override fun create(${entityName.toLowerCase()}Create: ${entityName}Create) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(${entityName.toUpperCase()}_STREAM_NAME, ${entityName.toLowerCase()}Create.id, ${entityName.toLowerCase()}Create.to${entityName}CreateEvent())
            .map { ${entityName.toLowerCase()}Create.id }
    }

    override fun update(${entityName.toLowerCase()}Update: ${entityName}Update) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(${entityName.toUpperCase()}_STREAM_NAME, ${entityName.toLowerCase()}Update.id, ${entityName.toLowerCase()}Update.to${entityName}UpdateEvent())
            .map { ${entityName.toLowerCase()}Update.id }
    }

    override fun delete(${entityName.toLowerCase()}Delete: ${entityName}Delete) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(${entityName.toUpperCase()}_STREAM_NAME, ${entityName.toLowerCase()}Delete.id, ${entityName.toLowerCase()}Delete.to${entityName}DeleteEvent())
            .map { ${entityName.toLowerCase()}Delete.id }
    }
}
