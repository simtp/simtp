/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.api

import ${package}.api.stream.*
import ${package}.api.stream.${entityName}FailureEvent as ${entityName}FailureStreamEvent
import org.simtp.common.toISOString
import org.simtp.common.toZonedDateTime
import org.slf4j.Logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import org.simtp.kotlin.util.uuid
import java.time.ZonedDateTime

const val ${entityName.toUpperCase()}_STREAM_NAME = "${entityName.toUpperCase()}"

interface I${entityName}CommandApi {
    fun create(${entityName.toLowerCase()}Create: ${entityName}Create): Mono<String>

    fun update(${entityName.toLowerCase()}Update: ${entityName}Update): Mono<String>

    fun delete(${entityName.toLowerCase()}Delete: ${entityName}Delete) : Mono<String>
}

interface I${entityName}QueryApi {
    operator fun get(id: String): Mono<${entityName}>
}

data class ${entityName}(
    val id: String = uuid(),
    val name: String = "",
    val lastModifiedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val lastModifiedBy: String? = "unknown",
    val version: Long = 0,
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ${entityName}

        if (id != other.id) return false
        if (name != other.name) return false
        if (version != other.version) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + version.hashCode()
        return result
    }
}


data class ${entityName}Create(
    val id: String = uuid(),
    val name: String = "",
    val createdTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val createdBy: String? = "unknown",
    val reference: String = uuid()
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ${entityName}Create

        if (id != other.id) return false
        if (name != other.name) return false
        if (createdBy != other.createdBy) return false
        if (reference != other.reference) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + (createdBy?.hashCode() ?: 0)
        result = 31 * result + reference.hashCode()
        return result
    }
}


data class ${entityName}Update(
    val id: String = uuid(),
    val name: String = "",
    val modifiedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val modifiedBy: String? = "unknown",
    val version: Long = 0,
    val reference: String = uuid()
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ${entityName}Update

        if (id != other.id) return false
        if (name != other.name) return false
        if (modifiedBy != other.modifiedBy) return false
        if (version != other.version) return false
        if (reference != other.reference) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + (modifiedBy?.hashCode() ?: 0)
        result = 31 * result + version.hashCode()
        result = 31 * result + reference.hashCode()
        return result
    }
}

data class ${entityName}Delete(
    val id: String = uuid(),
    val deletedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val deletedBy: String? = "unknown",
    val version: Long = 0,
    val reference: String = uuid()
)

fun ${entityName}CreateEvent.to${entityName}(): ${entityName} {
    return ${entityName}(
        id = this.id.toString(),
        name = this.name.toString(),
        lastModifiedTimestamp = this.createdTimestamp?.toString()?.toZonedDateTime() ?: ZonedDateTime.now(),
        lastModifiedBy = this.createdBy?.toString()
    )
}

fun ${entityName}UpdateEvent.to${entityName}(): ${entityName} {
    return ${entityName}(
        id = this.id.toString(),
        name = this.name.toString(),
        lastModifiedTimestamp = this.modifiedTimestamp?.toString()?.toZonedDateTime() ?: ZonedDateTime.now(),
        lastModifiedBy = this.modifiedBy?.toString(),
        version = this.version
    )
}

fun ${entityName}Create.to${entityName}CreateEvent(): ${entityName}CreateEvent {
    val ${entityName.toLowerCase()} = this
    return ${entityName}CreateEvent().apply {
        this.messageId = uuid()
        this.id = ${entityName.toLowerCase()}.id
        this.name = ${entityName.toLowerCase()}.name
        this.createdTimestamp = ${entityName.toLowerCase()}.createdTimestamp.toISOString()
        this.createdBy = ${entityName.toLowerCase()}.createdBy
        this.reference = ${entityName.toLowerCase()}.reference
    }
}

data class ${entityName}FailureEvent(
    val id: String = uuid(),
    val messageId: String = uuid(),
    val name: String = "",
    val updated: ZonedDateTime = ZonedDateTime.now(),
    val version: Long? = null,
    val modifiedBy: String = "",
    val source: String = "",
    val reference: String = uuid(),
    val failureDateTime: ZonedDateTime = ZonedDateTime.now(),
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ${entityName}FailureEvent

        if (id != other.id) return false
        if (name != other.name) return false
        if (version != other.version) return false
        if (modifiedBy != other.modifiedBy) return false
        if (source != other.source) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + version.hashCode()
        result = 31 * result + modifiedBy.hashCode()
        result = 31 * result + source.hashCode()
        return result
    }
}

fun ${entityName}Create.to${entityName}(): ${entityName} {
    val ${entityName.toLowerCase()} = this
    return ${entityName}(
        id = ${entityName.toLowerCase()}.id,
        name = ${entityName.toLowerCase()}.name,
        lastModifiedTimestamp = ${entityName.toLowerCase()}.createdTimestamp,
        lastModifiedBy = ${entityName.toLowerCase()}.createdBy
    )
}

fun ${entityName}Update.to${entityName}(): ${entityName} {
    return ${entityName}(
        id = this.id,
        name = this.name,
        lastModifiedTimestamp = this.modifiedTimestamp,
        lastModifiedBy = this.modifiedBy,
        version = this.version
    )
}


fun ${entityName}Update.to${entityName}UpdateEvent(): ${entityName}UpdateEvent {
    val ${entityName.toLowerCase()} = this
    return ${entityName}UpdateEvent().apply {
        this.messageId = uuid()
        this.id = ${entityName.toLowerCase()}.id
        this.name = ${entityName.toLowerCase()}.name
        this.modifiedTimestamp = ${entityName.toLowerCase()}.modifiedTimestamp.toISOString()
        this.modifiedBy = ${entityName.toLowerCase()}.modifiedBy
        this.version = ${entityName.toLowerCase()}.version
        this.reference = ${entityName.toLowerCase()}.reference
    }
}

fun ${entityName}Delete.to${entityName}DeleteEvent(): ${entityName}DeleteEvent {
    val ${entityName.toLowerCase()} = this
    return ${entityName}DeleteEvent().apply {
        this.messageId = uuid()
        this.id = ${entityName.toLowerCase()}.id
        this.deletedTimestamp = ${entityName.toLowerCase()}.deletedTimestamp.toISOString()
        this.deletedBy = ${entityName.toLowerCase()}.deletedBy
        this.version = ${entityName.toLowerCase()}.version
        this.reference = ${entityName.toLowerCase()}.reference
    }
}

fun ${entityName}.cloneAndIncrementVersion(): ${entityName} {
    return ${entityName}(
        id = this.id,
        name = this.name,
        version = this.version + 1,
        lastModifiedTimestamp = this.lastModifiedTimestamp,
        lastModifiedBy = this.lastModifiedBy
    )
}

fun ${entityName}CreateEvent.to${entityName}FailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): ${entityName}FailureEvent {
    return ${entityName}FailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        name = this.name.toString(),
        updated = this.createdTimestamp.toString().toZonedDateTime(),
        modifiedBy = this.createdBy.toString(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
        version = 0
    )
}

fun ${entityName}UpdateEvent.to${entityName}FailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): ${entityName}FailureEvent {
    return ${entityName}FailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        name = this.name.toString(),
        updated = this.modifiedTimestamp.toString().toZonedDateTime(),
        version = this.version,
        modifiedBy = this.modifiedBy.toString(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
    )
}


fun ${entityName}DeleteEvent.to${entityName}FailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): ${entityName}FailureEvent {
    return ${entityName}FailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
        version = this.version
    )
}


fun ${entityName}ChangeException.to${entityName}FailureStreamEvent(source: String): ${entityName}FailureStreamEvent {
    val input = this
    return ${entityName}FailureStreamEvent().apply {
        this.${entityName.toLowerCase()}Details = input.${entityName.toLowerCase()}FailureEvent.to${entityName}Details()
        this.reference = input.reference
        this.messageId = input.${entityName.toLowerCase()}FailureEvent.messageId
        this.failureTimestamp = input.${entityName.toLowerCase()}FailureEvent.failureDateTime.toISOString()
        this.failureReason = input.message
        this.failureReference = input.reference
        this.failureSource = source
    }

}

fun ${entityName}FailureEvent.to${entityName}Details(): ${entityName}Details {
    val ${entityName.toLowerCase()} = this
    return ${entityName}Details().apply {
        this.id = ${entityName.toLowerCase()}.id
        this.version = ${entityName.toLowerCase()}.version ?: 0
        this.name = ${entityName.toLowerCase()}.name
        this.reference = ${entityName.toLowerCase()}.reference
    }
}


interface I${entityName}DataSource {

    fun create(${entityName.toLowerCase()}CreateEvent: ${entityName}CreateEvent): Mono<${entityName}>

    fun update(${entityName.toLowerCase()}UpdateEvent: ${entityName}UpdateEvent): Mono<${entityName}>

    fun findById(id: String): Mono<${entityName}>

    fun findAll(): Flux<${entityName}>

    fun delete(${entityName.toLowerCase()}DeleteEvent: ${entityName}DeleteEvent): Mono<${entityName}>

    fun failure(${entityName.toLowerCase()}FailureEvent: ${entityName}FailureEvent): Mono<Unit>

}

@Suppress("unused")
open class ${entityName}ChangeException(
    val ${entityName.toLowerCase()}FailureEvent: ${entityName}FailureEvent,
    val eventType: String,
    val reference: String = uuid(),
    throwable: Throwable
) : RuntimeException(throwable)


val ${entityName.toLowerCase()}StreamHandler:
            (Any, I${entityName}DataSource, Logger, () -> ZonedDateTime, (${entityName}ChangeException) -> Mono<Unit>) -> Mono<${entityName}> = { event, ds, log, currentZonedDateTime, errorSender ->

    when (event) {
        is ${entityName}CreateEvent -> handleCreateEvent(event, ds, log, currentZonedDateTime)
        is ${entityName}UpdateEvent -> handleUpdateEvent(event, ds, log, currentZonedDateTime)
        is ${entityName}DeleteEvent -> handleDeleteEvent(event, ds, log, currentZonedDateTime)
        else -> {
            log.error("Unexpected record type={}", event::class.java)
            Mono.empty()
        }}
        .doOnError { log.error("Experienced ERROR", it) }
        .handleError(
            errorSender = errorSender
        ) { ds.failure(it.${entityName.toLowerCase()}FailureEvent) }
        .onErrorResume {
            log.error("Experienced error handling event", it)
            Mono.empty()
        }
    }



val handleCreateEvent: (${entityName}CreateEvent, I${entityName}DataSource, Logger, () -> ZonedDateTime) -> Mono<${entityName}> = { createEvent, ds, log, currentZonedDateTime ->
    log.info("handleCreateEvent {}", createEvent)
    ds.create(createEvent)
        .doOnError {
            log.error("Exception thrown in handleCreateEvent", it)
        }
        .onErrorMap {
            ${entityName}ChangeException(
                ${entityName.toLowerCase()}FailureEvent = createEvent.to${entityName}FailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "create",
                reference = createEvent.reference.toString(),
                throwable = it
            )
        }
}

val handleUpdateEvent: (${entityName}UpdateEvent, I${entityName}DataSource, Logger, () -> ZonedDateTime) -> Mono<${entityName}> = { updateEvent, ds, log, currentZonedDateTime ->
    log.info("handleUpdateEvent {}", updateEvent)
    ds.update(updateEvent)
        .onErrorMap {
            log.error("Failed to update {}", updateEvent, it)
            ${entityName}ChangeException(
                ${entityName.toLowerCase()}FailureEvent = updateEvent.to${entityName}FailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "update",
                reference = updateEvent.reference.toString(),
                throwable = it
            )
        }
}

val handleDeleteEvent: (${entityName}DeleteEvent, I${entityName}DataSource, Logger, () -> ZonedDateTime) -> Mono<${entityName}> = { deleteEvent, ds, log, currentZonedDateTime ->
    log.info("handleDeleteEvent {}", deleteEvent)
    ds
        .delete(deleteEvent)
        .doOnError {
            log.warn("***** Exception encountered in handleDeleteEvent", it)
        }
        .onErrorMap {
            log.error("Failed to delete {}", deleteEvent, it)
            ${entityName}ChangeException(
                ${entityName.toLowerCase()}FailureEvent = deleteEvent.to${entityName}FailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "delete",
                reference = deleteEvent.reference.toString(),
                throwable = it
            )
        }
        .doOnError {
            log.warn("***** Exception encountered in handleDeleteEvent", it)
        }
}

fun <V, E: Throwable> Mono<V>.handleError(
    errorSender: (E) -> Mono<Unit>,
    errorWriter: (E) -> Mono<Unit>,
): Mono<V> =
    this.onErrorResume { ex ->
        errorWriter(ex as E)
            .doOnError {
                println("error saving failure message")
            }
            .then(errorSender(ex))
            .log("Called error sender")
            .then(Mono.error(ex))

    }
