/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Tag
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpMethod
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.test.StepVerifier
import java.time.Duration

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integration")
class ${entityName}CommandWebRestControllerIntTest {

    @LocalServerPort
    private var localServerPort: Int? = null

    private var webClient = defaultWebClientBuilder.baseUrl("http://localhost:8080/api/${entityName.toLowerCase()}/1.0").build()

    @Autowired
    private lateinit var ${entityName.toLowerCase()}InMemoryService: ${entityName}InMemoryService

    @BeforeEach
    fun setUp() {
        webClient = defaultWebClientBuilder
            .baseUrl("http://localhost:$localServerPort/api/${entityName.toLowerCase()}/1.0")
            .build()
    }

    @Test
    fun create() {
        val ${entityName.toLowerCase()}Create = ${entityName}Create()

        val retrieveMono = webClient
            .put()
            .bodyValue(${entityName.toLowerCase()}Create)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(${entityName.toLowerCase()}Create.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(${entityName.toLowerCase()}Create.to${entityName}(), ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}Create.id]?.withLocalZonedDateTime())

    }

    @Test
    fun update() {
        val ${entityName.toLowerCase()}Update = ${entityName}Update()
        ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}Update.id] = ${entityName.toLowerCase()}Update.to${entityName}()
        val retrieveMono = webClient
            .post()
            .bodyValue(${entityName.toLowerCase()}Update)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(${entityName.toLowerCase()}Update.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(${entityName.toLowerCase()}Update.to${entityName}(), ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}Update.id]?.withLocalZonedDateTime())

    }

    @Test
    fun delete() {
        val ${entityName.toLowerCase()}Delete = ${entityName}Delete()
        ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}Delete.id] = ${entityName}(id = ${entityName.toLowerCase()}Delete.id)
        val retrieveMono = webClient
            .method(HttpMethod.DELETE)
            .bodyValue(${entityName.toLowerCase()}Delete)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(${entityName.toLowerCase()}Delete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertNull(${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}Delete.id]?.withLocalZonedDateTime())

    }

}

internal fun ${entityName}.withLocalZonedDateTime(): ${entityName} {
    return ${entityName}(
        id = this.id,
        version = this.version,
        name = this.name,
        lastModifiedTimestamp = this.lastModifiedTimestamp.withZoneSameInstant(java.time.ZoneId.systemDefault())
    )
}