/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration

class ${entityName}CommandWebRestControllerTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun create() {
        val service = mockk<I${entityName}CommandApi>()
        val ${entityName.toLowerCase()}Create = ${entityName}Create()
        every { service.create(${entityName.toLowerCase()}Create) } returns Mono.just(${entityName.toLowerCase()}Create.id)

        val controller = ${entityName}CommandWebRestController(service)

        StepVerifier
            .create(controller.create(${entityName.toLowerCase()}Create))
            .expectNext(${entityName.toLowerCase()}Create.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.create(${entityName.toLowerCase()}Create) }

    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun update() {
        val service = mockk<I${entityName}CommandApi>()
        val ${entityName.toLowerCase()}Update = ${entityName}Update()
        every { service.update(${entityName.toLowerCase()}Update) } returns Mono.just(${entityName.toLowerCase()}Update.id)

        val controller = ${entityName}CommandWebRestController(service)

        StepVerifier
            .create(controller.update(${entityName.toLowerCase()}Update))
            .expectNext(${entityName.toLowerCase()}Update.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.update(${entityName.toLowerCase()}Update) }

    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun delete() {
        val service = mockk<I${entityName}CommandApi>()
        val ${entityName.toLowerCase()}Delete = ${entityName}Delete()
        every { service.delete(${entityName.toLowerCase()}Delete) } returns Mono.just(${entityName.toLowerCase()}Delete.id)

        val controller = ${entityName}CommandWebRestController(service)

        StepVerifier
            .create(controller.delete(${entityName.toLowerCase()}Delete))
            .expectNext(${entityName.toLowerCase()}Delete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.delete(${entityName.toLowerCase()}Delete) }

    }
}
