/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import org.simtp.graphql.EnableSimTPGraphQl
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.*
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(${entityName}CommandWebServerConfiguration::class)
annotation class Enable${entityName}CommandWebServer

@Configuration
@ComponentScan
@EnableSimTPGraphQl
open class ${entityName}CommandWebServerConfiguration

@RestController
@RequestMapping("/api/${entityName.toLowerCase()}/1.0")
open class ${entityName}CommandWebRestController(
    private val ${entityName.toLowerCase()}CommandService: I${entityName}CommandApi
) {

    @PutMapping
    fun create(@RequestBody ${entityName.toLowerCase()}Create: ${entityName}Create): Mono<String> {
        return ${entityName.toLowerCase()}CommandService.create(${entityName.toLowerCase()}Create)
    }

    @PostMapping
    fun update(@RequestBody ${entityName.toLowerCase()}Update: ${entityName}Update): Mono<String> {
        return ${entityName.toLowerCase()}CommandService.update(${entityName.toLowerCase()}Update)
    }

    @DeleteMapping
    fun delete(@RequestBody ${entityName.toLowerCase()}Delete: ${entityName}Delete): Mono<String> {
        return ${entityName.toLowerCase()}CommandService.delete(${entityName.toLowerCase()}Delete)
    }
}

@Controller
class ${entityName}CommandController(
    private val ${entityName.toLowerCase()}CommandService: I${entityName}CommandApi,
) {

    init {
        log.info("Initializing ${entityName}CommandController")
    }

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(${entityName}CommandController::class.java)
    }

    @MutationMapping
    fun ${entityName.toLowerCase()}Create(@Argument ${entityName.toLowerCase()}Create: ${entityName}Create): Mono<String> {
        return ${entityName.toLowerCase()}CommandService.create(${entityName.toLowerCase()}Create)
    }

    @MutationMapping
    fun ${entityName.toLowerCase()}Update(@Argument ${entityName.toLowerCase()}Update: ${entityName}Update): Mono<String> {
        return ${entityName.toLowerCase()}CommandService.update(${entityName.toLowerCase()}Update)
    }

    @MutationMapping
    fun ${entityName.toLowerCase()}Delete(@Argument ${entityName.toLowerCase()}Delete: ${entityName}Delete): Mono<String> {
        return ${entityName.toLowerCase()}CommandService.delete(${entityName.toLowerCase()}Delete)
    }
}


