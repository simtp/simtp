
/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import org.simtp.graphql.EnableSimTPGraphQl
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.*
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(${entityName}QueryWebServerConfiguration::class)
annotation class Enable${entityName}QueryWebServer

@Configuration
@ComponentScan
@EnableSimTPGraphQl

open class ${entityName}QueryWebServerConfiguration

@RestController
@RequestMapping("/api/${entityName.toLowerCase()}/1.0")
open class ${entityName}QueryWebRestController(
    private val ${entityName.toLowerCase()}QueryService: I${entityName}QueryApi
) {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(${entityName}QueryWebRestController::class.java)
    }

    @GetMapping("/{id}")
    operator fun get(@PathVariable id: String): Mono<ResponseEntity<${entityName}>> {
        return ${entityName.toLowerCase()}QueryService[id]
            .doOnNext { log.info("Received value={}", it) }
            .doOnError { log.error("Exception handling query", it) }
            .map { ResponseEntity.ofNullable(it) }
            .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()))
            .doOnNext { log.info("Return ${entityName.toLowerCase()} query for id={} value={}", id, it.statusCode) }
    }
}

@Controller
class ${entityName}QueryController(
    private val ${entityName.toLowerCase()}QueryService: I${entityName}QueryApi
) {

    @QueryMapping
    fun get${entityName}(@Argument id: String): Mono<${entityName}> {
        return ${entityName.toLowerCase()}QueryService[id]
    }

}

