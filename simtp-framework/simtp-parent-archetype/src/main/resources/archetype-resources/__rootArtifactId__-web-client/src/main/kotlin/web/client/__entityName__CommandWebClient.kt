/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.client

import ${package}.api.*
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.*
import reactor.core.publisher.Mono
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.web.reactive.function.client.WebClient

@Suppress("unused")
@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(${entityName}CommandWebClientConfiguration::class)
annotation class Enable${entityName}CommandWebClient

@Configuration
open class ${entityName}CommandWebClientConfiguration {


    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    open fun ${entityName.toLowerCase()}CommandWebClient(
        webClientBuilder: WebClient.Builder,
        ${entityName.toLowerCase()}WebClientSettings: ${entityName}WebClientSettings
    ): ${entityName}CommandWebClient {
        return ${entityName}CommandWebClient(
            webClientBuilder = webClientBuilder,
            baseUrlProvider = { ${entityName.toLowerCase()}WebClientSettings.baseUrl }
        )
    }

    @Bean
    @ConfigurationProperties(prefix = "simtp.${entityName.toLowerCase()}.command.web.client")
    open fun ${entityName.toLowerCase()}CommandWebClientSettings(): ${entityName}CommandWebClientSettings {
        return ${entityName}CommandWebClientSettings()
    }
}

data class ${entityName}CommandWebClientSettings(
    var baseUrl: String = "http://localhost:8080"
)

open class ${entityName}CommandWebClient(
    webClientBuilder: WebClient.Builder,
    private val baseUrlProvider: () -> String
): I${entityName}CommandApi {

    private val webClient = webClientBuilder.build()

    override fun create(${entityName.toLowerCase()}Create: ${entityName}Create): Mono<String> {
        return webClient
            .put()
            .uri("${baseUrlProvider()}/api/${entityName.toLowerCase()}/1.0")
            .bodyValue(${entityName.toLowerCase()}Create)
            .retrieve()
            .bodyToMono(String::class.java)
    }

    override fun update(${entityName.toLowerCase()}Update: ${entityName}Update): Mono<String> {
        return webClient
            .post()
            .uri("${baseUrlProvider()}/api/${entityName.toLowerCase()}/1.0")
            .bodyValue(${entityName.toLowerCase()}Update)
            .retrieve()
            .bodyToMono(String::class.java)
    }

    override fun delete(${entityName.toLowerCase()}Delete: ${entityName}Delete): Mono<String> {
        return webClient
            .method(HttpMethod.DELETE)
            .uri("${baseUrlProvider()}/api/${entityName.toLowerCase()}/1.0")
            .bodyValue(${entityName.toLowerCase()}Delete)
            .retrieve()
            .bodyToMono(String::class.java)
    }
}

