#set($dollar = '$')
/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package ${package}.datasource.memory

import ${package}.api.*
import ${package}.api.stream.*
import ${package}.api.${entityName}FailureEvent
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.common.IDateTimeManager
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.receiver.ReceiverOptions


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(${entityName}MemoryDataSourceConfiguration::class)
annotation class Enable${entityName}MemoryDataSource


@Configuration
open class ${entityName}MemoryDataSourceConfiguration {

    @Bean
    open fun ${entityName.toLowerCase()}MemoryDataSource(): ${entityName}MemoryDataSource {
        return ${entityName}MemoryDataSource()
    }

    @Bean
    open fun ${entityName.toLowerCase()}MemoryReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\\${dollar}{spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "${entityName.toLowerCase()}MemoryReactiveKafkaConsumer:#[[$applicationName]]#"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "${entityName.toLowerCase()}MemoryReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(${entityName.toUpperCase()}_STREAM_NAME)))
    }


    @Bean
    open fun ${entityName.toLowerCase()}MemoryStreamConsumer(
        ${entityName.toLowerCase()}MemoryReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        ${entityName.toLowerCase()}MemoryDataSource: ${entityName}MemoryDataSource,
        dateTimeManager: IDateTimeManager
    ): ${entityName}MemoryStreamConsumer {
        return ${entityName}MemoryStreamConsumer(
            ${entityName.toLowerCase()}MemoryReactiveKafkaConsumerTemplate = ${entityName.toLowerCase()}MemoryReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            ${entityName.toLowerCase()}MemoryDataSource = ${entityName.toLowerCase()}MemoryDataSource,
            dateTimeManager = dateTimeManager
        )
    }

}

class ${entityName}MemoryStreamConsumer(
    private val ${entityName.toLowerCase()}MemoryReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val ${entityName.toLowerCase()}MemoryDataSource: ${entityName}MemoryDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(${entityName}MemoryStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<${entityName}> {
        return ${entityName.toLowerCase()}MemoryReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                ${entityName.toLowerCase()}StreamHandler(
                    it.value(),
                    ${entityName.toLowerCase()}MemoryDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, ${entityName.toUpperCase()}_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.to${entityName}FailureStreamEvent(this::class.simpleName!!) }
                }
            }

    }
}

@Suppress("MemberVisibilityCanBePrivate")
class ${entityName}MemoryDataSource(
    val map: MutableMap<String, ${entityName}> = mutableMapOf(),
    val messageIds: MutableSet<String> = mutableSetOf(),
    val failedMessages: MutableMap<String, Set<${entityName}FailureEvent>> = mutableMapOf()
): I${entityName}DataSource {

    companion object {
        private val log = LoggerFactory.getLogger(${entityName}MemoryDataSource::class.java)
    }

    override fun create(${entityName.toLowerCase()}CreateEvent: ${entityName}CreateEvent): Mono<${entityName}> {
        if (log.isDebugEnabled) log.debug("Called create {}", ${entityName.toLowerCase()}CreateEvent)
        return if (!messageIds.contains(${entityName.toLowerCase()}CreateEvent.messageId.toString())) {
            Mono.just(save${entityName}(${entityName.toLowerCase()}CreateEvent.to${entityName}()))
        } else {
            Mono.empty()
        }
            .saveMessageId(${entityName.toLowerCase()}CreateEvent.messageId.toString())
    }

    override fun delete(${entityName.toLowerCase()}DeleteEvent: ${entityName}DeleteEvent): Mono<${entityName}> {
        if (log.isDebugEnabled) log.debug("Called delete {}", ${entityName.toLowerCase()}DeleteEvent)
        return if (!messageIds.contains(${entityName.toLowerCase()}DeleteEvent.messageId.toString())) {
            val existing = map[${entityName.toLowerCase()}DeleteEvent.id.toString()]
            if (existing != null && existing.version != ${entityName.toLowerCase()}DeleteEvent.version)
                throw OptimisticLockingFailureException("Current version=$existing does not match provided version=${${entityName.toLowerCase()}DeleteEvent.version}")

            map.remove(${entityName.toLowerCase()}DeleteEvent.id.toString())?.let { Mono.just(it)} ?: Mono.empty()
        } else {
            Mono.empty()
        }
            .saveMessageId(${entityName.toLowerCase()}DeleteEvent.messageId.toString())

    }

    override fun update(${entityName.toLowerCase()}UpdateEvent: ${entityName}UpdateEvent): Mono<${entityName}> {
        if (log.isDebugEnabled) log.debug("Called update {}", ${entityName.toLowerCase()}UpdateEvent)
        return if (!messageIds.contains(${entityName.toLowerCase()}UpdateEvent.messageId.toString())) {
            Mono.just(save${entityName}(${entityName.toLowerCase()}UpdateEvent.to${entityName}()))
        } else {
            Mono.empty()
        }
            .saveMessageId(${entityName.toLowerCase()}UpdateEvent.messageId.toString())
    }

    private fun save${entityName}(${entityName.toLowerCase()}: ${entityName}): ${entityName} {
        if (log.isDebugEnabled) log.debug("Called save${entityName} {}", ${entityName.toLowerCase()})
        val existing = map[${entityName.toLowerCase()}.id]
        if (existing != null && existing.version != ${entityName.toLowerCase()}.version)
            throw OptimisticLockingFailureException("Current version=$existing does not match provided version=${${entityName.toLowerCase()}.version}")

        val newVal = ${entityName.toLowerCase()}.cloneAndIncrementVersion()
        map[${entityName.toLowerCase()}.id] = newVal

        return newVal

    }

    private fun Mono<${entityName}>.saveMessageId(messageId: String): Mono<${entityName}> {
        if (log.isDebugEnabled) log.debug("Called saveMessageId {}", messageId)
        return this
            .map {
                messageIds.add(messageId)
                it
            }
    }

    override fun findById(id: String): Mono<${entityName}> {
        return map[id]?.let { Mono.just(it)} ?: Mono.empty()
    }

    override fun findAll(): Flux<${entityName}> {
        return Flux.fromIterable(map.values)

    }

    override fun failure(${entityName.toLowerCase()}FailureEvent: ${entityName}FailureEvent): Mono<Unit> {
        return Mono.fromCallable {
            if (log.isWarnEnabled) log.debug("Called failure {}", ${entityName.toLowerCase()}FailureEvent)
            failedMessages.merge(${entityName.toLowerCase()}FailureEvent.id, setOf(${entityName.toLowerCase()}FailureEvent)) { p1, p2 -> p1+p2 }
        }
    }


}
