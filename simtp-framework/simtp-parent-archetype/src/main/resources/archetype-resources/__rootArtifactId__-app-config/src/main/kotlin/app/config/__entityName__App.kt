package ${package}.app.config

import org.simtp.common.EnableCommonConfiguration
import ${package}.datasource.memory.Enable${entityName}MemoryDataSource
import ${package}.datasource.redis.Enable${entityName}RedisDataSource
import ${package}.datasource.r2dbc.Enable${entityName}R2dbcDataSource
import ${package}.service.Enable${entityName}CommandService
import ${package}.service.Enable${entityName}QueryService
import ${package}.web.server.Enable${entityName}CommandWebServer
import ${package}.web.server.Enable${entityName}QueryWebServer
import org.simtp.stream.kafka.EnableSimtpKafka


/*
    NOTE currently it is not possible to use Spring Eureka with a dynamic port due to the ordering
    in which the application starts:-

    https://github.com/spring-cloud/spring-cloud-consul/issues/555
 */

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@Enable${entityName}R2dbcDataSource
@EnableCommonConfiguration
@Enable${entityName}QueryWebServer
@Enable${entityName}QueryService
annotation class Enable${entityName}QueryWithR2dbc

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@Enable${entityName}RedisDataSource
@EnableCommonConfiguration
@Enable${entityName}QueryWebServer
@Enable${entityName}QueryService
annotation class Enable${entityName}QueryWithRedis

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@Enable${entityName}MemoryDataSource
@EnableCommonConfiguration
@Enable${entityName}QueryWebServer
@Enable${entityName}QueryService
annotation class Enable${entityName}QueryWithMemory

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnableCommonConfiguration
@Enable${entityName}CommandWebServer
@Enable${entityName}CommandService
annotation class Enable${entityName}Command
