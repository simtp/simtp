package $

import org.junit.jupiter.api.Tag

{package}.app.config

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Tag
import ${package}.api.*
import ${package}.web.client.*
import org.simtp.common.reactive.retryOn4xx
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.test.annotation.DirtiesContext
import org.springframework.web.reactive.function.client.WebClientResponseException
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import reactor.test.StepVerifier
import reactor.util.retry.Retry
import java.time.Duration
import java.time.ZonedDateTime

@Suppress("SpringBootApplicationProperties")
@Tag("integration")
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = [
        "schema.registry.url=mock://localhost:8081"
    ]
)
@Testcontainers
@Enable${entityName}CommandWebClient
@Enable${entityName}QueryWebClient
@DirtiesContext
@EmbeddedKafka(partitions = 1, brokerProperties = [ "listeners=PLAINTEXT://localhost:9092" ])
class ${entityName}TestAppTest {

    @LocalServerPort
    private var localServerPort: Int? = null

    companion object {

        @Container
        @ServiceConnection
        @JvmStatic
        private val postgresContainer = PostgreSQLContainer("postgres:latest")


        @Container
        @ServiceConnection
        @JvmStatic
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)

        @BeforeAll
        @JvmStatic
        fun setUp() {
            postgresContainer
                .withReuse(true)
                .start()
            redisContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            postgresContainer.stop()
            redisContainer.stop()
        }

    }

    @Autowired
    private lateinit var ${entityName.toLowerCase()}CommandWebClientSettings: ${entityName}CommandWebClientSettings

    @Autowired
    private lateinit var ${entityName.toLowerCase()}QueryWebClientSettings: ${entityName}QueryWebClientSettings

    @BeforeEach
    fun beforeEach() {
        ${entityName.toLowerCase()}CommandWebClientSettings.baseUrl = "http://localhost:$localServerPort"

        ${entityName.toLowerCase()}QueryWebClientSettings.baseUrl = "http://localhost:$localServerPort"
    }


    @Test
    fun contextLoads() {}

    @Autowired
    private lateinit var ${entityName.toLowerCase()}CommandWebClient: ${entityName}CommandWebClient

    @Autowired
    private lateinit var ${entityName.toLowerCase()}QueryWebClient: ${entityName}QueryWebClient

    @Test
    fun `Create ${entityName} via API`() {
        val ${entityName.toLowerCase()}Create = ${entityName}Create(
            name = "Joe"
        )
        StepVerifier
            .create(${entityName.toLowerCase()}CommandWebClient.create(${entityName.toLowerCase()}Create))
            .expectNext(${entityName.toLowerCase()}Create.id)
            .verifyComplete()

        StepVerifier
            .create(${entityName.toLowerCase()}QueryWebClient[${entityName.toLowerCase()}Create.id].retryOn4xx())
            .expectNext(${entityName.toLowerCase()}Create.to${entityName}().cloneAndIncrementVersion())
            .expectComplete()
            .verify(Duration.ofSeconds(10))

    }

    @Test
    fun `Update ${entityName} via API`() {
        val ${entityName.toLowerCase()}Create = ${entityName}Create(
            name = "Joe"
        )
        StepVerifier
            .create(${entityName.toLowerCase()}CommandWebClient.create(${entityName.toLowerCase()}Create))
            .expectNext(${entityName.toLowerCase()}Create.id)
            .verifyComplete()

        var ${entityName.toLowerCase()}: ${entityName}? = null

        StepVerifier
            .create(${entityName.toLowerCase()}QueryWebClient[${entityName.toLowerCase()}Create.id].retryOn4xx())
            .consumeNextWith {
                ${entityName.toLowerCase()} = it
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))

        assertNotNull(${entityName.toLowerCase()})

        val ${entityName.toLowerCase()}Update = ${entityName}Update(
            id = ${entityName.toLowerCase()}!!.id,
            name = "Jason",
            modifiedTimestamp = ZonedDateTime.now(),
            modifiedBy = "test",
            version = ${entityName.toLowerCase()}!!.version
        )

        StepVerifier
            .create(${entityName.toLowerCase()}CommandWebClient.update(${entityName.toLowerCase()}Update))
            .expectNext(${entityName.toLowerCase()}Update.id)
            .verifyComplete()

        val flux = ${entityName.toLowerCase()}QueryWebClient[${entityName.toLowerCase()}Update.id]
            .doOnNext {
                assertEquals("Jason", it.name)
            }
            .retryWhen(
                Retry.backoff(3, Duration.ofSeconds(2))
                    .filter { t -> t is AssertionError }
            )


        StepVerifier
            .create(flux)
            .expectNextMatches {
                it.name == "Jason" && it.version > 0
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))
    }

    @Test
    fun `Delete ${entityName} via API`() {
        val ${entityName.toLowerCase()}Create = ${entityName}Create(
            name = "Joe"
        )
        StepVerifier
            .create(${entityName.toLowerCase()}CommandWebClient.create(${entityName.toLowerCase()}Create))
            .expectNext(${entityName.toLowerCase()}Create.id)
            .verifyComplete()

        var ${entityName.toLowerCase()}: ${entityName}? = null

        StepVerifier
            .create(${entityName.toLowerCase()}QueryWebClient[${entityName.toLowerCase()}Create.id].retryOn4xx())
            .consumeNextWith {
                ${entityName.toLowerCase()} = it
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))

        assertNotNull(${entityName.toLowerCase()})

        val ${entityName.toLowerCase()}Delete = ${entityName}Delete(
            id = ${entityName.toLowerCase()}!!.id,
            deletedTimestamp = ZonedDateTime.now(),
            deletedBy = "test",
            version = ${entityName.toLowerCase()}!!.version
        )

        StepVerifier
            .create(${entityName.toLowerCase()}CommandWebClient.delete(${entityName.toLowerCase()}Delete))
            .expectNext(${entityName.toLowerCase()}Delete.id)
            .verifyComplete()

        val flux = ${entityName.toLowerCase()}QueryWebClient[${entityName.toLowerCase()}Delete.id]
            .doOnNext {
                //When the value is delete from the datastore, this method shouldn't be
                //executed. Until then throw an AssertionError that will cause a retry until
                //the entry is actually deleted from the backing data store
                assertNull(it)
            }
            .retryWhen(
                Retry.backoff(100, Duration.ofMillis(200))
                    .filter { t -> t is AssertionError }
            )


        StepVerifier
            .create(flux)
            .expectError(WebClientResponseException::class.java)
            .verify(Duration.ofSeconds(10))
    }

}

@Enable${entityName}QueryWithR2dbc
@Enable${entityName}QueryWithRedis
@Enable${entityName}QueryWithMemory
@Enable${entityName}Command
@SpringBootApplication
open class ${entityName}TestApp