package org.simtp.stream.kafka

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.simtp.configuration.YamlPropertySourceFactory
import org.slf4j.Logger
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.receiver.ReceiverOptions
import reactor.kafka.sender.SenderOptions

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(KafkaStreamConfiguration::class)
annotation class EnableSimtpKafka

@Configuration
@EnableKafka
@PropertySource("classpath:kafka.yml", factory = YamlPropertySourceFactory::class)
open class KafkaStreamConfiguration {

    @Bean
    open fun reactiveKafkaProducerTemplate(
        kafkaProperties: KafkaProperties
    ): ReactiveKafkaProducerTemplate<String, Any> {
        return ReactiveKafkaProducerTemplate(SenderOptions.create(kafkaProperties.buildProducerProperties(null)))
    }

    @Bean
    open fun baseReceiverOptions(
        kafkaProperties: KafkaProperties): ReceiverOptions<String, Any> {
        return ReceiverOptions
            .create(kafkaProperties.buildConsumerProperties(null))
    }


}

fun <K, V, T> ReactiveKafkaConsumerTemplate<K, V>.consumeAutoAck(
    handler: (ConsumerRecord<K, V>) -> Mono<T>
): Flux<T> =
    this
        .receiveAutoAck()
        .flatMap { handler(it) }


fun <K, V, NV: Any> failureSender(
    log: Logger,
    streamName: String,
    reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<K, NV>,
    ex: Throwable,
    messageProvider: () -> NV,
): Mono<V> {
    return reactiveKafkaProducerTemplate
        .send("$streamName.DLQ", messageProvider())
        .doOnNext {
            log.info("Sent message to DLQ {}", messageProvider())
        }
        .doOnError {
            log.error("Failed to place message on DLQ {}", messageProvider())
        }
        .flatMap { Mono.error(ex) }

}
