package org.simtp.stream.kafka

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.kafka.receiver.ReceiverOptions
import kotlin.test.assertNotNull
import org.springframework.boot.autoconfigure.kafka.KafkaProperties

class KafkaStreamConfigurationTest {

    private lateinit var kafkaStreamConfiguration: KafkaStreamConfiguration

    @BeforeEach
    fun setup() {
        kafkaStreamConfiguration = KafkaStreamConfiguration()
    }

    @Test
    fun testReactiveKafkaProducerTemplate() {
        val kafkaProperties: KafkaProperties = mockk()
        val receiverProps: Map<String, Any> = mapOf("bootstrap.servers" to "localhost:9092")

        every { kafkaProperties.bootstrapServers } returns listOf(receiverProps.getValue("bootstrap.servers").toString())
        every { kafkaProperties.buildProducerProperties(any()) } returns receiverProps

        val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any> =
            kafkaStreamConfiguration.reactiveKafkaProducerTemplate(kafkaProperties)

        assertNotNull(reactiveKafkaProducerTemplate)
    }

    @Test
    fun testBaseReceiverOptions() {
        val kafkaProperties: KafkaProperties = mockk()
        val receiverProps: Map<String, Any> = mapOf("bootstrap.servers" to "localhost:9092")

        every { kafkaProperties.bootstrapServers } returns listOf(receiverProps.getValue("bootstrap.servers").toString())
        every { kafkaProperties.buildConsumerProperties(null) } returns receiverProps

        val receiverOptions: ReceiverOptions<String, Any> =
            kafkaStreamConfiguration.baseReceiverOptions(kafkaProperties)

        assertNotNull(receiverOptions)
    }
}