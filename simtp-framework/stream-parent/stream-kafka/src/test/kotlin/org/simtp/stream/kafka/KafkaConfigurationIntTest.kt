package org.simtp.stream.kafka

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import org.springframework.kafka.test.context.EmbeddedKafka
import reactor.kafka.receiver.ReceiverOptions
import reactor.test.StepVerifier
import java.time.Duration


@SpringBootTest(
    properties = [
        "spring.kafka.properties[schema.registry.url]: mock://localhost:8081",
        "spring.kafka.properties[auto.register.schemas]=true",
        "spring.kafka.properties[specific.avro.reader]=true"
    ]
)
@EmbeddedKafka(
    topics = ["TEST"],

)
@Tag("integration")
class KafkaConfigurationTest {

    @Autowired
    private lateinit var reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>

    @Autowired
    private lateinit var reactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, User>


    @Test
    fun contextLoads() {
        val user = User()
            .apply {
                this.name = "TESTING"
            }
        StepVerifier
            .create(reactiveKafkaProducerTemplate.send("TEST", user))
            .expectNextMatches { it.exception() == null }
            .verifyComplete()

        StepVerifier
            .create(reactiveKafkaConsumerTemplate.receiveAutoAck())
            .expectNextMatches {
                (it.value() as User).name.toString() == "TESTING"
            }
            .thenCancel()
            .verify(Duration.ofSeconds(5))

    }
}

//
//@Suppress("SpringJavaInjectionPointsAutowiringInspection")


@SpringBootApplication
open class TestApp {

    @Bean
    open fun reactiveKafkaConsumerTemplate(
        baseReceiverOptions: ReceiverOptions<String, *>
    ): ReactiveKafkaConsumerTemplate<String, *> {
        return ReactiveKafkaConsumerTemplate(baseReceiverOptions.subscription(listOf("TEST")))
    }

}

