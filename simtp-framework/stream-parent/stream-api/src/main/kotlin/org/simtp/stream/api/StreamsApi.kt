package org.simtp.stream.api

import reactor.core.publisher.Mono

enum class StreamMessageType {
    CREATE,
    UPDATE,
    DELETE

}

interface IStreamConfigurer {
    fun configure(streamConfiguration: StreamConfiguration)
}

data class StreamDefinition<T>(
    val streamName: String,
    val handler: (Map<String, String>) -> Mono<T>
)

class StreamConfiguration {

    private var streams: List<StreamDefinition<*>> = emptyList()

//    fun <T> addStreamConsumer(streamDefinition: StreamDefinition<T>) {
//        streams = streams + listOf(streamDefinition)
//    }
//
//    fun <T> addStreamConsumer(streamName: String, handler: (Map<String, String>) -> Mono<T>) {
//        addStreamConsumer(StreamDefinition(
//            streamName = streamName,
//            handler = handler
//        ))
//    }
//
//    fun getStreamDefinitions(): List<StreamDefinition<*>> {
//        return streams
//    }

    override fun toString(): String {
        return "StreamConfiguration(streams=$streams)"
    }


}