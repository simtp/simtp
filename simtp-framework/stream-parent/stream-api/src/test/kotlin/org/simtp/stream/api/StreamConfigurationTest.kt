package org.simtp.stream.api

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class StreamConfigurationTest {
    private lateinit var streamConfiguration: StreamConfiguration

    @BeforeEach
    fun setUp() {
        streamConfiguration = StreamConfiguration()
    }

    @Test
    fun testToString() {
        val expectedToString = "StreamConfiguration(streams=[])"
        assertEquals(expectedToString, streamConfiguration.toString())
    }

}