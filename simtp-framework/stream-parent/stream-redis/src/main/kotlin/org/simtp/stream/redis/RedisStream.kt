package org.simtp.stream.redis

import org.simtp.stream.api.IStreamConfigurer
import org.simtp.stream.api.StreamConfiguration
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.RedisSystemException
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.connection.stream.Consumer
import org.springframework.data.redis.connection.stream.MapRecord
import org.springframework.data.redis.connection.stream.ReadOffset
import org.springframework.data.redis.connection.stream.StreamOffset
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.redis.core.ReactiveStreamOperations
import org.springframework.data.redis.stream.StreamReceiver
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Import(RedisConfiguration::class)
annotation class EnableRedisStream

@Configuration
open class RedisConfiguration {


    @Bean
    open fun reactiveStreamOperations(redisOperations: ReactiveRedisOperations<String, String>)
            : ReactiveStreamOperations<String, String, String> {
        return redisOperations.opsForStream()
    }

    @Bean
    @ConditionalOnMissingBean(IStreamConfigurer::class)
    open fun defaultRedisStreamConfigurer(): IStreamConfigurer {
        return object: IStreamConfigurer {
            override fun configure(streamConfiguration: StreamConfiguration) {
                //Do nothing here
            }
        }
    }

}

open class RedisStream {
    companion object {
        @JvmStatic
        private val LOG = LoggerFactory.getLogger(RedisStream::class.java)


        @JvmStatic
        fun createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory,
            streamName: String,
            groupName: String,
            consumerName: String,
            reactiveRedisOperations: ReactiveRedisOperations<String, String>,
            offset: ReadOffset = ReadOffset.lastConsumed()
        ): Flux<MapRecord<String, String, String>> {
            return createConsumerGroup(
                reactiveRedisOperations,
                streamName,
                groupName
            ).flux()
            .flatMap {
                createStreamReceiverAutoAck(
                    reactiveRedisConnectionFactory,
                    streamName,
                    Consumer.from(groupName, consumerName),
                    offset
                )
            }
        }

        @JvmStatic
        fun createGroupAndReceiver(
            connectionFactory: ReactiveRedisConnectionFactory,
            streamName: String,
            groupName: String,
            consumerName: String,
            offset: ReadOffset = ReadOffset.lastConsumed(),
            reactiveRedisOperations: ReactiveRedisOperations<String, String>
        ): Flux<MapRecord<String, String, String>> {
            return createConsumerGroup(
                reactiveRedisOperations,
                streamName,
                groupName
            ).flux()
                .flatMap {
                    createStreamReceiver(
                        connectionFactory,
                        streamName,
                        Consumer.from(groupName, consumerName),
                        offset
                    )
                }
        }

        @JvmStatic
        fun createStreamReceiver(
            connectionFactory: ReactiveRedisConnectionFactory,
            streamName: String,
            consumer: Consumer,
            offset: ReadOffset = ReadOffset.lastConsumed(),
        ): Flux<MapRecord<String, String, String>> {
            val options = StreamReceiver
                .StreamReceiverOptions
                .builder()
                .pollTimeout(Duration.ofMillis(200))
                .build()
            return StreamReceiver
                .create<String, MapRecord<String, String, String>>(connectionFactory, options)
                .receive(
                    consumer,
                    StreamOffset.create(
                        streamName,
                        offset
                    )
                )
        }

        @JvmStatic
        fun createStreamReceiverAutoAck(
            connectionFactory: ReactiveRedisConnectionFactory,
            streamName: String,
            consumer: Consumer,
            offset: ReadOffset = ReadOffset.lastConsumed(),
        ): Flux<MapRecord<String, String, String>> {
            val options = StreamReceiver
                .StreamReceiverOptions
                .builder()
                .pollTimeout(Duration.ofMillis(200))
                .build()
            return StreamReceiver
                .create<String, MapRecord<String, String, String>>(connectionFactory, options)
                .receiveAutoAck(
                    consumer,
                    StreamOffset.create(
                        streamName,
                        offset
                    )
                )
        }

        @JvmStatic
        fun createConsumerGroup(
            reactiveRedisOperations: ReactiveRedisOperations<String, String>,
            streamName: String,
            groupName: String
        ): Mono<String> {
            return reactiveRedisOperations
                .opsForStream<String, String>()
                .createGroup(streamName, groupName)
                .doOnError(RedisSystemException::class.java) {
                    LOG.info("Group {} already exists", groupName)
                }
                .onErrorReturn(RedisSystemException::class.java, "OK")


        }


    }

}


