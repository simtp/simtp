package org.simtp.stream.redis

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.stream.redis.RedisStream.Companion.createConsumerGroup
import org.simtp.stream.redis.RedisStream.Companion.createGroupAndReceiverAutoAck
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.connection.stream.MapRecord
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.testcontainers.containers.GenericContainer
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import reactor.test.StepVerifier
import java.time.Duration

@SpringBootTest
@Testcontainers
@Tag("integration")
class RedisStreamConfigurationTest {

    companion object {
        @ServiceConnection
        var redisContainer: GenericContainer<*> = GenericContainer(DockerImageName.parse("redis:latest"))
            .withExposedPorts(6379)

        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
        }


    }

    @Autowired
    private lateinit var reactiveRedisOperations: ReactiveRedisOperations<String, String>

    @Autowired
    private lateinit var reactiveRedisConnectionFactory: ReactiveRedisConnectionFactory



    @Test
    fun contextLoads() {

    }

    @Test
    fun sendMessage() {

        val addOp = reactiveRedisOperations.opsForStream<String, String>().add(
            MapRecord.create(
                "testing",
                mapOf(
                    "text" to "test",
                )
            ))
            .block(Duration.ofSeconds(5))

        assertNotNull(addOp)


    }

    @Test
    fun createConsumer() {
        val flux = createConsumerGroup(
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = "testing",
            groupName = "testing"
        ).cache()


        StepVerifier.create(flux.flatMap { reactiveRedisOperations.opsForStream<String, String>().add(
            MapRecord.create(
                "testing",
                mapOf(
                    "text" to "test",
                )
            ))})
            .expectNextMatches { true }
            .verifyComplete()

        val consumeFlux = createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = reactiveRedisConnectionFactory,
            streamName = "testing",
            groupName = "testing",
            consumerName = "testing",
            reactiveRedisOperations = reactiveRedisOperations
        )

        StepVerifier
            .create(consumeFlux)
            .expectNextMatches { true }
            .thenCancel()
            .verify()

    }

}

@EnableRedisStream
@SpringBootApplication
open class RedisStreamTestApp