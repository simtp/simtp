package org.simtp.contacts.stream

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.framework.serialization.withDefaults
import org.simtp.stream.redis.RedisStream.Companion.createStreamReceiver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.connection.stream.*
import org.springframework.data.redis.core.ReactiveStreamOperations
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import java.time.Duration
import java.util.*


@SpringBootTest
@Testcontainers
@Tag("integration")
class ContactsConfigurationTest {
    companion object {


        @ServiceConnection
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)
                .withNetwork(Network.SHARED)

        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
        }
        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
        }


    }

    @Autowired
    private lateinit var contactsReactiveStreamOperations: ReactiveStreamOperations<String, String, String>

    @Autowired
    private lateinit var lettuceConnectionFactory: LettuceConnectionFactory

    @Autowired
    private lateinit var objectMapper: ObjectMapper


    @Test
    fun lifecycle() {

        val contactMessage = ContactMessage(
            id = UUID.randomUUID().toString(),
            firstName = "John",
            lastName = "Smith",
            email = "john@smith.me",
            mobile = "041000000"
        )

        contactsReactiveStreamOperations
            .createGroup(CONTACTS_STREAM_KEY, "ContactsConfigurationTest")
            .block(Duration.ofSeconds(5))

        val record: MapRecord<String, String, String> = StreamRecords
            .mapBacked<String, String, String>(mapOf("json" to objectMapper.writeValueAsString(contactMessage)))
            .withStreamKey(CONTACTS_STREAM_KEY)



        contactsReactiveStreamOperations
            .add(record)
            .flux()
            .flatMap {
                createStreamReceiver(
                    connectionFactory = lettuceConnectionFactory,
                    streamName = CONTACTS_STREAM_KEY,
                    consumer = Consumer.from("ContactsConfigurationTest", "ContactsConfigurationTest")
                )
            }
            .blockFirst(Duration.ofSeconds(10))
    }
}

@SpringBootApplication
@EnableContactsStream
open class ContactsStreamConfigurationTestApp {


    @Bean
    open fun objectMapper(): ObjectMapper {
        return ObjectMapper().withDefaults()
    }


}
