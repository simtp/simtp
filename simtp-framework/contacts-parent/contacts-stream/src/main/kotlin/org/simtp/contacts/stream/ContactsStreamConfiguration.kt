package org.simtp.contacts.stream

import org.simtp.stream.redis.EnableRedisStream
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import java.util.*

const val CONTACTS_STREAM_KEY = "stream.org.simtp.contacts"

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(ContactsStreamConfiguration::class)
annotation class EnableContactsStream

@Configuration
@EnableRedisStream
open class ContactsStreamConfiguration

data class ContactMessage(
    var id: String? = UUID.randomUUID().toString(),

    var firstName: String? = null,

    var lastName: String? = null,

    var email: String? = null,

    var mobile: String? = null
)

