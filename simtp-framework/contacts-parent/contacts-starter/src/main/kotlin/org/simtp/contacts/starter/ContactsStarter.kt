package org.simtp.contacts.starter

import com.fasterxml.jackson.databind.ObjectMapper
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.simtp.contacts.stream.ContactMessage
import org.simtp.origination.api.Application
import org.simtp.origination.stream.OriginationStreamProperties
import org.simtp.stream.redis.RedisStream
import org.simtp.utilities.scheduleForMono
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.redis.core.index.Indexed
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import reactor.core.Disposable
import reactor.core.publisher.Flux
import java.util.*


@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(ContactsConfiguration::class)
annotation class EnableContacts

@Configuration
@ComponentScan
@EnableJpaRepositories
@EntityScan(basePackageClasses = [ContactEntity::class])
open class ContactsConfiguration {

    companion object {
        @Suppress("unused")
        private val log = LoggerFactory.getLogger(ContactsConfiguration::class.java)
    }

    @Bean
    open fun originationClientConsumer(
        connectionFactory: ReactiveRedisConnectionFactory,
        objectMapper: ObjectMapper,
        contactsJpaRepository: ContactsJpaRepository,
        applicationEventPublisher: ApplicationEventPublisher,
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        originationStreamProperties: OriginationStreamProperties,
    ) : Disposable {

        return RedisStream.createGroupAndReceiverAutoAck(
            reactiveRedisConnectionFactory = connectionFactory,
            reactiveRedisOperations = reactiveRedisOperations,
            streamName = originationStreamProperties.name,
            groupName = "ContactsConfiguration",
            consumerName = "ContactsConfiguration"

        )
        .flatMap { originationMessage ->
            val originationData = objectMapper.readValue(originationMessage.value["json"]!!, Application::class.java)
            val contacts = (originationData.applicants?.values ?: emptyList())
                .map {
                    ContactEntity(
                        id = it.id,
                        firstName = it.firstName,
                        lastName = it.lastName,
                        email = it.email,
                        mobile = it.mobile
                    )
                }
            Flux.fromIterable(contacts)
        }
        .flatMap {
            scheduleForMono { contactsJpaRepository.save(it) }
        }
        .flatMap {
            reactiveRedisOperations.opsForStream<String, String>().add(StreamRecords
                .newRecord()
                .ofMap(mapOf("json" to objectMapper.writeValueAsString(it.toContactMessage())))
                .withStreamKey("contacts"))
        }


//        }
        .subscribe ()


    }

}

@Repository
interface ContactsJpaRepository: CrudRepository<ContactEntity, String>

@Entity
@Table(name = "contact")
open class ContactEntity(
    @Id
    open var id: String? = UUID.randomUUID().toString(),

    open var firstName: String? = null,

    @Indexed
    open var lastName: String? = null,

    open var email: String? = null,

    open var mobile: String? = null,

//    open var addresses: Map<String, Address>? = null


) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ContactEntity

        if (id != other.id) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (email != other.email) return false
        if (mobile != other.mobile) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (firstName?.hashCode() ?: 0)
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (mobile?.hashCode() ?: 0)
        return result
    }
}

fun ContactEntity.toContactMessage(): ContactMessage {
    return ContactMessage(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        email = this.email,
        mobile = this.mobile
    )
}

