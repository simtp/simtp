package org.simtp.contacts.starter.entities

import java.util.*



/**
 * Created by Grant Little grant@grantlittle.me
 */

open class Contact(
    open var id: String? = UUID.randomUUID().toString(),

    open var firstName: String? = null,

    open var lastName: String? = null,

    open var email: String? = null,

    open var mobile: String? = null,

//    open var addresses: Map<String, Address>? = null


) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Contact

        if (id != other.id) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (email != other.email) return false
        if (mobile != other.mobile) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (firstName?.hashCode() ?: 0)
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (mobile?.hashCode() ?: 0)
        return result
    }
}
