package org.simtp.contacts.starter

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Tag
import org.simtp.contacts.starter.entities.Contact
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.web.reactive.function.client.WebClient
import java.time.Duration
import java.util.UUID

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Disabled
@Tag("integration")
class ContactsConfigurationTest {


    @LocalServerPort
    private var port: Int = 8080

//    @Test
    fun lifecycle() {
        val webClient = WebClient
            .create("http://localhost:${port}/contacts-1.0")

        val contact = ContactEntity(
            id = UUID.randomUUID().toString(),
            firstName = "John",
            lastName = "Smith",
            email = "john@smith.me",
            mobile = "041000000"
        )
        val result = webClient
            .post()
            .bodyValue(contact)
            .retrieve()
            .toEntity(String::class.java)
            .block(Duration.ofSeconds(5))!!
        assertTrue(result.statusCode.is2xxSuccessful)

        val retrievedEntity = WebClient.create("http://localhost:${port}/contacts-1.0")
            .get()
            .uri("/{contact-id}", mapOf("contact-id" to contact.id))
            .header("Accept", "application/json")
            .retrieve()
            .bodyToMono(Contact::class.java)
            .block(Duration.ofSeconds(3))

        assertEquals(contact, retrievedEntity)
    }
}

@SpringBootApplication
@EnableContacts
open class ContactsConfigurationTestApp
