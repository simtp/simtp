package org.simtp.products.common

import org.simtp.kotlin.spring.web.responseEntity
import org.simtp.products.common.entities.ProductEntity
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@Configuration
@ComponentScan
@EnableRedisRepositories
open class ProductsConfiguration

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(ProductsConfiguration::class)
annotation class EnableProducts


@Repository
interface ProductsRepository: CrudRepository<ProductEntity, String>, PagingAndSortingRepository<ProductEntity, String>

@RestController
@RequestMapping("/api/products")
open class ProductsRestController(private val productsRepository: ProductsRepository) {

    @PostMapping
    @Transactional(propagation = Propagation.REQUIRED)
    open fun post(@RequestBody entity: ProductEntity): ResponseEntity<ProductEntity> {
        return productsRepository.save(entity).responseEntity()
    }

    @GetMapping(path = ["/{id}"])
    @Transactional(propagation = Propagation.REQUIRED)
    open fun getById(@PathVariable("id") id: String): ResponseEntity<ProductEntity> {
        return productsRepository.findById(id).responseEntity()
    }

}


