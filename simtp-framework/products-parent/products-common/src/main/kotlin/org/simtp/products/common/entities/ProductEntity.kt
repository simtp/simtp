package org.simtp.products.common.entities

import java.math.BigDecimal
import java.util.*
import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

/**
 * Created by Grant Little grant@grantlittle.me
 */
@RedisHash("products")
open class ProductEntity(
        @Id
        open var id: String? = UUID.randomUUID().toString(),

        open var name: String? = null,

        open var description: String? = null,

        open var organisationId: String? = null,

        open var price: BigDecimal? = null

)

