package org.simtp.kube.gateway

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.runApplication
import org.springframework.cloud.gateway.route.RouteDefinitionLocator
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import java.net.URI
import java.util.Optional

@SpringBootApplication
open class GatewayApplication {

    @Autowired
    private lateinit var configurableEnvironment: org.springframework.core.env.ConfigurableEnvironment


    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(GatewayApplication::class.java)
    }

    @Bean
    open fun routes(routeLocatorBuilder: RouteLocatorBuilder): RouteLocator {
        return routeLocatorBuilder.routes()
            .route("favicon") {  r ->
                r.path("/favicon.ico")
                .filters { f ->
                    f.setStatus("404")
                }
                .uri("no://op")
            }
            .route("dynamic-route") { r ->
                r.order(999)
                    .path("/**") // Match any path with /<component>
                    .filters { f ->
                        f.changeRequestUri {
                            println(configurableEnvironment.getProperty("spring.cloud.gateway.routes"))
                            val regex = Regex("(?<baseUrl>^.*://.*?/)(?<component>.*?)(?:/(?<remainder>.*))?$")
                            val match = regex.matchEntire(it.request.uri.toString())
                            val component = match?.groups?.get("component")?.value ?: ""
                            val remainder = match?.groups?.get("remainder")?.value ?: ""

                            val newPath = "http://$component:8080/$component/$remainder"
                            log.atInfo().addKeyValue("newPath", newPath).setMessage("Rewriting path").log()
                            Optional.of<URI>(URI.create(newPath))

                        }
                        // Strip "/<component>" from the path and capture everything after it in `remaining`
                        f.rewritePath("//(?<component>.*)/(?<remaining>.*)", "/\${remaining}")

                        // Optionally add a request header to all forwarded requests
                        f.addRequestHeader("X-Gateway-Origin", "Spring-Gateway")
                    }
                    // Dynamically route to the service `http://<component>.default.svc.cluster.local`
                    .uri("no://op")
            }
            .build()

    }


}

@Component
class GatewayRoutesLogger() : ApplicationListener<ApplicationReadyEvent> {
    private val log = LoggerFactory.getLogger(GatewayRoutesLogger::class.java)

    @Autowired
    var routeDefinitionLocator: RouteDefinitionLocator? = null

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        routeDefinitionLocator!!.routeDefinitions.subscribe { route ->
            log.info("Loaded Route: id=${route.id}, predicates=${route.predicates}, filters=${route.filters}, uri=${route.uri}")
        }
    }

}


fun main(args: Array<String>) {
    runApplication<GatewayApplication>(*args)
}
