package org.simtp.web.messaging

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.rsocket.RSocketSecurity
import org.springframework.security.rsocket.core.PayloadSocketAcceptorInterceptor
import reactor.core.publisher.Flux
import java.util.*


// See this article for help https://www.baeldung.com/spring-5-reactive-websockets
@Configuration
open class ServerConfiguration {


    @Bean
    @ConditionalOnMissingBean
    open fun defaultAllEventsHandler(allEventsQueue: Queue<String>): DefaultAllEventsHandler {
        return DefaultAllEventsHandler(allEventsQueue)
    }

    @Bean
    open fun allEventsQueue(): LinkedList<String> {
        return LinkedList<String>()
    }

    @Bean
    open fun userEventManager(): UserEventManager {
        return UserEventManager()
    }


    @Bean
    open fun authorization(security: RSocketSecurity): PayloadSocketAcceptorInterceptor {
        return security
            .authorizePayload { authorize ->
                authorize.route("auth").authenticated()
                    .anyRequest().permitAll()
                    .anyExchange().permitAll()
            }
            .simpleAuthentication(Customizer.withDefaults())
            .build()
    }


}


interface AllEventsHandler {
    operator fun invoke(): Flux<String>

}

class DefaultAllEventsHandler(
    private val queue: Queue<String>
): AllEventsHandler {

    override fun invoke(): Flux<String> {
        return Flux.fromIterable(queue)
    }
}

class UserEventManager(
    private val queues: MutableMap<String, LinkedList<String>> = mutableMapOf()
) {

    fun queueFor(user: String): Queue<String> {
        return queues.computeIfAbsent(user) { LinkedList<String>() }
    }

    fun addMessageForUser(user: String, message: String) {
        queueFor(user)
            .add(message)
    }
}