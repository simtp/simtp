package org.simtp.web.messaging

import org.slf4j.LoggerFactory
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Controller
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Controller
open class NotificationServiceController(
        private val allEventsHandler: AllEventsHandler,
        private val userEventManager: UserEventManager
    ) {

        companion object {
            private val log = LoggerFactory.getLogger(NotificationServiceController::class.java)
        }

        @MessageMapping("events.all")
        fun handleAllEvents(): Flux<String> {
            log.info("Channel Stream")
            return this.allEventsHandler()
        }

        @MessageMapping("events.user")
        fun userEventsHandler(
            @AuthenticationPrincipal user: Mono<UserDetails>
        ): Flux<String> {
            log.info("Entering userEventsHandler")
            return user
                .map { it.username }
                .doOnNext { log.info("User event stream for user {}", it) }
                .flatMapMany {
                    Flux.fromIterable(userEventManager.queueFor(it))
                }
                .doOnNext { log.info("Sending message={}", it) }

        }

}