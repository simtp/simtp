package org.simtp.web.messaging

import io.rsocket.metadata.WellKnownMimeType
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.RSocketStrategies
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService
import org.springframework.security.core.userdetails.User
import org.springframework.security.messaging.handler.invocation.reactive.AuthenticationPrincipalArgumentResolver
import org.springframework.security.rsocket.metadata.SimpleAuthenticationEncoder
import org.springframework.security.rsocket.metadata.UsernamePasswordMetadata
import org.springframework.util.MimeTypeUtils
import org.springframework.web.reactive.socket.WebSocketMessage
import java.net.URI
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference
import kotlin.test.assertTrue


@SpringBootTest
@Tag("integration")
class NotificationServiceTest {

    companion object {
        private val log = LoggerFactory.getLogger(NotificationServiceTest::class.java)
    }

    @Test
    @DisplayName("Broadcast a message to all")
    fun `broadcast a message to all`() {
        `given an rsocket is opened to the events all endpoint`()
        `when that message is broadcast to all rsocket listeners`()
        `then each socket listener should receive the message`()
    }

    @Test
    @DisplayName("Send a message to the specific client")
    @Disabled
    fun `send a message to the specific client`() {
        `given an rsocket is opened to the events user endpoint`()
        `when that message is broadcast to a specific rsocket listener`()
        `then the user socket listener should receive the message`()
    }

    private var allEventsReceivedLatch: CountDownLatch = CountDownLatch(1)
    private var userEventsReceivedLatch: CountDownLatch = CountDownLatch(1)


    private val ellEventsReceivedMessages: MutableList<String> = mutableListOf()
    private val userEventsReceivedMessages: MutableList<String> = mutableListOf()

    @Autowired
    private lateinit var rSocketRequester: RSocketRequester

    private fun `given an rsocket is opened to the events all endpoint`() {
        rSocketRequester
            .route("events.all")
            .retrieveFlux(String::class.java)
            .doOnNext(log::info)
            .doOnNext {
                ellEventsReceivedMessages.add(it)
                allEventsReceivedLatch.countDown()
            }
            .subscribe()
        assertNotNull(rSocketRequester)

        log.info("given a websocket session is established - complete")
    }

    private fun `given an rsocket is opened to the events user endpoint`() {
        rSocketRequester
            .route("events.user")
            .retrieveFlux(String::class.java)
            .doOnNext(log::info)
            .doOnNext {
                userEventsReceivedMessages.add(it)
                userEventsReceivedLatch.countDown()
            }
            .subscribe()
        assertNotNull(rSocketRequester)

        log.info("given a websocket session for a specific client is established - complete")
    }

    @Autowired
    private lateinit var allEventsQueue: LinkedList<String>


    private fun `when that message is broadcast to all rsocket listeners`() {
        allEventsQueue.push("some message")
        assertTrue(allEventsReceivedLatch.await(10, TimeUnit.SECONDS))
        log.info("when that message is broadcast to all socket listeners - complete")
    }

    @Autowired
    private lateinit var userEventManager: UserEventManager

    private fun `when that message is broadcast to a specific rsocket listener`() {
        userEventManager.addMessageForUser("jay", "user message")
        assertTrue(userEventsReceivedLatch.await(10, TimeUnit.SECONDS))
        log.info("when that message is broadcast to a specific socket listeners - complete")
    }

    private fun `then each socket listener should receive the message`() {
        log.info("Received Messages {}", ellEventsReceivedMessages)
        assertTrue { ellEventsReceivedMessages.contains("some message") }
        log.info("then each socket listener should receive the message - complete")
    }

    private fun `then the user socket listener should receive the message`() {
        log.info("Received Messages {}", userEventsReceivedMessages)
        assertTrue { userEventsReceivedMessages.contains("user message") }
        log.info("then the user socket listener should receive the message - complete")
    }

}

@SpringBootApplication
open class WebMessageApplication {
//    companion object {
//        private val log = LoggerFactory.getLogger(WebMessageApplication::class.java)
//    }

    @Bean
    open fun rSocketRequester(builder: RSocketRequester.Builder): RSocketRequester {
        return builder
            .rsocketStrategies{ b -> b.encoder(SimpleAuthenticationEncoder()) }
            .setupMetadata(UsernamePasswordMetadata("jay", "pw"), MimeTypeUtils.parseMimeType(WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION.string))
            .websocket(URI.create("ws://localhost:7000"))
    }

    @Bean
    open fun rSocketMessageHandler(strategies: RSocketStrategies): RSocketMessageHandler {
        val handler = RSocketMessageHandler()
        handler.argumentResolverConfigurer
            .addCustomResolver(AuthenticationPrincipalArgumentResolver())
        handler.rSocketStrategies = strategies
        return handler
    }

    @Bean
    open fun atomicReference(): AtomicReference<CountDownLatch> {
        return AtomicReference(CountDownLatch(1))
    }

    @Bean
    open fun eventsAtomicReference(): AtomicReference<CountDownLatch> {
        return eventsAtomicReference
    }

    private val eventsAtomicReference = AtomicReference(CountDownLatch(1))

    @Bean
    @Primary
    open fun testingWebSocketMessageHandler(atomicReference: AtomicReference<CountDownLatch>): (WebSocketMessage) -> Unit {
        return { atomicReference().get().countDown() }
    }

    @Bean
    open fun authentication(): MapReactiveUserDetailsService {
        return MapReactiveUserDetailsService(
            User.builder()
                .username("jay")
                .password("{noop}pw")
                .roles("ADMIN", "USER")
                .build()
        )
    }



}




//fun main(args : Array<String>) {
//    runApplication<WebMessageApplication>(*args)
//}