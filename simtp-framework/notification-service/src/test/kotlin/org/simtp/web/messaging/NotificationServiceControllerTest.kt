package org.simtp.web.messaging

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.security.core.userdetails.UserDetails
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.util.*

class NotificationServiceControllerTest {

    private val allEventsHandler: AllEventsHandler = mockk()
    private val userEventManager: UserEventManager = mockk()
    private lateinit var notificationServiceController: NotificationServiceController
    private val userDetails: UserDetails = mockk()

    @BeforeEach
    fun setUp() {
        notificationServiceController = NotificationServiceController(allEventsHandler, userEventManager)
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun `handleAllEvents should return correct events`() {
        val events = listOf("event1", "event2", "event3")
        every { allEventsHandler() } returns Flux.fromIterable(events)

        StepVerifier
            .create(notificationServiceController.handleAllEvents())
            .expectNext("event1")
            .expectNext("event2")
            .expectNext("event3")
            .verifyComplete()
    }

    @Test
    fun `userEventsHandler should return correct user events`() {
        val events = listOf("event1", "event2", "event3")
        every { userDetails.username } returns "userName"
        every { userEventManager.queueFor("userName") } returns LinkedList(events)

        StepVerifier
            .create(notificationServiceController.userEventsHandler(Mono.just(userDetails)))
            .expectNext("event1")
            .expectNext("event2")
            .expectNext("event3")
            .verifyComplete()
    }
}