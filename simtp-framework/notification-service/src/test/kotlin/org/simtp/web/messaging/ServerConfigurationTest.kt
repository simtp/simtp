package org.simtp.web.messaging

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.security.config.annotation.rsocket.RSocketSecurity
import org.springframework.security.rsocket.core.PayloadSocketAcceptorInterceptor
import java.util.*

internal class ServerConfigurationTest {

    @Test
    fun `test defaultAllEventsHandler bean creation`() {
        val configuration = ServerConfiguration()
        val allEventsHandler = configuration.defaultAllEventsHandler(LinkedList())
        assertNotNull(allEventsHandler)
    }

    @Test
    fun `test allEventsQueue bean creation`() {
        val configuration = ServerConfiguration()
        val allEventsQueue = configuration.allEventsQueue()
        assertNotNull(allEventsQueue)
    }

    @Test
    fun `test userEventManager bean creation`() {
        val configuration = ServerConfiguration()
        val userEventManager = configuration.userEventManager()
        assertNotNull(userEventManager)
    }

    @Test
    fun `test authorization bean creation`() {
        val configuration = ServerConfiguration()
        val rSocketSecurity = mockk<RSocketSecurity>()
        val mockPayloadSocketAcceptorInterceptor = mockk<PayloadSocketAcceptorInterceptor>()
        every { rSocketSecurity.authorizePayload(any()) } returns rSocketSecurity
        every { rSocketSecurity.simpleAuthentication(any()) } returns rSocketSecurity
        every { rSocketSecurity.build() } returns mockPayloadSocketAcceptorInterceptor
        val payloadSocketAcceptorInterceptor = configuration.authorization(rSocketSecurity)
        assertNotNull(payloadSocketAcceptorInterceptor)
    }
}