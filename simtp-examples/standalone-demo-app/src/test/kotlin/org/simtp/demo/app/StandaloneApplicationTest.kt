package org.simtp.demo.app

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.account.api.AccountType
import org.simtp.account.api.IAccountDataSource
import org.simtp.contacts.starter.ContactEntity
import org.simtp.origination.api.*
import org.simtp.utilities.scheduleForFlux
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.data.repository.CrudRepository
import org.springframework.retry.backoff.ExponentialBackOffPolicy
import org.springframework.retry.policy.MaxAttemptsRetryPolicy
import org.springframework.retry.support.RetryTemplate
import org.springframework.web.reactive.function.client.WebClient
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.utility.DockerImageName
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integration")
class StandaloneApplicationTest {

    @LocalServerPort
    private var localServerPort: Int? = null


    companion object {
        @Container
        @ServiceConnection
        private val postgresContainer = PostgreSQLContainer("postgres")

        @Container
        @ServiceConnection
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)


        @BeforeAll
        @JvmStatic
        fun setUp() {
            postgresContainer
                .withReuse(true)
                .start()

            redisContainer
                .withReuse(true)
                .start()


        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            postgresContainer.stop()
            redisContainer.stop()
        }
    }

    @Autowired
    lateinit var originationDataSource: IApplicationDataSource

    @Autowired
    private lateinit var contactsJpaRepository: CrudRepository<ContactEntity, String>

    @Autowired
    private lateinit var accountDataSource: IAccountDataSource

    @Test
    fun contextLoads() {
//        fail("Test currency serialization")
    }

    @Test
    @DisplayName("Originate a client")
    //TODO Re-look at this one
    @Disabled
    fun testOrigination() {

        val result = originateAnAccount(localServerPort!!).block()
        assertEquals(defaultOriginationData, result)

//        StepVerifier
//            .create()
//            .expectNext(defaultOriginationData)
//            .expectComplete()
//            .verify(Duration.ofSeconds(50))


        StepVerifier
            .create( originationDataSource.findById(defaultOriginationData.id) )
            .expectNext(defaultOriginationData)
            .expectComplete()
            .verify(Duration.ofSeconds(5))



        //There should be a contact in the database
        StepVerifier
            .create(scheduleForFlux { contactsJpaRepository.findAll() })
            .expectNextMatches { contact ->
                val retryTemplate = RetryTemplate().apply {
                    this.setBackOffPolicy(ExponentialBackOffPolicy())
                    this.setRetryPolicy(MaxAttemptsRetryPolicy(5))
                }
                retryTemplate.execute<Boolean, Exception> {
                    assertNotNull(it)
                    assertEquals("John", contact!!.firstName)
                    assertEquals("Smith", contact.lastName)
                    assertEquals("johnsmith@error.com", contact.email)
                    assertEquals("12345678", contact.mobile)
                    true
                }
//                assertAll(
//                    { assertNotNull(it) },
//                    { assertEquals("John", it!!.firstName) },
//                    { assertEquals("Smith", it.lastName) },
//                    { assertEquals("johnsmith@error.com", it.email) },
//                    { assertEquals("12345678", it.mobile) }
//                )
                true
            }
            .expectComplete()
            .verify(Duration.ofSeconds(5))



        //There should be an account in the database
        StepVerifier
            .create(accountDataSource.findAll())
            .expectNextMatches {
                assertNotNull(it)
                assertEquals(AccountType.EQUITIES, it?.type)
                true
            }
            .expectComplete()
            .verify(Duration.ofSeconds(5))

//        StepVerifier
//            .create(settlementAccountRepository.findAll())
//            .expectNextMatches {
//                assertNotNull(it)
//                true
//            }
//            .expectComplete()
//            .verify(Duration.ofSeconds(5))

    }
}

internal val defaultOriginationData = Application(
    id = "1",
    applicants = mutableMapOf(
        "primary" to Applicant(
            id = "1",
            firstName = "John",
            lastName = "Smith",
            email = "johnsmith@error.com",
            mobile = "12345678",
            addresses = mapOf(
                "work" to ApplicantAddress(
                    id = "1",
                    houseNumber = "1",
                    streetNumber = "2",
                    streetAddress1 = "streetAddress1",
                    streetAddress2 = "streetAddress2",
                    city = "city",
                    postcode = "postCode",
                )
            )
        )
    ),
    account = ApplicationAccount(
        id = "1",
        type = "TEST",
        settlementAccounts = mapOf(
            "primary" to ApplicationSettlementAccount(
                id = "1",
                name = "John Smith"
            )
        )
    )
)

internal fun originateAnAccount(port: Int): Mono<Application> {
    val webClient = WebClient
        .create("http://localhost:$port")

    return webClient
        .post()
        .uri("/api/origination/1.0")
        .bodyValue(defaultOriginationData)
        .retrieve()
        .bodyToMono(Application::class.java)

}