import { createRouter, createWebHistory } from 'vue-router'
// import TradingPanel from "@/components/TradingPanel";
import LoginOrRegisterComponent from "@/components/LoginOrRegisterComponent";

const routes = [
  {
    path: '/',
    component: LoginOrRegisterComponent
  },
  // {
  //   path: '/trading',
  //   name: 'trading',
  //   component: TradingPanel
  // },
  // {
  //   path: '/clientmanagement',
  //   name: 'clientmanagement',
  //   component: ClientManagementPanel
  // },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
