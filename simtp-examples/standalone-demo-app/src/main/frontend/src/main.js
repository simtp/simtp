import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// import axios from "axios";

// const getInitialData = async function() {
//     console.log("Getting initial data")
//     let promise = axios.get("/api/server-settings/1.0", { timeout: 10 * 1000});
//     promise.then( (response) => {
//         if (response.status === 200) {
//             console.log("Server settings are: " + response.data)
//             app.config.globalProperties.serverSettings = response.data
//         } else {
//             console.log("Error getting server settings")
//         }
//
//
//     }).catch((ex) => {
//         console.log("Exception getting server settings " + ex)
//
//         let serverSettings = {
//             gatewayAddress: "http://localhost:8001",
//             socketAddress: "ws://localhost:8010/rsocket",
//             ordersAddress: "http://localhost:8010",
//             contactsAddress: "",
//             accountsAddress: "http://localhost:8010",
//             cashAccountsAddress: "http://localhost:8010",
//             holdingsAddress: "http://localhost:8010",
//             contractNotesAddress: "http://localhost:8010",
//         }
//         app.config.globalProperties.serverSettings = serverSettings
//         console.log("Error getting server settings " + JSON.stringify(serverSettings))
//     })
//     return promise
// }

// const getLoggedInUserDetailsPromise = async function() {
//
//     let promise = axios.get("/user", { timeout: 10 * 1000});
//     promise.then( (response) => {
//         if (response.status === 200) {
//             app.config.globalProperties.loggedInUserDetails = response.data
//         }
//
//     }).catch(() => {
//         app.config.globalProperties.loggedInUserDetails = {
//             name: "Anonymous User",
//             email: "joe.bloggs@joebloggs.me",
//             username: "joebloggs"
//         }
//         console.log("Error getting user settings " + JSON.stringify(app.config.globalProperties.loggedInUserDetails))
//     })
//     return promise
// }

const app = createApp(App)

// app.config.globalProperties.serverSettingsPromise = getInitialData()
// app.config.globalProperties.loggedInUserDetailsPromise = getLoggedInUserDetailsPromise()

// app.config.globalProperties.loggedInUserDetailsPromise
//     .then((response) => {
//         axios.post("/api/client-messages/1.0/", JSON.stringify({"clientId": response.data.email}), {
//             timeout: 10 * 1000,
//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         }).catch(() => {
//             console.log("Failed to register for streaming with details " + response.data.email)
//         })
//     })
//     .catch((ex) => {
//         axios.post("/api/client-messages/1.0/", JSON.stringify({"clientId": "joe.bloggs@joebloggs.me"}), {
//             timeout: 10 * 1000,
//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         }).catch(() => {
//             console.log("Failed to register for streaming with joe.bloggs@joebloggs.me" + ex)
//         })
//
//     })


app
    .use(router)
    .use(ElementPlus)
    .mount('#app')

// document.addEventListener('keyup', function(event) {
// });



// document.onkeyup = function(event) {
//     if (event.ctrlKey && event.key === 's') {
//         alert('Undo!');
//         event.preventDefault()
//         return false;
//     } else {
//         return true;
//     }
// };

