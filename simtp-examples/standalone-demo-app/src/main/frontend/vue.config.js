const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: '/standalone-demo-app/',
  transpileDependencies: true,
  devServer: {
    host: '0.0.0.0',
    port: 8020,
    // https: false,
    // hotOnly: false,
  },
})
