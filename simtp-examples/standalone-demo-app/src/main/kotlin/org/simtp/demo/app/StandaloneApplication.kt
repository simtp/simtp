package org.simtp.demo.app

import org.simtp.account.datasource.EnableJpaAccountDataSource
import org.simtp.account.rest.server.EnableAccountRestServer
import org.simtp.account.starter.EnableAccountBackend
import org.simtp.address.backend.EnableAddressBackend
import org.simtp.contacts.starter.EnableContacts
import org.simtp.contacts.stream.EnableContactsStream
import org.simtp.framework.serialization.EnableSerialization
import org.simtp.market.data.mock.provider.securities.EnableMockSecuritiesProvider
import org.simtp.market.data.securities.backend.EnableSecuritiesBackend
import org.simtp.market.data.securities.datasource.redis.EnableRedisMarketDataDataSource
import org.simtp.order.lifecycle.datasource.jpa.EnableJpaOrderDataSource
import org.simtp.origination.data.source.jpa.EnableJpaOriginationDataSource
import org.simtp.origination.module.EnableOrigination
import org.simtp.origination.stream.EnableOriginationStream
import org.simtp.stream.redis.EnableRedisStream
import org.simtp.vetting.datasource.redis.EnableOrderVerificationRedisDataSource
import org.simtp.vetting.modules.basic.EnableOrderVerification
import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


/**
 * Created by Grant Little grant@grantlittle.me
 */
@SpringBootApplication
@EnableContacts
@EnableContactsStream
@EnableOrigination
@EnableOriginationStream
@EnableAccountBackend
@EnableAccountRestServer
@EnableAddressBackend
@EnableSerialization
@EnableRedisStream
@EnableSecuritiesBackend
@EnableMockSecuritiesProvider
@EnableOrderVerification

@EnableJpaOrderDataSource
@EnableJpaAccountDataSource
@EnableJpaOriginationDataSource
@EnableRedisMarketDataDataSource
@EnableOrderVerificationRedisDataSource
//@EnableIgnite
//@EnableCommon
//@EnableHazelcastStorageNode
//@EnableOrders
//@EnableKafkaFeatures
//@EnablePricesBackend
//@EnablePricesApi
//@EnableBasicVetting
//@EnableVettingService
//@EnableTradesProcessor
//@EnableSimpleMatchingEngine
//@EnableSlackIntegration
//@EnableSecuritiesOrdersProcessing
open class StandaloneApplication

fun main(args : Array<String>) {
    runApplication<StandaloneApplication>(*args) {
        setBannerMode(Banner.Mode.OFF)
    }
}
