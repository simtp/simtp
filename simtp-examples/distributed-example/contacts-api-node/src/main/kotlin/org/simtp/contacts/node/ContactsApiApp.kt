package org.simtp.contacts.node

import org.simtp.contacts.starter.EnableContacts
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration

@SpringBootApplication
open class ContactsApiApp

fun main(args : Array<String>) {
    runApplication<ContactsApiApp>(*args)
}

@EnableContacts
@Configuration
@EnableRedisStream
open class AccountApiConfiguration


