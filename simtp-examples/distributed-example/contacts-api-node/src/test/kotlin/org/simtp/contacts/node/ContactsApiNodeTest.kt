package org.simtp.contacts.node

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.consul.ConsulContainer
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Testcontainers
@Disabled
@Tag("integration")
internal class ContactsApiNodeTest {


    companion object {
        @Container
        @ServiceConnection
        private val postgresContainer =PostgreSQLContainer("postgres").withNetwork(Network.SHARED)

        @Container
        private val consulContainer = ConsulContainer("hashicorp/consul").withNetwork(Network.SHARED)

        @Container
        private val dbContainer = PostgreSQLContainer("postgres").withNetwork(Network.SHARED)

        @Container
        @ServiceConnection
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379).withNetwork(Network.SHARED)
        @DynamicPropertySource
        @JvmStatic
        fun overrideProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.cloud.consul.host") { consulContainer.host }
            registry.add("spring.cloud.consul.port") { consulContainer.getMappedPort(8500) }

            registry.add("spring.datasource.url") { "jdbc:postgresql://${dbContainer.host}:${dbContainer.firstMappedPort}/" }
            registry.add("spring.datasource.username") { dbContainer.username }
            registry.add("spring.datasource.password") { dbContainer.password }
        }



        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()

            consulContainer
                .withReuse(true)
                .start()
            consulContainer.waitingFor(HostPortWaitStrategy())

            postgresContainer
                .withReuse(true)
                .start()

            dbContainer
                .withReuse(true)
                .start()

        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
            consulContainer.stop()
            postgresContainer.stop()
            dbContainer.stop()
        }
    }

    @Test
    fun contextLoads() {

        println("Test")
    }

}