package org.simtp.config.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.config.server.EnableConfigServer
import org.springframework.context.annotation.Configuration


@SpringBootApplication
@EnableConfigServer
open class SimtpConfigServer

fun main(args : Array<String>) {
    runApplication<SimtpConfigServer>(*args)
}

@Configuration
open class AccountApiConfiguration


