package org.simtp.openapi.documentation

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@Tag("integration")
internal class OpenApiDocumentationIntTest {

    @Test
    fun contextLoads() {

    }

}