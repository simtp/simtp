package org.simtp.openapi.documentation

import org.simtp.swagger.starter.EnableSwagger
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


/*
    NOTE currently it is not possible to use Spring Eureka with a dynamic port due to the ordering
    in which the application starts:-

    https://github.com/spring-cloud/spring-cloud-consul/issues/555
 */
@SpringBootApplication
@EnableSwagger
open class OpenApiDocumentation

fun main(args : Array<String>) {
    runApplication<OpenApiDocumentation>(*args)
}

