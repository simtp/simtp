package org.simtp.gateway.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.cloud.client.DefaultServiceInstance
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier
import org.springframework.cloud.loadbalancer.support.ServiceInstanceListSuppliers
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment


class LoadBalancerConfig {
    @LocalServerPort
    var port = 0
    @Bean
    fun fixedServiceInstanceListSupplier(env: Environment?): ServiceInstanceListSupplier {
        return ServiceInstanceListSuppliers.from(
            "httpbin",
            DefaultServiceInstance("httpbin-1", "httpbin", "localhost", port, false)
        )
    }
}
@SpringBootApplication
//@LoadBalancerClient(name = "httpbin", configuration = [LoadBalancerConfig::class])
open class GatewayApp

fun main(args : Array<String>) {
    runApplication<GatewayApp>(*args)
}

