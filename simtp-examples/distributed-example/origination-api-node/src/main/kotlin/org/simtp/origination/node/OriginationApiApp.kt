package org.simtp.origination.node

import org.simtp.origination.data.source.jpa.EnableJpaOriginationDataSource
import org.simtp.origination.demo.data.EnableOriginationDemoData
import org.simtp.origination.module.EnableOrigination
import org.simtp.origination.stream.EnableOriginationStream
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration


/*
    NOTE currently it is not possible to use Spring Eureka with a dynamic port due to the ordering
    in which the application starts:-

    https://github.com/spring-cloud/spring-cloud-consul/issues/555
 */
@SpringBootApplication
open class OriginationApiApp

fun main(args : Array<String>) {
    runApplication<OriginationApiApp>(*args)
}

@Configuration
@EnableOrigination
@EnableOriginationStream
@EnableOriginationDemoData
@EnableJpaOriginationDataSource
@EnableRedisStream
open class OrganisationsApiConfiguration

