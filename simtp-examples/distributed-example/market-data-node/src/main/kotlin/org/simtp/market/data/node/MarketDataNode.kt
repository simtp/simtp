package org.simtp.market.data.node

import org.simtp.market.data.mock.provider.securities.EnableMockSecuritiesProvider
import org.simtp.market.data.securities.backend.EnableSecuritiesBackend
import org.simtp.market.data.securities.datasource.redis.EnableRedisMarketDataDataSource
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
@EnableSecuritiesBackend
@EnableRedisMarketDataDataSource
@EnableMockSecuritiesProvider
@EnableRedisStream
open class MarketDataApp


fun main(args : Array<String>) {
    runApplication<MarketDataApp>(*args)
}
