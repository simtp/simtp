package org.simtp.admin.simtpadmin

import de.codecentric.boot.admin.server.cloud.discovery.DefaultServiceInstanceConverter
import de.codecentric.boot.admin.server.cloud.discovery.ServiceInstanceConverter
import de.codecentric.boot.admin.server.config.EnableAdminServer
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.ServiceInstance
import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.security.config.annotation.web.builders.HttpSecurity
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.util.StringUtils


@SpringBootApplication
@EnableAdminServer
class SimtpAdminApplication {

    @Bean
    fun serviceInstanceConverter(): ServiceInstanceConverter {
        return ConsulServiceInstanceConverter()
    }
}

class ConsulServiceInstanceConverter: DefaultServiceInstanceConverter() {
    override fun getManagementHost(instance: ServiceInstance): String {
        val managementServerHost = instance.metadata["management_address"]
        return if (StringUtils.hasLength(managementServerHost)) {
            managementServerHost!!
        } else super.getManagementHost(instance)
    }
}

//@Configuration
//class SecurityPermitAllConfig : WebSecurityConfigurerAdapter() {
//    @Throws(Exception::class)
//    override fun configure(http: HttpSecurity) {
//        http.authorizeRequests().anyRequest().permitAll()
//                .and().csrf().disable()
//    }
//}

fun main(args: Array<String>) {
    runApplication<SimtpAdminApplication>(*args)
}
