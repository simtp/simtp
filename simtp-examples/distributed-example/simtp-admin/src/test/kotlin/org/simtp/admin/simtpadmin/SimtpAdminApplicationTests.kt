package org.simtp.admin.simtpadmin

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.consul.ConsulContainer
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy

@SpringBootTest
@Tag("integration")
class SimtpAdminApplicationTests {

    companion object {

        val consulContainer = ConsulContainer("hashicorp/consul")

        @DynamicPropertySource
        @JvmStatic
        fun overrideProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.cloud.consul.host") { consulContainer.host }
            registry.add("spring.cloud.consul.port") { consulContainer.getMappedPort(8500) }
        }

        @BeforeAll
        @JvmStatic
        fun setUp() {

            consulContainer
                .withReuse(true)
                .start()
            consulContainer.waitingFor(HostPortWaitStrategy())
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            consulContainer.stop()

        }
    }

    @Test
    fun contextLoads() {
    }

}
