package org.simtp.account.node

import org.simtp.account.datasource.EnableJpaAccountDataSource
import org.simtp.account.rest.server.EnableAccountRestServer
import org.simtp.account.starter.EnableAccountBackend
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


/*
    NOTE currently it is not possible to use Spring Eureka with a dynamic port due to the ordering
    in which the application starts:-

    https://github.com/spring-cloud/spring-cloud-consul/issues/555
 */

@EnableAccountBackend
@EnableAccountRestServer
@SpringBootApplication
@EnableJpaAccountDataSource
@EnableRedisStream
open class AccountApiApp


fun main(args : Array<String>) {
    runApplication<AccountApiApp>(*args)
}
