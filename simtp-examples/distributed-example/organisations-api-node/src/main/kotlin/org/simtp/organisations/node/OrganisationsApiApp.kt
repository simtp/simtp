package org.simtp.organisations.node

import org.simtp.organisations.starter.EnableOrganisations
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration


/*
    NOTE currently it is not possible to use Spring Eureka with a dynamic port due to the ordering
    in which the application starts:-

    https://github.com/spring-cloud/spring-cloud-consul/issues/555
 */
@SpringBootApplication
open class OrganisationsApiApp

fun main(args : Array<String>) {
    runApplication<OrganisationsApiApp>(*args)
}

@Configuration
@EnableOrganisations
//@EnableCommon
//@EnableAccountBackend
//@EnableAccountRest
open class OrganisationsApiConfiguration

