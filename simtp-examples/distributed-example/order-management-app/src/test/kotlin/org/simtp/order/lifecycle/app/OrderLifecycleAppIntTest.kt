@file:Suppress("unused")

package org.simtp.order.lifecycle.app

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.order.lifecycle.backend.EnableInMemoryOrderDao
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.consul.ConsulContainer
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.wait.strategy.HostPortWaitStrategy
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@EnableRedisStream
@EnableInMemoryOrderDao
@Tag("integration")
internal class OrderLifecycleAppIntTest {

    companion object {
        @Container
        @ServiceConnection
        private val postgresContainer =PostgreSQLContainer("postgres").withNetwork(Network.SHARED)

        @Container
        @ServiceConnection
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)
                .withNetwork(Network.SHARED)

        @Container
        private val consulContainer = ConsulContainer("hashicorp/consul").withNetwork(Network.SHARED)

        @DynamicPropertySource
        @JvmStatic
        fun overrideProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.cloud.consul.host") { consulContainer.host }
            registry.add("spring.cloud.consul.port") { consulContainer.getMappedPort(8500) }
        }



        @BeforeAll
        @JvmStatic
        fun setUp() {

            redisContainer
                .withReuse(true)
                .start()

            consulContainer
                .withReuse(true)
                .start()
            consulContainer.waitingFor(HostPortWaitStrategy())

            postgresContainer
                .withReuse(true)
                .start()

        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
            consulContainer.stop()
            postgresContainer.stop()
        }
    }

    @Test
    fun contextLoads() {

        println("Test")
    }

}
