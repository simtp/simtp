package org.simtp.order.lifecycle.app

import org.simtp.order.lifecycle.backend.EnableOrderLifecycleBackend
import org.simtp.order.lifecycle.rest.server.EnableOrderLifecycleRestServer
import org.simtp.simple.matching.engine.EnableSimpleMatchingEngine
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


/*
    NOTE currently it is not possible to use Spring Eureka with a dynamic port due to the ordering
    in which the application starts:-

    https://github.com/spring-cloud/spring-cloud-consul/issues/555
 */

@EnableOrderLifecycleBackend
@EnableOrderLifecycleRestServer
@EnableSimpleMatchingEngine
@SpringBootApplication
open class OrderLifecycleApp {

}

fun main(args : Array<String>) {
    runApplication<OrderLifecycleApp>(*args)
}
