/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.datasource.memory

import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import org.simtp.person.api.Person
import org.junit.jupiter.api.Assertions.*
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.kotlin.util.uuid
import org.simtp.person.api.cloneAndIncrementVersion
import org.simtp.person.api.toPerson

class PersonMemoryDataSourceTest {

    @Test
    fun save() {
        val personCreateEvent = PersonCreateEvent().apply {
            this.id = uuid()
            this.messageId = uuid()
        }
        val personDataSource = PersonMemoryDataSource()
        StepVerifier
            .create(personDataSource.create(personCreateEvent))
            .expectNext(personCreateEvent.toPerson().cloneAndIncrementVersion())
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val person = Person()
        val personDataSource = PersonMemoryDataSource(mutableMapOf( person.id to person ))
        StepVerifier
            .create(personDataSource.findById(person.id))
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val person = Person()
        val personDataSource = PersonMemoryDataSource(mutableMapOf( person.id to person ))
        StepVerifier
            .create(personDataSource.findAll())
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun delete() {
        val person = Person()
        val personDataSource = PersonMemoryDataSource(mutableMapOf( person.id to person ))
        val personDeleteEvent = PersonDeleteEvent().apply {
            this.id = person.id
            this.messageId = uuid()
        }
        StepVerifier
            .create(personDataSource.delete(personDeleteEvent))
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(0, personDataSource.map.size)
    }


}