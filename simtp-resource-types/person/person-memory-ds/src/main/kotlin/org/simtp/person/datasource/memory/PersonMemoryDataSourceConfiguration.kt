/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package org.simtp.person.datasource.memory

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.api.person.api.stream.PersonUpdateEvent
import org.simtp.common.IDateTimeManager
import org.simtp.person.api.*
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.receiver.ReceiverOptions


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(PersonMemoryDataSourceConfiguration::class)
annotation class EnablePersonMemoryDataSource


@Configuration
open class PersonMemoryDataSourceConfiguration {

    @Bean
    open fun personMemoryDataSource(): PersonMemoryDataSource {
        return PersonMemoryDataSource()
    }

    @Bean
    open fun personMemoryReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\${spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "personMemoryReactiveKafkaConsumer:$applicationName"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "personMemoryReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(PERSON_STREAM_NAME)))
    }


    @Bean
    open fun personMemoryStreamConsumer(
        personMemoryReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        personMemoryDataSource: PersonMemoryDataSource,
        dateTimeManager: IDateTimeManager
    ): PersonMemoryStreamConsumer {
        return PersonMemoryStreamConsumer(
            personMemoryReactiveKafkaConsumerTemplate = personMemoryReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            personMemoryDataSource = personMemoryDataSource,
            dateTimeManager = dateTimeManager
        )
    }

}

class PersonMemoryStreamConsumer(
    private val personMemoryReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val personMemoryDataSource: PersonMemoryDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(PersonMemoryStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<Person> {
        return personMemoryReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                personStreamHandler(
                    it.value(),
                    personMemoryDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, PERSON_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.toPersonFailureStreamEvent(this::class.simpleName!!) }
                }
            }

    }
}

@Suppress("MemberVisibilityCanBePrivate")
class PersonMemoryDataSource(
    val map: MutableMap<String, Person> = mutableMapOf(),
    val messageIds: MutableSet<String> = mutableSetOf(),
    val failedMessages: MutableMap<String, Set<PersonFailureEvent>> = mutableMapOf()
): IPersonDataSource {

    companion object {
        private val log = LoggerFactory.getLogger(PersonMemoryDataSource::class.java)
    }

    override fun create(personCreateEvent: PersonCreateEvent): Mono<Person> {
        log
            .atDebug()
            .setMessage("Called create")
            .addKeyValue("personCreateEvent") { personCreateEvent }
            .log()
        return if (!messageIds.contains(personCreateEvent.messageId.toString())) {
            Mono.just(savePerson(personCreateEvent.toPerson()))
        } else {
            Mono.empty()
        }
        .saveMessageId(personCreateEvent.messageId.toString())
    }

    override fun delete(personDeleteEvent: PersonDeleteEvent): Mono<Person> {
        log
            .atDebug()
            .setMessage("Called delete")
            .addKeyValue("messageId") { personDeleteEvent.messageId.toString() }
            .log()

        if (log.isDebugEnabled) log.debug("Called delete {}", personDeleteEvent)
        return if (!messageIds.contains(personDeleteEvent.messageId.toString())) {
            val existing = map[personDeleteEvent.id.toString()]
            if (existing != null && existing.version != personDeleteEvent.version)
                throw OptimisticLockingFailureException("Current version=$existing does not match provided version=${personDeleteEvent.version}")

            map.remove(personDeleteEvent.id.toString())?.let { Mono.just(it)} ?: Mono.empty()
        } else {
            Mono.empty()
        }
        .saveMessageId(personDeleteEvent.messageId.toString())

    }

    override fun update(personUpdateEvent: PersonUpdateEvent): Mono<Person> {
        if (log.isDebugEnabled) log.debug("Called update {}", personUpdateEvent)
        return if (!messageIds.contains(personUpdateEvent.messageId.toString())) {
            Mono.just(savePerson(personUpdateEvent.toPerson()))
        } else {
            Mono.empty()
        }
        .saveMessageId(personUpdateEvent.messageId.toString())
    }

    private fun savePerson(person: Person): Person {
        if (log.isDebugEnabled) log.debug("Called savePerson {}", person)
        val existing = map[person.id]
        if (existing != null && existing.version != person.version)
            throw OptimisticLockingFailureException("Current version=$existing does not match provided version=${person.version}")

        val newVal = person.cloneAndIncrementVersion()
        map[person.id] = newVal

        return newVal

    }

    private fun Mono<Person>.saveMessageId(messageId: String): Mono<Person> {
        if (log.isDebugEnabled) log.debug("Called saveMessageId {}", messageId)
        return this
            .map {
                messageIds.add(messageId)
                it
            }
    }

    override fun findById(id: String): Mono<Person> {
            return map[id]?.let { Mono.just(it)} ?: Mono.empty()
    }

    override fun findAll(): Flux<Person> {
            return Flux.fromIterable(map.values)

    }

    override fun failure(personFailureEvent: PersonFailureEvent): Mono<Unit> {
        return Mono.fromCallable {
            if (log.isWarnEnabled) log.debug("Called failure {}", personFailureEvent)
            failedMessages.merge(personFailureEvent.id, setOf(personFailureEvent)) { p1, p2 -> p1+p2 }
        }
    }


}
