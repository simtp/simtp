/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package org.simtp.person.datasource.redis

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.api.person.api.stream.PersonUpdateEvent
import org.simtp.common.IDateTimeManager
import org.simtp.common.toISOString
import org.simtp.common.toLocalDate
import org.simtp.common.toZonedDateTime
import org.simtp.person.api.*
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.core.ReactiveHashOperations
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.receiver.ReceiverOptions
import kotlin.reflect.full.memberProperties

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(PersonRedisDataSourceConfiguration::class)
annotation class EnablePersonRedisDataSource

@Configuration
open class PersonRedisDataSourceConfiguration {

    @Bean
    open fun personRedisDataSource(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        objectMapper: ObjectMapper
    ): PersonRedisDataSource {
        return PersonRedisDataSource(reactiveRedisOperations, objectMapper)
    }

    @Bean
    open fun personRedisReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\${spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "personRedisReactiveKafkaConsumer:$applicationName"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "personRedisReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(PERSON_STREAM_NAME)))
    }

    @Bean
    open fun personRedisStreamConsumer(
        personRedisReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        personRedisDataSource: PersonRedisDataSource,
        dateTimeManager: IDateTimeManager
    ): PersonRedisStreamConsumer {
        return PersonRedisStreamConsumer(
            personRedisReactiveKafkaConsumerTemplate = personRedisReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            personRedisDataSource = personRedisDataSource,
            dateTimeManager = dateTimeManager
        )
    }
}


class PersonRedisStreamConsumer(
    private val personRedisReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val personRedisDataSource: PersonRedisDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(PersonRedisStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<Person> {
        return personRedisReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                personStreamHandler(
                    it.value(),
                    personRedisDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, PERSON_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.toPersonFailureStreamEvent(this::class.simpleName!!) }
                }
            }

    }

}

class PersonRedisDataSource(
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val objectMapper: ObjectMapper,
): IPersonDataSource {

    companion object {
        private val log = LoggerFactory.getLogger(PersonRedisDataSource::class.java)
    }

    private val hashOps = reactiveRedisOperations
        .opsForHash<String, String>()

    private val setOps = reactiveRedisOperations
        .opsForSet()


    override fun create(personCreateEvent: PersonCreateEvent): Mono<Person> {
        return setOps
            .isMember("person:messageIds", personCreateEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                save(personCreateEvent.toPerson())
            }
            .saveMessageId(personCreateEvent.messageId.toString())
    }

    override fun update(personUpdateEvent: PersonUpdateEvent): Mono<Person> {
        return setOps
            .isMember("person:messageIds", personUpdateEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                save(personUpdateEvent.toPerson())
            }
            .saveMessageId(personUpdateEvent.messageId.toString())

    }

    internal fun save(person: Person): Mono<Person> {
        val existing = hashOps
            .multiGet("person:${person.id}", listOf("version"))
            .cache()

        val newPerson = person.cloneAndIncrementVersion()

        return existing
            .flatMap {
                val result = it.filterNotNull().isNotEmpty()
                //This isn't very elegant
                if (result && it[0].toLong() != person.version) {
                    Mono.error(OptimisticLockingFailureException("Current version=${it[0]} does not match provided version=${person.version}"))
                } else {
                    Mono.just(it)
                }
            }
            .flatMap {
                hashOps.savePerson(newPerson)
            }
    }

    private fun Mono<Person>.saveMessageId(messageId: String): Mono<Person> {
        return this
            .flatMap { person ->
                setOps.add("person:messageIds:$messageId", messageId).map { person }
            }
    }



    override fun findById(id: String): Mono<Person> {
        return hashOps
            .multiGet("person:${id}", allPersonFields)
            .mapNotNull {
                when (it.filterNotNull().isEmpty()) {
                    true -> {
                        null
                    }
                    else -> {
                        it.toPerson()
                    }
                }
            }
            .cast(Person::class.java)
            .doOnNext { log.info("findById: {} = {}", id, it) }
    }

    override fun findAll(): Flux<Person> {
        return hashOps
            .keys("person:*")
            .flatMap { key ->
                reactiveRedisOperations
                    .opsForHash<String, String>()
                    .multiGet(key, allPersonFields)
                    .map { it.toPerson() }
            }
    }

    override fun delete(personDeleteEvent: PersonDeleteEvent): Mono<Person> {
        return setOps
            .isMember("person:messageIds", personDeleteEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                delete(personDeleteEvent.id.toString(), personDeleteEvent.version)
            }
            .doOnNext {
                log.info("Successfully deleted item")
            }
            .doOnError {
                log.error("Exception encountered", it)
            }
            .saveMessageId(personDeleteEvent.messageId.toString())

    }

    internal fun delete(id: String, version: Long): Mono<Person> {
        val value = findById(id).cache()

        val deleteOp = hashOps
            .delete("person:${id}")

        return value
            .flatMap {
                if (it.version != version) {
                    Mono.error(OptimisticLockingFailureException("Provided person.version=${version} version does not latest person.version=${it.version} version"))
                } else {
                    Mono.just(it)
                }
            }
            .doOnError {
                log.error("Failed with exception", it)
            }
            .flatMap {
                deleteOp
            }
            .flatMap {
                value
            }


    }

    override fun failure(personFailureEvent: PersonFailureEvent): Mono<Unit> {
        return setOps
            .add("person:failures:${personFailureEvent.id}", objectMapper.writeValueAsString(personFailureEvent))
            .doOnError {
                log.error("Failed to store person failure event")
            }
            .doOnNext {
                log.info("Stored person failure event for id={}", personFailureEvent.id)
            }
            .then(Mono.empty())
    }
}

internal val allPersonFields = PersonMap()::class.memberProperties.map { it.name }

fun ReactiveHashOperations<String, String, String>.savePerson(person: Person): Mono<Person> {
    return this
        .putAll("person:${person.id}", person.toMap())
        .map { person }
}

open class PersonMap(val map: MutableMap<String, String?> = mutableMapOf<String, String?>().withDefault { null }) {
    var id: String by map
    var firstName: String by map
    var lastName: String by map
    var dateOfBirth: String? by map
    var lastModifiedBy: String by map
    var version: String by map
    var lastModifiedTimestamp: String by map
}

fun List<String?>.toPerson(): Person {
    return this.mapIndexed { index, it -> allPersonFields[index] to it }
        .toMap()
        .toMutableMap()
        .let { PersonMap(it) }
        .toPerson()
}

fun Person.toPersonMap(): PersonMap {
    val person = this
    return PersonMap().apply {
        id = person.id
        version = person.version.toString()
        lastModifiedTimestamp = person.lastModifiedTimestamp.toISOString()
        firstName = person.firstName
        lastName = person.lastName
        dateOfBirth = person.dateOfBirth?.toISOString()
        lastModifiedBy = person.lastModifiedBy ?: "unknown"
    }
}

fun PersonMap.toPerson(): Person {
    return Person(
        id = this.id,
        version = this.version.toLong(),
        lastModifiedTimestamp = this.lastModifiedTimestamp.toZonedDateTime(),
        firstName = this.firstName,
        lastName = this.lastName,
        dateOfBirth = this.dateOfBirth?.toLocalDate(),
        lastModifiedBy = this.lastModifiedBy
    )
}

fun PersonMap.toValuesList(): List<String?> {
    return allPersonFields.map { this.map[it] }
}


fun Person.toMap(): Map<String, String> {
    return this
        .toPersonMap()
        .map
        .asSequence()
        .filter { it.value != null }
        .associate{ it.key to it.value!! }
}