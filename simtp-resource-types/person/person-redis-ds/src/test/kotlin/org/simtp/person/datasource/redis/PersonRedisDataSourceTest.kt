/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.datasource.redis

import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.*
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import org.junit.jupiter.api.Assertions.*
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.common.serialization.withDefaults
import org.simtp.common.toISOString
import org.simtp.kotlin.util.uuid
import org.simtp.person.api.Person
import org.simtp.person.api.cloneAndIncrementVersion
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.core.ReactiveHashOperations
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.redis.core.ReactiveSetOperations
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Suppress("ReactiveStreamsUnusedPublisher")
class PersonRedisDataSourceTest {

    @Test
    fun save() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val person = Person()
        val personMap = person.cloneAndIncrementVersion().toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("person:${person.id}", personMap) } returns Mono.just(true)
        every { opsForHash.multiGet("person:${person.id}", listOf("version")) } returns Mono.just(listOf(null))

        val personDataSource = PersonRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        val mono = personDataSource.save(person).cache()

        StepVerifier
            .create(mono)
            .expectNext(person.cloneAndIncrementVersion())
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }

    @Test
    fun update() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val person = Person()
        val newPerson = person.cloneAndIncrementVersion()
        val personMap = newPerson.toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("person:${person.id}", personMap) } returns Mono.just(true)
        every { opsForHash.multiGet("person:${person.id}", listOf("version")) } returns Mono.just(listOf("0"))

        val personDataSource = PersonRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        val mono = personDataSource.save(person).cache()

        StepVerifier
            .create(mono)
            .expectNext(newPerson)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }

    @Test
    fun optimisticLockFailure() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val person = Person()
        val newPerson = person.cloneAndIncrementVersion()
        val personMap = newPerson.toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("person:${person.id}", personMap) } returns Mono.just(true)
        every { opsForHash.multiGet("person:${person.id}", listOf("version")) } returns Mono.just(listOf("1"))

        val personDataSource = PersonRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        val mono = personDataSource.save(person).cache()

        StepVerifier
            .create(mono)
            .expectError(OptimisticLockingFailureException::class.java)
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun deleteFailure() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val opsForSet = mockk<ReactiveSetOperations<String, String>>(relaxed = true)
        val person = Person()
        val newPerson = person.cloneAndIncrementVersion()
        val personMap = newPerson.toMap()

        every { opsForHash.delete(any()) } returns Mono.just(true)
        every { opsForHash.multiGet(any(), any()) } returns Mono.just(newPerson.toPersonMap().toValuesList())
        every { reactiveRedisOperations.opsForSet() } returns opsForSet
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForSet.isMember(any(), any()) } returns Mono.just(false)
        every { opsForSet.add(any(), any()) } returns Mono.just(1)

        every { opsForHash.putAll("person:${person.id}", personMap) } returns Mono.just(true)
        every { opsForHash.multiGet("person:${person.id}", listOf("version")) } returns Mono.just(listOf("1"))

        val personDeleteEvent = PersonDeleteEvent().apply {
            this.id = person.id
            this.messageId = uuid()
            this.version = person.version
        }

        val personDataSource = PersonRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        val mono = personDataSource.delete(personDeleteEvent).cache()

        StepVerifier
            .create(mono)
            .expectError(OptimisticLockingFailureException::class.java)
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val person = Person()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.multiGet("person:${person.id}", allPersonFields) } returns Mono.just(person.toPersonMap().toValuesList())
        val personDataSource = PersonRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        StepVerifier
            .create(personDataSource.findById(person.id))
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val person = Person()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.keys("person:*") } returns Flux.just("person:${person.id}")
        every { opsForHash.multiGet("person:${person.id}", allPersonFields) } returns Mono.just(person.toPersonMap().toValuesList())
        val personDataSource = PersonRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        StepVerifier
            .create(personDataSource.findAll())
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }


    @Test
    fun delete() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val person = Person()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.delete("person:${person.id}") } returns Mono.just(true)
        every { opsForHash.multiGet("person:${person.id}", allPersonFields) } returns Mono.just(person.toPersonMap().toValuesList())

        val personDataSource = PersonRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        StepVerifier
            .create(personDataSource.delete(person.id, person.version))
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun testToMap() {
        val person = Person()
        val map = person.toMap()
        val expected = mapOf(
            "id" to person.id,
            "version" to person.version.toString(),
            "lastModifiedTimestamp" to person.lastModifiedTimestamp.toISOString(),
            "firstName" to "",
            "lastName" to "",
            "lastModifiedBy" to "unknown"
        )
        assertEquals(expected, map)
    }


}