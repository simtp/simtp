/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.datasource.redis

import org.simtp.person.datasource.test.PersonDataSourceTestAdapter

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.simtp.common.EnableCommonConfiguration
import org.simtp.framework.serialization.EnableSerialization
import org.simtp.stream.kafka.EnableSimtpKafka
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.boot.test.context.SpringBootTest
import org.testcontainers.containers.GenericContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName


@SpringBootTest
@Testcontainers
@Tag("integration")
class PersonRedisDataSourceConfigurationTest: PersonDataSourceTestAdapter() {


    companion object {
        @Container
        @ServiceConnection
        var redisContainer: GenericContainer<*> = GenericContainer(DockerImageName.parse("redis:latest"))
            .withExposedPorts(6379)

        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()
        }

    }
}

@SpringBootApplication
@EnableSerialization
@EnablePersonRedisDataSource
@EnableSimtpKafka
@EnableCommonConfiguration
open class PersonRedisDataSourceTestApp