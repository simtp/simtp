/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "R2dbcDataSourceORMInspection")
package org.simtp.person.datasource.r2dbc

import io.r2dbc.spi.ConnectionFactory
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.api.person.api.stream.PersonUpdateEvent
import org.simtp.common.IDateTimeManager
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import org.simtp.kotlin.util.uuid
import org.simtp.person.api.*
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.core.io.ClassPathResource
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import org.springframework.r2dbc.connection.init.CompositeDatabasePopulator
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import reactor.kafka.receiver.ReceiverOptions
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZonedDateTime

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(PersonR2dbcDataSourceConfiguration::class)
annotation class EnablePersonR2dbcDataSource

@Repository
interface PersonRepository: ReactiveCrudRepository<PersonEntity, String> {
    fun deleteByIdAndVersion(id: String, version: Long): Mono<Void>
}

@Repository
interface PersonMessageIdRepository: ReactiveCrudRepository<PersonMessageIdEntity, String>

@Repository
interface PersonFailedMessageRepository: ReactiveCrudRepository<PersonFailedMessageEntity, Long>

@Configuration
@EnableR2dbcRepositories
@EntityScan
open class PersonR2dbcDataSourceConfiguration {

    @Bean
    open fun personR2dbcDataSource(
        personRepository: PersonRepository,
        personMessageIdRepository: PersonMessageIdRepository,
        personFailedMessageRepository: PersonFailedMessageRepository
    ): PersonR2dbcDataSource {
        return PersonR2dbcDataSource(
            personRepository = personRepository,
            personMessageIdRepository = personMessageIdRepository,
            personFailedMessageRepository = personFailedMessageRepository
        )
    }

    @Bean
    open fun personR2dbcReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\${spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "personR2dbcReactiveKafkaConsumer:$applicationName"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "personR2dbcReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(PERSON_STREAM_NAME)))
    }

    @Bean
    open fun personR2dbcStreamConsumer(
        personR2dbcReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        personR2dbcDataSource: PersonR2dbcDataSource,
        dateTimeManager: IDateTimeManager
    ): PersonR2dbcStreamConsumer {
        return PersonR2dbcStreamConsumer(
            personR2dbcReactiveKafkaConsumerTemplate = personR2dbcReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            personR2dbcDataSource = personR2dbcDataSource,
            dateTimeManager = dateTimeManager
        )
    }

    @Bean
    open fun initializer(connectionFactory: ConnectionFactory): ConnectionFactoryInitializer {
        return ConnectionFactoryInitializer().apply {
            setConnectionFactory(connectionFactory)
            setDatabasePopulator(
                CompositeDatabasePopulator().apply {
                    addPopulators(
                        ResourceDatabasePopulator(
                            ClassPathResource("schema.sql"),
                        )
                    )
                }
            )
        }
    }
}

class PersonR2dbcStreamConsumer(
    private val personR2dbcReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val personR2dbcDataSource: PersonR2dbcDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(PersonR2dbcStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<Person> {
        return personR2dbcReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                personStreamHandler(
                    it.value(),
                    personR2dbcDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, PERSON_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.toPersonFailureStreamEvent(this::class.simpleName!!) }
                }
            }

    }

}

open class PersonR2dbcDataSource(
    private val personRepository: PersonRepository,
    private val personMessageIdRepository: PersonMessageIdRepository,
    private val personFailedMessageRepository: PersonFailedMessageRepository
): IPersonDataSource {

    companion object {
        private val log = LoggerFactory.getLogger(PersonR2dbcDataSource::class.java)
    }

    private fun Mono<Person>.saveMessageId(messageId: String): Mono<Person> {
        return this
            .flatMap {
                personMessageIdRepository
                    .save(PersonMessageIdEntity(id = messageId))
                    .doOnError { ex -> log.error("Failed to save {}", messageId, ex) }
                    .doOnNext { messageId -> log.info("Saving messageId {}", messageId.id) }
                    .thenReturn(it)
            }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    override fun create(personCreateEvent: PersonCreateEvent): Mono<Person> {
        return personMessageIdRepository
            .existsById(personCreateEvent.messageId.toString())
            .filter { it == false }
            .flatMap { save(personCreateEvent.toPerson()) }
            .saveMessageId(personCreateEvent.messageId.toString())
    }

    @Transactional(propagation = Propagation.REQUIRED)
    override fun update(personUpdateEvent: PersonUpdateEvent): Mono<Person> {
        return personMessageIdRepository
            .existsById(personUpdateEvent.messageId.toString())
            .filter { it == false }
            .flatMap { save(personUpdateEvent.toPerson()) }
            .saveMessageId(personUpdateEvent.messageId.toString())
    }

    @Transactional(propagation = Propagation.REQUIRED)
    override fun delete(personDeleteEvent: PersonDeleteEvent): Mono<Person> {
        return personMessageIdRepository
            .existsById(personDeleteEvent.messageId.toString())
            .filter { it == false }
            .flatMap {
                delete(personDeleteEvent.id.toString(), personDeleteEvent.version)
            }
            .saveMessageId(personDeleteEvent.messageId.toString())

    }

    private fun delete(id: String, version: Long): Mono<Person> {
        val cachedValue = personRepository
            .findById(id)
            .log("INFO")
            .doOnNext { if (it.version != version) throw OptimisticLockingFailureException("Provided providedValue=${version} currentValue=${it.version}")  }
            .cache()

        return cachedValue
            .flatMap {
                personRepository.deleteByIdAndVersion(id, version)
            }
            .then(cachedValue)
            .map { it.toPerson() }
    }

    internal fun save(person: Person): Mono<Person> {
        return personRepository
            .save(person.toPersonEntity())
            .map { it.toPerson() }
    }

    override fun findById(id: String): Mono<Person> {
        return personRepository
            .findById(id)
            .map { it.toPerson() }
    }

    override fun findAll(): Flux<Person> {
        return personRepository
            .findAll()
            .map { it.toPerson() }
    }

    override fun failure(personFailureEvent: PersonFailureEvent): Mono<Unit> {
        return personFailedMessageRepository
            .save(personFailureEvent.toPersonFailedMessageEntity())
            .doOnNext { log.error("Failed to consumer message") }
            .then(Mono.empty())

    }

}


@Table(name = "person")
open class PersonEntity(
    @Id
    open var id : String = uuid(),



    @Column("first_name")
    open var firstName : String = "",

    @Column("last_name")
    open var lastName : String = "",

    @Column("dob")
    open var dateOfBirth: LocalDate? = null,

    @Column("last_modified_timestamp")
    open var lastModifiedTimestamp: OffsetDateTime = OffsetDateTime.now(),

    @Version()
    open var version: Long = 0
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PersonEntity

        if (id != other.id) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (dateOfBirth != other.dateOfBirth) return false
        if (lastModifiedTimestamp != other.lastModifiedTimestamp) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + firstName.hashCode()
        result = 31 * result + lastName.hashCode()
        result = 31 * result + dateOfBirth.hashCode()
        result = 31 * result + lastModifiedTimestamp.hashCode()
        result = 31 * result + version.hashCode()
        return result
    }

    override fun toString(): String {
        return "PersonEntity(id='$id', firstName='$firstName', lastName='$lastName', dateOfBirth=$dateOfBirth, lastModifiedTimestamp=$lastModifiedTimestamp, version=$version)"
    }


}

@Table(name = "person_message_id")
open class PersonMessageIdEntity(
    @Suppress("unused")
    @Id
    @Column("id")
    var id: String? = null,

    @Version
    open var version: Long? = null
)

fun PersonFailureEvent.toPersonFailedMessageEntity(): PersonFailedMessageEntity {
    return PersonFailedMessageEntity(
        id = null,
        personId = this.id,
        messageId = this.messageId,
        firstName = this.firstName,
        lastName = this.lastName,
        dateOfBirth = this.dateOfBirth,
        updated = this.updated,
        personVersion = this.version,
        modifiedBy = this.modifiedBy,
        source = this.source,
        reference = this.reference,
        failureDateTime = this.failureDateTime
    )
}

@Table(name = "person_message_id")
open class PersonFailedMessageEntity(
    @Id
    val id: Long? = null,
    @Column("person_id")
    val personId: String? = null,
    @Column("message_id")
    val messageId: String = uuid(),
    @Column("first_name")
    val firstName: String = "",
    @Column("last_name")
    val lastName: String = "",
    @Column("dob")
    val dateOfBirth: LocalDate? = null,
    @Column("last_modified_timestamp")
    val updated: ZonedDateTime = ZonedDateTime.now(),
    @Column("previous_version")
    val personVersion: Long? = null,
    @Column("last_modified_by")
    val modifiedBy: String = "",
    @Column("failure_timestamp")
    val source: String = "",
    val reference: String = uuid(),
    @Column("failure_timestamp")
    val failureDateTime: ZonedDateTime = ZonedDateTime.now(),

    @Version
    val version: Long = 0

)

internal fun Person.toPersonEntity(): PersonEntity {
    return PersonEntity(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        dateOfBirth = null,
        lastModifiedTimestamp = this.lastModifiedTimestamp.toOffsetDateTime(),
        version = this.version
    )
}

internal fun PersonEntity.toPerson(): Person {
    return Person(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        dateOfBirth = this.dateOfBirth,
        lastModifiedTimestamp = this.lastModifiedTimestamp.toZonedDateTime(),
        version = this.version
    )
}
