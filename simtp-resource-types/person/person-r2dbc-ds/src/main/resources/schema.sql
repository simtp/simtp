CREATE TABLE IF NOT EXISTS person
(
    id                      varchar(36) not null,
    version                 BIGINT                   not null,
    last_modified_timestamp TIMESTAMP WITH TIME ZONE not null,
    last_modified_by        varchar(200),
    first_name              varchar(100)             not null,
    last_name               varchar(100)             not null,
    dob                     DATE,
    primary key (id)
);

CREATE TABLE IF NOT EXISTS person_message_id
(
    id varchar(36) not null,
    version BIGINT not null,
    primary key (id)
);

CREATE TABLE IF NOT EXISTS person_failed_message
(
    id BIGINT not null,
    person_id                varchar(36) not null,
    message_id                varchar(36) not null,
    version                 BIGINT                   not null,
    last_modified_timestamp TIMESTAMP WITH TIME ZONE not null,
    last_modified_by        varchar(200),
    first_name              varchar(100)             not null,
    last_name               varchar(100)             not null,
    dob                     DATE,
    source               varchar(100)             not null,
    previous_version                 BIGINT                   not null,
    reference               varchar(100)             not null,
    failure_timestamp TIMESTAMP WITH TIME ZONE not null,
    primary key (id)
);

