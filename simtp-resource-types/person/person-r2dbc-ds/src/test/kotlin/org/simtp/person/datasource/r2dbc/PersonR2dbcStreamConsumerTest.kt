package org.simtp.person.datasource.r2dbc

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.junit.jupiter.api.Test
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonFailureEvent
import org.simtp.common.IDateTimeManager
import org.simtp.common.toISOString
import org.simtp.kotlin.util.uuid
import org.simtp.person.api.PERSON_STREAM_NAME
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration
import java.time.ZonedDateTime

class PersonR2dbcStreamConsumerTest {
    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun consumerHandlesError() {
        val template = mockk<ReactiveKafkaConsumerTemplate<String, *>>()
        val consumerRecord = mockk<ConsumerRecord<String, Any>>()
        val zonedDateTime = ZonedDateTime.now()

        every { template.receiveAutoAck() } returns Flux.fromIterable(listOf(consumerRecord))

        every { consumerRecord.value() } returns PersonCreateEvent().apply {
            this.id = "123"
            this.firstName = "Joe"
            this.lastName = "Bloggs"
            this.createdBy = "test"
            this.createdTimestamp = zonedDateTime.toISOString()
            this.reference = "ref"
            this.messageId = uuid()
        }


        val dataSource = mockk<PersonR2dbcDataSource>()

        every { dataSource.create(any()) } returns Mono.error(IllegalStateException("ERROR"))
        every { dataSource.failure(any()) } returns Mono.empty()


        val dateTimeManager = mockk<IDateTimeManager>()
        val reactiveKafkaProducerTemplate = mockk<ReactiveKafkaProducerTemplate<String, Any>>()

        every { reactiveKafkaProducerTemplate.send(any<String>(), any<PersonFailureEvent>()) } returns Mono.just(mockk(relaxed = true))

        every { dateTimeManager.currentZonedDateTime } returns zonedDateTime
        val consumer = PersonR2dbcStreamConsumer(
            personR2dbcReactiveKafkaConsumerTemplate = template,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            personR2dbcDataSource = dataSource,
            dateTimeManager = dateTimeManager
        )

        StepVerifier
            .create(consumer.createConsumer())
            .expectNextCount(0)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) {reactiveKafkaProducerTemplate.send("$PERSON_STREAM_NAME.DLQ", any<Any>())  }
    }

}