/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("ReactiveStreamsUnusedPublisher")

package org.simtp.person.datasource.r2dbc

import io.mockk.*
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import org.simtp.person.api.Person
import org.junit.jupiter.api.Assertions.*
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.kotlin.util.uuid
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class PersonR2dbcDataSourceTest {

    @Test
    fun save() {
        val personRepository = mockk<PersonRepository>(relaxed = true)
        val personMessageIdRepository = mockk<PersonMessageIdRepository>(relaxed = true)
        val personFailedMessageRepository = mockk<PersonFailedMessageRepository>(relaxed = true)
        val person = Person()
        val entity = person.toPersonEntity()
        every { personRepository.save(any()) } returns Mono.just(entity)
        every { personMessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { personMessageIdRepository.save(any<PersonMessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as PersonMessageIdEntity) }
        val personDataSource = PersonR2dbcDataSource(personRepository, personMessageIdRepository, personFailedMessageRepository)
        StepVerifier
            .create(personDataSource.save(person))
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val personRepository = mockk<PersonRepository>(relaxed = true)
        val personMessageIdRepository = mockk<PersonMessageIdRepository>(relaxed = true)
        val personFailedMessageRepository = mockk<PersonFailedMessageRepository>(relaxed = true)
        val person = Person()
        val entity = person.toPersonEntity()
        every { personRepository.findById(person.id) } returns Mono.just(entity)
        every { personMessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { personMessageIdRepository.save(any<PersonMessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as PersonMessageIdEntity) }
        val personDataSource = PersonR2dbcDataSource(personRepository, personMessageIdRepository, personFailedMessageRepository)
        StepVerifier
            .create(personDataSource.findById(person.id))
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val personRepository = mockk<PersonRepository>(relaxed = true)
        val personMessageIdRepository = mockk<PersonMessageIdRepository>(relaxed = true)
        val personFailedMessageRepository = mockk<PersonFailedMessageRepository>(relaxed = true)
        val person = Person()
        val entity = person.toPersonEntity()
        every { personRepository.findAll() } returns Flux.fromIterable(listOf(entity))
        val personDataSource = PersonR2dbcDataSource(personRepository, personMessageIdRepository, personFailedMessageRepository)
        StepVerifier
            .create(personDataSource.findAll())
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun delete() {
        val personRepository = mockk<PersonRepository>(relaxed = true)
        val personMessageIdRepository = mockk<PersonMessageIdRepository>(relaxed = true)
        val personFailedMessageRepository = mockk<PersonFailedMessageRepository>(relaxed = true)

        val personDeleteEvent = PersonDeleteEvent().apply {
            id = uuid()
            messageId = uuid()
            version = 0
        }

        val person = Person(
            id = personDeleteEvent.id.toString(),
            version = personDeleteEvent.version
        )

        val entity = person.toPersonEntity()
        every { personRepository.findById(person.id) } returns Mono.just(entity)
        every { personRepository.deleteByIdAndVersion(person.id, person.version) } returns Mono.empty()

        every { personMessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { personMessageIdRepository.save(any<PersonMessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as PersonMessageIdEntity) }

        val personDataSource = PersonR2dbcDataSource(personRepository, personMessageIdRepository, personFailedMessageRepository)

        StepVerifier
            .create(personDataSource.delete(personDeleteEvent))
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify { personRepository.deleteByIdAndVersion(person.id, person.version) }
    }

    @Test
    fun testMapping() {
        val person = Person()
        assertEquals(person, person.toPersonEntity().toPerson())
    }
}