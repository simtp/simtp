/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.server

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.simtp.person.api.*
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration

class PersonCommandWebRestControllerTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun create() {
        val service = mockk<IPersonCommandApi>()
        val personCreate = PersonCreate()
        every { service.create(personCreate) } returns Mono.just(personCreate.id)

        val controller = PersonCommandWebRestController(service)

        StepVerifier
            .create(controller.create(personCreate))
            .expectNext(personCreate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.create(personCreate) }

    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun update() {
        val service = mockk<IPersonCommandApi>()
        val personUpdate = PersonUpdate()
        every { service.update(personUpdate) } returns Mono.just(personUpdate.id)

        val controller = PersonCommandWebRestController(service)

        StepVerifier
            .create(controller.update(personUpdate))
            .expectNext(personUpdate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.update(personUpdate) }

    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun delete() {
        val service = mockk<IPersonCommandApi>()
        val personDelete = PersonDelete()
        every { service.delete(personDelete) } returns Mono.just(personDelete.id)

        val controller = PersonCommandWebRestController(service)

        StepVerifier
            .create(controller.delete(personDelete))
            .expectNext(personDelete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.delete(personDelete) }

    }    
}