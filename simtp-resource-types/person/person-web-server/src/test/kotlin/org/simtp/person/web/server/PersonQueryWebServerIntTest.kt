/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.server

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.simtp.person.api.Person
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.test.StepVerifier
import java.time.Duration

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integration")
class PersonQueryWebRestControllerIntTest {

    @LocalServerPort
    private var localServerPort: Int? = null

    private var webClient = defaultWebClientBuilder.baseUrl("http://localhost:8080/api/person/1.0").build()

    @Autowired
    private lateinit var personInMemoryService: PersonInMemoryCommandService

    @BeforeEach
    fun setUp() {
         webClient = defaultWebClientBuilder
             .baseUrl("http://localhost:$localServerPort/api/person/1.0")
             .build()
    }

    @Test
    fun get() {
        val person = Person()
        personInMemoryService.map[person.id] = person
        val retrieveMono = webClient
            .get()
            .uri("/{id}", mapOf("id" to person.id))
            .retrieve()
            .bodyToMono<Person>()
        StepVerifier
            .create(retrieveMono)
            .expectNextMatches {
                it.id == person.id
                && it.firstName == person.firstName
                && it.lastName == person.lastName
                && it.version == person.version
            }
            .expectComplete()
            .verify(Duration.ofSeconds(5))


    }
}