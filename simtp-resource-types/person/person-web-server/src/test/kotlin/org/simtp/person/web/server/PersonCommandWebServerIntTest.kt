/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.server

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.simtp.person.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpMethod
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.test.StepVerifier
import java.time.Duration
import java.time.ZoneId

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integration")
class PersonCommandWebRestControllerIntTest {

    @LocalServerPort
    private var localServerPort: Int? = null

    private var webClient = defaultWebClientBuilder.baseUrl("http://localhost:8080/api/person/1.0").build()

    @Autowired
    private lateinit var personInMemoryCommandService: PersonInMemoryCommandService


    @BeforeEach
    fun setUp() {
         webClient = defaultWebClientBuilder
             .baseUrl("http://localhost:$localServerPort/api/person/1.0")
             .build()
    }

    @Test
    fun create() {
        val personCreate = PersonCreate()

        val retrieveMono = webClient
            .put()
            .bodyValue(personCreate)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(personCreate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(personCreate.toPerson(), personInMemoryCommandService.map[personCreate.id]?.withLocalZonedDateTime())

    }

    @Test
    fun update() {
        val personUpdate = PersonUpdate()
        personInMemoryCommandService.map[personUpdate.id] = personUpdate.toPerson()
        val retrieveMono = webClient
            .post()
            .bodyValue(personUpdate)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(personUpdate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(personUpdate.toPerson(), personInMemoryCommandService.map[personUpdate.id]?.withLocalZonedDateTime())

    }

    @Test
    fun delete() {
        val personDelete = PersonDelete()
        personInMemoryCommandService.map[personDelete.id] = Person(id = personDelete.id)
        val retrieveMono = webClient
            .method(HttpMethod.DELETE)
            .bodyValue(personDelete)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(personDelete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertNull(personInMemoryCommandService.map[personDelete.id]?.withLocalZonedDateTime())

    }

}

internal fun Person.withLocalZonedDateTime(): Person {
    return Person(
        id = this.id,
        version = this.version,
        firstName = this.firstName,
        lastName = this.lastName,
        dateOfBirth = this.dateOfBirth,
        lastModifiedTimestamp = this.lastModifiedTimestamp.withZoneSameInstant(ZoneId.systemDefault())
    )
}