/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.server

import org.simtp.person.api.*
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import reactor.core.publisher.Mono


@SpringBootApplication
@EnablePersonCommandWebServer
@EnablePersonQueryWebServer
open class PersonWebServerTestApp {

    private val map: MutableMap<String, Person> = mutableMapOf()

    @Bean
    open fun personInMemoryCommandService(): PersonInMemoryCommandService {
        return PersonInMemoryCommandService(map)
    }

    @Bean
    open fun personInMemoryQueryService(): PersonInMemoryQueryService {
        return PersonInMemoryQueryService(map)
    }

}


open class PersonInMemoryCommandService(
    internal val map: MutableMap<String, Person> = mutableMapOf()
): IPersonCommandApi {

    override fun create(personCreate: PersonCreate): Mono<String> {
        return Mono.fromCallable {
            map[personCreate.id] = personCreate.toPerson()
            personCreate.id
        }
    }

    override fun update(personUpdate: PersonUpdate): Mono<String> {
        return Mono.fromCallable {
            map[personUpdate.id] = personUpdate.toPerson()
            personUpdate.id
        }

    }

    override fun delete(personDelete: PersonDelete): Mono<String> {
        return Mono.fromCallable {
            map.remove(personDelete.id)?.id
        }

    }
}

open class PersonInMemoryQueryService(
    internal val map: MutableMap<String, Person> = mutableMapOf()
): IPersonQueryApi {

    override fun get(id: String): Mono<Person> {
        return Mono.justOrEmpty(map[id])
    }

}

