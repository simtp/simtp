
/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.server

import org.simtp.person.api.IPersonQueryApi
import org.simtp.person.api.Person
import org.simtp.graphql.EnableSimTPGraphQl
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.*
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(PersonQueryWebServerConfiguration::class)
annotation class EnablePersonQueryWebServer

@Configuration
@ComponentScan
@EnableSimTPGraphQl
open class PersonQueryWebServerConfiguration

@RestController
@RequestMapping("/api/person/1.0")
open class PersonQueryWebRestController(
    private val personQueryService: IPersonQueryApi
) {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(PersonQueryWebRestController::class.java)
    }

        @GetMapping("/{id}")
        fun get(@PathVariable id: String): Mono<ResponseEntity<Person>> {
            return personQueryService[id]
                .doOnNext { log.info("Received value={}", it) }
                .doOnError { log.error("Exception handling query", it) }
                .map { ResponseEntity.ofNullable(it) }
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()))
                .doOnNext { log.info("Return person query for id={} value={}", id, it.statusCode) }
        }
}

@Controller
class PersonQueryController(
    private val personQueryService: IPersonQueryApi
) {

    @QueryMapping
    fun getPerson(@Argument id: String): Mono<Person> {
        return personQueryService[id]
    }

}


