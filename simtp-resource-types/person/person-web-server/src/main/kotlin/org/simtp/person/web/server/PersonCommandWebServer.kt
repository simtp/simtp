/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.server

import org.simtp.person.api.*
import org.simtp.graphql.EnableSimTPGraphQl
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.*
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(PersonCommandWebServerConfiguration::class)
annotation class EnablePersonCommandWebServer

@Configuration
@ComponentScan
@EnableSimTPGraphQl
open class PersonCommandWebServerConfiguration

@RestController
@RequestMapping("/api/person/1.0")
open class PersonCommandWebRestController(
    private val personCommandService: IPersonCommandApi
) {

        @PutMapping
        fun create(@RequestBody personCreate: PersonCreate): Mono<String> {
            return personCommandService.create(personCreate)
        }

        @PostMapping
        fun update(@RequestBody personUpdate: PersonUpdate): Mono<String> {
            return personCommandService.update(personUpdate)
        }

        @DeleteMapping
        fun delete(@RequestBody personDelete: PersonDelete): Mono<String> {
            return personCommandService.delete(personDelete)
        }
}

@Controller
class PersonCommandController(
    private val personCommandService: IPersonCommandApi,
) {

    init {
        log.info("Initializing ProductCommandController")
    }

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(PersonCommandController::class.java)
    }

    @MutationMapping
    fun personCreate(@Argument personCreate: PersonCreate): Mono<String> {
        return personCommandService.create(personCreate)
    }

    @MutationMapping
    fun personUpdate(@Argument personUpdate: PersonUpdate): Mono<String> {
        return personCommandService.update(personUpdate)
    }

    @MutationMapping
    fun personDelete(@Argument personDelete: PersonDelete): Mono<String> {
        return personCommandService.delete(personDelete)
    }
}







