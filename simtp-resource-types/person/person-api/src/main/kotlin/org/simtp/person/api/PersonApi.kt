/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.api

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import org.simtp.kotlin.util.uuid
import org.slf4j.Logger
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.api.person.api.stream.PersonDetails
import org.simtp.api.person.api.stream.PersonUpdateEvent
import org.simtp.common.toISOString
import org.simtp.common.toLocalDate
import org.simtp.common.toZonedDateTime
import java.lang.RuntimeException
import java.time.LocalDate
import java.time.ZonedDateTime
import java.util.UUID
import org.simtp.api.person.api.stream.PersonFailureEvent as PersonFailureStreamEvent

const val PERSON_STREAM_NAME = "person"


interface IPersonCommandApi {
    fun create(personCreate: PersonCreate): Mono<String>

    fun update(personUpdate: PersonUpdate): Mono<String>

    fun delete(personDelete: PersonDelete) : Mono<String>
}

interface IPersonQueryApi {
    operator fun get(id: String): Mono<Person>
}

data class Person(
    val id: String = uuid(),
    val firstName: String = "",
    val lastName: String = "",
    val dateOfBirth: LocalDate? = null,
    val lastModifiedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val lastModifiedBy: String? = "unknown",
    val version: Long = 0,
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Person

        if (id != other.id) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (dateOfBirth != other.dateOfBirth) return false
        if (version != other.version) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + firstName.hashCode()
        result = 31 * result + lastName.hashCode()
        result = 31 * result + (dateOfBirth?.hashCode() ?: 0)
        result = 31 * result + version.hashCode()
        return result
    }
}



data class PersonCreate(
    val id: String = uuid(),
    val messageId: String = uuid(),
    val firstName: String = "",
    val lastName: String = "",
    val dateOfBirth: LocalDate? = null,
    val createdTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val createdBy: String? = "unknown",
    val reference: String = uuid()
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PersonCreate

        if (id != other.id) return false
        if (messageId != other.messageId) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (dateOfBirth != other.dateOfBirth) return false
        if (createdBy != other.createdBy) return false
        if (reference != other.reference) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + messageId.hashCode()
        result = 31 * result + firstName.hashCode()
        result = 31 * result + lastName.hashCode()
        result = 31 * result + (dateOfBirth?.hashCode() ?: 0)
        result = 31 * result + (createdBy?.hashCode() ?: 0)
        result = 31 * result + (reference.hashCode())
        return result
    }
}


data class PersonUpdate(
    val id: String = uuid(),
    val messageId: String = uuid(),
    val firstName: String = "",
    val lastName: String = "",
    val dateOfBirth: LocalDate? = null,
    val modifiedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val modifiedBy: String? = "unknown",
    val reference: String = UUID.randomUUID().toString(),
    val version: Long = 0
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PersonUpdate

        if (id != other.id) return false
        if (messageId != other.messageId) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (dateOfBirth != other.dateOfBirth) return false
        if (modifiedBy != other.modifiedBy) return false
        if (version != other.version) return false
        if (reference != other.reference) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + messageId.hashCode()
        result = 31 * result + firstName.hashCode()
        result = 31 * result + lastName.hashCode()
        result = 31 * result + (dateOfBirth?.hashCode() ?: 0)
        result = 31 * result + (modifiedBy?.hashCode() ?: 0)
        result = 31 * result + version.hashCode()
        result = 31 * result + reference.hashCode()
        return result
    }
}

data class PersonDelete(
    val id: String = uuid(),
    val messageId: String = uuid(),
    val deletedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val deletedBy: String? = "unknown",
    val reference: String = UUID.randomUUID().toString(),
    val version: Long = 0
)

fun PersonCreateEvent.toPerson(): Person {
    return Person(
        id = this.id.toString(),
        firstName = this.firstName?.toString() ?: "",
        lastName = this.lastName?.toString() ?: "",
        dateOfBirth = this.dateOfBirth?.toString()?.toLocalDate(),
        lastModifiedTimestamp = this.createdTimestamp?.toString()?.toZonedDateTime() ?: ZonedDateTime.now(),
        lastModifiedBy = this.createdBy?.toString()
    )
}

fun PersonUpdateEvent.toPerson(): Person {
    return Person(
        id = this.id.toString(),
        firstName = this.firstName?.toString() ?: "",
        lastName = this.lastName?.toString() ?: "",
        dateOfBirth = this.dateOfBirth?.toString()?.toLocalDate(),
        lastModifiedTimestamp = this.modifiedTimestamp?.toString()?.toZonedDateTime() ?: ZonedDateTime.now(),
        lastModifiedBy = this.modifiedBy?.toString(),
        version = this.version
    )
}

fun PersonCreate.toPersonCreateEvent(): PersonCreateEvent {
    val person = this
    return PersonCreateEvent().apply {
        this.id = person.id
        this.firstName = person.firstName
        this.lastName = person.lastName
        this.dateOfBirth = person.dateOfBirth?.toISOString()
        this.createdTimestamp = person.createdTimestamp.toISOString()
        this.createdBy = person.createdBy
        this.reference = person.reference
        this.messageId = person.messageId
    }
}

data class PersonFailureEvent(
    val id: String = uuid(),
    val messageId: String = uuid(),
    val firstName: String = "",
    val lastName: String = "",
    val dateOfBirth: LocalDate? = null,
    val updated: ZonedDateTime = ZonedDateTime.now(),
    val version: Long? = null,
    val modifiedBy: String = "",
    val source: String = "",
    val reference: String = uuid(),
    val failureDateTime: ZonedDateTime = ZonedDateTime.now(),
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PersonFailureEvent

        if (id != other.id) return false
        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false
        if (dateOfBirth != other.dateOfBirth) return false
        if (version != other.version) return false
        if (modifiedBy != other.modifiedBy) return false
        if (source != other.source) return false
        if (reference != other.source) return false


        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + firstName.hashCode()
        result = 31 * result + lastName.hashCode()
        result = 31 * result + (dateOfBirth?.hashCode() ?: 0)
        result = 31 * result + version.hashCode()
        result = 31 * result + modifiedBy.hashCode()
        result = 31 * result + source.hashCode()
        return result
    }
}

fun PersonCreate.toPerson(): Person {
    val person = this
    return Person(
        id = person.id,
        firstName = person.firstName,
        lastName = person.lastName,
        dateOfBirth = person.dateOfBirth,
        lastModifiedTimestamp = person.createdTimestamp,
        lastModifiedBy = person.createdBy
    )
}

fun PersonUpdate.toPerson(): Person {
    return Person(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        dateOfBirth = this.dateOfBirth,
        lastModifiedTimestamp = this.modifiedTimestamp,
        lastModifiedBy = this.modifiedBy,
        version = this.version
    )
}


fun PersonUpdate.toPersonUpdateEvent(): PersonUpdateEvent {
    val person = this
    return PersonUpdateEvent().apply {
        this.id = person.id
        this.firstName = person.firstName
        this.lastName = person.lastName
        this.dateOfBirth = person.dateOfBirth?.toISOString()
        this.modifiedTimestamp = person.modifiedTimestamp.toISOString()
        this.modifiedBy = person.modifiedBy
        this.version = person.version
        this.reference = person.reference
        this.messageId = person.messageId

    }
}

fun PersonDelete.toPersonDeleteEvent(): PersonDeleteEvent {
    val person = this
    return PersonDeleteEvent().apply {
        this.id = person.id
        this.deletedTimestamp = person.deletedTimestamp.toISOString()
        this.deletedBy = person.deletedBy
        this.version = person.version
        this.reference = person.reference
        this.messageId = person.messageId
    }
}

fun Person.cloneAndIncrementVersion(): Person {
    return Person(
        id = this.id,
        firstName = this.firstName,
        lastName = this.lastName,
        dateOfBirth = this.dateOfBirth,
        version = this.version + 1,
        lastModifiedTimestamp = this.lastModifiedTimestamp,
        lastModifiedBy = this.lastModifiedBy,
    )
}

fun PersonCreateEvent.toPersonFailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): PersonFailureEvent {
    return PersonFailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        firstName = this.firstName.toString(),
        lastName = this.lastName.toString(),
        dateOfBirth = this.dateOfBirth?.toString()?.toLocalDate(),
        updated = this.createdTimestamp.toString().toZonedDateTime(),
        modifiedBy = this.createdBy.toString(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
        version = 0
    )
}

fun PersonUpdateEvent.toPersonFailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): PersonFailureEvent {
    return PersonFailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        firstName = this.firstName.toString(),
        lastName = this.lastName.toString(),
        dateOfBirth = this.dateOfBirth?.toString()?.toLocalDate(),
        updated = this.modifiedTimestamp.toString().toZonedDateTime(),
        version = this.version,
        modifiedBy = this.modifiedBy.toString(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
    )
}


fun PersonDeleteEvent.toPersonFailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): PersonFailureEvent {
    return PersonFailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
        version = this.version,
    )
}

fun PersonChangeException.toPersonFailureStreamEvent(source: String): PersonFailureStreamEvent {
    val input = this
    return PersonFailureStreamEvent().apply {
        this.personDetails = input.personFailureEvent.toPersonDetails()
        this.reference = input.reference
        this.messageId = input.personFailureEvent.messageId
        this.failureTimestamp = input.personFailureEvent.failureDateTime.toISOString()
        this.failureReason = input.message
        this.failureReference = input.reference
        this.failureSource = source
    }

}

fun PersonFailureEvent.toPersonDetails(): PersonDetails {
    val person = this
    return PersonDetails().apply {
        this.id = person.id
        this.version = person.version ?: 0
        this.lastName = person.lastName
        this.firstName = person.firstName
        this.dateOfBirth = person.dateOfBirth?.toISOString()
        this.reference = person.reference
    }
}


interface IPersonDataSource {

    fun create(personCreateEvent: PersonCreateEvent): Mono<Person>

    fun update(personUpdateEvent: PersonUpdateEvent): Mono<Person>

    fun findById(id: String): Mono<Person>

    fun findAll(): Flux<Person>

    fun delete(personDeleteEvent: PersonDeleteEvent): Mono<Person>

    fun failure(personFailureEvent: PersonFailureEvent): Mono<Unit>

}

@Suppress("unused")
open class PersonChangeException(
    val personFailureEvent: PersonFailureEvent,
    val eventType: String,
    val reference: String = uuid(),
    throwable: Throwable
) : RuntimeException(throwable)


val personStreamHandler:
            (Any, IPersonDataSource, Logger, () -> ZonedDateTime, (PersonChangeException) -> Mono<Unit>) -> Mono<Person> = { event, ds, log, currentZonedDateTime, errorSender ->

    when (event) {
        is PersonCreateEvent -> handleCreateEvent(event, ds, log, currentZonedDateTime)
        is PersonUpdateEvent -> handleUpdateEvent(event, ds, log, currentZonedDateTime)
        is PersonDeleteEvent -> handleDeleteEvent(event, ds, log, currentZonedDateTime)
        else -> {
            log.error("Unexpected record type={}", event::class.java)
            Mono.empty()
        }}
        .doOnError {
            log.error("Experienced ERROR", it)
        }
        .handleError(
//            errorClass = PersonChangeException::class.java,
            errorSender = errorSender
        ) { ds.failure(it.personFailureEvent) }
        .onErrorResume {
            log.error("Experienced error handling event", it)
            Mono.empty()
        }


}

val handleCreateEvent: (PersonCreateEvent, IPersonDataSource, Logger, () -> ZonedDateTime) -> Mono<Person> = { createEvent, ds, log, currentZonedDateTime ->
    log.info("handleCreateEvent {}", createEvent)
    ds.create(createEvent)
        .doOnError {
            log.error("Exception thrown in handleCreateEvent", it)
        }
        .onErrorMap {
            PersonChangeException(
                personFailureEvent = createEvent.toPersonFailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "create",
                reference = createEvent.reference.toString(),
                throwable = it
            )
        }
}

val handleUpdateEvent: (PersonUpdateEvent, IPersonDataSource, Logger, () -> ZonedDateTime) -> Mono<Person> = { updateEvent, ds, log, currentZonedDateTime ->
    log.info("handleUpdateEvent {}", updateEvent)
    ds.update(updateEvent)
        .onErrorMap {
            log.error("Failed to update {}", updateEvent, it)
            PersonChangeException(
                personFailureEvent = updateEvent.toPersonFailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "update",
                reference = updateEvent.reference.toString(),
                throwable = it
            )
        }
}

val handleDeleteEvent: (PersonDeleteEvent, IPersonDataSource, Logger, () -> ZonedDateTime) -> Mono<Person> = { deleteEvent, ds, log, currentZonedDateTime ->
    log.info("handleDeleteEvent {}", deleteEvent)
    ds
        .delete(deleteEvent)
        .doOnError {
            log.warn("***** Exception encountered in handleDeleteEvent", it)
        }
        .onErrorMap {
            log.error("Failed to delete {}", deleteEvent, it)
            PersonChangeException(
                personFailureEvent = deleteEvent.toPersonFailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "delete",
                reference = deleteEvent.reference.toString(),
                throwable = it

            )
        }
        .doOnError {
            log.warn("***** Exception encountered in handleDeleteEvent", it)
        }
}

/*
fun <V, E: Throwable> Mono<V>.handleError(
    errorSender: (E) -> Mono<Unit>,
    errorWriter: (E) -> Mono<Unit>,
): Mono<V> =
    this.doOnError { ex -> errorWriter(ex as E) }
        .doOnError { ex -> errorSender(ex as E) }
 */

fun <V, E: Throwable> Mono<V>.handleError(
    errorSender: (E) -> Mono<Unit>,
    errorWriter: (E) -> Mono<Unit>,
): Mono<V> =
    this.onErrorResume { ex ->
        errorWriter(ex as E)
            .doOnError {
                println("error saving failure message")
            }
            .then(errorSender(ex))
            .log("Called error sender")
            .then(Mono.error(ex))

    }

