package org.simtp.person.api

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.ZonedDateTime

class PersonApiKtTest {

    @Test
    fun testCloneAndIncrementVersion() {
        val localDate = LocalDate.now()
        val modified = ZonedDateTime.now()
        val person = Person(
            id = "1",
            firstName = "Joe",
            lastName = "smith",
            dateOfBirth = localDate,
            lastModifiedTimestamp = modified,
            lastModifiedBy = "test",
            version = 0
        )
        val person1 = Person(
            id = "1",
            firstName = "Joe",
            lastName = "smith",
            dateOfBirth = localDate,
            lastModifiedTimestamp = modified,
            lastModifiedBy = "test",
            version = 1
        )

        assertEquals(person1, person.cloneAndIncrementVersion())
    }
}