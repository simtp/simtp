/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.api

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import java.time.Duration

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.api.person.api.stream.PersonUpdateEvent
import org.simtp.common.toISOString
import org.simtp.person.api.IPersonDataSource
import org.simtp.person.api.PersonFailureEvent
import org.simtp.person.api.PersonUpdate
import org.simtp.person.api.handleError
import org.simtp.person.api.personStreamHandler
import org.simtp.person.api.toPerson
import org.simtp.person.api.toPersonDetails
import org.simtp.person.api.toPersonUpdateEvent
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.io.IOException
import java.lang.RuntimeException
import java.nio.ByteBuffer
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZonedDateTime
import java.time.ZonedDateTime.now


internal class IPersonCommandApiTest {

    @Test
    fun testPersonStreamHandler() {
        val personCreateEvent = PersonCreateEvent().apply {
            id = "123"
            firstName = "John"
            lastName = "Smith"
            dateOfBirth = "2024-01-01"
            createdTimestamp = now().toISOString()
        }
        val person = personCreateEvent.toPerson()

        val personDataSource = mockk<IPersonDataSource>(relaxed = true)
        @Suppress("ReactiveStreamsUnusedPublisher")
        every { personDataSource.create(any()) } returns Mono.just(person)

        val zonedDateTime = now()

        val mono = personStreamHandler(
            personCreateEvent,
            personDataSource,
            mockk(relaxed = true),
            { zonedDateTime }
        )  { Mono.empty() }

        StepVerifier
            .create(mono)
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun testHandleError() {
        var called2 = false
        val callable1 = Mono.error<Unit>(RuntimeException("error"))
        val callable2 = Mono.fromCallable { called2 = true }
        val mono = Mono
            .error<String>(RuntimeException("TEST"))
            .handleError<String, RuntimeException>({callable1}, {callable2})

        StepVerifier
            .create(mono)
            .expectError(RuntimeException::class.java)
            .verify(Duration.ofSeconds(5))

        assertTrue(called2)
    }
}

class PersonCreateEventTest {

    @Test
    fun testPersonCreateEventAllArgsConstructor() {
        val event = PersonCreateEvent("id", "messageId", "firstName", "lastName", "dateOfBirth", "createdBy", "createdTimestamp", "reference")

        assertEquals("id", event.id)
        assertEquals("messageId", event.messageId)
        assertEquals("firstName", event.firstName)
        assertEquals("lastName", event.lastName)
        assertEquals("dateOfBirth", event.dateOfBirth)
        assertEquals("createdBy", event.createdBy)
        assertEquals("createdTimestamp", event.createdTimestamp)
        assertEquals("reference", event.reference)
    }

    @Test
    fun testPersonCreateEventDefaultConstructor() {
        val event = PersonCreateEvent()

        assertNull(event.id)
        assertNull(event.messageId)
        assertNull(event.firstName)
        assertNull(event.lastName)
        assertNull(event.dateOfBirth)
        assertNull(event.createdBy)
        assertNull(event.createdTimestamp)
        assertNull(event.reference)
    }

    @Test
    @Throws(IOException::class)
    fun testToByteBuffer() {
        val event = PersonCreateEvent("id", "messageId", "firstName", "lastName", "dateOfBirth", "createdBy", "createdTimestamp", "reference")
        val buffer: ByteBuffer = event.toByteBuffer()

        assertNotNull(buffer)

        val decodedEvent = PersonCreateEvent.fromByteBuffer(buffer)
        assertEquals(event, decodedEvent)
    }
}

class PersonUpdateEventTest {

    @Test
    fun testPersonUpdateEventAllArgsConstructor() {
        val event = PersonUpdateEvent("id", "messageId", "firstName", "lastName", "dateOfBirth", 1L, "modifiedBy", "modifiedTimestamp", "reference")

        assertEquals("id", event.id)
        assertEquals("messageId", event.messageId)
        assertEquals("firstName", event.firstName)
        assertEquals("lastName", event.lastName)
        assertEquals("dateOfBirth", event.dateOfBirth)
        assertEquals(1L, event.version)
        assertEquals("modifiedBy", event.modifiedBy)
        assertEquals("modifiedTimestamp", event.modifiedTimestamp)
        assertEquals("reference", event.reference)
    }

    @Test
    fun testPersonUpdateEventDefaultConstructor() {
        val event = PersonUpdateEvent()

        assertNull(event.id)
        assertNull(event.messageId)
        assertNull(event.firstName)
        assertNull(event.lastName)
        assertNull(event.dateOfBirth)
        assertEquals(0L, event.version)
        assertNull(event.modifiedBy)
        assertNull(event.modifiedTimestamp)
        assertNull(event.reference)
    }

    @Test
    @Throws(IOException::class)
    fun testToByteBuffer() {
        val event = PersonUpdateEvent("id", "messageId", "firstName", "lastName", "dateOfBirth", 1L, "modifiedBy", "modifiedTimestamp", "reference")
        val buffer: ByteBuffer = event.toByteBuffer()

        assertNotNull(buffer)

        val decodedEvent = PersonUpdateEvent.fromByteBuffer(buffer)
        assertEquals(event, decodedEvent)
    }
}

class PersonDeleteEventTest {

    @Test
    fun testPersonDeleteEventAllArgsConstructor() {
        val event = PersonDeleteEvent("id", "messageId", 1L, "deletedBy", "deletedTimestamp", "reference")

        assertEquals("id", event.id)
        assertEquals("messageId", event.messageId)
        assertEquals(1L, event.version)
        assertEquals("deletedBy", event.deletedBy)
        assertEquals("deletedTimestamp", event.deletedTimestamp)
        assertEquals("reference", event.reference)
    }

    @Test
    fun testPersonDeleteEventDefaultConstructor() {
        val event = PersonDeleteEvent()

        assertNull(event.id)
        assertNull(event.messageId)
        assertEquals(0L, event.version)
        assertNull(event.deletedBy)
        assertNull(event.deletedTimestamp)
        assertNull(event.reference)
    }

    @Test
    @Throws(IOException::class)
    fun testToByteBuffer() {
        val event = PersonDeleteEvent("id", "messageId", 1L, "deletedBy", "deletedTimestamp", "reference")
        val buffer: ByteBuffer = event.toByteBuffer()

        assertNotNull(buffer)

        val decodedEvent = PersonDeleteEvent.fromByteBuffer(buffer)
        assertEquals(event, decodedEvent)
    }
}

class PersonFailureEventTest {

    @Test
    fun testToPersonDetails() {
        // Given a PersonFailureEvent object
        val failureEvent = PersonFailureEvent(
            id = "123",
            version = 1L,
            lastName = "Doe",
            firstName = "John",
            dateOfBirth = LocalDate.of(1990, 1, 1),
            reference = "ref-001"
        )

        // When toPersonDetails is called
        val personDetails = failureEvent.toPersonDetails()

        // Then the fields should match
        assertEquals("123", personDetails.id)
        assertEquals(1L, personDetails.version)
        assertEquals("Doe", personDetails.lastName)
        assertEquals("John", personDetails.firstName)
        assertEquals("1990-01-01", personDetails.dateOfBirth)
        assertEquals("ref-001", personDetails.reference)
    }

    @Test
    fun testToPersonDetailsWithNullValues() {
        // Given a PersonFailureEvent object with null fields
        val failureEvent = PersonFailureEvent(
            id = "123",
            version = null,
            dateOfBirth = null,
        )

        // When toPersonDetails is called
        val personDetails = failureEvent.toPersonDetails()

        assertAll(
            { assertEquals("123", personDetails.id) },
            { assertEquals(0, personDetails.version) },  // Defaulting to 0
            { assertEquals("", personDetails.lastName) },
            { assertEquals("", personDetails.firstName) },
            { assertNull(personDetails.dateOfBirth) },
            { assertNotNull(personDetails.reference) }
        )
        // Then the fields should be handled correctly for nulls
    }
}

class PersonUpdateTest {

    @Test
    fun testToPersonUpdateEvent() {
        // Given a PersonUpdate object
        val update = PersonUpdate(
            id = "123",
            firstName = "John",
            lastName = "Doe",
            dateOfBirth = LocalDate.of(1990, 1, 1),
            version = 1L,
            modifiedBy = "modifier",
            modifiedTimestamp = ZonedDateTime.parse("2023-10-10T10:00:00Z"),
            reference = "ref-001",
            messageId = "msg-001",
        )

        // When toPersonUpdateEvent is called
        val updateEvent = update.toPersonUpdateEvent()

        // Then the fields should match
        assertEquals("123", updateEvent.id)
        assertEquals("John", updateEvent.firstName)
        assertEquals("Doe", updateEvent.lastName)
        assertEquals("1990-01-01", updateEvent.dateOfBirth)
        assertEquals("2023-10-10T10:00:00Z", updateEvent.modifiedTimestamp)
        assertEquals("modifier", updateEvent.modifiedBy)
        assertEquals(1L, updateEvent.version)
        assertEquals("ref-001", updateEvent.reference)
        assertEquals("msg-001", updateEvent.messageId)
    }

    @Test
    fun testToPersonUpdateEventWithNullValues() {
        // Given a PersonUpdate object with nullable fields set to null
        val update = PersonUpdate(
            id = "123",
            dateOfBirth = null,
            modifiedBy = null,
            messageId = "msg-002",
        )

        // When toPersonUpdateEvent is called
        val updateEvent = update.toPersonUpdateEvent()

        assertAll(
            { assertEquals("123", updateEvent.id) },
            { assertEquals("", updateEvent.firstName) },
            { assertEquals("", updateEvent.lastName) },
            { assertNull(updateEvent.dateOfBirth) },
            { assertNotNull(updateEvent.modifiedTimestamp) },
            { assertNull(updateEvent.modifiedBy) },
            { assertEquals(0, updateEvent.version) },
            { assertNotNull(updateEvent.reference) },
            { assertEquals("msg-002", updateEvent.messageId) },
        )
        // Then the fields should be handled correctly for nulls
    }
}

// Extension function for LocalDate to convert it to an ISO String
fun LocalDate.toISOString(): String {
    return this.toString()
}

