package org.simtp.person.app

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.person.api.*
import org.simtp.person.web.client.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.test.annotation.DirtiesContext
import org.springframework.web.server.ResponseStatusException
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import reactor.test.StepVerifier
import reactor.util.retry.Retry
import java.time.Duration
import java.time.ZonedDateTime

@Suppress("SpringBootApplicationProperties")
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = [
        "schema.registry.url=mock://localhost:8081"
    ]
)
@Testcontainers
@EnablePersonCommandWebClient
@EnablePersonQueryWebClient
@DirtiesContext
@EmbeddedKafka(partitions = 1, brokerProperties = [ "listeners=PLAINTEXT://localhost:9092" ])
@Tag("integration")
class PersonTestAppTest {

    @LocalServerPort
    private var localServerPort: Int? = null

    companion object {

        @Container
        @ServiceConnection
        @JvmStatic
        private val postgresContainer = PostgreSQLContainer("postgres:latest")


        @Container
        @ServiceConnection
        @JvmStatic
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)

        @BeforeAll
        @JvmStatic
        fun setUp() {
            postgresContainer
                .withReuse(true)
                .start()
            redisContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            postgresContainer.stop()
            redisContainer.stop()
        }

    }

    @Autowired
    private lateinit var personCommandWebClientSettings: PersonCommandWebClientSettings

    @Autowired
    private lateinit var personQueryWebClientSettings: PersonQueryWebClientSettings

    @BeforeEach
    fun beforeEach() {
        personCommandWebClientSettings.baseUrl = "http://localhost:$localServerPort/api/person/1.0"

        personQueryWebClientSettings.baseUrl = "http://localhost:$localServerPort/api/person/1.0"
    }


    @Test
    fun contextLoads() {}

    @Autowired
    private lateinit var personCommandWebClient: PersonCommandWebClient

    @Autowired
    private lateinit var personQueryWebClient: PersonQueryWebClient

    @Test
    fun `Create Person via API`() {
        val personCreate = PersonCreate(
            firstName = "Joe",
            lastName = "Bloggs",
        )
        StepVerifier
            .create(personCommandWebClient.create(personCreate))
            .expectNext(personCreate.id)
            .verifyComplete()

        StepVerifier
            .create(personQueryWebClient[personCreate.id])
            .expectNext(personCreate.toPerson().cloneAndIncrementVersion())
            .expectComplete()
            .verify(Duration.ofSeconds(10))

    }

    @Test
    fun `Update Person via API`() {
        val personCreate = PersonCreate(
            firstName = "Joe",
            lastName = "Bloggs"
        )
        StepVerifier
            .create(personCommandWebClient.create(personCreate))
            .expectNext(personCreate.id)
            .verifyComplete()

        var person: Person? = null

        StepVerifier
            .create(personQueryWebClient[personCreate.id])
            .consumeNextWith {
                person = it
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))

        assertNotNull(person)

        val personUpdate = PersonUpdate(
            id = person!!.id,
            firstName = "John",
            lastName = "Smith",
            dateOfBirth = person.dateOfBirth,
            modifiedTimestamp = ZonedDateTime.now(),
            modifiedBy = "test",
            version = person.version
        )

        StepVerifier
            .create(personCommandWebClient.update(personUpdate))
            .expectNext(personUpdate.id)
            .verifyComplete()

        val flux = personQueryWebClient[personUpdate.id]
            .doOnNext {
                assertEquals("Smith", it.lastName)
            }
            .retryWhen(
                Retry.backoff(3, Duration.ofSeconds(2))
                .filter { t -> t is AssertionError }
            )


        StepVerifier
            .create(flux)
            .expectNextMatches {
                it.lastName == "Smith" && it.firstName == "John" && it.version > 0
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))
    }

    @Test
    fun `Delete Person via API`() {
        val personCreate = PersonCreate(
            firstName = "Joe",
            lastName = "Bloggs"
        )
        StepVerifier
            .create(personCommandWebClient.create(personCreate))
            .expectNext(personCreate.id)
            .verifyComplete()

        var person: Person? = null

        StepVerifier
            .create(personQueryWebClient[personCreate.id])
            .consumeNextWith {
                person = it
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))

        assertNotNull(person)

        val personDelete = PersonDelete(
            id = person!!.id,
            deletedTimestamp = ZonedDateTime.now(),
            deletedBy = "test",
            version = person.version
        )

        StepVerifier
            .create(personCommandWebClient.delete(personDelete))
            .expectNext(personDelete.id)
            .verifyComplete()

        val flux = personQueryWebClient[personDelete.id]
            .doOnNext {
                //When the value is delete from the datastore, this method shouldn't be
                //executed. Until then throw an AssertionError that will cause a retry until
                //the entry is actually deleted from the backing data store
                assertNull(it)
            }
            .retryWhen(
                Retry.backoff(100, Duration.ofMillis(200))
                    .filter { t -> t is AssertionError }
            )


        StepVerifier
            .create(flux)
            .expectError(ResponseStatusException::class.java)
            .verify(Duration.ofSeconds(10))
    }

}

@EnablePersonQueryWithJpa
@EnablePersonQueryWithRedis
@EnablePersonQueryWithMemory
@EnablePersonCommand
@SpringBootApplication
open class PersonTestApp