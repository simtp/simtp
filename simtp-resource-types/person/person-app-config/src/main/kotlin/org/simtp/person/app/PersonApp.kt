package org.simtp.person.app

import org.simtp.common.EnableCommonConfiguration
import org.simtp.person.datasource.memory.EnablePersonMemoryDataSource
import org.simtp.person.datasource.redis.EnablePersonRedisDataSource
import org.simtp.person.service.EnablePersonCommandService
import org.simtp.person.service.EnablePersonQueryService
import org.simtp.person.web.server.EnablePersonCommandWebServer
import org.simtp.person.web.server.EnablePersonQueryWebServer
import org.simtp.stream.kafka.EnableSimtpKafka


/*
    NOTE currently it is not possible to use Spring Eureka with a dynamic port due to the ordering
    in which the application starts:-

    https://github.com/spring-cloud/spring-cloud-consul/issues/555
 */

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnableCommonConfiguration
@EnablePersonQueryWebServer
@EnablePersonQueryService
annotation class EnablePersonQueryWithJpa

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnablePersonRedisDataSource
@EnableCommonConfiguration
@EnablePersonQueryWebServer
@EnablePersonQueryService
annotation class EnablePersonQueryWithRedis

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnablePersonMemoryDataSource
@EnableCommonConfiguration
@EnablePersonQueryWebServer
@EnablePersonQueryService
annotation class EnablePersonQueryWithMemory

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnableCommonConfiguration
@EnablePersonCommandWebServer
@EnablePersonCommandService
annotation class EnablePersonCommand
