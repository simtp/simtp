/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.client

import org.simtp.person.api.IPersonCommandApi
import org.simtp.person.api.PersonCreate
import org.simtp.person.api.PersonDelete
import org.simtp.person.api.PersonUpdate
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.*
import reactor.core.publisher.Mono
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.web.reactive.function.client.WebClient

@Suppress("unused")
@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(PersonCommandWebClientConfiguration::class)
annotation class EnablePersonCommandWebClient

@Configuration
open class PersonCommandWebClientConfiguration {


    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    open fun personCommandWebClient(
        webClientBuilder: WebClient.Builder,
        personCommandWebClientSettings: PersonCommandWebClientSettings
    ): PersonCommandWebClient {
        return PersonCommandWebClient(
            webClientBuilder = webClientBuilder,
            baseUrlProvider = { personCommandWebClientSettings.baseUrl }
        )
    }

    @Bean
    @ConfigurationProperties(prefix = "simtp.person.command.web.client")
    open fun personCommandWebClientSettings(): PersonCommandWebClientSettings {
        return PersonCommandWebClientSettings()
    }
}

data class PersonCommandWebClientSettings(
    var baseUrl: String = "http://localhost:8080/api/person/1.0"
)

open class PersonCommandWebClient(
    webClientBuilder: WebClient.Builder,
    private val baseUrlProvider: () -> String
): IPersonCommandApi {

    private val webClient = webClientBuilder.build()

    override fun create(personCreate: PersonCreate): Mono<String> {
        return webClient
            .put()
            .uri(baseUrlProvider())
            .bodyValue(personCreate)
            .retrieve()
            .bodyToMono(String::class.java)
    }

    override fun update(personUpdate: PersonUpdate): Mono<String> {
        return webClient
            .post()
            .uri(baseUrlProvider())
            .bodyValue(personUpdate)
            .retrieve()
            .bodyToMono(String::class.java)
    }

    override fun delete(personDelete: PersonDelete): Mono<String> {
        return webClient
            .method(HttpMethod.DELETE)
            .uri(baseUrlProvider())
            .bodyValue(personDelete)
            .retrieve()
            .bodyToMono(String::class.java)
    }
}

