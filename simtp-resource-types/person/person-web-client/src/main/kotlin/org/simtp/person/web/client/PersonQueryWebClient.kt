/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.client

import org.simtp.person.api.IPersonQueryApi
import org.simtp.person.api.Person
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.*
import reactor.core.publisher.Mono
import org.springframework.context.annotation.Bean
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.http.HttpStatusCode
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.server.ResponseStatusException
import reactor.util.retry.Retry
import java.time.Duration

@Suppress("unused")
@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(PersonQueryWebClientConfiguration::class)
annotation class EnablePersonQueryWebClient

@Configuration
open class PersonQueryWebClientConfiguration {

    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    open fun personQueryWebClient(
        webClientBuilder: WebClient.Builder,
        personQueryWebClientSettings: PersonQueryWebClientSettings
    ): PersonQueryWebClient {
        return PersonQueryWebClient(
            webClientBuilder = webClientBuilder,
            baseUrlProvider = { personQueryWebClientSettings.baseUrl }
        )
    }

    @Bean
    @ConfigurationProperties(prefix = "simtp.person.query.web.client")
    open fun personQueryWebClientSettings(): PersonQueryWebClientSettings {
        return PersonQueryWebClientSettings()
    }
}

data class PersonQueryWebClientSettings(
    var baseUrl: String = "http://localhost:8080/api/person/1.0"
)

open class PersonQueryWebClient(
    webClientBuilder: WebClient.Builder,
    private val baseUrlProvider: () -> String
): IPersonQueryApi {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(PersonQueryWebClient::class.java)
    }

    private val webClient = webClientBuilder.build()

    override fun get(id: String): Mono<Person> {
        return webClient
            .get()
            .uri("${baseUrlProvider()}/{id}", mapOf("id" to id))
            .retrieve()
            .onStatus(HttpStatusCode::is4xxClientError) { response ->
                log.info("Received 4xx client error, converting to ResponseStatusException")
                Mono.error(ResponseStatusException(response.statusCode()))
            }
            .bodyToMono(Person::class.java)
            .doOnNext {
                log.info("Received $it")
            }
            .retryWhen(
                Retry.backoff(5, Duration.ofMillis(200))
                    .filter {
                        throwable -> throwable is ResponseStatusException && throwable.statusCode.is4xxClientError
                    }
                    .onRetryExhaustedThrow { _, signal ->
                        signal.failure()
                    }
            )
            .doOnError { log.error("Exception occurred", it) }
    }

}

