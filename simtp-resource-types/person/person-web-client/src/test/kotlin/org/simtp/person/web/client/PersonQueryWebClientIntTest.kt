/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.web.client

import org.junit.jupiter.api.Test
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import reactor.test.StepVerifier
import java.time.Duration
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.simtp.person.api.Person
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException


@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@Tag("integration")
open class PersonQueryWebClientIntTest {

    @Autowired
    private lateinit var personInMemoryCommandService: PersonInMemoryCommandService

    @LocalServerPort
    private val localServerPort: Int? = null

    private var personQueryWebClient: PersonQueryWebClient? = null

    @BeforeEach
    fun setUp() {
        personQueryWebClient= PersonQueryWebClient(
            webClientBuilder = defaultWebClientBuilder
        ) {
            "http://localhost:${localServerPort}/api/person/1.0"
        }
    }


    @Test
    fun get() {
        val person = Person()
        personInMemoryCommandService.map[person.id] = person
        val retrieveMono = personQueryWebClient!![person.id]
        StepVerifier
            .create(retrieveMono)
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))


    }

    @Test
    fun retry() {
        val retrieveMono = personQueryWebClient!!["123"].map { ResponseEntity.ofNullable(it) }
        StepVerifier
            .create(retrieveMono)
            .expectErrorMatches { it is ResponseStatusException && it.statusCode.is4xxClientError }
            .verify(Duration.ofSeconds(10))


    }

}
