/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.service

import io.mockk.*

import org.junit.jupiter.api.Test
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.api.person.api.stream.PersonUpdateEvent
import org.simtp.person.api.*
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Mono
import reactor.kafka.sender.SenderResult
import reactor.test.StepVerifier
import java.time.Duration

class PersonCommandServiceTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun create() {
        val personCreate = PersonCreate()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(PERSON_STREAM_NAME, any<String>(), any<PersonCreateEvent>()) } returns Mono.just(senderResult)
        val personService = PersonCommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(personService.create(personCreate))
            .expectNext(personCreate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(PERSON_STREAM_NAME, any<String>(), any<PersonCreateEvent>()) }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun update() {
        val personUpdate = PersonUpdate()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(PERSON_STREAM_NAME, any<String>() ,any<PersonUpdateEvent>()) } returns Mono.just(senderResult)
        val personService = PersonCommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(personService.update(personUpdate))
            .expectNext(personUpdate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(PERSON_STREAM_NAME, any<String>(), any<PersonUpdateEvent>()) }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun delete() {
        val personDelete = PersonDelete()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(PERSON_STREAM_NAME, any<String>(), any<PersonDeleteEvent>()) } returns Mono.just(senderResult)
        val personService = PersonCommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(personService.delete(personDelete))
            .expectNext(personDelete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(PERSON_STREAM_NAME, any<String>(), any<PersonDeleteEvent>()) }
    }
}