/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.service

import org.simtp.person.datasource.memory.PersonMemoryDataSource
import org.junit.jupiter.api.Test
import org.simtp.person.api.Person
import reactor.test.StepVerifier
import java.time.Duration

class PersonQueryServiceTest {

    @Test
    fun get() {
        val person = Person()
        val personDataSource = PersonMemoryDataSource()
        personDataSource.map[person.id] = person
        val personQueryService = PersonQueryService(
            personDataSource = personDataSource
        )
        StepVerifier
            .create(personQueryService[person.id])
            .expectNext(person)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }


}