/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.service

import org.simtp.person.api.IPersonCommandApi
import org.simtp.person.api.IPersonDataSource
import org.simtp.person.api.IPersonQueryApi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate


@Configuration
open class PersonCommandServiceConfiguration {

    @Bean
    open fun personCommandService(
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>
    ): IPersonCommandApi {
        return PersonCommandService(
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate
        )
    }

}

@Configuration
open class PersonQueryServiceConfiguration {

    @Bean
    open fun personQueryService(
        personRedisDataSource: IPersonDataSource,
    ): IPersonQueryApi {
        return PersonQueryService(
            personDataSource = personRedisDataSource
        )
    }

}


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(PersonCommandServiceConfiguration::class)
annotation class EnablePersonCommandService

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(PersonQueryServiceConfiguration::class)
annotation class EnablePersonQueryService