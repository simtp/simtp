/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.person.service

import org.simtp.person.api.*
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Mono


class PersonCommandService(
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>
): IPersonCommandApi {


    override fun create(personCreate: PersonCreate) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(PERSON_STREAM_NAME, personCreate.id, personCreate.toPersonCreateEvent())
            .map { personCreate.id }
    }

    override fun update(personUpdate: PersonUpdate) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(PERSON_STREAM_NAME, personUpdate.id, personUpdate.toPersonUpdateEvent())
            .map { personUpdate.id }
    }

    override fun delete(personDelete: PersonDelete) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(PERSON_STREAM_NAME, personDelete.id, personDelete.toPersonDeleteEvent())
            .map { personDelete.id }
    }
}
