/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("DuplicatedCode")

package org.simtp.person.datasource.test

import org.simtp.person.api.IPersonDataSource
import java.time.Duration
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.simtp.api.person.api.stream.PersonCreateEvent
import org.simtp.api.person.api.stream.PersonDeleteEvent
import org.simtp.api.person.api.stream.PersonUpdateEvent
import org.simtp.common.toISOString
import org.simtp.kotlin.util.uuid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.OptimisticLockingFailureException
import java.time.ZonedDateTime

abstract class PersonDataSourceTestAdapter {

    @Autowired
    protected lateinit var personDataSource: IPersonDataSource

    @Test
    open fun contextLoads() {
        assertNotNull(personDataSource)
    }


    @Test
    open fun optimisticUpdatePasses() {
        val personCreateEvent = PersonCreateEvent().apply {
            this.id = uuid()
            this.messageId = uuid()
            this.firstName = ""
            this.lastName = ""
            this.reference = ""
        }
        val result1 = personDataSource
            .create(personCreateEvent)
            .block(Duration.ofSeconds(5))

        val newValue = PersonUpdateEvent().apply {
            id = result1!!.id
            this.messageId = uuid()
            this.modifiedTimestamp = ZonedDateTime.now().toISOString()
            version = result1.version
        }

        val result2 = personDataSource
            .update(newValue)
            .block(Duration.ofSeconds(5))

        assertEquals(result1!!.version+1, result2!!.version)
    }

    @Test
    open fun optimisticDeleteFails() {
        val personCreateEvent = PersonCreateEvent().apply {
            this.id = uuid()
            this.messageId = uuid()
            this.firstName = ""
            this.lastName = ""
            this.reference = ""
        }
        val result1 = personDataSource
            .create(personCreateEvent)
            .block(Duration.ofSeconds(5))

        val personUpdateEvent = PersonUpdateEvent().apply {
            id = result1!!.id
            messageId = uuid()
            modifiedTimestamp = ZonedDateTime.now().toISOString()
            version = result1.version
        }


        val result2 = personDataSource
            .update(personUpdateEvent)
            .block(Duration.ofSeconds(5))

        assertEquals(result1!!.version+1, result2!!.version)

        val otherPersonDeleteEvent = PersonDeleteEvent().apply {
            id = result1.id
            messageId = uuid()
            deletedTimestamp = ZonedDateTime.now().toISOString()
            version = result1.version
        }

        assertThrows(OptimisticLockingFailureException::class.java) {
            personDataSource
                .delete(otherPersonDeleteEvent)
                .block(Duration.ofSeconds(50))
        }

    }


}