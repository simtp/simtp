/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.web.client

import org.simtp.user.api.*
import org.junit.jupiter.api.Test
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import reactor.test.StepVerifier
import java.time.Duration
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag


@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@Tag("integration")
open class UserQueryWebClientIntTest {

    @Autowired
    private lateinit var userInMemoryService: UserInMemoryService

    @LocalServerPort
    private val localServerPort: Int? = null

    private var userQueryWebClient: UserQueryWebClient? = null

    @BeforeEach
    fun setUp() {
        userQueryWebClient= UserQueryWebClient(
            webClientBuilder = defaultWebClientBuilder
        ) {
            "http://localhost:${localServerPort}"
        }
    }


    @Test
    fun get() {
        val user = User()
        userInMemoryService.map[user.id] = user
        val retrieveMono = userQueryWebClient!![user.id]
        StepVerifier
            .create(retrieveMono)
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))


    }

}
