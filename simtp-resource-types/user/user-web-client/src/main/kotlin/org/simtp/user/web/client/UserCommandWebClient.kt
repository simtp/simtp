/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.web.client

import org.simtp.user.api.*
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.*
import reactor.core.publisher.Mono
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.web.reactive.function.client.WebClient

@Suppress("unused")
@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(UserCommandWebClientConfiguration::class)
annotation class EnableUserCommandWebClient

@Configuration
open class UserCommandWebClientConfiguration {


    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    open fun userCommandWebClient(
        webClientBuilder: WebClient.Builder,
        userCommandWebClientSettings: UserCommandWebClientSettings
    ): UserCommandWebClient {
        return UserCommandWebClient(
            webClientBuilder = webClientBuilder,
            baseUrlProvider = {
                userCommandWebClientSettings.baseUrl
            }
        )
    }

    @Bean
    @ConfigurationProperties(prefix = "simtp.user.command.web.client")
    open fun userCommandWebClientSettings(): UserCommandWebClientSettings {
        return UserCommandWebClientSettings()
    }    
}

data class UserCommandWebClientSettings(
    var baseUrl: String = "http://localhost:8080"
)

open class UserCommandWebClient(
    webClientBuilder: WebClient.Builder,
    private val baseUrlProvider: () -> String
): IUserCommandApi {

    private val webClient = webClientBuilder.build()

    override fun create(userCreate: UserCreate): Mono<String> {
        return webClient
            .put()
            .uri("${baseUrlProvider()}/api/user/1.0")
            .bodyValue(userCreate)
            .retrieve()
            .bodyToMono(String::class.java)
    }

    override fun update(userUpdate: UserUpdate): Mono<String> {
        return webClient
            .post()
            .uri("${baseUrlProvider()}/api/user/1.0")
            .bodyValue(userUpdate)
            .retrieve()
            .bodyToMono(String::class.java)
    }

    override fun delete(userDelete: UserDelete): Mono<String> {
        return webClient
            .method(HttpMethod.DELETE)
            .uri("${baseUrlProvider()}/api/user/1.0")
            .bodyValue(userDelete)
            .retrieve()
            .bodyToMono(String::class.java)
    }
}

