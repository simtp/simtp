/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.web.server

import org.simtp.user.api.*
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.springframework.http.ResponseEntity
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration

class UserQueryWebRestControllerTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun get() {
        val service = mockk<IUserQueryApi>()
        val user = User()
        every { service[user.id] } returns Mono.just(user)

        val controller = UserQueryWebRestController(service)

        StepVerifier
            .create(controller[user.id])
            .expectNext(ResponseEntity.ofNullable(user))
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service[user.id] }

    }

}