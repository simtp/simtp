/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.web.server

import org.simtp.user.api.*
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration

class UserCommandWebRestControllerTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun create() {
        val service = mockk<IUserCommandApi>()
        val userCreate = UserCreate()
        every { service.create(userCreate) } returns Mono.just(userCreate.id)

        val controller = UserCommandWebRestController(service)

        StepVerifier
            .create(controller.create(userCreate))
            .expectNext(userCreate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.create(userCreate) }

    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun update() {
        val service = mockk<IUserCommandApi>()
        val userUpdate = UserUpdate()
        every { service.update(userUpdate) } returns Mono.just(userUpdate.id)

        val controller = UserCommandWebRestController(service)

        StepVerifier
            .create(controller.update(userUpdate))
            .expectNext(userUpdate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.update(userUpdate) }

    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun delete() {
        val service = mockk<IUserCommandApi>()
        val userDelete = UserDelete()
        every { service.delete(userDelete) } returns Mono.just(userDelete.id)

        val controller = UserCommandWebRestController(service)

        StepVerifier
            .create(controller.delete(userDelete))
            .expectNext(userDelete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service.delete(userDelete) }

    }
}
