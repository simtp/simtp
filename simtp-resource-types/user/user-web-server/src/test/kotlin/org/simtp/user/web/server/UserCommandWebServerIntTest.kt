/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.web.server

import org.simtp.user.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpMethod
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.test.StepVerifier
import java.time.Duration

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("integration")
class UserCommandWebRestControllerIntTest {

    @LocalServerPort
    private var localServerPort: Int? = null

    private var webClient = defaultWebClientBuilder.baseUrl("http://localhost:8080/api/user/1.0").build()

    @Autowired
    private lateinit var userInMemoryService: UserInMemoryService

    @BeforeEach
    fun setUp() {
        webClient = defaultWebClientBuilder
            .baseUrl("http://localhost:$localServerPort/api/user/1.0")
            .build()
    }

    @Test
    fun create() {
        val userCreate = UserCreate()

        val retrieveMono = webClient
            .put()
            .bodyValue(userCreate)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(userCreate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(userCreate.toUser(), userInMemoryService.map[userCreate.id]?.withLocalZonedDateTime())

    }

    @Test
    fun update() {
        val userUpdate = UserUpdate()
        userInMemoryService.map[userUpdate.id] = userUpdate.toUser()
        val retrieveMono = webClient
            .post()
            .bodyValue(userUpdate)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(userUpdate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(userUpdate.toUser(), userInMemoryService.map[userUpdate.id]?.withLocalZonedDateTime())

    }

    @Test
    fun delete() {
        val userDelete = UserDelete()
        userInMemoryService.map[userDelete.id] = User(id = userDelete.id)
        val retrieveMono = webClient
            .method(HttpMethod.DELETE)
            .bodyValue(userDelete)
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(userDelete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertNull(userInMemoryService.map[userDelete.id]?.withLocalZonedDateTime())

    }

}

internal fun User.withLocalZonedDateTime(): User {
    return User(
        id = this.id,
        version = this.version,
        name = this.name,
        lastModifiedTimestamp = this.lastModifiedTimestamp.withZoneSameInstant(java.time.ZoneId.systemDefault())
    )
}