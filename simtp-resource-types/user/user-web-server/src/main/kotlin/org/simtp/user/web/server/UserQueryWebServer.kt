
/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.web.server

import org.simtp.user.api.*
import org.simtp.graphql.EnableSimTPGraphQl
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.*
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(UserQueryWebServerConfiguration::class)
annotation class EnableUserQueryWebServer

@Configuration
@ComponentScan
@EnableSimTPGraphQl

open class UserQueryWebServerConfiguration

@RestController
@RequestMapping("/api/user/1.0")
open class UserQueryWebRestController(
    private val userQueryService: IUserQueryApi
) {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(UserQueryWebRestController::class.java)
    }

    @GetMapping("/{id}")
    operator fun get(@PathVariable id: String): Mono<ResponseEntity<User>> {
        return userQueryService[id]
            .doOnNext { log.info("Received value={}", it) }
            .doOnError { log.error("Exception handling query", it) }
            .map { ResponseEntity.ofNullable(it) }
            .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()))
            .doOnNext { log.info("Return user query for id={} value={}", id, it.statusCode) }
    }
}

@Controller
class UserQueryController(
    private val userQueryService: IUserQueryApi
) {

    @QueryMapping
    fun getUser(@Argument id: String): Mono<User> {
        return userQueryService[id]
    }

}

