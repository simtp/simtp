/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.web.server

import org.simtp.user.api.*
import org.simtp.graphql.EnableSimTPGraphQl
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.*
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(UserCommandWebServerConfiguration::class)
annotation class EnableUserCommandWebServer

@Configuration
@ComponentScan
@EnableSimTPGraphQl
open class UserCommandWebServerConfiguration

@RestController
@RequestMapping("/api/user/1.0")
open class UserCommandWebRestController(
    private val userCommandService: IUserCommandApi
) {

    @PutMapping
    fun create(@RequestBody userCreate: UserCreate): Mono<String> {
        return userCommandService.create(userCreate)
    }

    @PostMapping
    fun update(@RequestBody userUpdate: UserUpdate): Mono<String> {
        return userCommandService.update(userUpdate)
    }

    @DeleteMapping
    fun delete(@RequestBody userDelete: UserDelete): Mono<String> {
        return userCommandService.delete(userDelete)
    }
}

@Controller
class UserCommandController(
    private val userCommandService: IUserCommandApi,
) {

    init {
        log.info("Initializing UserCommandController")
    }

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(UserCommandController::class.java)
    }

    @MutationMapping
    fun userCreate(@Argument userCreate: UserCreate): Mono<String> {
        return userCommandService.create(userCreate)
    }

    @MutationMapping
    fun userUpdate(@Argument userUpdate: UserUpdate): Mono<String> {
        return userCommandService.update(userUpdate)
    }

    @MutationMapping
    fun userDelete(@Argument userDelete: UserDelete): Mono<String> {
        return userCommandService.delete(userDelete)
    }
}


