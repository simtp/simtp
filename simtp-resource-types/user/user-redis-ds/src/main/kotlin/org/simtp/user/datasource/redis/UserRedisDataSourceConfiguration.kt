/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package org.simtp.user.datasource.redis

import org.simtp.user.api.*
import org.simtp.user.api.UserFailureEvent
import org.simtp.user.api.stream.*
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.common.IDateTimeManager
import org.simtp.common.toISOString
import org.simtp.common.toZonedDateTime
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.core.ReactiveHashOperations
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.receiver.ReceiverOptions
import kotlin.reflect.full.memberProperties
import com.fasterxml.jackson.databind.ObjectMapper

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(UserRedisDataSourceConfiguration::class)
annotation class EnableUserRedisDataSource

@Configuration
open class UserRedisDataSourceConfiguration {

    @Bean
    open fun userRedisDataSource(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>,
        objectMapper: ObjectMapper
    ): UserRedisDataSource {
        return UserRedisDataSource(reactiveRedisOperations, objectMapper)
    }

    @Bean
    open fun userRedisReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\${spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "userRedisReactiveKafkaConsumer:$applicationName"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "userRedisReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(USER_STREAM_NAME)))
    }

    @Bean
    open fun userRedisStreamConsumer(
        userRedisReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        userRedisDataSource: UserRedisDataSource,
        dateTimeManager: IDateTimeManager
    ): UserRedisStreamConsumer {
        return UserRedisStreamConsumer(
            userRedisReactiveKafkaConsumerTemplate = userRedisReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            userRedisDataSource = userRedisDataSource,
            dateTimeManager = dateTimeManager
        )
    }
}


class UserRedisStreamConsumer(
    private val userRedisReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val userRedisDataSource: UserRedisDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(UserRedisStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<User> {
        return userRedisReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                userStreamHandler(
                    it.value(),
                    userRedisDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, USER_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.toUserFailureStreamEvent(this::class.simpleName!!) }
                }
            }

    }
}

class UserRedisDataSource(
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>,
    private val objectMapper: ObjectMapper,
): IUserDataSource {

    companion object {
        private val log = LoggerFactory.getLogger(UserRedisDataSource::class.java)
    }

    private val hashOps = reactiveRedisOperations
        .opsForHash<String, String>()

    private val setOps = reactiveRedisOperations
        .opsForSet()


    override fun create(userCreateEvent: UserCreateEvent): Mono<User> {
        return setOps
            .isMember("user:messageIds", userCreateEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                save(userCreateEvent.toUser())
            }
            .saveMessageId(userCreateEvent.messageId.toString())
    }

    override fun update(userUpdateEvent: UserUpdateEvent): Mono<User> {
        return setOps
            .isMember("user:messageIds", userUpdateEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                save(userUpdateEvent.toUser())
            }
            .saveMessageId(userUpdateEvent.messageId.toString())

    }

    internal fun save(user: User): Mono<User> {
        val existing = hashOps
            .multiGet("user:${user.id}", listOf("version"))
            .cache()

        val newUser = user.cloneAndIncrementVersion()

        return existing
            .flatMap {
                val result = it.filterNotNull().isNotEmpty()
                //This isn't very elegant
                if (result && it[0].toLong() != user.version) {
                    Mono.error(OptimisticLockingFailureException("Current version=${it[0]} does not match provided version=${user.version}"))
                } else {
                    Mono.just(it)
                }
            }
            .hasElement()
            .flatMap {
                hashOps.saveUser(newUser)
            }
    }

    private fun Mono<User>.saveMessageId(messageId: String): Mono<User> {
        return this
            .flatMap { user ->
                setOps.add("user:messageIds:$messageId", messageId).map { user }
            }
    }



    override fun findById(id: String): Mono<User> {
        return hashOps
            .multiGet("user:${id}", allUserFields)
            .mapNotNull {
                when (it.filterNotNull().isEmpty()) {
                    true -> {
                        null
                    }
                    else -> {
                        it.toUser()
                    }
                }
            }
            .cast(User::class.java)
            .doOnNext { log.info("findById: {} = {}", id, it) }
    }

    override fun findAll(): Flux<User> {
        return hashOps
            .keys("user:*")
            .flatMap { key ->
                reactiveRedisOperations
                    .opsForHash<String, String>()
                    .multiGet(key, allUserFields)
                    .map { it.toUser() }
            }
    }

    override fun delete(userDeleteEvent: UserDeleteEvent): Mono<User> {
        return setOps
            .isMember("user:messageIds", userDeleteEvent.messageId.toString())
            .filter { !it }
            .flatMap {
                delete(userDeleteEvent.id.toString(), userDeleteEvent.version)
            }
            .doOnNext {
                log.info("Successfully deleted item")
            }
            .doOnError {
                log.error("Exception encountered", it)
            }
            .saveMessageId(userDeleteEvent.messageId.toString())

    }

    internal fun delete(id: String, version: Long): Mono<User> {
        val value = findById(id).cache()

        val deleteOp = hashOps
            .delete("user:${id}")

        return value
            .flatMap {
                if (it.version != version) {
                    Mono.error(OptimisticLockingFailureException("Provided user.version=0-SNAPSHOT version does not latest user.version=${it.version} version"))
                } else {
                    Mono.just(it)
                }
            }
            .doOnError {
                log.error("Failed with exception", it)
            }
            .flatMap {
                deleteOp
            }
            .flatMap {
                value
            }


    }

    override fun failure(userFailureEvent: UserFailureEvent): Mono<Unit> {
        return setOps
            .add("user:failures:${userFailureEvent.id}", objectMapper.writeValueAsString(userFailureEvent))
            .doOnError {
                log.error("Failed to store user failure event")
            }
            .doOnNext {
                log.info("Stored user failure event for id={}", userFailureEvent.id)
            }
            .then(Mono.empty())
    }
}

internal val allUserFields = UserMap()::class.memberProperties.map { it.name }

fun ReactiveHashOperations<String, String, String>.saveUser(user: User): Mono<User> {
    return this
        .putAll("user:${user.id}", user.toMap())
        .map { user }
}

open class UserMap(val map: MutableMap<String, String?> = mutableMapOf<String, String?>().withDefault { null }) {
    var id: String by map
    var name: String by map
    var lastModifiedBy: String by map
    var version: String by map
    var lastModifiedTimestamp: String by map
}

fun List<String?>.toUser(): User {
    return this.mapIndexed { index, it -> allUserFields[index] to it }
        .toMap()
        .toMutableMap()
        .let { UserMap(it) }
        .toUser()
}

fun User.toUserMap(): UserMap {
    val user = this
    return UserMap().apply {
        id = user.id
        version = user.version.toString()
        lastModifiedTimestamp = user.lastModifiedTimestamp.toISOString()
        name = user.name
        lastModifiedBy = user.lastModifiedBy ?: "unknown"
    }
}

fun UserMap.toUser(): User {
    return User(
        id = this.id,
        version = this.version.toLong(),
        lastModifiedTimestamp = this.lastModifiedTimestamp.toZonedDateTime(),
        name = this.name,
        lastModifiedBy = this.lastModifiedBy
    )
}

fun UserMap.toValuesList(): List<String?> {
    return allUserFields.map { this.map[it] }
}


fun User.toMap(): Map<String, String> {
    return this
        .toUserMap()
        .map
        .asSequence()
        .filter { it.value != null }
        .associate{ it.key to it.value!! }
}