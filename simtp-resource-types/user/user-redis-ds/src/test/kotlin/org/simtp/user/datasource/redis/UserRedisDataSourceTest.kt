/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.datasource.redis

import org.simtp.user.api.*
import org.simtp.user.api.stream.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.common.toISOString
import io.mockk.*
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import org.junit.jupiter.api.Assertions.*
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.core.ReactiveHashOperations
import org.springframework.data.redis.core.ReactiveRedisOperations
import org.springframework.data.redis.core.ReactiveSetOperations
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import org.simtp.kotlin.util.uuid
import org.simtp.common.serialization.withDefaults

@Suppress("ReactiveStreamsUnusedPublisher")
class UserRedisDataSourceTest {

    @Test
    fun save() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val user = User()
        val userMap = user.cloneAndIncrementVersion().toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("user:${user.id}", userMap) } returns Mono.just(true)
        every { opsForHash.multiGet("user:${user.id}", listOf("version")) } returns Mono.just(listOf(null))

        val userDataSource = UserRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        val mono = userDataSource.save(user).cache()

        StepVerifier
            .create(mono)
            .expectNext(user.cloneAndIncrementVersion())
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }

    @Test
    fun update() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val user = User()
        val newUser = user.cloneAndIncrementVersion()
        val userMap = newUser.toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("user:${user.id}", userMap) } returns Mono.just(true)
        every { opsForHash.multiGet("user:${user.id}", listOf("version")) } returns Mono.just(listOf("0"))

        val userDataSource = UserRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        val mono = userDataSource.save(user).cache()

        StepVerifier
            .create(mono)
            .expectNext(newUser)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }

    @Test
    fun optimisticLockFailure() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val user = User()
        val newUser = user.cloneAndIncrementVersion()
        val userMap = newUser.toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("user:${user.id}", userMap) } returns Mono.just(true)
        every { opsForHash.multiGet("user:${user.id}", listOf("version")) } returns Mono.just(listOf("1"))

        val userDataSource = UserRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        val mono = userDataSource.save(user).cache()

        StepVerifier
            .create(mono)
            .expectError(OptimisticLockingFailureException::class.java)
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun deleteFailure() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val opsForSet = mockk<ReactiveSetOperations<String, String>>(relaxed = true)
        val user = User()
        val newUser = user.cloneAndIncrementVersion()
        val userMap = newUser.toMap()

        every { opsForHash.delete(any()) } returns Mono.just(true)
        every { opsForHash.multiGet(any(), any()) } returns Mono.just(newUser.toUserMap().toValuesList())
        every { reactiveRedisOperations.opsForSet() } returns opsForSet
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForSet.isMember(any(), any()) } returns Mono.just(false)
        every { opsForSet.add(any(), any()) } returns Mono.just(1)

        every { opsForHash.putAll("user:${user.id}", userMap) } returns Mono.just(true)
        every { opsForHash.multiGet("user:${user.id}", listOf("version")) } returns Mono.just(listOf("1"))

        val userDeleteEvent = UserDeleteEvent().apply {
            this.id = user.id
            this.messageId = uuid()
            this.version = user.version
        }

        val userDataSource = UserRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        val mono = userDataSource.delete(userDeleteEvent).cache()

        StepVerifier
            .create(mono)
            .expectError(OptimisticLockingFailureException::class.java)
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val user = User()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.multiGet("user:${user.id}", allUserFields) } returns Mono.just(user.toUserMap().toValuesList())
        val userDataSource = UserRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        StepVerifier
            .create(userDataSource.findById(user.id))
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val user = User()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.keys("user:*") } returns Flux.just("user:${user.id}")
        every { opsForHash.multiGet("user:${user.id}", allUserFields) } returns Mono.just(user.toUserMap().toValuesList())
        val userDataSource = UserRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        StepVerifier
            .create(userDataSource.findAll())
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }


    @Test
    fun delete() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val user = User()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.delete("user:${user.id}") } returns Mono.just(true)
        every { opsForHash.multiGet("user:${user.id}", allUserFields) } returns Mono.just(user.toUserMap().toValuesList())

        val userDataSource = UserRedisDataSource(reactiveRedisOperations, ObjectMapper().withDefaults())

        StepVerifier
            .create(userDataSource.delete(user.id, user.version))
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun testToMap() {
        val user = User()
        val map = user.toMap()
        val expected = mapOf(
            "id" to user.id,
            "version" to user.version.toString(),
            "lastModifiedTimestamp" to user.lastModifiedTimestamp.toISOString(),
            "name" to "",
            "lastModifiedBy" to "unknown"
        )
        assertEquals(expected, map)
    }

}