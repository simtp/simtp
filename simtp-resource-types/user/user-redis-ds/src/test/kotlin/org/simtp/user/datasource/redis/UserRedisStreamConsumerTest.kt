package org.simtp.user.datasource.redis

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.junit.jupiter.api.Test
import org.simtp.user.api.*
import org.simtp.user.api.stream.*
import org.simtp.user.api.stream.UserFailureEvent

import org.simtp.common.IDateTimeManager
import org.simtp.common.toISOString
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration
import java.time.ZonedDateTime

class UserRedisStreamConsumerTest {
    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun consumerHandlesError() {
        val template = mockk<ReactiveKafkaConsumerTemplate<String, *>>()
        val consumerRecord = mockk<ConsumerRecord<String, Any>>()
        val zonedDateTime = ZonedDateTime.now()
        every { template.receiveAutoAck() } returns Flux.fromIterable(listOf(consumerRecord))

        every { consumerRecord.value() } returns UserCreateEvent().apply {
            this.id = "123"
            this.name = "Joe"
            this.createdBy = "test"
            this.createdTimestamp = zonedDateTime.toISOString()
            this.reference = "ref"
        }

        val dataSource = mockk<UserRedisDataSource>()

        every { dataSource.create(any()) } returns Mono.error(IllegalStateException("ERROR"))
        every { dataSource.failure(any()) } returns Mono.empty()

        val dateTimeManager = mockk<IDateTimeManager>()
        val reactiveKafkaProducerTemplate = mockk<ReactiveKafkaProducerTemplate<String, Any>>()

        every { reactiveKafkaProducerTemplate.send(any<String>(), any<UserFailureEvent>()) } returns Mono.just(mockk(relaxed = true))

        every { dateTimeManager.currentZonedDateTime } returns zonedDateTime
        val consumer = UserRedisStreamConsumer(
            userRedisReactiveKafkaConsumerTemplate = template,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            userRedisDataSource = dataSource,
            dateTimeManager = dateTimeManager
        )

        StepVerifier
            .create(consumer.createConsumer())
            .expectNextCount(0)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) {reactiveKafkaProducerTemplate.send("$USER_STREAM_NAME.DLQ", any<Any>())  }
    }

}