/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.api

import org.simtp.user.api.stream.*
import org.simtp.user.api.stream.UserFailureEvent as UserFailureStreamEvent
import org.simtp.common.toISOString
import org.simtp.common.toZonedDateTime
import org.slf4j.Logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import org.simtp.kotlin.util.uuid
import java.time.ZonedDateTime

const val USER_STREAM_NAME = "USER"

interface IUserCommandApi {
    fun create(userCreate: UserCreate): Mono<String>

    fun update(userUpdate: UserUpdate): Mono<String>

    fun delete(userDelete: UserDelete) : Mono<String>
}

interface IUserQueryApi {
    operator fun get(id: String): Mono<User>
}

data class User(
    val id: String = uuid(),
    val name: String = "",
    val lastModifiedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val lastModifiedBy: String? = "unknown",
    val version: Long = 0,
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (id != other.id) return false
        if (name != other.name) return false
        if (version != other.version) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + version.hashCode()
        return result
    }
}


data class UserCreate(
    val id: String = uuid(),
    val name: String = "",
    val createdTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val createdBy: String? = "unknown",
    val reference: String = uuid()
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserCreate

        if (id != other.id) return false
        if (name != other.name) return false
        if (createdBy != other.createdBy) return false
        if (reference != other.reference) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + (createdBy?.hashCode() ?: 0)
        result = 31 * result + reference.hashCode()
        return result
    }
}


data class UserUpdate(
    val id: String = uuid(),
    val name: String = "",
    val modifiedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val modifiedBy: String? = "unknown",
    val version: Long = 0,
    val reference: String = uuid()
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserUpdate

        if (id != other.id) return false
        if (name != other.name) return false
        if (modifiedBy != other.modifiedBy) return false
        if (version != other.version) return false
        if (reference != other.reference) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + (modifiedBy?.hashCode() ?: 0)
        result = 31 * result + version.hashCode()
        result = 31 * result + reference.hashCode()
        return result
    }
}

data class UserDelete(
    val id: String = uuid(),
    val deletedTimestamp: ZonedDateTime = ZonedDateTime.now(),
    val deletedBy: String? = "unknown",
    val version: Long = 0,
    val reference: String = uuid()
)

fun UserCreateEvent.toUser(): User {
    return User(
        id = this.id.toString(),
        name = this.name.toString(),
        lastModifiedTimestamp = this.createdTimestamp?.toString()?.toZonedDateTime() ?: java.time.ZonedDateTime.now(),
        lastModifiedBy = this.createdBy?.toString()
    )
}

fun UserUpdateEvent.toUser(): User {
    return User(
        id = this.id.toString(),
        name = this.name.toString(),
        lastModifiedTimestamp = this.modifiedTimestamp?.toString()?.toZonedDateTime() ?: java.time.ZonedDateTime.now(),
        lastModifiedBy = this.modifiedBy?.toString(),
        version = this.version
    )
}

fun UserCreate.toUserCreateEvent(): UserCreateEvent {
    val user = this
    return UserCreateEvent().apply {
        this.messageId = uuid()
        this.id = user.id
        this.name = user.name
        this.createdTimestamp = user.createdTimestamp.toISOString()
        this.createdBy = user.createdBy
        this.reference = user.reference
    }
}

data class UserFailureEvent(
    val id: String = uuid(),
    val messageId: String = uuid(),
    val name: String = "",
    val updated: ZonedDateTime = ZonedDateTime.now(),
    val version: Long? = null,
    val modifiedBy: String = "",
    val source: String = "",
    val reference: String = uuid(),
    val failureDateTime: ZonedDateTime = ZonedDateTime.now(),
) {
    @Suppress("DuplicatedCode")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserFailureEvent

        if (id != other.id) return false
        if (name != other.name) return false
        if (version != other.version) return false
        if (modifiedBy != other.modifiedBy) return false
        if (source != other.source) return false

        return true
    }

    @Suppress("DuplicatedCode")
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + version.hashCode()
        result = 31 * result + modifiedBy.hashCode()
        result = 31 * result + source.hashCode()
        return result
    }
}

fun UserCreate.toUser(): User {
    val user = this
    return User(
        id = user.id,
        name = user.name,
        lastModifiedTimestamp = user.createdTimestamp,
        lastModifiedBy = user.createdBy
    )
}

fun UserUpdate.toUser(): User {
    return User(
        id = this.id,
        name = this.name,
        lastModifiedTimestamp = this.modifiedTimestamp,
        lastModifiedBy = this.modifiedBy,
        version = this.version
    )
}


fun UserUpdate.toUserUpdateEvent(): UserUpdateEvent {
    val user = this
    return UserUpdateEvent().apply {
        this.messageId = uuid()
        this.id = user.id
        this.name = user.name
        this.modifiedTimestamp = user.modifiedTimestamp.toISOString()
        this.modifiedBy = user.modifiedBy
        this.version = user.version
        this.reference = user.reference
    }
}

fun UserDelete.toUserDeleteEvent(): UserDeleteEvent {
    val user = this
    return UserDeleteEvent().apply {
        this.messageId = uuid()
        this.id = user.id
        this.deletedTimestamp = user.deletedTimestamp.toISOString()
        this.deletedBy = user.deletedBy
        this.version = user.version
        this.reference = user.reference
    }
}

fun User.cloneAndIncrementVersion(): User {
    return User(
        id = this.id,
        name = this.name,
        version = this.version + 1,
        lastModifiedTimestamp = this.lastModifiedTimestamp,
        lastModifiedBy = this.lastModifiedBy
    )
}

fun UserCreateEvent.toUserFailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): UserFailureEvent {
    return UserFailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        name = this.name.toString(),
        updated = this.createdTimestamp.toString().toZonedDateTime(),
        modifiedBy = this.createdBy.toString(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
        version = 0
    )
}

fun UserUpdateEvent.toUserFailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): UserFailureEvent {
    return UserFailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        name = this.name.toString(),
        updated = this.modifiedTimestamp.toString().toZonedDateTime(),
        version = this.version,
        modifiedBy = this.modifiedBy.toString(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
    )
}


fun UserDeleteEvent.toUserFailureEvent(source: String, currentZonedDateTime: () -> ZonedDateTime): UserFailureEvent {
    return UserFailureEvent(
        id = this.id.toString(),
        messageId = this.messageId?.toString() ?: uuid(),
        source = source,
        failureDateTime = currentZonedDateTime(),
        reference = this.reference?.toString() ?: uuid(),
        version = this.version
    )
}


fun UserChangeException.toUserFailureStreamEvent(source: String): UserFailureStreamEvent {
    val input = this
    return UserFailureStreamEvent().apply {
        this.userDetails = input.userFailureEvent.toUserDetails()
        this.reference = input.reference
        this.messageId = input.userFailureEvent.messageId
        this.failureTimestamp = input.userFailureEvent.failureDateTime.toISOString()
        this.failureReason = input.message
        this.failureReference = input.reference
        this.failureSource = source
    }

}

fun UserFailureEvent.toUserDetails(): UserDetails {
    val user = this
    return UserDetails().apply {
        this.id = user.id
        this.version = user.version ?: 0
        this.name = user.name
        this.reference = user.reference
    }
}


interface IUserDataSource {

    fun create(userCreateEvent: UserCreateEvent): Mono<User>

    fun update(userUpdateEvent: UserUpdateEvent): Mono<User>

    fun findById(id: String): Mono<User>

    fun findAll(): Flux<User>

    fun delete(userDeleteEvent: UserDeleteEvent): Mono<User>

    fun failure(userFailureEvent: UserFailureEvent): Mono<Unit>

}

@Suppress("unused")
open class UserChangeException(
    val userFailureEvent: UserFailureEvent,
    val eventType: String,
    val reference: String = uuid(),
    throwable: Throwable
) : RuntimeException(throwable)


val userStreamHandler:
            (Any, IUserDataSource, Logger, () -> ZonedDateTime, (UserChangeException) -> Mono<Unit>) -> Mono<User> = { event, ds, log, currentZonedDateTime, errorSender ->

    when (event) {
        is UserCreateEvent -> handleCreateEvent(event, ds, log, currentZonedDateTime)
        is UserUpdateEvent -> handleUpdateEvent(event, ds, log, currentZonedDateTime)
        is UserDeleteEvent -> handleDeleteEvent(event, ds, log, currentZonedDateTime)
        else -> {
            log.error("Unexpected record type={}", event::class.java)
            Mono.empty()
        }}
        .doOnError { log.error("Experienced ERROR", it) }
        .handleError(
            errorSender = errorSender
        ) { ds.failure(it.userFailureEvent) }
        .onErrorResume {
            log.error("Experienced error handling event", it)
            Mono.empty()
        }
    }



val handleCreateEvent: (UserCreateEvent, IUserDataSource, Logger, () -> ZonedDateTime) -> Mono<User> = { createEvent, ds, log, currentZonedDateTime ->
    log.info("handleCreateEvent {}", createEvent)
    ds.create(createEvent)
        .doOnError {
            log.error("Exception thrown in handleCreateEvent", it)
        }
        .onErrorMap {
            UserChangeException(
                userFailureEvent = createEvent.toUserFailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "create",
                reference = createEvent.reference.toString(),
                throwable = it
            )
        }
}

val handleUpdateEvent: (UserUpdateEvent, IUserDataSource, Logger, () -> ZonedDateTime) -> Mono<User> = { updateEvent, ds, log, currentZonedDateTime ->
    log.info("handleUpdateEvent {}", updateEvent)
    ds.update(updateEvent)
        .onErrorMap {
            log.error("Failed to update {}", updateEvent, it)
            UserChangeException(
                userFailureEvent = updateEvent.toUserFailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "update",
                reference = updateEvent.reference.toString(),
                throwable = it
            )
        }
}

val handleDeleteEvent: (UserDeleteEvent, IUserDataSource, Logger, () -> ZonedDateTime) -> Mono<User> = { deleteEvent, ds, log, currentZonedDateTime ->
    log.info("handleDeleteEvent {}", deleteEvent)
    ds
        .delete(deleteEvent)
        .doOnError {
            log.warn("***** Exception encountered in handleDeleteEvent", it)
        }
        .onErrorMap {
            log.error("Failed to delete {}", deleteEvent, it)
            UserChangeException(
                userFailureEvent = deleteEvent.toUserFailureEvent(ds::class.simpleName ?: "unknown", currentZonedDateTime),
                eventType = "delete",
                reference = deleteEvent.reference.toString(),
                throwable = it
            )
        }
        .doOnError {
            log.warn("***** Exception encountered in handleDeleteEvent", it)
        }
}

fun <V, E: Throwable> Mono<V>.handleError(
    errorSender: (E) -> Mono<Unit>,
    errorWriter: (E) -> Mono<Unit>,
): Mono<V> =
    this.onErrorResume { ex ->
        errorWriter(ex as E)
            .doOnError {
                println("error saving failure message")
            }
            .then(errorSender(ex))
            .log("Called error sender")
            .then(Mono.error(ex))

    }
