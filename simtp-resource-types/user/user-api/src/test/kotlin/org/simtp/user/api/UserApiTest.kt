/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.api


import io.mockk.every
import io.mockk.mockk

import java.time.Duration
import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test
import org.simtp.user.api.stream.UserCreateEvent
import org.simtp.common.toISOString
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.ZonedDateTime.now
import java.lang.RuntimeException

internal class IUserCommandApiTest {

    @Test
    fun testUserStreamHandler() {
        val userCreateEvent = UserCreateEvent().apply {
            id = "123"
            name = "John"
            createdTimestamp = now().toISOString()
        }
        val user = userCreateEvent.toUser()

        val userDataSource = mockk<IUserDataSource>(relaxed = true)
        @Suppress("ReactiveStreamsUnusedPublisher")
        every { userDataSource.create(any()) } returns Mono.just(user)

        val zonedDateTime = now()

        val mono = userStreamHandler(
            userCreateEvent,
            userDataSource,
            mockk(relaxed = true),
            { zonedDateTime }
        )  { Mono.empty() }

        StepVerifier
            .create(mono)
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun testHandleError() {
        var called2 = false
        val callable1 = Mono.error<Unit>(RuntimeException("error"))
        val callable2 = Mono.fromCallable { called2 = true }
        val mono = Mono
            .error<String>(RuntimeException("TEST"))
            .handleError<String, RuntimeException>({callable1}, {callable2})

        StepVerifier
            .create(mono)
            .expectError(RuntimeException::class.java)
            .verify(Duration.ofSeconds(5))

        assertTrue(called2)
    }

}