/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.datasource.test
import org.simtp.user.api.stream.*
import org.simtp.user.api.IUserDataSource
import java.time.Duration
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.simtp.common.toISOString
import org.simtp.kotlin.util.uuid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.OptimisticLockingFailureException
import java.time.ZonedDateTime


open class UserDataSourceTestAdapter {

    @Autowired
    protected lateinit var userDataSource: IUserDataSource

    @Test
    open fun contextLoads() {
        assertNotNull(userDataSource)
    }


    @Test
    open fun optimisticUpdatePasses() {
        val userCreateEvent = UserCreateEvent().apply {
            this.id = uuid()
            this.messageId = uuid()
            this.name = ""
            this.reference = ""
        }
        val result1 = userDataSource
            .create(userCreateEvent)
            .block(Duration.ofSeconds(5))

        val newValue = UserUpdateEvent().apply {
            this.id = result1!!.id
            this.messageId = uuid()
            this.modifiedTimestamp = ZonedDateTime.now().toISOString()
            this.version = result1.version
            this.name = result1.name
        }

        val result2 = userDataSource
            .update(newValue)
            .block(Duration.ofSeconds(5))

        assertEquals(result1!!.version+1, result2!!.version)
    }

    @Test
    open fun optimisticDeleteFails() {
        val userCreateEvent = UserCreateEvent().apply {
            this.id = uuid()
            this.messageId = uuid()
            this.name = ""
            this.reference = ""
        }
        val result1 = userDataSource
            .create(userCreateEvent)
            .block(Duration.ofSeconds(5))

        val userUpdateEvent = UserUpdateEvent().apply {
            this.id = result1!!.id
            this.messageId = uuid()
            this.modifiedTimestamp = ZonedDateTime.now().toISOString()
            this.version = result1.version
            this.name = result1.name
        }


        val result2 = userDataSource
            .update(userUpdateEvent)
            .block(Duration.ofSeconds(5))

        assertEquals(result1!!.version+1, result2!!.version)

        val otherUserDeleteEvent = UserDeleteEvent().apply {
            id = result1.id
            messageId = uuid()
            deletedTimestamp = ZonedDateTime.now().toISOString()
            version = result1.version
        }

        assertThrows(OptimisticLockingFailureException::class.java) {
            userDataSource
                .delete(otherUserDeleteEvent)
                .block(Duration.ofSeconds(50))
        }

    }


}