/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package org.simtp.user.datasource.memory

import org.simtp.user.api.*
import org.simtp.user.api.stream.*
import org.simtp.user.api.UserFailureEvent
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.common.IDateTimeManager
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.receiver.ReceiverOptions


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(UserMemoryDataSourceConfiguration::class)
annotation class EnableUserMemoryDataSource


@Configuration
open class UserMemoryDataSourceConfiguration {

    @Bean
    open fun userMemoryDataSource(): UserMemoryDataSource {
        return UserMemoryDataSource()
    }

    @Bean
    open fun userMemoryReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\${spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "userMemoryReactiveKafkaConsumer:$applicationName"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "userMemoryReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(USER_STREAM_NAME)))
    }


    @Bean
    open fun userMemoryStreamConsumer(
        userMemoryReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        userMemoryDataSource: UserMemoryDataSource,
        dateTimeManager: IDateTimeManager
    ): UserMemoryStreamConsumer {
        return UserMemoryStreamConsumer(
            userMemoryReactiveKafkaConsumerTemplate = userMemoryReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            userMemoryDataSource = userMemoryDataSource,
            dateTimeManager = dateTimeManager
        )
    }

}

class UserMemoryStreamConsumer(
    private val userMemoryReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val userMemoryDataSource: UserMemoryDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(UserMemoryStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<User> {
        return userMemoryReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                userStreamHandler(
                    it.value(),
                    userMemoryDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, USER_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.toUserFailureStreamEvent(this::class.simpleName!!) }
                }
            }

    }
}

@Suppress("MemberVisibilityCanBePrivate")
class UserMemoryDataSource(
    val map: MutableMap<String, User> = mutableMapOf(),
    val messageIds: MutableSet<String> = mutableSetOf(),
    val failedMessages: MutableMap<String, Set<UserFailureEvent>> = mutableMapOf()
): IUserDataSource {

    companion object {
        private val log = LoggerFactory.getLogger(UserMemoryDataSource::class.java)
    }

    override fun create(userCreateEvent: UserCreateEvent): Mono<User> {
        if (log.isDebugEnabled) log.debug("Called create {}", userCreateEvent)
        return if (!messageIds.contains(userCreateEvent.messageId.toString())) {
            Mono.just(saveUser(userCreateEvent.toUser()))
        } else {
            Mono.empty()
        }
            .saveMessageId(userCreateEvent.messageId.toString())
    }

    override fun delete(userDeleteEvent: UserDeleteEvent): Mono<User> {
        if (log.isDebugEnabled) log.debug("Called delete {}", userDeleteEvent)
        return if (!messageIds.contains(userDeleteEvent.messageId.toString())) {
            val existing = map[userDeleteEvent.id.toString()]
            if (existing != null && existing.version != userDeleteEvent.version)
                throw OptimisticLockingFailureException("Current version=$existing does not match provided version=${userDeleteEvent.version}")

            map.remove(userDeleteEvent.id.toString())?.let { Mono.just(it)} ?: Mono.empty()
        } else {
            Mono.empty()
        }
            .saveMessageId(userDeleteEvent.messageId.toString())

    }

    override fun update(userUpdateEvent: UserUpdateEvent): Mono<User> {
        if (log.isDebugEnabled) log.debug("Called update {}", userUpdateEvent)
        return if (!messageIds.contains(userUpdateEvent.messageId.toString())) {
            Mono.just(saveUser(userUpdateEvent.toUser()))
        } else {
            Mono.empty()
        }
            .saveMessageId(userUpdateEvent.messageId.toString())
    }

    private fun saveUser(user: User): User {
        if (log.isDebugEnabled) log.debug("Called saveUser {}", user)
        val existing = map[user.id]
        if (existing != null && existing.version != user.version)
            throw OptimisticLockingFailureException("Current version=$existing does not match provided version=${user.version}")

        val newVal = user.cloneAndIncrementVersion()
        map[user.id] = newVal

        return newVal

    }

    private fun Mono<User>.saveMessageId(messageId: String): Mono<User> {
        if (log.isDebugEnabled) log.debug("Called saveMessageId {}", messageId)
        return this
            .map {
                messageIds.add(messageId)
                it
            }
    }

    override fun findById(id: String): Mono<User> {
        return map[id]?.let { Mono.just(it)} ?: Mono.empty()
    }

    override fun findAll(): Flux<User> {
        return Flux.fromIterable(map.values)

    }

    override fun failure(userFailureEvent: UserFailureEvent): Mono<Unit> {
        return Mono.fromCallable {
            if (log.isWarnEnabled) log.debug("Called failure {}", userFailureEvent)
            failedMessages.merge(userFailureEvent.id, setOf(userFailureEvent)) { p1, p2 -> p1+p2 }
        }
    }


}
