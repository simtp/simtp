package org.simtp.user.datasource.memory

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.junit.jupiter.api.Test
import org.simtp.user.api.stream.*
import org.simtp.user.api.stream.UserFailureEvent
import org.simtp.common.IDateTimeManager
import org.simtp.common.toISOString
import org.simtp.user.api.*
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Mono
import reactor.core.publisher.Flux
import reactor.test.StepVerifier
import java.time.Duration
import java.time.ZonedDateTime

class UserMemoryStreamConsumerTest {
    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun consumerHandlesError() {
        val template = mockk<ReactiveKafkaConsumerTemplate<String, *>>()
        val consumerRecord = mockk<ConsumerRecord<String, Any>>()
        val zonedDateTime = ZonedDateTime.now()
        every { template.receiveAutoAck() } returns Flux.fromIterable(listOf(consumerRecord))

        every { consumerRecord.value() } returns UserCreateEvent().apply {
            id = "123"
            name = "Joe"
            this.createdBy = "test"
            this.createdTimestamp = zonedDateTime.toISOString()
            this.reference = "ref"
        }


        val dataSource = mockk<UserMemoryDataSource>()
        every { dataSource.create(any()) } returns Mono.error(IllegalStateException("ERROR"))
        every { dataSource.failure(any()) } returns Mono.empty()

        val dateTimeManager = mockk<IDateTimeManager>()
        val reactiveKafkaProducerTemplate = mockk<ReactiveKafkaProducerTemplate<String, Any>>()
        every { reactiveKafkaProducerTemplate.send(any<String>(), any<UserFailureEvent>()) } returns Mono.just(mockk(relaxed = true))
        every { dateTimeManager.currentZonedDateTime } returns zonedDateTime
        val consumer = UserMemoryStreamConsumer(
            userMemoryReactiveKafkaConsumerTemplate = template,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            userMemoryDataSource = dataSource,
            dateTimeManager = dateTimeManager
        )

        StepVerifier
            .create(consumer.createConsumer())
            .expectNextCount(0)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) {reactiveKafkaProducerTemplate.send("$USER_STREAM_NAME.DLQ", any<Any>())  }
    }

}