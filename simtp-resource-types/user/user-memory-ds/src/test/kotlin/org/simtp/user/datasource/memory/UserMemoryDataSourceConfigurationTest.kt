/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.datasource.memory

import org.simtp.user.datasource.test.UserDataSourceTestAdapter
import org.junit.jupiter.api.Tag
import org.simtp.framework.serialization.EnableSerialization
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.simtp.stream.kafka.EnableSimtpKafka
import org.simtp.common.EnableCommonConfiguration

@SpringBootTest
@Tag("integration")
class UserMemoryDataSourceConfigurationTest: UserDataSourceTestAdapter()

@SpringBootApplication
@EnableSerialization
@EnableUserMemoryDataSource
@EnableSimtpKafka
@EnableCommonConfiguration
open class UserMemoryDataSourceTestApp