/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.datasource.memory

import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import org.simtp.user.api.*
import org.simtp.user.api.stream.*
import org.junit.jupiter.api.Assertions.*
import org.simtp.kotlin.util.uuid

class UserMemoryDataSourceTest {

    @Test
    fun save() {
        val userCreateEvent = UserCreateEvent().apply {
            this.id = uuid()
            this.messageId = uuid()
            this.name = uuid()

        }
        val userDataSource = UserMemoryDataSource()
        StepVerifier
            .create(userDataSource.create(userCreateEvent))
            .expectNext(userCreateEvent.toUser().cloneAndIncrementVersion())
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val user = User()
        val userDataSource = UserMemoryDataSource(mutableMapOf( user.id to user ))
        StepVerifier
            .create(userDataSource.findById(user.id))
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val user = User()
        val userDataSource = UserMemoryDataSource(mutableMapOf( user.id to user ))
        StepVerifier
            .create(userDataSource.findAll())
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun delete() {
        val user = User()
        val userDataSource = UserMemoryDataSource(mutableMapOf( user.id to user ))
        val userDeleteEvent = UserDeleteEvent().apply {
            this.id = user.id
            this.messageId = uuid()
        }
        StepVerifier
            .create(userDataSource.delete(userDeleteEvent))
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(0, userDataSource.map.size)
    }

}