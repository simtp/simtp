package org.simtp.user.app.config

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.simtp.common.reactive.retryOn4xx
import org.simtp.user.api.*
import org.simtp.user.web.client.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.test.annotation.DirtiesContext
import org.springframework.web.reactive.function.client.WebClientResponseException
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import reactor.test.StepVerifier
import reactor.util.retry.Retry
import java.time.Duration
import java.time.ZonedDateTime

@Suppress("SpringBootApplicationProperties")
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = [
        "schema.registry.url=mock://localhost:8081"
    ]
)
@Testcontainers
@EnableUserCommandWebClient
@EnableUserQueryWebClient
@DirtiesContext
@EmbeddedKafka(partitions = 1, brokerProperties = [ "listeners=PLAINTEXT://localhost:9092" ])
@Tag("integration")
class UserAppTest {

    @LocalServerPort
    private var localServerPort: Int? = null

    companion object {

        @Container
        @ServiceConnection
        @JvmStatic
        private val postgresContainer = PostgreSQLContainer("postgres:latest")

        @Container
        @ServiceConnection
        @JvmStatic
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)

        @BeforeAll
        @JvmStatic
        fun setUp() {
            postgresContainer
                .withReuse(true)
                .start()
            redisContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            postgresContainer.stop()
            redisContainer.stop()
        }

    }

    @Autowired
    private lateinit var userCommandWebClientSettings: UserCommandWebClientSettings

    @Autowired
    private lateinit var userQueryWebClientSettings: UserQueryWebClientSettings

    @BeforeEach
    fun beforeEach() {
        userCommandWebClientSettings.baseUrl = "http://localhost:$localServerPort"

        userQueryWebClientSettings.baseUrl = "http://localhost:$localServerPort"
    }


    @Test
    fun contextLoads() {}

    @Autowired
    private lateinit var userCommandWebClient: UserCommandWebClient

    @Autowired
    private lateinit var userQueryWebClient: UserQueryWebClient

    @Test
    fun `Create User via API`() {
        val userCreate = UserCreate(
            name = "Joe"
        )
        StepVerifier
            .create(userCommandWebClient.create(userCreate))
            .expectNext(userCreate.id)
            .verifyComplete()

        val fetch = userQueryWebClient[userCreate.id].retryOn4xx()

        StepVerifier
            .create(fetch)
            .expectNext(userCreate.toUser().cloneAndIncrementVersion())
            .expectComplete()
            .verify(Duration.ofSeconds(30))

    }

    @Test
    fun `Update User via API`() {
        val userCreate = UserCreate(
            name = "Joe"
        )
        StepVerifier
            .create(userCommandWebClient.create(userCreate))
            .expectNext(userCreate.id)
            .verifyComplete()

        var user: User? = null

        StepVerifier
            .create(userQueryWebClient[userCreate.id].retryOn4xx())
            .consumeNextWith {
                user = it
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))

        assertNotNull(user)

        val userUpdate = UserUpdate(
            id = user!!.id,
            name = "Jason",
            modifiedTimestamp = ZonedDateTime.now(),
            modifiedBy = "test",
            version = user.version
        )

        StepVerifier
            .create(userCommandWebClient.update(userUpdate))
            .expectNext(userUpdate.id)
            .verifyComplete()

        val flux = userQueryWebClient[userUpdate.id]
            .doOnNext {
                assertEquals("Jason", it.name)
            }
            .retryWhen(
                Retry.backoff(3, Duration.ofSeconds(2))
                    .filter { t -> t is AssertionError }
            )


        StepVerifier
            .create(flux)
            .expectNextMatches {
                it.name == "Jason" && it.version > 0
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))
    }

    @Test
    fun `Delete User via API`() {
        val userCreate = UserCreate(
            name = "Joe"
        )
        StepVerifier
            .create(userCommandWebClient.create(userCreate))
            .expectNext(userCreate.id)
            .verifyComplete()

        var user: User? = null

        val mono = userQueryWebClient[userCreate.id].retryOn4xx(5, Duration.ofMillis(500))

        StepVerifier
            .create(mono)
            .consumeNextWith {
                user = it
            }
            .expectComplete()
            .verify(Duration.ofSeconds(10))

        assertNotNull(user)

        val userDelete = UserDelete(
            id = user!!.id,
            deletedTimestamp = ZonedDateTime.now(),
            deletedBy = "test",
            version = user.version
        )

        StepVerifier
            .create(userCommandWebClient.delete(userDelete))
            .expectNext(userDelete.id)
            .verifyComplete()

        val flux = userQueryWebClient[userDelete.id]
            .doOnNext {
                //There is a delay between sending the event
                //and the datasource removing it. assert the value
                //is null until we get an empty result and
                //doOnNext is not called then
                assertNull(it)
            }
            .retryWhen(
                Retry.backoff(100, Duration.ofMillis(200))
                    .filter { t -> t is AssertionError }
            )


        StepVerifier
            .create(flux)
            .expectError(WebClientResponseException::class.java)
            .verify(Duration.ofSeconds(10))
    }

}


@EnableUserQueryWithR2dbc
@EnableUserQueryWithRedis
@EnableUserQueryWithMemory
@EnableUserCommand
@SpringBootApplication
open class UserTestApp