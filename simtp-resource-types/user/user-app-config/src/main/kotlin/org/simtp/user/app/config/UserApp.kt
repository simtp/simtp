package org.simtp.user.app.config

import org.simtp.common.EnableCommonConfiguration
import org.simtp.user.datasource.memory.EnableUserMemoryDataSource
import org.simtp.user.datasource.redis.EnableUserRedisDataSource
import org.simtp.user.service.EnableUserCommandService
import org.simtp.user.service.EnableUserQueryService
import org.simtp.user.web.server.EnableUserCommandWebServer
import org.simtp.user.web.server.EnableUserQueryWebServer
import org.simtp.stream.kafka.EnableSimtpKafka
import org.simtp.user.datasource.r2dbc.EnableUserR2dbcDataSource


/*
    NOTE currently it is not possible to use Spring Eureka with a dynamic port due to the ordering
    in which the application starts:-

    https://github.com/spring-cloud/spring-cloud-consul/issues/555
 */

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnableUserR2dbcDataSource
@EnableCommonConfiguration
@EnableUserQueryWebServer
@EnableUserQueryService
annotation class EnableUserQueryWithR2dbc

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnableUserRedisDataSource
@EnableCommonConfiguration
@EnableUserQueryWebServer
@EnableUserQueryService
annotation class EnableUserQueryWithRedis

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnableUserMemoryDataSource
@EnableCommonConfiguration
@EnableUserQueryWebServer
@EnableUserQueryService
annotation class EnableUserQueryWithMemory

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@EnableSimtpKafka
@EnableCommonConfiguration
@EnableUserCommandWebServer
@EnableUserCommandService
annotation class EnableUserCommand
