/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.service

import org.simtp.user.api.*
import org.slf4j.LoggerFactory
import reactor.core.publisher.Mono


class UserQueryService(
    private val userDataSource: IUserDataSource
): IUserQueryApi {

    companion object {

        @JvmStatic
        private val log = LoggerFactory.getLogger(UserQueryService::class.java)
    }


    override fun get(id: String): Mono<User> {
        return userDataSource
            .findById(id)
            .doOnNext { log.info("Retrieved user={} with id {}", it, id) }
            .doOnError { log.error("Could not find user with id {}", id, it) }
            .switchIfEmpty(Mono.fromCallable { log.error("Empty user with id {}", id); null })
    }

}
