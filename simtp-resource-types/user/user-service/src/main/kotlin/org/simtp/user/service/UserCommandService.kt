/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.service

import org.simtp.user.api.*
import org.slf4j.LoggerFactory
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Mono


class UserCommandService(
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>
): IUserCommandApi {

    companion object {
        private val log = LoggerFactory.getLogger(UserCommandService::class.java)
    }



    override fun create(userCreate: UserCreate) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(USER_STREAM_NAME, userCreate.id, userCreate.toUserCreateEvent())
            .doOnError { log.error("Failed to send create command to Kafka", it) }
            .doOnNext { log.error("Success to send create command to Kafka {}", it) }
            .map { userCreate.id }
    }

    override fun update(userUpdate: UserUpdate) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(USER_STREAM_NAME, userUpdate.id, userUpdate.toUserUpdateEvent())
            .doOnError { log.error("Failed to send update command to Kafka", it) }
            .map { userUpdate.id }
    }

    override fun delete(userDelete: UserDelete) : Mono<String> {
        return reactiveKafkaProducerTemplate
            .send(USER_STREAM_NAME, userDelete.id, userDelete.toUserDeleteEvent())
            .doOnError { log.error("Failed to send delete command to Kafka", it) }
            .map { userDelete.id }
    }
}
