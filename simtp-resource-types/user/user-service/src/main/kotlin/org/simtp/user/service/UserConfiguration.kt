/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.service

import org.simtp.user.api.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate


@Configuration
open class UserCommandServiceConfiguration {

    @Bean
    open fun userCommandService(
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>
    ): IUserCommandApi {
        return UserCommandService(
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate
        )
    }

}

@Configuration
open class UserQueryServiceConfiguration {

    @Bean
    open fun userQueryService(
        userRedisDataSource: IUserDataSource,
    ): IUserQueryApi {
        return UserQueryService(
            userDataSource = userRedisDataSource
        )
    }

}


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(UserCommandServiceConfiguration::class)
annotation class EnableUserCommandService

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(UserQueryServiceConfiguration::class)
annotation class EnableUserQueryService