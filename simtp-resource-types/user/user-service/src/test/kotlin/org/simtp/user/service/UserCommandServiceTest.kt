/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.service

import org.simtp.user.api.*
import org.simtp.user.api.stream.UserCreateEvent
import org.simtp.user.api.stream.UserDeleteEvent
import org.simtp.user.api.stream.UserUpdateEvent

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.core.publisher.Mono
import reactor.kafka.sender.SenderResult
import reactor.test.StepVerifier
import java.time.Duration

class UserCommandServiceTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun create() {
        val userCreate = UserCreate()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(USER_STREAM_NAME, any<String>(), any<UserCreateEvent>()) } returns Mono.just(senderResult)
        val userService = UserCommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(userService.create(userCreate))
            .expectNext(userCreate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(USER_STREAM_NAME, any<String>(), any<UserCreateEvent>()) }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun update() {
        val userUpdate = UserUpdate()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(USER_STREAM_NAME, any<String>(), any<UserUpdateEvent>()) } returns Mono.just(senderResult)
        val userService = UserCommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(userService.update(userUpdate))
            .expectNext(userUpdate.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(USER_STREAM_NAME, any<String>(), any<UserUpdateEvent>()) }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun delete() {
        val userDelete = UserDelete()
        val kafkaProducer = mockk<ReactiveKafkaProducerTemplate<String, Any>>(relaxed = true)
        val senderResult = mockk<SenderResult<Void>>(relaxed = true)
        every { kafkaProducer.send(USER_STREAM_NAME, any<String>(), any<UserDeleteEvent>()) } returns Mono.just(senderResult)
        val userService = UserCommandService(
            reactiveKafkaProducerTemplate = kafkaProducer
        )
        StepVerifier
            .create(userService.delete(userDelete))
            .expectNext(userDelete.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { kafkaProducer.send(USER_STREAM_NAME, any<String>(), any<UserDeleteEvent>()) }
    }
}