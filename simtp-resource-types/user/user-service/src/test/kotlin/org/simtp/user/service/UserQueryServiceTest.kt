/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.service

import org.simtp.user.api.*
import org.simtp.user.datasource.memory.UserMemoryDataSource
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration

class UserQueryServiceTest {

    @Test
    fun get() {
        val user = User()
        val userDataSource = UserMemoryDataSource()
        userDataSource.map[user.id] = user
        val userQueryService = UserQueryService(
            userDataSource = userDataSource
        )
        StepVerifier
            .create(userQueryService[user.id])
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }


}