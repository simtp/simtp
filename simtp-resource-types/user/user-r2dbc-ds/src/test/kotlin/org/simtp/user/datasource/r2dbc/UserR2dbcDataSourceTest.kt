/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("ReactiveStreamsUnusedPublisher")

package org.simtp.user.datasource.r2dbc

import io.mockk.*
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import org.simtp.user.api.User
import org.simtp.user.api.stream.*
import org.junit.jupiter.api.Assertions.*
import org.simtp.kotlin.util.uuid
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class UserR2dbcDataSourceTest {

    @Test
    fun save() {
        val userRepository = mockk<UserRepository>(relaxed = true)
        val userMessageIdRepository = mockk<UserMessageIdRepository>(relaxed = true)
        val userFailedMessageRepository = mockk<UserFailedMessageRepository>(relaxed = true)
        val user = User()
        val entity = user.toUserEntity()
        every { userRepository.save(any()) } returns Mono.just(entity)
        every { userMessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { userMessageIdRepository.save(any<UserMessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as UserMessageIdEntity) }
        val userDataSource = UserR2dbcDataSource(userRepository, userMessageIdRepository, userFailedMessageRepository)
        StepVerifier
            .create(userDataSource.save(user))
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val userRepository = mockk<UserRepository>(relaxed = true)
        val userMessageIdRepository = mockk<UserMessageIdRepository>(relaxed = true)
        val userFailedMessageRepository = mockk<UserFailedMessageRepository>(relaxed = true)
        val user = User()
        val entity = user.toUserEntity()
        every { userRepository.findById(user.id) } returns Mono.just(entity)
        every { userMessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { userMessageIdRepository.save(any<UserMessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as UserMessageIdEntity) }
        val userDataSource = UserR2dbcDataSource(userRepository, userMessageIdRepository, userFailedMessageRepository)
        StepVerifier
            .create(userDataSource.findById(user.id))
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val userRepository = mockk<UserRepository>(relaxed = true)
        val userMessageIdRepository = mockk<UserMessageIdRepository>(relaxed = true)
        val userFailedMessageRepository = mockk<UserFailedMessageRepository>(relaxed = true)
        val user = User()
        val entity = user.toUserEntity()
        every { userRepository.findAll() } returns Flux.fromIterable(listOf(entity))
        val userDataSource = UserR2dbcDataSource(userRepository, userMessageIdRepository, userFailedMessageRepository)
        StepVerifier
            .create(userDataSource.findAll())
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun delete() {
        val userRepository = mockk<UserRepository>(relaxed = true)
        val userMessageIdRepository = mockk<UserMessageIdRepository>(relaxed = true)
        val userFailedMessageRepository = mockk<UserFailedMessageRepository>(relaxed = true)

        val userDeleteEvent = UserDeleteEvent().apply {
            id = uuid()
            messageId = uuid()
            version = 0
        }

        val user = User(
            id = userDeleteEvent.id.toString(),
            version = userDeleteEvent.version
        )

        val entity = user.toUserEntity()
        every { userRepository.findById(user.id) } returns Mono.just(entity)
        every { userRepository.deleteByIdAndVersion(user.id, user.version) } returns Mono.empty()

        every { userMessageIdRepository.existsById(any<String>()) } returns Mono.just(false)
        every { userMessageIdRepository.save(any<UserMessageIdEntity>()) } answers { Mono.just(it.invocation.args[0] as UserMessageIdEntity) }

        val userDataSource = UserR2dbcDataSource(userRepository, userMessageIdRepository, userFailedMessageRepository)

        StepVerifier
            .create(userDataSource.delete(userDeleteEvent))
            .expectNext(user)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify { userRepository.deleteByIdAndVersion(user.id, user.version) }
    }

    @Test
    fun testMapping() {
        val user = User()
        assertEquals(user, user.toUserEntity().toUser())
    }

}