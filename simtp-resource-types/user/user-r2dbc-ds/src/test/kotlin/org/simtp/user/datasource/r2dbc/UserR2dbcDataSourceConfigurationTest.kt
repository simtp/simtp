/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package org.simtp.user.datasource.r2dbc

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Tag
import org.simtp.common.EnableCommonConfiguration
import org.simtp.framework.serialization.EnableSerialization
import org.simtp.user.datasource.test.UserDataSourceTestAdapter
import org.simtp.stream.kafka.EnableSimtpKafka
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers


@SpringBootTest
@Tag("integration")
@Testcontainers
class UserR2dbcDataSourceConfigurationTest: UserDataSourceTestAdapter() {
    companion object {
        @Container
        @ServiceConnection
        private val postgresqlContainer = PostgreSQLContainer("postgres").withNetwork(Network.SHARED)

        @BeforeAll
        @JvmStatic
        fun setUp() {

            postgresqlContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            postgresqlContainer.stop()
        }
    }


}

@SpringBootApplication
@EnableSerialization
@EnableUserR2dbcDataSource
@EnableSimtpKafka
@EnableCommonConfiguration
open class UserR2dbcDataSourceTestApp