/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "R2dbcDataSourceORMInspection")
package org.simtp.user.datasource.r2dbc

import io.r2dbc.spi.ConnectionFactory
import org.springframework.r2dbc.connection.init.CompositeDatabasePopulator
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator
import org.springframework.core.io.ClassPathResource
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.simtp.common.IDateTimeManager
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import org.simtp.kotlin.util.uuid
import org.simtp.user.api.*
import org.simtp.user.api.UserFailureEvent
import org.simtp.user.api.stream.*
import org.simtp.stream.kafka.consumeAutoAck
import org.simtp.stream.kafka.failureSender
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import reactor.kafka.receiver.ReceiverOptions
import java.time.OffsetDateTime
import java.time.ZonedDateTime

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(UserR2dbcDataSourceConfiguration::class)
annotation class EnableUserR2dbcDataSource

@Repository
interface UserRepository: ReactiveCrudRepository<UserEntity, String> {
    fun deleteByIdAndVersion(id: String, version: Long): Mono<Void>
}

@Repository
interface UserMessageIdRepository: ReactiveCrudRepository<UserMessageIdEntity, String>

@Repository
interface UserFailedMessageRepository: ReactiveCrudRepository<UserFailedMessageEntity, Long>

@Configuration
@EnableR2dbcRepositories
@EntityScan
open class UserR2dbcDataSourceConfiguration {

    @Bean
    open fun userR2dbcDataSource(
        userRepository: UserRepository,
        userMessageIdRepository: UserMessageIdRepository,
        userFailedMessageRepository: UserFailedMessageRepository
    ): UserR2dbcDataSource {
        return UserR2dbcDataSource(
            userRepository = userRepository,
            userMessageIdRepository = userMessageIdRepository,
            userFailedMessageRepository = userFailedMessageRepository
        )
    }

    @Bean
    open fun userR2dbcReactiveKafkaConsumerTemplate(
        kafkaProperties: KafkaProperties,
        @Value("\${spring.application.name:unknown}") applicationName: String
    ): ReactiveKafkaConsumerTemplate<String, *> {
        val kafkaConfig = kafkaProperties.buildConsumerProperties(null).apply {
            this[ConsumerConfig.CLIENT_ID_CONFIG] = "userR2dbcReactiveKafkaConsumer:$applicationName"
            this[ConsumerConfig.GROUP_ID_CONFIG] = "userR2dbcReactiveKafkaConsumer"
        }
        val receiverOptions = ReceiverOptions.create<String, Any>(kafkaConfig)
        return ReactiveKafkaConsumerTemplate(receiverOptions.subscription(listOf(USER_STREAM_NAME)))
    }

    @Bean
    open fun userR2dbcStreamConsumer(
        userR2dbcReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
        reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
        userR2dbcDataSource: UserR2dbcDataSource,
        dateTimeManager: IDateTimeManager
    ): UserR2dbcStreamConsumer {
        return UserR2dbcStreamConsumer(
            userR2dbcReactiveKafkaConsumerTemplate = userR2dbcReactiveKafkaConsumerTemplate,
            reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate,
            userR2dbcDataSource = userR2dbcDataSource,
            dateTimeManager = dateTimeManager
        )
    }


    @Bean
    open fun userInitializer(connectionFactory: ConnectionFactory): ConnectionFactoryInitializer {
        return ConnectionFactoryInitializer().apply {
            setConnectionFactory(connectionFactory)
            setDatabasePopulator(
                CompositeDatabasePopulator().apply {
                    addPopulators(
                        ResourceDatabasePopulator(
                            ClassPathResource("User.sql"),
                        )
                    )
                }
            )
        }
    }
}

class UserR2dbcStreamConsumer(
    private val userR2dbcReactiveKafkaConsumerTemplate: ReactiveKafkaConsumerTemplate<String, *>,
    private val reactiveKafkaProducerTemplate: ReactiveKafkaProducerTemplate<String, Any>,
    private val userR2dbcDataSource: UserR2dbcDataSource,
    private val dateTimeManager: IDateTimeManager
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        @JvmStatic
        private val log = LoggerFactory.getLogger(UserR2dbcStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createConsumer().subscribe()
    }

    internal fun createConsumer(): Flux<User> {
        return userR2dbcReactiveKafkaConsumerTemplate
            .consumeAutoAck {
                userStreamHandler(
                    it.value(),
                    userR2dbcDataSource,
                    log,
                    { dateTimeManager.currentZonedDateTime }
                ) { ex ->
                    failureSender(log, USER_STREAM_NAME, reactiveKafkaProducerTemplate, ex ) { ex.toUserFailureStreamEvent(this::class.simpleName!!) }
                }
            }


    }

}



open class UserR2dbcDataSource(
    private val userRepository: UserRepository,
    private val userMessageIdRepository: UserMessageIdRepository,
    private val userFailedMessageRepository: UserFailedMessageRepository
): IUserDataSource {

    companion object {
        private val log = LoggerFactory.getLogger(UserR2dbcDataSource::class.java)
    }

    private fun Mono<User>.saveMessageId(messageId: String): Mono<User> {
        return this
            .flatMap {
                userMessageIdRepository
                    .save(UserMessageIdEntity(id = messageId))
                    .doOnError { ex -> log.error("Failed to save {}", messageId, ex) }
                    .doOnNext { messageId -> log.info("Saving messageId {}", messageId.id) }
                    .thenReturn(it)
            }
    }

    override fun create(userCreateEvent: UserCreateEvent): Mono<User> {
        return userMessageIdRepository
            .existsById(userCreateEvent.messageId.toString())
            .filter { it == false }
            .flatMap { save(userCreateEvent.toUser()) }
            .saveMessageId(userCreateEvent.messageId.toString())
    }

    override fun update(userUpdateEvent: UserUpdateEvent): Mono<User> {
        return userMessageIdRepository
            .existsById(userUpdateEvent.messageId.toString())
            .filter { it == false }
            .flatMap { save(userUpdateEvent.toUser()) }
            .saveMessageId(userUpdateEvent.messageId.toString())
    }

    override fun delete(userDeleteEvent: UserDeleteEvent): Mono<User> {
        return userMessageIdRepository
            .existsById(userDeleteEvent.messageId.toString())
            .filter { it == false }
            .flatMap {
                delete(userDeleteEvent.id.toString(), userDeleteEvent.version)
            }
            .saveMessageId(userDeleteEvent.messageId.toString())

    }

    private fun delete(id: String, version: Long): Mono<User> {
        val cachedValue = userRepository
            .findById(id)
            .log("INFO")
            .doOnNext { if (it.version != version) throw OptimisticLockingFailureException("Provided providedValue=0-SNAPSHOT currentValue=${it.version}")  }
            .cache()

        return cachedValue
            .flatMap {
                userRepository.deleteByIdAndVersion(id, version)
            }
            .then(cachedValue)
            .map { it.toUser() }
    }

    internal fun save(user: User): Mono<User> {
        return userRepository
            .save(user.toUserEntity())
            .map { it.toUser() }
    }

    override fun findById(id: String): Mono<User> {
        return userRepository
            .findById(id)
            .map { it.toUser() }
    }

    override fun findAll(): Flux<User> {
        return userRepository
            .findAll()
            .map { it.toUser() }
    }

    override fun failure(userFailureEvent: UserFailureEvent): Mono<Unit> {
        return userFailedMessageRepository
            .save(userFailureEvent.toUserFailedMessageEntity())
            .doOnNext { log.error("Failed to consumer message") }
            .then(Mono.empty())

    }

}


@Table(name = "\"user\"")
open class UserEntity(
    @Id
    open var id : String = uuid(),

    @Column("name")
    open var name : String = "",

    @Column("last_modified_timestamp")
    open var lastModifiedTimestamp: OffsetDateTime = OffsetDateTime.now(),

    @Version
    open var version: Long = 0
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (lastModifiedTimestamp != other.lastModifiedTimestamp) return false
        if (version != other.version) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + lastModifiedTimestamp.hashCode()
        result = 31 * result + version.hashCode()
        return result
    }

    override fun toString(): String {
        return "UserEntity(id='$id', name='$name', lastModifiedTimestamp=$lastModifiedTimestamp, version=0-SNAPSHOT)"
    }


}

@Table(name = "user_message_id")
open class UserMessageIdEntity(
    @Suppress("unused")
    @Id
    @Column("id")
    var id: String? = null,

    @Version
    open var version: Long? = null
)

internal fun User.toUserEntity(): UserEntity {
    return UserEntity(
        id = this.id,
        name = this.name,
        lastModifiedTimestamp = this.lastModifiedTimestamp.toOffsetDateTime(),
        version = this.version
    )
}

internal fun UserEntity.toUser(): User {
    return User(
        id = this.id,
        name = this.name,
        lastModifiedTimestamp = this.lastModifiedTimestamp.toZonedDateTime(),
        version = this.version
    )
}

fun UserFailureEvent.toUserFailedMessageEntity(): UserFailedMessageEntity {
    return UserFailedMessageEntity(
        id = null,
        userId = this.id,
        messageId = this.messageId,
        name = this.name,
        updated = this.updated,
        userVersion = this.version,
        modifiedBy = this.modifiedBy,
        source = this.source,
        reference = this.reference,
        failureDateTime = this.failureDateTime
    )
}

@Table(name = "user_failed_message")
open class UserFailedMessageEntity(
    @Id
    val id: Long? = null,
    val userId: String? = null,
    val messageId: String = uuid(),
    val name: String = "",
    val updated: ZonedDateTime = ZonedDateTime.now(),
    val userVersion: Long? = null,
    val modifiedBy: String = "",
    val source: String = "",
    val reference: String = uuid(),
    val failureDateTime: ZonedDateTime = ZonedDateTime.now(),

    @Version
    val version: Long = 0

)
